Kinetochore segmentation and tracking pipeline.

Developed by Jana Wittmann, Ellenberg group, EMBL Heidelberg, 2021.

See detailed instructions for installation and use and code with examples in the subfolders.
