from scipy.ndimage import filters
from skimage import filters as skifilters
import numpy as np
import SimpleITK as sitk
import scipy.ndimage


# calculate the a trous wavelets in the image (image_1) to a specified level (level_1)
def get_a_trous_wavelets(image_1, level_1):
    # create an empty list to store the wavelets
    wavelets_1 = []
    current_image_state = image_1
    # calculate the a trous kernel in 3D using a B3 spline interpolation
    convolution_array_1d = np.array([1 / 16., 1 / 4., 3 / 8., 1 / 4., 1 / 16.])
    convolution_matrix_2d = np.outer(convolution_array_1d, convolution_array_1d)
    convolution_matrix_3d = np.multiply.outer(convolution_array_1d, convolution_matrix_2d)
    # calculate the wavelets by iterative convolution with the kernel and subtraction of this convolved image
    # from the current wavelet
    for j in range(level_1):
        # for the convolution: use mirrored edge conditions, because if a constant condition is chosen,
        # for higher iterations "constant objects" are detected
        # However, this prevents correct reconstruction of the image from the wavelets!
        convolved_image = scipy.ndimage.convolve(current_image_state, convolution_matrix_3d, mode='mirror')
        # subtract the convolved image from the current state
        wavelets_1.append(current_image_state - convolved_image)
        current_image_state = convolved_image
    return np.array(wavelets_1), current_image_state


# define the segmentation as the part of the image where the wavelet at the maximum level and the wavelet at the
# level one below are greater than 0
def get_segmentation_from_denoised_a_trous_wavelets(denoised_wavelets_1, start_index, stop_index):
    segmentation_1 = np.ones(shape=denoised_wavelets_1[0].shape)
    for index in range(start_index, stop_index + 1):
        segmentation_1 *= denoised_wavelets_1[index] > 0
    # create a np boolean array from the segmentation
    segmentation_1 = np.array(segmentation_1 > 0, dtype='bool')
    return segmentation_1


# segment an image using a trous wavelets as described in "Multiresolution Support Applied to Image Filtering and
# Restoration" by Starck et al. 1995 (see appendix 1).
# Calculate denoised wavelets using the Amplitude-scale-invariant Bayes Estimator using Jeffreys' non-informative prior
# as described in "Wavelet-based image estimation: an empirical Bayes approach using Jeffreys’ noninformative prior"
# by Figueiredo et al. 2001 to perform a segmentation similar to the method described in "Coupled minimum-cost flow
# cell tracking for high-throughput quantitative analysis" by Padfield et al 2011.
# Change compared to papers: increase the 'denoising term' for the wavelets, because there are spurious effects
# around the detected objects, that are bigger than noise artifacts in the background, but still unwanted in the
# segmentation.
# inputs: image: np.array containing a 3D image stack with dimensions x,y,z (in that order)
#         level: the level up to which the wavelets should be calculated
# returns: segmented image
def segment_image_using_a_trous_wavelets(image, level=5):
    # before starting, convert the image to floating point as the scipy.ndimage.convolve algorithm keeps the data type
    # of the input
    image = image.astype('float64')
    zeros_array = np.zeros(shape=image.shape, dtype='float64')

    # calculate the a trous wavelets_1
    wavelets, final_approx_image = get_a_trous_wavelets(image, level)

    # copy the wavelets for calculating their denoised version
    denoised_wavelets = np.copy(wavelets)

    # denoise the wavelets:
    # 1) w_denoised \approx y/ w_i where y = [w**2 - 3*sigma**2]+, the plus stands for max(0,x)
    # 2) sigma is the estimated noise std in the calculated from the background
    for i, wavelet in enumerate(wavelets):
        # get the variance of smallest 90% of the image (background), assume most of the 3D image is background
        threshold_5_percent = np.percentile(wavelet, 5)
        var = np.var(wavelet[wavelet < threshold_5_percent])
        # calculate the enumerator of the fraction
        # change compared to paper: increase variance by factor 20, because in the data there are spurious effects
        # around the detected objects due to imaging effects that are bigger than the variance, but not wanted
        # in the segmentation
        wavelet_before_max = np.square(wavelet) - 3 * var * 20
        # get the maximum of zero and the enumerator (apply the []+ operation)
        wavelet_new_enum = np.maximum(wavelet_before_max, zeros_array)
        # divide by the wavelet to obtain its denoised form
        denoised_wavelets[i] = np.divide(wavelet_new_enum, wavelet, where=wavelet > 0)

    # get the segmentation from the denoised wavelets
    segmentation = get_segmentation_from_denoised_a_trous_wavelets(denoised_wavelets, level - 2, level - 1)
    return segmentation


# basic thresholding function: apply a gaussian filter to the image  and threshold using a binary threshold variable
# inputs: image_xyz_var: a 3D numpy array containing a 3D image stack with dimensions x,y,z (in that order)
#                        for proper function of the gaussian blur the physical x,y and z dimensions should be the same
#         sigma_var: the STD of the Gaussian used for the blurring
#         binary_thresh_var: the binary threshold applied to the image after blurring
# outputs: thresholded_image_xyz_var: the image after Gaussian filtering and thresholding
def segment_xyz_image_gaussian_and_thresh(image_xyz_var, sigma_var, binary_thresh_var):
    # apply a gaussian filter with the given sigma
    gauss_blur_image_xyz = filters.gaussian_filter(image_xyz_var, sigma=sigma_var)
    # apply the binary threshold as given in the input
    thresholded_image_xyz_var = gauss_blur_image_xyz > binary_thresh_var
    return thresholded_image_xyz_var


# global Otsu's method thresholding:
# input: image_xyz_var: a 3D numpy array containing a 3D image stack with dimensions x,y,z (in that order)
# output: thresholded_image_xyz_var: a 3D numpy array containing a binary 3D image stack with dimensions x,y,z
#         (in that order) 1: above threshold, 0: below threshold
def segment_xyz_image_otsu(image_xyz_var):
    # get a global threshold using Otsu's method for the whole image
    otsu_thresh = skifilters.threshold_otsu(image_xyz_var)
    # threshold the image using Otsu's method
    thresholded_image_xyz_var = image_xyz_var > otsu_thresh
    return thresholded_image_xyz_var


# Using weighted global and slice Otsu's method
# Julius idea: to treat a picture with empty slices, segmentation for noise might be better when using a global and a
# local value for Otsu's method. The resulting image is obtained by adding the results up with a weight.
# - weight = 0.0: local Otsu only
# - weight = 1.0 global Otsu only
def weighted_global_and_slice_otsu(image_xyz_var, weight):
    global_otsu_thresh = skifilters.threshold_otsu(image_xyz_var)
    thresholded_image_xyz_var = np.zeros(shape=image_xyz_var.shape)
    for i in range(image_xyz_var.shape[2]):
        local_otsu_thresh = skifilters.threshold_otsu(image_xyz_var[..., i])
        thresholded_image_xyz_var[..., i] = (weight * (image_xyz_var[..., i] > global_otsu_thresh)
                                             + (1 - weight) * (image_xyz_var[..., i] > local_otsu_thresh))
    return thresholded_image_xyz_var


def execute_SITK_thresholding_filter(filter_var, image_xyz_var, mask_var=None, return_threshold=False):
    sitk_image = sitk.GetImageFromArray(image_xyz_var)
    if mask_var is None:
        filter_var.Execute(sitk_image)
    else:
        mask_var_int = mask_var.astype(np.uint8)

        sitk_mask = sitk.GetImageFromArray(mask_var_int)

        test_filter = sitk.MinimumMaximumImageFilter()
        test_filter.Execute(sitk_mask)

        filter_var.SetMaskValue(1)

        filter_var.Execute(sitk_image, sitk_mask)
    threshold = filter_var.GetThreshold()
    thresholded_image = image_xyz_var > threshold
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image, None


def segment_image_otsu_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.OtsuThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_huang_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.HuangThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_iso_data_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.IsoDataThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_li_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.LiThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_max_entropy(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.MaximumEntropyThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_kittler_illingworth_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.KittlerIllingworthThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_moments_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.MomentsThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_yen_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.YenThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_renyi_entropy_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.RenyiEntropyThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image


def segment_image_shanbag_threshold(image_xyz_var, mask_var=None, return_threshold=False):
    threshold_filter = sitk.ShanbhagThresholdImageFilter()
    thresholded_image, threshold = execute_SITK_thresholding_filter(threshold_filter, image_xyz_var, mask_var,
                                                                    return_threshold)
    if return_threshold:
        return thresholded_image, threshold
    else:
        return thresholded_image
