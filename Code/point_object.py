import numpy as np
from scipy.spatial import Voronoi
import skimage.measure
from termcolor import colored


# for each detected object, create an instance storing all important information about it
class DetectedObjectsList:
    # inputs: image: np.array, containing the intensity image
    #         points_list: list of np.arrays containing the coordinates of the points
    def __init__(self, points_list, image, metadata,
                 thresholds_dict=None, frame_no=None, number_expected=None, watershed_sizes=None):

        basic_parameters = get_basic_parameters(points_list, image, metadata=metadata)
        intensities, mean_intensities, resolution_corrected_coordinates, im_shape, resolution_corrected_im_shape = \
            basic_parameters

        # number of points in this list
        self.frame_no = frame_no
        self.microscopy_data = image
        self.score_image = None
        self.length = len(points_list)
        self.metadata = metadata
        if thresholds_dict is not None:
            self.threshold_dict = thresholds_dict
            self.threshold_dict["min_dist_local_max"] = 2
        self.number_expected = number_expected

        # manual points
        self.manual_points = None
        self.manual_tracking_indices = None

        # the shape of the image the objects were extracted from
        self.im_shape = im_shape
        self.resolution_corrected_im_shape = resolution_corrected_im_shape

        # coordinates: the point in the image, with and without resolution correction
        self.coordinates = points_list
        self.resolution_corrected_coordinates = resolution_corrected_coordinates

        # the tracking number
        self.names = None

        # indicator whether this object is chosen or not
        self.is_chosen = np.array([], dtype=bool)
        self.is_low_value_point = np.array([], dtype=bool)
        self.is_mid_value_point = np.array([], dtype=bool)
        self.is_hig_value_point = np.array([], dtype=bool)
        self.is_local_max_point = np.array([], dtype=bool)
        self.is_local_max_point_prod_score = np.array([], dtype=bool)
        self.is_local_max_point_intensity = np.array([], dtype=bool)
        self.local_max_point_dists = None
        self.local_max_point_intensity_dists = None
        self.local_max_point_prod_score_dists = None

        # all points
        self.all_low_value_points = np.zeros(shape=(0, 3), dtype=float)
        self.all_mid_value_points = np.zeros(shape=(0, 3), dtype=float)
        self.all_hig_value_points = np.zeros(shape=(0, 3), dtype=float)
        self.all_local_max_points = np.zeros(shape=(0, 3), dtype=float)
        self.all_local_max_points_prod_score = np.zeros(shape=(0, 3), dtype=float)
        self.all_local_max_points_intensity = np.zeros(shape=(0, 3), dtype=float)
        self.all_points = points_list

        # all active points
        self.active_low_value_points = np.zeros(shape=(0, 3), dtype=float)
        self.active_mid_value_points = np.zeros(shape=(0, 3), dtype=float)
        self.active_hig_value_points = np.zeros(shape=(0, 3), dtype=float)
        self.active_local_max_points = np.zeros(shape=(0, 3), dtype=float)
        self.active_local_max_points_prod_score = np.zeros(shape=(0, 3), dtype=float)
        self.active_local_max_points_intensity = np.zeros(shape=(0, 3), dtype=float)
        self.active_points = points_list

        # intensities at the center
        self.centre_intensities = np.array(intensities)
        self.mean_intensities = np.array(mean_intensities)

        # sizes of each point
        if watershed_sizes is None:
            self.watershed_sizes = None
        else:
            self.watershed_sizes = watershed_sizes
        self.sizes = None
        self.is_split = None
        self.split_points = None

        # the voronoy_stars
        self.voronoy_stars = None
        self.resolution_corrected_voronoy_stars = None

    # calculate the Voronoy Stars for every chosen object, the voronoy star contains all vectors pointing 'away' from
    # this object that are part of the Voronoy tessellation
    def get_voronoy_star_points_list(self):
        if np.sum(self.is_chosen) == 0:
            self.is_chosen = np.ones(len(self.coordinates), dtype=bool)

        points_list = self.coordinates[self.is_chosen]

        # create a scipy.spatial "Voronoy" object
        print(points_list)
        voronoy = Voronoi(points_list)
        # get the 'ridge_tuples': the tuples of points which connections are part of the Voronoy tessellation
        ridge_tuples = voronoy.ridge_points
        # calculate the 'voronoy stars' for every object, by appending an empty list
        voronoy_stars = []
        for i in range(len(points_list)):
            # get all tuples that contain the current point:
            # 1) get all tuples that contain the point in the first position
            tuple_list_containing_point_i = [ridge_tuple for ridge_tuple in ridge_tuples if ridge_tuple[0] == i]
            # 2) get all tuples that contain the point at the second position and reverse them before adding them
            tuple_list_containing_point_i += [[ridge_tuple[1], ridge_tuple[0]] for ridge_tuple in ridge_tuples if
                                              ridge_tuple[1] == i]
            # create every voronoy star by looping over all connection with this point
            voronoy_star_i = []
            for tuple_containing_point_i in tuple_list_containing_point_i:
                # calculate the vector pointing from the current point to their neighbours, if the connection
                # is part of the Voronoy tessellation
                diff = points_list[tuple_containing_point_i[1]] - points_list[tuple_containing_point_i[0]]
                voronoy_star_i.append(diff)
            voronoy_stars.append(voronoy_star_i)
        # save the voronoy stars and a resolution corrected version
        self.voronoy_stars = voronoy_stars  # before: np.array()
        resolution_corrected_voronoy_stars = get_resolution_corrected_points_list(voronoy_stars, self.metadata)
        self.resolution_corrected_voronoy_stars = resolution_corrected_voronoy_stars  # before: np.array()

        return

    # function that takes an image and a list of points and returns the size of each object as a
    # list, this function is used to add the sizes as an attribute to the DetectedObjectsList for each image
    # output: sizes: list of volume of the objects in pixels with the same length as "points_list" containing the sizes
    #                at the points in "points_list"
    def get_sizes_of_objects_at_points_from_image(self, image, return_image=False):

        import split_big_objects as sbo
        from segmentation_methods import segment_image_using_a_trous_wavelets

        print("getting sizes")

        points_list = self.coordinates

        # segment the image using a trous wavelets to get binary mask of objects
        segmentation = segment_image_using_a_trous_wavelets(image)
        # in case some detected objects are not within the segmentation: 'artificially' set them to 1
        object_markers = np.zeros(shape=image.shape, dtype=bool)
        for point in points_list:
            object_markers[int(point[0]), int(point[1]), int(point[2])] = True
        segmentation = np.logical_or(segmentation, object_markers)
        segmentation_image = segmentation * image
        # perform a watershed from the points found in the segmentation image
        watershed_image = sbo.get_watershed_from_image_within_mask(segmentation_image, points_list, 0)

        print(np.max(watershed_image))
        print("maximum of watershed image")

        # calculate the sizes of the watershed labels
        _, _, _, sizes = sbo.calculate_statistics_from_label_and_intensity_image(watershed_image, image)[0:4]

        self.sizes = sizes

        if return_image:
            return watershed_image
        else:
            return

    def get_watershed_sizes_of_objects_at_point_from_image(self, image, watershed_threshold=None, return_image=False):

        print("getting watershed sizes")
        import split_big_objects as sbo

        # if no new watershed_threshold is passed to the function, get the threshold from the dictionary
        if watershed_threshold is None:
            watershed_threshold = self.threshold_dict["watershed_intensity"]

        # copy the intensity image for the watershed, because some points are artificially set to 1
        segmentation_image = np.copy(image)
        # in case some detected point have an intensity lower than the watershed_intensity: increase them, so the size
        # is at least 1
        for point in self.coordinates:
            if segmentation_image[point[0], point[1], point[2]] <= watershed_threshold:
                segmentation_image[point[0], point[1], point[2]] = watershed_threshold + 1
        # perform a watershed from the points found in the segmentation image
        watershed_image = sbo.get_watershed_from_image_within_mask(segmentation_image, self.coordinates,
                                                                   threshold=-watershed_threshold)
        # calculate the sizes of the watershed labels
        region_properties = skimage.measure.regionprops(watershed_image)
        sizes = [region.area for region in region_properties]

        # store the value of the watershed sizes in the points_object
        self.watershed_sizes = np.array(sizes)

        if return_image:
            return watershed_image
        else:
            return

    def update_active_points(self):

        print("updating active points that were chosen")
        active_points = self.coordinates[self.is_chosen & ~self.is_split]

        if len(active_points) < self.number_expected:
            booleans_indicating_of_split_points = self.is_chosen & self.is_split
            split_point_indices = np.argwhere(booleans_indicating_of_split_points)

            print(colored("Splitting " + str(np.sum(booleans_indicating_of_split_points)) + " points", "green"))

            for index in split_point_indices:
                try:
                    active_points = np.append(active_points, self.split_points[index[0]], axis=0)
                except IndexError:
                    pass

        else:
            print(colored("Not trying to split any points, as the number of points already corresponds"
                          "to the number expected.", "green"))

        # update the length of points found and active points
        self.active_points = active_points
        self.length = len(self.active_points)

        # update the active low, middle, high and local maximum value points
        if len(self.all_low_value_points) > 0:
            self.active_low_value_points = self.all_low_value_points[self.is_chosen[self.is_low_value_point]]
        if len(self.all_mid_value_points) > 0:
            self.active_mid_value_points = self.all_mid_value_points[self.is_chosen[self.is_mid_value_point]]
        if len(self.all_hig_value_points) > 0:
            self.active_hig_value_points = self.all_hig_value_points[self.is_chosen[self.is_hig_value_point]]
        if len(self.all_local_max_points) > 0:
            self.active_local_max_points = self.all_local_max_points[self.is_chosen[self.is_local_max_point]]
        if len(self.all_local_max_points_prod_score) > 0:
            self.active_local_max_points_prod_score = self.all_local_max_points_prod_score[
                self.is_chosen[self.is_local_max_point_prod_score]]
        if len(self.all_local_max_points_intensity) > 0:
            self.active_local_max_points_intensity = self.all_local_max_points_intensity[
                self.is_chosen[self.is_local_max_point_intensity]]

        return

    def update_low_value_points(self, new_low_value_thresh):
        print("updating low value points")
        self.threshold_dict["low_value_points_threshold"] = new_low_value_thresh
        low_val_booleans = self.is_low_value_point
        self.is_chosen[low_val_booleans] = self.centre_intensities[low_val_booleans] > new_low_value_thresh
        self.update_active_points()
        return

    def update_mid_and_high_value_points(self, new_mid_and_high_val_thresh):
        print("updating mid and high value threshold values")

        self.threshold_dict["mid_and_high_value_points_threshold"] = new_mid_and_high_val_thresh

        hig_mid_val_booleans = self.is_hig_value_point | self.is_mid_value_point

        self.is_chosen[hig_mid_val_booleans] = \
            self.centre_intensities[hig_mid_val_booleans] > new_mid_and_high_val_thresh
        self.update_active_points()
        return

    def update_double_thresh(self, new_double_thresh):

        print("updating double threshold values")

        self.threshold_dict["double_thresholding"] = new_double_thresh

        double_thresh_booleans = self.is_hig_value_point | self.is_mid_value_point | self.is_low_value_point

        self.is_chosen[double_thresh_booleans] = self.centre_intensities[double_thresh_booleans] > new_double_thresh
        self.update_active_points()
        return

    def get_local_max_points_using_threshold(self, new_local_max_thresh, point_centre_intensities, where_this_type,
                                             point_distances):

        if point_distances is not None and self.threshold_dict["min_dist_local_max"] is not None:
            is_chosen_intensity = np.array(point_centre_intensities > new_local_max_thresh, dtype=bool)
            is_chosen_distance = np.array(point_distances > self.threshold_dict["min_dist_local_max"],
                                          dtype=bool)
            is_chosen_local_max = np.array(np.logical_and(is_chosen_distance, is_chosen_intensity))

        else:
            is_chosen_local_max = np.array(point_centre_intensities > new_local_max_thresh, dtype=bool)

        self.is_chosen[where_this_type] = is_chosen_local_max

        return

    def update_local_max_thresh(self, new_local_max_thresh):

        print("updating local max threshold")

        self.threshold_dict["max_points"] = new_local_max_thresh
        local_max_intensities = self.centre_intensities[self.is_local_max_point]

        self.get_local_max_points_using_threshold(new_local_max_thresh, local_max_intensities,
                                                  self.is_local_max_point, self.local_max_point_dists)

        self.update_active_points()

        return

    def update_local_max_intensity_thresh(self, new_local_max_intensity_thresh):

        print("updating intensity local max threshold")

        self.threshold_dict["max_points_intensity"] = new_local_max_intensity_thresh
        local_max_intensities = self.centre_intensities[self.is_local_max_point_intensity]

        self.get_local_max_points_using_threshold(new_local_max_intensity_thresh, local_max_intensities,
                                                  self.is_local_max_point_intensity,
                                                  self.local_max_point_intensity_dists)

        self.update_active_points()

        return

    def update_local_max_prod_score_thresh(self, new_local_max_prod_score_thresh):

        print("updating prod_score local max threshold")

        self.threshold_dict["max_points_prod_score"] = new_local_max_prod_score_thresh
        local_max_intensities = self.centre_intensities[self.is_local_max_point_prod_score]

        self.get_local_max_points_using_threshold(new_local_max_prod_score_thresh, local_max_intensities,
                                                  self.is_local_max_point_prod_score,
                                                  self.local_max_point_prod_score_dists)

        self.update_active_points()
        return

    def update_slope_bleaching(self, image, new_double_thresh, new_local_max_thresh, new_ws_intensity):

        print("updating slope bleaching")

        self.update_double_thresh(new_double_thresh)
        self.update_local_max_thresh(new_local_max_thresh)
        self.update_ws_intensity(image, new_ws_intensity)
        self.update_active_points()
        return

    def update_ws_intensity(self, image, new_ws_intensity):

        print("updating ws intensity")

        self.threshold_dict["watershed_intensity"] = new_ws_intensity
        self.get_watershed_sizes_of_objects_at_point_from_image(image, new_ws_intensity)
        self.update_ws_size(self.threshold_dict["watershed_obj_size"])
        self.update_active_points()
        return

    def update_ws_size(self, new_watershed_size):

        print("updating ws size")

        self.threshold_dict["watershed_obj_size"] = new_watershed_size

        self.is_split = np.array(self.watershed_sizes > new_watershed_size, dtype=bool)

        # if the number of points expected is given to the function, only split so many points that the number is
        # not bigger than the number of points expected, starting from the biggest object
        if self.number_expected is not None:
            number_chosen, number_split_possibly = np.sum(self.is_chosen), np.sum(self.is_split)
            diff_expected_chosen = self.number_expected - number_chosen  # number of objects missing
            # if there are less object than the number expected that are already chosen, but more possible splits
            # than the number of objects missing (diff_expected_chosen), correct the split and only split the n biggest
            # objects, where n = diff_expected_chosen e
            if 0 < diff_expected_chosen < number_split_possibly:
                # set is_split to true for the biggest n objects, where n = diff_expected_chosen
                indices_of_largest_n_objects = np.argpartition(self.watershed_sizes,
                                                               -diff_expected_chosen)[-diff_expected_chosen:]
                self.is_split = np.array([True if index in indices_of_largest_n_objects else False
                                          for index in range(len(self.watershed_sizes))], dtype=bool)
            # if there are more objects than the number expected already, don't split any
            elif diff_expected_chosen < 0:
                self.is_split = np.zeros(shape=self.watershed_sizes.shape, dtype=bool)

        self.update_active_points()
        return

    def update_which_points_are_chosen(self):

        print("updating which points are chosen")
        self.update_local_max_thresh(self.threshold_dict["max_points"])
        self.update_double_thresh(self.threshold_dict["double_thresholding"])
        return

    def get_all_points_for_first_time(self):

        # all points with a watershed-size bigger than the watershed_obj_size are split
        self.is_split = np.array(self.watershed_sizes > self.threshold_dict["watershed_obj_size"], dtype=bool)

        # all local maximum points with intensities bigger than the "max_points" threshold are counted
        self.is_chosen[self.is_local_max_point] = np.array(self.centre_intensities[self.is_local_max_point] >
                                                           self.threshold_dict["max_points"],
                                                           dtype=bool)

        # make sure the local maxima in the intensity image and the prod_score image are at least the required distance
        # apart
        chosen_intensity_local_max_intensity = np.array(self.centre_intensities[self.is_local_max_point_intensity]
                                                        > self.threshold_dict["max_points"], dtype=bool)
        chosen_distance_local_max_intensity = np.array(self.local_max_point_intensity_dists >
                                                       self.threshold_dict["min_dist_local_max"], dtype=bool)

        self.is_chosen[self.is_local_max_point_intensity] = np.logical_and(chosen_intensity_local_max_intensity,
                                                                           chosen_distance_local_max_intensity)

        chosen_intensity_local_max_prod_score = np.array(self.centre_intensities[self.is_local_max_point_prod_score]
                                                         > self.threshold_dict["max_points"], dtype=bool)
        chosen_distance_local_max_prod_score = np.array(self.local_max_point_prod_score_dists >
                                                        self.threshold_dict["min_dist_local_max"], dtype=bool)

        self.is_chosen[self.is_local_max_point_prod_score] = np.logical_and(chosen_intensity_local_max_prod_score,
                                                                            chosen_distance_local_max_prod_score)

        # all double threshold points with intensities bigger than the 'double_thresholding' intensity threshold are
        # counted
        double_thresh_booleans = self.is_hig_value_point | self.is_mid_value_point | self.is_low_value_point

        self.is_chosen[double_thresh_booleans] = np.array(self.centre_intensities[double_thresh_booleans] >
                                                          self.threshold_dict["double_thresholding"],
                                                          dtype=bool)
        self.update_active_points()
        self.update_all_points()
        return

    def update_all_points(self):

        print("updating all points")

        if len(self.all_low_value_points) > 0:
            self.active_low_value_points = self.all_low_value_points[self.is_chosen[self.is_low_value_point]]
        if len(self.all_mid_value_points) > 0:
            self.active_mid_value_points = self.all_mid_value_points[self.is_chosen[self.is_mid_value_point]]
        if len(self.all_hig_value_points) > 0:
            self.active_hig_value_points = self.all_hig_value_points[self.is_chosen[self.is_hig_value_point]]
        if len(self.all_local_max_points) > 0:
            self.active_local_max_points = self.all_local_max_points[self.is_chosen[self.is_local_max_point]]
        if len(self.all_local_max_points_prod_score) > 0:
            self.active_local_max_points_prod_score = self.all_local_max_points_prod_score[
                self.is_chosen[self.is_local_max_point_prod_score]]
        if len(self.all_local_max_points_intensity) > 0:
            self.active_local_max_points_intensity = self.all_local_max_points_intensity[
                self.is_chosen[self.is_local_max_point_intensity]]

        print(colored("found " + str(len(self.active_low_value_points)) + " low value points", 'green'))
        print(colored("found " + str(len(self.active_mid_value_points)) + " mid value points", 'green'))
        print(colored("found " + str(len(self.active_hig_value_points)) + " high mid value points", 'green'))
        print(colored("found " + str(len(self.active_local_max_points)) + " local max points", 'green'))
        print(colored("found " + str(len(self.active_local_max_points_prod_score)) + " intensity local max points",
                      'green'))
        print(colored("found " + str(len(self.active_local_max_points_intensity)) + "prod score local max points",
                      'green'))

        return

    def add_points(self, points_list_1, type_string, image):

        if len(points_list_1) == 0:
            return

        print("adding " + type_string + " points")

        # number of points in this list
        self.length += len(points_list_1)

        # coordinates: the point in the image, with and without resolution correction
        if len(self.coordinates) == 0:
            self.coordinates = points_list_1
        else:
            self.coordinates = np.append(self.coordinates, points_list_1, axis=0)

        if len(self.resolution_corrected_coordinates) == 0:
            self.resolution_corrected_coordinates = get_resolution_corrected_points_list(points_list_1, self.metadata)
        else:
            self.resolution_corrected_coordinates = np.append(self.resolution_corrected_coordinates,
                                                              get_resolution_corrected_points_list(points_list_1,
                                                                                                   self.metadata),
                                                              axis=0)
        if len(self.centre_intensities) == 0:
            self.centre_intensities = np.array(get_intensities_at_points_from_image(image, points_list_1))
        else:
            self.centre_intensities = np.append(self.centre_intensities,
                                                get_intensities_at_points_from_image(image, points_list_1))

        if len(self.mean_intensities) == 0:
            self.mean_intensities = np.array(get_mean_intensity_in_cube_with_point(image, points_list_1, 1))
        else:
            self.mean_intensities = np.append(self.mean_intensities,
                                              get_mean_intensity_in_cube_with_point(image, points_list_1, 1))

        # indicator whether this object is chosen or not
        if len(self.is_chosen) == 0:
            self.is_chosen = np.array([True] * len(points_list_1), dtype=bool)
        else:
            self.is_chosen = np.append(self.is_chosen,
                                       np.array([True] * len(points_list_1), dtype=bool))

        if type_string == "low_value_point":
            self.is_low_value_point = np.append(self.is_low_value_point,
                                                np.array([True] * len(points_list_1), dtype=bool))
            self.all_low_value_points = points_list_1
        else:
            self.is_low_value_point = np.append(self.is_low_value_point,
                                                np.array([False] * len(points_list_1), dtype=bool))

        if type_string == "mid_value_point":
            self.is_mid_value_point = np.append(self.is_mid_value_point,
                                                np.array([True] * len(points_list_1), dtype=bool))
            self.all_mid_value_points = points_list_1
        else:
            self.is_mid_value_point = np.append(self.is_mid_value_point,
                                                np.array([False] * len(points_list_1), dtype=bool))

        if type_string == "hig_value_point":
            self.is_hig_value_point = np.append(self.is_hig_value_point,
                                                np.array([True] * len(points_list_1), dtype=bool))
            self.all_hig_value_points = points_list_1
        else:
            self.is_hig_value_point = np.append(self.is_hig_value_point,
                                                np.array([False] * len(points_list_1), dtype=bool))

        if type_string == "local_max_point":
            self.is_local_max_point = np.append(self.is_local_max_point,
                                                np.array([True] * len(points_list_1), dtype=bool))
            self.all_local_max_points = points_list_1
        else:
            self.is_local_max_point = np.append(self.is_local_max_point,
                                                np.array([False] * len(points_list_1), dtype=bool))

        if type_string == "local_max_point_prod_score":
            self.is_local_max_point_prod_score = np.append(self.is_local_max_point_prod_score,
                                                           np.array([True] * len(points_list_1), dtype=bool))
            self.all_local_max_points_prod_score = points_list_1
        else:
            self.is_local_max_point_prod_score = np.append(self.is_local_max_point_prod_score,
                                                           np.array([False] * len(points_list_1), dtype=bool))

        if type_string == "local_max_point_intensity":
            self.is_local_max_point_intensity = np.append(self.is_local_max_point_intensity,
                                                          np.array([True] * len(points_list_1), dtype=bool))
            self.all_local_max_points_intensity = points_list_1
        else:
            self.is_local_max_point_intensity = np.append(self.is_local_max_point_intensity,
                                                          np.array([False] * len(points_list_1), dtype=bool))

        return

    def add_manual_points_and_tracking_indices(self, manual_point, manual_tracking_indices):

        print("adding manual points and tracking indices")

        self.manual_points = manual_point
        self.manual_tracking_indices = manual_tracking_indices
        return

    def get_split_objects_for_all_objects(self, image):

        import split_big_objects as sbo

        print("getting split objects for all objects")

        watershed_threshold = -self.threshold_dict["watershed_intensity"]
        # create a watershed using the list of points as seeds until the threshold level is reached
        segmentation_image = np.copy(image)
        # in case some detected point have an intensity lower than the watershed_intensity: increase them, so the size
        # is at least 1
        for point in self.coordinates:
            if segmentation_image[point[0], point[1], point[2]] <= - watershed_threshold:
                segmentation_image[point[0], point[1], point[2]] = - watershed_threshold + 1

        intensity_img_ws = sbo.get_watershed_from_image_within_mask(segmentation_image, self.coordinates,
                                                                    threshold=watershed_threshold)

        regionprops = skimage.measure.regionprops(intensity_img_ws)

        self.split_points = []

        # go through all regions, calculate the eigenvecs and eigenvals of their inertia tensor and get the
        # eigenvectors corresponding to the max/min eigenvalue to calculate the principal axis, then split the
        # object along the principal axis
        for region_index, region in enumerate(regionprops):
            major_axis_lengths = region.major_axis_length
            obj_min_inertia_vec = sbo.calculate_min_and_max_inertia_vectors_from_region(region)[0]
            # use a numerical factor to shrink the distance between the points within the object (in range [0,1])
            numerical_factor = 0.8
            # add two points by an offset going in positive/negative direction along the principal axes from the
            # 'original' point as a 'base value' a 1/4 of the major axis length is chosen
            offset = numerical_factor * major_axis_lengths / 4 * obj_min_inertia_vec
            if major_axis_lengths == 0:
                offset += 0.1
            self.split_points.append([self.coordinates[region_index] + offset, self.coordinates[region_index] - offset])
        return

    # add a score image to the information parameters
    def add_score_image(self, score_image):
        self.score_image = score_image
        return


# function that takes an image and a list of points and returns the intensity at each of the points in the image as a
# list, this function is used to add the intensities as an attribute to the DetectedObjectsList for each image
# inputs: image: np.array, containing the intensity image
#         points_list: list of np.arrays containing the coordinates of the points
# output: intensities: list of intensities with the same length as "points_list" containing the intensities at the
#                      points in "points_list"
def get_intensities_at_points_from_image(image, points_list):
    intensities = [image[int(points_list[i][0]), int(points_list[i][1]), int(points_list[i][2])]
                   for i in range(len(points_list))]

    return intensities


# function that takes an image and a list of points and returns the mean intensity in a cube around each of the points
# in the image as a list, this function is used to add the mean intensities as an attribute to the DetectedObjectsList
# for each image.
# inputs: image: np.array, containing the intensity image
#         points_list: list of np.arrays containing the coordinates of the points
#         cube_size: size of the cube around the middle point, where the 'layers' around the point are counted, e.g.
#                    in 3D if cube_size = 1 a 3x3x3 cube will be examined --> the cube has a side length of
#                    (1 + cube_size*2)
# output: mean intensities: list of intensities with the same length as "points_list" containing the mean intensities
#                           in a cube of size (1 + cube_size*2)^3 centred at the points in "points_list"
def get_mean_intensity_in_cube_with_point(image, points_list, cube_size):
    mean_intensities = [np.mean(
        image[max(0, int(points_list[i][0] - cube_size)):int(points_list[i][0] + cube_size + 1) or None,
        max(0, int(points_list[i][1] - cube_size)):int(points_list[i][1] + cube_size + 1) or None,
        max(0, int(points_list[i][2] - cube_size)):int(points_list[i][2] + cube_size + 1) or None])
        for i in range(len(points_list))]

    return mean_intensities


# calculate the resolution corrected version of a list of coordinates of points by stretching it in z-direction
def get_resolution_corrected_points_list(coordinates_list, metadata):
    resolution_corrected_points = coordinates_list.copy()
    if metadata is not None:
        for i, point in enumerate(resolution_corrected_points):
            try:
                point[2] = (point[2] * (metadata["phys_size_z"] / metadata["phys_size_x"])).astype(float)
            except IndexError:
                print("Index Error for point " + str(i))
                print(point)
                print("This indicates that two of the points you have chosen are identical!")
    return resolution_corrected_points


# get basic parameters for the initialisation of a DetectedObjectsList object from a points_list and an image
def get_basic_parameters(points_list, image, metadata=None):
    # intensities at the point in the 'points_list' in the 'image'
    intensities = get_intensities_at_points_from_image(image, points_list)

    # mean intensity in a neighbourhood with 9 points (cube around point) centred at the point in the 'points_list'
    # in the 'image'
    mean_intensities = get_mean_intensity_in_cube_with_point(image, points_list, 1)

    # for some properties (e.g. distance) the resolution and thus pixel size has to be taken into account,
    # thus calculate the centres of points in a resolution corrected manner
    resolution_corrected_coordinates = get_resolution_corrected_points_list(points_list, metadata)

    # same goes for the image shape --> also save the resolution corrected version
    im_shape = image.shape
    if metadata is not None:
        resolution_corrected_im_shape = (image.shape[0], image.shape[1],
                                         metadata["phys_size_z"] / metadata["phys_size_x"] * image.shape[2])
    else:
        resolution_corrected_im_shape = im_shape

    return intensities, mean_intensities, resolution_corrected_coordinates, im_shape, resolution_corrected_im_shape
