from load_from_and_save_to_memory import load_class_object_from_memory, load_point_objects_from_memory
from os_functions import try_creating_folder
from find_better_tracks_using_alternatives_obj_oriented import find_better_tracks_using_alternatives
import pandas as pd
import read_in_config_file

# define the configuration file
# config_file = ""
config_file = "C:/Users/janaw/Documents/GitHub/kinetochore-tracking/Code/config_judith_old_data_new_computer.yml"
try:
    if config_file == "":
        config_file = read_in_config_file.gui_file_name()
except NameError:
    config_file = read_in_config_file.gui_file_name()

p = read_in_config_file.read_in_config_file(config_file)

# load the tracking outcome
tracking_folder = p.analysis_folder + "Tracking_outcome/linear_programming_problem_before_correction.pkl"
i_lpp = load_class_object_from_memory(tracking_folder)
if not hasattr(i_lpp, "points_removed"):
    i_lpp.points_removed = []

# try loading alternative outcomes
loaded_alt_i_lpps = []
for i in range(10):
    try:
        class_to_load = p.analysis_folder + "Tracking_outcome/linear_programming_problem_before_correction_alt" \
                        + str(i) + ".pkl"
        loaded_alt_i_lpps.append(load_class_object_from_memory(class_to_load))
    except FileNotFoundError:
        break

# load the segmentation
segmentation_folder = p.analysis_folder + "manually_adjusted_segmentation_points/final_segmentation/"
point_objects = load_point_objects_from_memory(p, folder=segmentation_folder)

# make sure all folders exist (in case the "create plots" cell wasn't run)
p.tracking_outcome_folder = p.analysis_folder + "Tracking_Outcome_Plots/"
try_creating_folder(p.tracking_outcome_folder)
p.tracking_distance_plot_folder = p.analysis_folder + "Tracking_distance_plots/"
try_creating_folder(p.tracking_distance_plot_folder)
p.tracking_pair_plot_folder = p.analysis_folder + "Tracking_pair_plots/"
try_creating_folder(p.tracking_pair_plot_folder)

pandas_df = pd.read_pickle(p.analysis_folder + "Tracking_outcome/" + "combined_tracking_outcomes.pkl")

find_better_tracks_using_alternatives(pandas_df, i_lpp, loaded_alt_i_lpps, p)
