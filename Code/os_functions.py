import os


# try creating a folder with the name/path given to the function
# inputs: folder_name: str, indicating the path and name of the folder that should be created
def try_creating_folder(folder_name):
    # try creating a folder with the name given in the input
    try:
        os.mkdir(folder_name)
    # if this process fails (e.g. if the folder already exist, print that the creation failed and
    # warn the user that it might already exist)
    except OSError:
        print("Creation of the directory %s failed. It might already exist." % folder_name)
    # if the creation is successful, state this
    else:
        print("Successfully created the directory %s " % folder_name)
    return
