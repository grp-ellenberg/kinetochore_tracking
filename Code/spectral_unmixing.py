import numpy as np
from sklearn.cluster import KMeans
import napari
from scipy.ndimage import median_filter


# median filter
# only keep the median of cubic region around voxel with 'filter'-size --> keep edges, remove impulse noise
# inputs: image (nD numpy array of the image)
#         filter_size (int, size of the median filter (square/n-cube with side-length filter_size))
# outputs: image (nD numpy array of the image)
def median_filter_xyz(orig_xyz_image, filter_size):
    # padding around is zero --> might reduce object intensity of objects entering/leaving the frame,
    # but better for noise handling
    return median_filter(orig_xyz_image, size=filter_size, mode='constant')


# implementation of the LUMoS spectral unmixing algorithm (10.1371/journal.pone.0225410)
# differences: all z-stacks are considered for the k-means algorithm, and only 1 time-step
# is given at a time, optional input parameter: display to display the outcome in 3D
# inputs: image_cxyz: np.array, 4D: first dimension is the channel, the other correspond to spatial dims
#         max_iter:  int, maximum of iterations for the k-means clustering
#         num_replicates: int, # initialisations of the k-means clustering
#         num_fluorophores int, the number of fluorescent markers used in the image, it may be bigger or smaller
#                            than the number of channels
#         intensity ratios: bool, whether or not to pre-process the input data such that the relative intensity,
#                           compared to other channels is used as an input
#         display: bool, whether or not to display the outcome of the spectral unmixing in the end
def spectral_unmixing_LUMoS(image_cxyz, max_iter=100, num_replicates=10, num_fluorophores=2,
                            intensity_ratios=True, display=False):
    # the number of clusters is the number of fluorophores, plus 1 for the background
    num_clusters = num_fluorophores + 1
    num_channels = image_cxyz.shape[0]

    # copy the image, to store the unclustered version, convert it to floating point precision
    preprocessed_image = np.array(image_cxyz.copy(), dtype=float)

    # if signal intensity differs much across imaging depth, account for intensity of pixel in each channel
    # by dividing through the total signal in that channel
    if intensity_ratios:
        for i, channel in enumerate(image_cxyz):
            preprocessed_image[i] = channel / np.sum(image_cxyz, axis=0)

    # pre-processing step: perform clustering on z-scores, where the z-scores are the # std
    # away from the mean signal
    for i, channel in enumerate(preprocessed_image):
        channel_mean = np.mean(channel)
        channel_std = np.std(channel)
        preprocessed_image[i] = (channel - channel_mean) / channel_std

    # apply median filter to each channel in the image
    for i, channel in enumerate(preprocessed_image):
        preprocessed_image[i] = median_filter_xyz(channel, 3)  # filter_size = 3

    # run the k-means clustering algorithm on an array clustering together all channels
    array_for_clustering = np.transpose(
        preprocessed_image.reshape(num_channels, (int(preprocessed_image.size / num_channels))))
    # use the k-means++ initialisation as chosen in the LUMoS paper (better for speed + convergence properties)
    k_means = KMeans(num_clusters, init="k-means++", max_iter=max_iter, n_init=num_replicates).fit(array_for_clustering)
    # get the cluster centers and labels from the solution of the k_means clustering
    cluster_centers = k_means.cluster_centers_
    labels = k_means.labels_
    num = image_cxyz.shape[1] * image_cxyz.shape[2] * image_cxyz.shape[3]

    # reshape the labels into a 3D image
    reshaped_labels = labels.reshape((image_cxyz.shape[1], image_cxyz.shape[2], image_cxyz.shape[3]))

    # use the cluster centres to return the image with the clusters sorted in the same way
    # (only use the first dimension)
    argsorted_cluster_centers = np.argsort(cluster_centers, axis=0)[:, 1]

    # assign the pixel with the maximum intensity along all channels to the labelled channel, if it
    # is in the cluster
    # 1) get the maximum along all channels
    max_along_c_xyz_image = np.max(image_cxyz, axis=0)
    # 2) assign the max-pixels to their clusters
    unmixed_image = np.zeros(shape=(num_clusters, image_cxyz.shape[1], image_cxyz.shape[2], image_cxyz.shape[3]),
                             dtype=float)
    for i, unmixed_image_channel in enumerate(unmixed_image):
        unmixed_image[i] = max_along_c_xyz_image * np.array(reshaped_labels == argsorted_cluster_centers[i])

    if display:
        # display the image in a napari viewer
        with napari.gui_qt():
            viewer = napari.Viewer()
            viewer.add_image(unmixed_image,
                             name='unmixed image',
                             colormap='turbo')

    return unmixed_image
