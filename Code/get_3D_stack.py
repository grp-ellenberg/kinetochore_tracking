import numpy as np
import warnings
import sys
from skimage import measure


# get a 3D xyz image (np.array) from a 5D np array image
# inputs: channel (c), time (t) (both integers that correspond to the index in the array),
#         full_image: 5D np.array of image data where the dimension order is TZYXC or TZCYX where t is time,
#                     z is depth, y height, x width and c channel of the image
# output: image_xyz: an 3D np.array of image data for input time index t and channel index c where the dimensions
#                    are XYZ in that order.
def get_xyz_image_from_5d(c, t, full_image, metadata):
    if full_image.ndim == 5:
        # get the desired time and channel slices 
        if metadata['DimOrder BF Array'] == 'TZYXC':
            image_zyx = full_image[t, :, :, :, c]
            # reorder the array such that it is an xyz array
            image_xyz = np.ndarray.transpose(image_zyx)
        elif metadata['DimOrder BF Array'] == 'TZCYX':
            image_zyx = full_image[t, :, c, :, :]
            # reorder the array such that it is an xyz array
            image_xyz = np.ndarray.transpose(image_zyx)
        else:
            warnings.warn(f"Adapt the function get_xyz_image_from_5d to include {metadata['DimOrder BF Array']}",
                          UserWarning)
            print("Adapt the function get_xyz_image_from_5d to include {metadata['DimOrder BF Array']}")
            sys.exit()
    elif full_image.ndim == 4:
        print("assume only one time-frame was given")
        # get the desired channel slices
        if metadata['DimOrder BF Array'] == 'TZYXC':
            image_zyx = full_image[:, :, :, c]
            # reorder the array such that it is an xyz array
            image_xyz = np.ndarray.transpose(image_zyx)
        elif metadata['DimOrder BF Array'] == 'TZCYX':
            image_zyx = full_image[:, :, :, c]
            # reorder the array such that it is an xyz array
            image_xyz = np.ndarray.transpose(image_zyx)
        else:
            warnings.warn(f"Adapt the function get_xyz_image_from_5d to include {metadata['DimOrder BF Array']}",
                          UserWarning)
            print("Adapt the function get_xyz_image_from_5d to include {metadata['DimOrder BF Array']}")
            sys.exit()
    else:
        warnings.warn(f"Number of dims too high or too low: {full_image.ndim}", UserWarning)
        sys.exit()
    return image_xyz


# Interpolate the stack in z-direction, such that the output voxels will have roughly the same physical size in all
# dimensions
# assuming that the physical sizes in x- and y-direction are equal before interpolation.
# inputs: image_var: 3D np.array of image data where dimensions are in order xyz,
#         metadata_var: a dictionary containing the metadata (needed to get the physical sizes in x and z-direction)
# outputs: image_var: 3D np.array of image data where dimensions are in order xyz (but the size of the z-axis has
#          changed)
def interpolate_xyz_image_in_z(image_var, metadata_var):
    # get the nearest integer number to the ratio between the z- and the x-axis (the x-y-resolution is assumed to
    # be the same)
    float_ratio1 = float(metadata_var["phys_size_z"]) / float(metadata_var["phys_size_x"])
    int_ratio1 = round(float_ratio1)
    print(f"Ratio of x-size to z-size is: {int_ratio1}")

    # get the shape of the original 'old' array, and make it bigger in z-direction
    old_shape = image_var.shape
    old_z_shape = old_shape[2]
    z_shape = int_ratio1 * old_z_shape - int_ratio1 + 1  # only want to interpolate in between different z-values
    new_shape = (old_shape[0], old_shape[1], z_shape)

    # update the dictionary
    metadata_var.update({'new_size_z': z_shape})
    metadata_var.update({'new_phys_size_z': float(metadata_var["phys_size_z"]) / float(int_ratio1)})

    # create an empty array in which the new values can be stored
    interpolated_image_xyz = np.zeros(shape=new_shape)

    # interpolate linearly between all old pixels, by iteration over all new pixels:
    # for all interpolation points in between two old pixels
    for k in range(int_ratio1):
        # for every one of the old pixels
        for j in range(old_z_shape):
            # linear weight anti-proportional to the distance of an interpolation point to the original point
            weight_ratio = float(int_ratio1 - k) / float(int_ratio1)
            # except for the positive iteration at the very last point:
            if j != old_z_shape - 1:
                # add the weighted z-value to all points in 'positive' z-direction
                interpolated_image_xyz[..., (int_ratio1 * j) + k] += weight_ratio * image_var[..., j]
            # if k is not zero (if the interpolation point is not directly on the grid), also interpolate in negative
            # direction
            if k != 0 and j != 0:
                interpolated_image_xyz[..., (int_ratio1 * j) - k] += weight_ratio * image_var[..., j]

    # the very last point is not included in the iteration, so it is set to its new value explicitly
    interpolated_image_xyz[..., z_shape - 1] = image_var[..., old_z_shape - 1]

    return interpolated_image_xyz, metadata_var


# apply a filtering function from filters.py with their different inputs:
# input: packed_seg_method: all values that belong to the filter method as a tuple:
#                           the name of the segmentation method,
#                           the filter function,
#                           the parameters of the filter function (also as a tuple)
#        image: the 3D (for the Lee filter) otherwise nD image that will be filtered (np.array)
# output: filtered_image: the 3D (for the Lee filter) otherwise nD image that was filtered (np.array)
# for information about the different filters and their inputs and outputs: see filters.py
def apply_filter_function(packed_filter_method, image):
    # unpack the method name, the filter function and the corresponding values.
    method_name, filter_function, method_values = packed_filter_method
    if method_name == "gaussian_blur_xyz":
        sigma = method_values[0]
        filtered_image = filter_function(image, sigma)
    elif method_name == "lee_filter":
        size = method_values[0]
        filtered_image = filter_function(image, size)
    elif method_name == "median_filter":
        size = method_values[0]
        filtered_image = filter_function(image, size)
    elif method_name == "percentile_filter":
        size, percentile = method_values
        filtered_image = filter_function(image, size, percentile)
    elif method_name == "patch_based_denoising_filter":
        apply_grad_diff = method_values[0]
        filtered_image = filter_function(image, apply_grad_diff)
    elif method_name == "curvature_diff_image_filter":
        no_iterations = method_values[0]
        filtered_image = filter_function(image, no_iterations)
    elif method_name == "grad_diff_image_filter":
        no_iterations = method_values[0]
        filtered_image = filter_function(image, no_iterations)
    elif method_name == "bilateral_filter":
        filtered_image = filter_function(image)
    # If the method specified does not exist, exit the program
    else:
        warnings.warn(f"In function: apply_filter_function: this filter function does not exit: {method_name}",
                      UserWarning)
        sys.exit()
    return filtered_image


# apply a thresholding function from segmentation_methods.py with their different inputs:
# input: packed_thresh_method: all values that belong to the thresholding method as a tuple:
#                              the name of the thresholding method,
#                              the corresponding thresholding function,
#                              the parameters for the thresholding (also as a tuple)
#        image: an nD np.array, but for weighted_global_and_slice_otsu the last dimension has to be
#               the z-dimension and the stack has to be 3D for the result to make sense
# output: filtered_image: a nD image that was thresholded (np.array)
# for information about the different segmentation methods and their inputs and outputs: see segmentation_methods.py
def apply_thresholding_function(packed_thresh_method, image):
    method_name, thresholding_function, method_values = packed_thresh_method
    if method_name == "gaussian_and_thresh":
        sigma, thresh = method_values
        thresholded_image = thresholding_function(image, sigma, thresh)
    elif method_name in ["global_otsu", "Huang", "IsoData", "Li", "MaxEntropy", "KittlerIllingworth", "Moments", "Yen",
                         "RenyiEntropy", "Shanbhag"]:
        thresholded_image = thresholding_function(image)
    elif method_name == "weighted_global_and_slice_otsu":
        weight = method_values[0]
        thresholded_image = thresholding_function(image, weight)
    else:
        warnings.warn(f"In function: apply_thresholding_function: this thresholding function does not exit: "
                      f"{method_name}", UserWarning)
        sys.exit()
    return thresholded_image


# get a 3D part from a 5D image, filter the image prior to interpolation, interpolate the image, filter the
# image after interpolation, perform the thresholding, create a labeled image from the thresholded image
# and return it
# inputs: full_image: 5D numpy array containing the full image stack, including time and channels
#         metadata_var: a dictionary containing the metadata values as defined in read_in.py
#         c_var: an index defining the channel of the 3D stack of interest
#         t_var: an index defining the time-slice of the 3D stack of interest
#         processing_method_prior: a tuple containing all information needed for filtering prior
#                                  to interpolating the image (see apply_filter_function for more detailed info
#                                  on what these information are)
#         processing_method_post: a tuple containing all information needed for filtering after
#                                 interpolating the image (see apply_filter_function for more detailed info
#                                 on what these information are)
#         segmentation_method: a tuple containing all information needed for segmenting the image
# outputs: label_image_xyz: 3D numpy array containing the segmented and labeled objects that were found in
#                           the image_xyz stack
#          image_xyz: 3D numpy array created by slicing and interpolating the 5D input stack corresponding to
#                     time-point t_var and channel c_var (as defined in the input)
#          metadata_var: the updated metadata dictionary (contains 'new_size_z' after interpolation)
def get_labeled_xyz_image(full_image, metadata_var, c_var, t_var, processing_method_prior=None,
                          processing_method_post=None, segmentation_method=None):
    # get the 5D image
    image_xyz = get_xyz_image_from_5d(c_var, t_var, full_image, metadata_var)

    # apply filtering prior to interpolation
    if processing_method_prior[0] is None and t_var == 0:
        print("No processing before interpolation")
        pre_processing_image_xyz = image_xyz
    else:
        pre_processing_image_xyz = apply_filter_function(processing_method_prior, image_xyz)

    # interpolate the image
    interpolated_image_xyz, metadata_var = interpolate_xyz_image_in_z(pre_processing_image_xyz, metadata_var)

    # apply filtering after interpolation
    if processing_method_post[0] is None and t_var == 0:
        print("No processing after interpolation")
        past_processing_image_xyz = interpolated_image_xyz
    else:
        past_processing_image_xyz = apply_filter_function(processing_method_post, interpolated_image_xyz)

    # apply the segmentation
    if segmentation_method is None and t_var == 0:
        print("No segmentation method specified")
        thresholded_image_xyz = past_processing_image_xyz
    else:
        thresholded_image_xyz = apply_thresholding_function(segmentation_method, past_processing_image_xyz)

    # create a labeled image
    try:
        # use the skimage measure function to get a labeled image
        # the region_labels_xyz variable contains the #regions, but this can be computed using np.max(label_image_xyz)
        # which is more practical, because only one variable has to be passed
        label_image_xyz, region_labels_xyz = measure.label(thresholded_image_xyz, background=0, return_num=True)

    # Catch some exceptions: If the metadata dictionary is empty, exit the function
    except UnboundLocalError:
        print("Error: Segmentation was not successful, can't label image.")
        sys.exit()

    # return the labeled image, the interpolated image and the updated metadata dictionary
    return label_image_xyz, interpolated_image_xyz, metadata_var
