import scipy.optimize
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema


# returns a parabola in 'vertex-form' 
# inputs: x, a, b, c, all numeric values or equivalent np forms
# returns: a(x-b)^2 + c
def parabola(x, a, b, c):
    return a*(x-b)**2 + c


# returns a Gaussian with amplitude a, shift mu and STD sigma
# inputs: x, a, mu, sigma, all numeric values or equivalent np forms
# returns: a*exp( [-(x-mu)^2] / [2*sigma^2] )
def gaussian(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2.*sigma**2))


# fits a function and returns the optimum parameters popt
# inputs: function: to which to parameters are fit (the first input parameter of the function must be x, the return y)
#         y_sequence: the data the function should be fit to
#         x_sequence (optional): the corresponding x_values in the same arrangement as the y-values, 
#                                if none are given, integer indices will be used
#         return_cov (optional): bool indicating whether the covariance of the fit should be returned or not,
#                                (default: False)
# output: popt: the optimal parameters that were determined by the curve fit, corresponding to are all input parameters 
#               to the (input) function, except for x 
def function_fit(function, y_sequence, x_sequence=None, return_cov=False):
    # if no x-sequence is given, create indices
    if x_sequence is None:
        x_sequence = np.arange(len(y_sequence))
    # get the optimal parameters and their covariances from fitting the x- and y-sequence to the input data
    popt, pcov = scipy.optimize.curve_fit(function, x_sequence, y_sequence)
    # return the optimum parameters and optionally (input) the covariance
    if return_cov:
        return popt, pcov
    else:
        return popt


# determine the full=width-half-maximum (FWHM) of a sequence
# input: x-vales (x_val) and corresponding y-values (y_val) of a function
# outputs: fwhm: the FWHM of the underlying function
#          left_val: the smaller x-value where f(x) = half-maximum
#          right_val: the bigger x-value where f(x) = half-maximum
#          half_max: the half maximum of the curve (max/2.)
def get_fwhm(x_val, y_val):
    
    # get max position:
    max_index = np.argmax(y_val)
    
    # calculate the half-maximum
    half_max = y_val[max_index] / 2.
    
    # calculate the the absolute value of the difference to the half_max
    abs_diff_to_half_max = np.abs(y_val - half_max)
    
    # get the x-value on the left side of the max where this absolute difference to the half max is minimal
    left_val = x_val[np.argmin(abs_diff_to_half_max[:max_index])]
    
    # get the x-value on the right side of the max where the absolute difference to the half max is minimal  
    right_val = x_val[np.argmin(abs_diff_to_half_max[max_index:]) + max_index]  # have to add max_index due to slicing
    
    # calculate the FWHM
    fwhm = right_val - left_val
    
    # return the FWHM, the corresponding left and right x-values and the HM
    return fwhm, left_val, right_val, half_max


# this function plots the mean-projection of the intensity of an object
# inputs: object_var: a 2D or 3D np.array containing numerical values (e.g. an intensity image)
#         fit (optional): str, specifies which function should be used as a fit,
#                         the default is none, options are 'gauss' or 'parabola'
#         return_fit_params (optional): whether or not the fit-parameters should be returned
# outputs: if return_fit_params (input) is True: a list of optimal fit parameters is returned 
#          containing the optimum fit parameters for each dimension 
def plot_intensity_projection_of_object(object_var, fit=None, return_fit_params=False, scale_factor=1, title=None):
    
    # check which dimension the object has, and create labels for them
    if len(object_var.shape) == 3:
        dims = ["x", "y", "z"]
        no_dim = 3
    elif len(object_var.shape) == 2:
        dims = ["x", "y"]
        no_dim = 2
    # raise a ValueError if the number of dimensions is too high or too low.=
    else:
        raise ValueError(f"ValueError: the input object is supposed to be 2 or 3-dimensional," +
                         f"this one is {len(object_var.shape)}-dimensional")
    
    # create a figure with no_dim (number of  dimensions) columns and size scaling with no_dim 
    fig, ax = plt.subplots(ncols=no_dim, figsize=(no_dim * 5, 5))
    
    # make the plot translucent
    fig.patch.set_facecolor('#E0E0E0')
    fig.patch.set_alpha(0.0)

    # create a dictionary that gives the axes along which the projection will be done
    if no_dim == 3:
        axis_dict = {0: (1, 2), 1: (0, 2), 2: (0, 1)}
    elif no_dim == 2:
        axis_dict = {0: 1, 1: 0}
    else:
        raise ValueError("Number of dimensions must be 2 or 3")
    
    # create an array to store the FWHMs
    fwhms = np.zeros(shape=len(object_var.shape), dtype=float)
    
    # create an empty list to store the optimal parameters
    if return_fit_params:
        optimal_parameters_list = []
    else:
        optimal_parameters_list = None
    
    # loop over all dimensions
    for dim, dim_str in enumerate(dims):

        # project the object down to the axis of the current dimension (dim)
        plot_obj = np.sum(object_var, axis=(axis_dict[dim]))/np.sum(object_var)*scale_factor

        # create an x-axis to plot the data
        x_data = np.arange(0, len(plot_obj)/scale_factor, 1./scale_factor)

        # plot the projection over the x-data
        ax[dim].plot(x_data, plot_obj)    
        # calculate the local maxima and plot them with red dots
        local_max = argrelextrema(plot_obj, np.greater)[0]  # opposites: np.greater/ np.less
        ax[dim].plot(local_max, plot_obj[local_max], 'o', color='red', label='data')
        
        # add a y-label to specify the dimension of the axis
        ax[dim].set_ylabel(f"Projection in {dim_str}")
        
        # if fitting should be done
        if fit is not None:
            # specify the function that should be used for the fit (gaussian or parabola)
            if fit == 'gauss':
                fit_function = gaussian
            elif fit == 'parabola':
                fit_function = parabola
            else:
                raise ValueError(f"ValueError: fit must be either 'gauss' or 'parabola': " +
                                 f"Your input is {fit}")
            
            # perform the fit an get the optimal parameters
            optimal_params = function_fit(fit_function, plot_obj, x_data)
            
            # plot the result of fitting using a new x-axis to augment precision
            x_fit = np.arange(0, len(plot_obj)/scale_factor, 0.01/scale_factor)
            y_fit = fit_function(x_fit, optimal_params[0], optimal_params[1], optimal_params[2])
            ax[dim].plot(x_fit, y_fit, label='fit')
            
            # calculate the FWHM of the fit and create a window to display it
            fwhms[dim], left_val, right_val, half_max = get_fwhm(x_fit, y_fit)
            ax[dim].axvspan(left_val, right_val, facecolor='g', alpha=0.2)
            
            # if the fit-parameters should be returned in the end, append the list to store them
            if return_fit_params:
                optimal_parameters_list.append(optimal_params)
        # adjust the y-limits of the fit
        ax[dim].set_ylim(0, 1.0)
        ax[dim].set_xlim(0, np.max(x_data))
        
        # create an empty title for layout purposes (otherwise there is no space for the suptitle)
        plt.title(" ")

    # add a title depending on the number of dimensions
    if no_dim == 3:
        fig.suptitle(f"Projection of one object on x-, y- and z-axis, FWHM {fwhms[0]:.1f}, {fwhms[1]:.1f}, "
                     f"{fwhms[2]:.1f}")
    if no_dim == 2:
        fig.suptitle(f"Projection of one object on x-and y-axis, FWHM {fwhms[0]:.1f}, {fwhms[1]:.1f}")
        
    # rearrange the layout of the plot
    # plt.tight_layout()
    plt.tight_layout()
    plt.savefig(title + ".png", dpi=300)

    # return the fit-parameters, if desired in the input 
    if return_fit_params:
        return optimal_parameters_list
    
    
# create a n-dimensional probability density function that is parabolic in each
# dimension, given the non-normalised parabola-parameters as a list and a 3D point x:
# - To interpret the parabola as a 1D probability function, it is assumed that the parabola
#   is facing downwards, with some part above the x-axis. The probability density p(x_i) is created
#   from the parabola function f(x_i) using a stepwise definition: p(x_i) = f(x_i), if f(x_i) > 0
#                                                                    = 0, otherwise
# - f(x_i) > 0 in the interval>0 = [b_i - sqrt(-c_i/a_i), [b_i + sqrt(-c_i/a_i)] for 
#   f(x_i) = a_i(x_i - b_i) + c_i
# - To normalise the probability function, the integral of f(x_i) over this interval 
#   Int_interval>0(f(x_i)) = 4/3 sqrt(-c_i/a_i)*c_i is used
# inputs: parabola_parameter_list[i,j]: list, i gives the dimension, j is in (0,1,2), where 
#                                      0 = a, 1 = b, 2 = c in a(x_i-b)**2 + c
#        x: an array, a tuple or a list giving the spatial position in all dimensions
# output: y: a numeric value specifying p(x), the probability of the intensity distribution
#            at point x
def nd_parabola_probability_density(parabola_parameter_list, x):
    # create an array from the parameter list and transpose it for easier handling of a,b,c
    param_array = np.asarray(parabola_parameter_list).T
    # extract the a,b and c vectors from the parameter array
    a, b, c = param_array[0], param_array[1], param_array[2]
    # set y to 1
    y = 1.
    # for each dimension i: multiply y with p[i] to obtain p = p[0]*p[1]*[p2]
    for i in range(len(x)):
        # calculate the square root of - c/a
        sqrt = np.sqrt(-c[i]/a[i])
        # in the interval, where the parabola is greater zero: return the parabola's
        # value (reasoning: see above)
        if b[i] - sqrt < x[i] < b[i] + sqrt:
            normalisation = sqrt*4./3.*c[i]
            value = a[i]*(x[i] - b[i])**2 + c[i]
            y *= value/normalisation
        # otherwise set y = 0 (reasoning for stepwise function, see above)
        else:
            y = 0.
    # return the probability density value
    return y


# create a n-dimensional probability density function that is Gaussian in each
# dimension, given the non-normalised Gaussian-parameters as a list and a 3D point x
# - To normalise the probability function use the fact that the distribution is normalised
#   for a = 1/(sigma sqrt(2pi))
# inputs: gaussian_parameter_list[i,j]: list, i gives the dimension, j is in (0,1,2), where 
#                                      0 = a, 1 = mu, 2 = sigma in a*exp(-(x-mu)^2/(2sigma^2))
#        x: an array, a tuple or a list giving the spatial position in all dimensions
# output: y: a numeric value specifying p(x), the probability of the intensity distribution
#            at point x
def nd_gaussian_probability_density(parabola_parameter_list, x):
    # create an array from the parameter list and transpose it for easier handling of a,mu,sigma
    param_array = np.asarray(parabola_parameter_list).T
    # extract the a,b and c vectors from the parameter array
    a, mu, sigma = param_array[0], param_array[1], param_array[2]
    # set y to 1
    y = 1.
    # calculate sqrt(2*pi) for the normalisation amplitude
    sqrt = np.sqrt(2*np.pi)
    # for each dimension i: multiply y with p[i] to obtain p = p[0]*p[1]*[p2]
    for i in range(len(x)):
        # calculate the new pre-factor:
        a_new = 1/(sigma[i]*sqrt)
        # calculate the value for the Gaussian with the normalised amplitude
        y *= gaussian(x[i], a_new, mu[i], sigma[i])
    return y


# this function calculates the probability distribution of the intensity of a given object, assuming it follows a
# certain distribution in the projection of every dimension
# inputs: obj: np.array: a 2D or 3D image region containing some object that will be fitted
#         fit: either a Gaussian model or a parabola model can be used, so the options are 'gauss' and 'parabola'
#              for more information on how the parabola is used as a probability density, read nd_parabola_probability_
#              density
#         return_model_image: whether or not a model image containing the modelled object should be created
#         scale_factor: the scale factor influences the coarseness of the model image, scale factor 10 means the model
#                       is 10 times finer than the original object in terms of sampling precision
#         plot: if the plot flag is True, plots of the projected object and their fits will be plotted. If additionally
#               return_model is True, a plot of the coarse model will be shown
# outputs: params: the parameters of the fit function in each dimension, they are scaled such that they can be
#                  interpreted as an intensity distribution in each dimension. The first index gives the dimension,
#                  the second the variable.
#                  For the parabola the order of parameters is a,b,c, where y = a(x-b)^2 + c
#                  For the Gaussian the order of parameters is a, mu, sigma, where y = a*exp[-(x-mu)^2/(2*sigma^2)]
#          if return_model_image = True: If the input was 2D/3D, the model image is also 2D/3D, but the sampling might 
#                                        be higher (scale_factor-times), it spans the space corresponding to the
#                                        original space spanned by the obj after upsampling.

def get_model_parameters_from_object(obj, fit='parabola', return_model_image=False, scale_factor=1, plot=False,
                                     plot_title=None):
    
    # get the dimension and the total intensity of the object that should be modelled
    no_dim = len(obj.shape)
    total_sum = np.sum(obj)
    
    # if the plots should be displayed: call the function that plots the intensity projection of an object,
    # also return the fitting parameters
    if plot:
        # fit the object with a parabola or a Gaussian fit, careful: here the scale_factor should always be 1
        unnormalised_params = plot_intensity_projection_of_object(obj, fit=fit, return_fit_params=True,
                                                                  title=plot_title + str(1))
    
    # otherwise: only determine the fit parameters
    else:
        
        # specify the function that should be used for the fit (gaussian or parabola)
        if fit == 'gauss':
            fit_function = gaussian
        elif fit == 'parabola':
            fit_function = parabola
        else:
            raise ValueError(f"ValueError: fit must be either 'gauss' or 'parabola': " +
                             f"Your input is {fit}")
            
        # create a dictionary that gives the axes along which the projection will be done
        if no_dim == 3:
            axis_dict = {0: (1, 2), 1: (0, 2), 2: (0, 1)}
        elif no_dim == 2:
            axis_dict = {0: 1, 1: 0}
        else:
            raise Exception("Function get_model_parameters_from_object, in projection_and_analysis.py: this function" 
                            "is only defined for 1 or two dimensional inputs.")
        
        # create an empty list to store the parameters
        unnormalised_params = []
        
        # project the object in all dimensions and get the parameters of the fit
        for dim in range(no_dim):
            projected_obj = np.sum(obj, axis=(axis_dict[dim]))/total_sum
            unnormalised_params.append(function_fit(fit_function, projected_obj))

    # now the parameters have to be normalised, such that a function constructed from them can be interpreted
    # as a probability density
    params = []
    
    # for the un-normalised parameters in every dimension:
    for un_p in unnormalised_params:
        # rescale the parabola such that the positive part corresponds to a probability density
        # for more details, see: nd_parabola_probability_density
        if fit == 'parabola':
            a, b, c = un_p[0], un_p[1], un_p[2]
            factor = 4./3*c*np.sqrt(-c/a)
            params.append([a/factor, b, c/factor])
        # rescale the Gaussian parameters such that the resulting curve corresponds to a probability density
        # for more details, see: nd_gaussian_probability_density
        elif fit == 'gauss':
            a, mu, sigma = un_p[0], un_p[1], un_p[2]
            factor = 1/(np.abs(sigma)*np.sqrt(2*np.pi))
            params.append([factor, mu, np.abs(sigma)])
        else:
            raise ValueError(f"ValueError: fit must be either 'gauss' or 'parabola': " +
                             f"Your input is {fit}")
    
    # if the model image is to be returned, create a new object
    if return_model_image:
        
        # get the shape of the new object
        old_shape = obj.shape
        new_shape = tuple((i-1)*scale_factor+1 for i in old_shape)

        # create the new object of appropriate size
        new_obj = np.zeros(shape=new_shape, dtype=float)

        # loop over all dimensions of the new object to obtain the corresponding y-value
        for i, x_i in enumerate(np.linspace(0, old_shape[0]-1, num=new_shape[0])):
            for j, x_j in enumerate(np.linspace(0, old_shape[1]-1, num=new_shape[1])):
                for k, x_k in enumerate(np.linspace(0, old_shape[2]-1, num=new_shape[2])):
                    x = [x_i, x_j, x_k]
                    if fit == 'parabola':
                        new_obj[i, j, k] = nd_parabola_probability_density(params, x)
                    elif fit == 'gauss':
                        new_obj[i, j, k] = nd_gaussian_probability_density(params, x)
                    else:
                        raise ValueError(f"ValueError: fit must be either 'gauss' or 'parabola': " +
                                         f"Your input is {fit}")
        
        # multiply the resulting object such that its total intensity corresponds to the total intensity of the object
        new_obj *= total_sum/(scale_factor**no_dim)
        
        # if the plotting option is active, plot the new_obj over the same axis as the old object
        # (for this the 'scale_factor' is needed)
        if plot:
            plot_intensity_projection_of_object(new_obj, fit=fit, return_fit_params=True, scale_factor=scale_factor,
                                                title=plot_title + str(2))
        
        # return the parameters and the new_object if "return_model_image" is True
        return params, new_obj
    
    # otherwise only return the parameters list
    else:
        return params
