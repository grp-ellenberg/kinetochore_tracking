import numpy as np
import itertools
import list_functions as lf


# return the sum from 1 to x using the 'kleiner Gauss' (Gauss Summenformel)
def kg(x):
    return int(x * (x + 1) / 2)


# initialise connection matrix slices from a list of values
def initialise_connection_matrix_slices(list_of_values):
    # create an emtpy list to store the slices as tuples
    connection_matrix_slices = []
    # take the value[i] as start and the value[i+1]-1 as stop value to initialise ConnectionMatrixSlice objects
    for i in range(len(list_of_values) - 1):
        connection_matrix_slices.append(ConnectionMatrixSlice(list_of_values[i], list_of_values[i + 1]))
    # return the connection matrix slices as a tuple
    return tuple(connection_matrix_slices)


# returns a unit matrix of size kxk with a row of ones on top of it, so a (k+1)xk matrix
def get_unit_matrix_plus_row_of_ones_on_top(k):
    matrix = np.ones((k + 1, k), dtype=np.int64)
    matrix[1:, :] = np.identity(k, dtype=np.int64)
    return matrix


# gets a matrix in which each of the columns contain exactly two ones and zeros otherwise,
# the ones are distributed such that all combinations of rows are explored, but none twice
def get_all_combinations_between_two_values_in_matrix_form(k):
    matrix = np.zeros((k, kg(k - 1)), dtype=np.int64)
    start_col = 0
    for i in reversed(range(k)):
        matrix[k - i - 1:, start_col:start_col + i] = get_unit_matrix_plus_row_of_ones_on_top(i)
        start_col += i
    return matrix


# create slices that contain the start point, the stop point and a slice object from 
# start to stop
class ConnectionMatrixSlice:
    def __init__(self, start, stop):
        # set the start and the stop points of the ConnectionMatrixSlice object
        self.start = start
        self.stop = stop
        # set the slice value
        self.slice = np.s_[start:stop]


class ConnectionMatrix:

    def __init__(self, m, n, can_split, can_merge, can_appear, can_disappear, frame_numbers,
                 connections_to_examine=None, no_expected=None,
                 exclusion_sets_in=None, exclusion_sets_out=None,
                 is_first_connection_matrix=False, is_last_connection_matrix=False):

        # set the variables indicating whether the objects can appear split, merge appear and disappear in this
        # data set (appear from the boundary, disappear behind the boundary)
        self.can_split = can_split
        self.can_merge = can_merge
        self.can_appear = can_appear
        self.can_disappear = can_disappear

        # set the number expected
        if no_expected is not None:
            self.no_expected = no_expected
        elif is_first_connection_matrix:
            self.no_expected = m
        elif is_last_connection_matrix:
            self.no_expected = n
        else:
            self.no_expected = int((m + n) / 2)

        # set the frame numbers
        self.frame_numbers = frame_numbers

        # set an indicator whether the connection matrix is the first or last of a set
        self.is_first_connection_matrix = is_first_connection_matrix
        self.is_last_connection_matrix = is_last_connection_matrix

        # set an indicator whether there are any exclusion sets (conflicting hypotheses - if one of them is chosen,
        # the other one can't be chosen anymore)
        self.exclusion_sets_in, self.exclusion_sets_out = exclusion_sets_in, exclusion_sets_out

        # np.arrays indicating input/output of the connection
        # in_1 contains the input node (-1 for TP, -3 for A(appear) of each connection. If objects can merge, the second
        # in node will be stored in in_2, if this is not a merge connection, this entry will be None
        # out_1 contains the output node (-2 for TM, -4 for D(disappear) of each connection. If objects can split, the
        # second out node will be stored in out_2, if this is not a split connection, this entry will be None
        self.in_1 = []
        if self.can_merge or self.can_disappear:
            self.in_2 = []
        else:
            self.in_2 = None
        self.out_1 = []
        if self.can_split or self.can_appear:
            self.out_2 = []
        else:
            self.out_2 = None

        if connections_to_examine is not None:
            self.connections_given = True
        else:
            self.connections_given = False

        # get how many rows and columns the matrix has

        # nodes that always exist: TP, TM, A, D, L, R
        row_lengths = [1, 1, 1, 1, m, n]
        row_names = ["TP", "TM", "A", "D"]
        row_names += [frame_numbers[0] + i * 0.001 for i in range(m)]  # row_names += ["L" + str(i) for i in range(m)]
        row_names += [frame_numbers[1] + i * 0.001 for i in range(n)]  # ["R" + str(i) for i in range(n)]
        # get the start indices of the individual nodes as row segments      
        cum_sum_row_lengths = np.cumsum(row_lengths, dtype=np.int64)
        # the first start index is 0, the next corresponds to the number of objects before
        # (the cumulated sum of these objects)
        row_start_list = np.concatenate((np.array([0], dtype=np.int64), cum_sum_row_lengths))
        self.t_plus, self.t_minus, self.a, self.d, self.l, self.r = initialise_connection_matrix_slices(row_start_list)

        # create always existing columns

        # (TP to A)
        column_lengths = [1]
        column_names = ["TP to A"]
        # set the in and out connection for (TP to A) transition
        self.in_1 = self.in_1 + [-1]  # incoming connection from TP (-1)
        self.out_1 = self.out_1 + [-3]  # connection going out to A (-3)
        if self.can_merge or self.can_disappear:
            self.in_2 = self.in_2 + [None]
        if self.can_split:
            self.out_2 = self.out_2 + [None]

        # (TP to Li)
        # create these edges only, if this is the first ConnectionMatrix in a series
        if self.is_first_connection_matrix:
            column_lengths += [m]
            column_names += ["TP" + str(frame_numbers[0]) +
                             " to " + str(frame_numbers[0] + i * 0.001) for i in range(m)]
            # set the in and out connections for (TP to Li) transitions
            self.in_1 = self.in_1 + [-1] * m  # going out of object T+ (-1)
            self.out_1 = self.out_1 + [frame_numbers[0] + i * 0.001 for i in range(m)]  # going into object Li
            if self.can_merge or self.can_disappear:
                self.in_2 = self.in_2 + [None] * m
            if self.can_split or self.can_appear:
                self.out_2 = self.out_2 + [None] * m

        # (D to TM)
        column_lengths += [1]
        column_names += ["D to TM" + str(frame_numbers[1])]
        # set the in and out connection for (D to TM) transition
        self.in_1 = self.in_1 + [-4]  # connection coming out of D (-4)
        self.out_1 = self.out_1 + [-2]  # connection going to T- (-2)
        if self.can_merge or self.can_disappear:
            self.in_2 = self.in_2 + [None]
        if self.can_split or self.can_appear:
            self.out_2 = self.out_2 + [None]

        # (Ri to TM)
        # create these edges only, if this is the last ConnectionMatrix in a series
        if self.is_last_connection_matrix:
            column_lengths += [n]
            column_names += [str(frame_numbers[1] + i * 0.001) + " to TM" + str(frame_numbers[1]) for i in range(n)]
            # set the in and out connections for (Ri to TM) transitions
            self.in_1 = self.in_1 + [frame_numbers[1] + i * 0.001 for i in range(n)]  # going out of Ri
            self.out_1 = self.out_1 + [-2] * n  # going into T-
            if self.can_merge or self.can_disappear:
                self.in_2 = self.in_2 + [None] * n
            if self.can_split or self.can_appear:
                self.out_2 = self.out_2 + [None] * n

        # (Li to Ri)
        if not self.connections_given:
            column_lengths += [m * n]
            column_names += [str(frame_numbers[0] + i * 0.001) + " to " + str(frame_numbers[1] + j * 0.001)
                             for i, j in itertools.product(range(m), range(n))]
            # set the in and out connections for (Li to Ri) transition
            # going out of Li
            self.in_1 = self.in_1 + [frame_numbers[0] + i * 0.001 for i, j in itertools.product(range(m), range(n))]
            # going into Rj
            self.out_1 = self.out_1 + [frame_numbers[1] + j * 0.001 for i, j in itertools.product(range(m), range(n))]
            if self.can_merge or self.can_disappear:
                self.in_2 = self.in_2 + [None] * (m * n)
            if self.can_split or self.can_appear:
                self.out_2 = self.out_2 + [None] * (m * n)
        else:
            self.l_to_r_connections = connections_to_examine[0]
            column_lengths += [len(lf.remove_nesting(self.l_to_r_connections))]
            column_names_lr = []
            for i, li in enumerate(self.l_to_r_connections):
                column_names_lr += [str(frame_numbers[0] + i * 0.001) + " to " + str(frame_numbers[1] + list_el * 0.001)
                                    for list_el in li]
                # set the in and out connections for (Li to Ri) transitions
                # going out of Li
                self.in_1 = self.in_1 + [frame_numbers[0] + i * 0.001 for _ in li]
                # going into Rj
                self.out_1 = self.out_1 + [frame_numbers[1] + j * 0.001 for j in li]
                if self.can_merge or self.can_disappear:
                    self.in_2 = self.in_2 + [None] * len(li)
                if self.can_split or self.can_appear:
                    self.out_2 = self.out_2 + [None] * len(li)

            column_names += column_names_lr

        # (A to D)
        column_lengths += [1]
        column_names += ["A" + str(frame_numbers[0]) + " to D" + str(frame_numbers[1])]
        # set the in and out connection for (A to D) transition
        self.in_1 = self.in_1 + [-3]  # connection coming out of A (-3)
        self.out_1 = self.out_1 + [-4]  # connection going into D (-4)
        if self.can_merge or self.can_disappear:
            self.in_2 = self.in_2 + [None]
        if self.can_split or self.can_appear:
            self.out_2 = self.out_2 + [None]

        # get the start indices of the individual edges as column segments
        cum_sum_column_lengths = np.cumsum(column_lengths, dtype=np.int64)
        column_start_list = np.concatenate((np.array([0], dtype=np.int64), cum_sum_column_lengths))

        # if the connection matrix is the first/last one in a series, the connections to the source (TP) or sink (TM)
        # must be made
        if self.is_first_connection_matrix:
            # check if it also is the last connection matrix (the series only consists of a single transition)
            if self.is_last_connection_matrix:
                self.t_plus_to_a, self.t_plus_to_l, self.d_to_t_minus, self.r_to_t_minus, self.l_to_r, self.a_to_d = \
                    initialise_connection_matrix_slices(column_start_list)
            else:
                self.t_plus_to_a, self.t_plus_to_l, self.d_to_t_minus, self.l_to_r, self.a_to_d = \
                    initialise_connection_matrix_slices(column_start_list)
        elif self.is_last_connection_matrix and not self.is_first_connection_matrix:
            self.t_plus_to_a, self.d_to_t_minus, self.r_to_t_minus, self.l_to_r, self.a_to_d = \
                initialise_connection_matrix_slices(column_start_list)
        else:
            self.t_plus_to_a, self.d_to_t_minus, self.l_to_r, self.a_to_d = \
                initialise_connection_matrix_slices(column_start_list)

        # if the object can split, add the columns needed to keep track of splitting
        if can_split:
            if not self.connections_given:
                self.a_l_to_rx2 = ConnectionMatrixSlice(int(np.sum(column_lengths)),
                                                        int(np.sum(column_lengths) + m * kg(n - 1)))
                # handle split nodes implicitly: from every split node the same number of edges go in and out (2 each)
                # -to do not need any rows to handle the nodes
                # split nodes: n(n+1)/2 (one for every pair of R), but implicitly: not needed
                row_lengths.append(0)
                # split edges: connect every L node (m*) to every pair of R nodes n(n+1)/2
                column_lengths.append(m * kg(n - 1))
                # nested iteration object for naming the vertices
                nested_it = itertools.product(range(m), list(itertools.combinations(range(n), 2)))
                column_names += ["A and " + str(frame_numbers[0] + item[0] * 0.001) + " to " +
                                 str(frame_numbers[1] + item[1][0] * 0.001) + " and " +
                                 str(frame_numbers[1] + item[1][1] * 0.001)
                                 for item in nested_it]
                # set the in and out connections for split transitions
                self.in_1 = self.in_1 + [frame_numbers[0] + item[0] * 0.001 for item in nested_it]
                self.out_1 = self.out_1 + [frame_numbers[1] + item[1][0] * 0.001 for item in nested_it]
                self.out_2 = self.out_2 + [frame_numbers[1] + item[1][1] * 0.001 for item in nested_it]
                # careful the len(list(nested_it)) consumes the iterator, so this has to be done last
                if self.can_merge or self.can_disappear:
                    list_for_merge_or_disappear: list = [-3] * len(list(nested_it))
                    self.in_2 = self.in_2 + list_for_merge_or_disappear

            else:
                self.split_connections = connections_to_examine[1]
                self.a_l_to_rx2 = ConnectionMatrixSlice(int(np.sum(column_lengths)), int(np.sum(column_lengths) +
                                                                                         len(self.split_connections)))
                # handle split nodes implicitly: from every split node the same number of edges go in and out (2 each)
                # -to do not need any rows to handle the nodes
                row_lengths.append(0)
                # the number of columns is defined by the number of entries in the connections tuples list
                column_lengths.append(len(self.split_connections))
                # the names can be directly derived from the connections tuples list
                column_names += ["A and " + str(frame_numbers[0] + 0.001 * item[0]) + " to " +
                                 str(frame_numbers[1] + 0.001 * item[1]) + " and " +
                                 str(frame_numbers[1] + 0.001 * item[2])
                                 for item in self.split_connections]
                # set the in and out connections for split transitions
                self.in_1 = self.in_1 + [frame_numbers[0] + 0.001 * item[0] for item in self.split_connections]
                self.out_1 = self.out_1 + [frame_numbers[1] + 0.001 * item[1] for item in self.split_connections]
                self.out_2 = self.out_2 + [frame_numbers[1] + 0.001 * item[2] for item in self.split_connections]
                if self.can_merge or self.can_disappear:
                    list_for_merge_or_disappear: list = [-3] * len(self.split_connections)
                    self.in_2 = self.in_2 + list_for_merge_or_disappear

        if can_merge:
            if not self.connections_given:
                self.lx2_to_d_r = ConnectionMatrixSlice(int(np.sum(column_lengths)),
                                                        int(np.sum(column_lengths) + n * kg(m - 1)))
                # handle merge nodes implicitly: from every merge node the same number of edges go in and out (2 each) 
                # -to do not need any rows to handle the nodes
                # merge nodes: m(m+1)/2 (one for every pair of L), but implicitly: not needed
                row_lengths.append(0)
                # merge edges: connect every pair of L nodes n(n+1)/2 to every R node (n*)
                column_lengths.append(n * kg(m - 1))
                # nested iteration object for naming the vertices
                nested_it = itertools.product(range(n), list(itertools.combinations(range(m), 2)))
                column_names += [str(frame_numbers[0] + 0.001 * item[1][0]) + " and " +
                                 str(frame_numbers[0] + 0.001 * item[1][1]) + " to D and " +
                                 str(frame_numbers[1] + 0.001 * item[0])
                                 for item in nested_it]
                # set the in and out connections for merge transitions
                self.in_1 = self.in_1 + [frame_numbers[0] + 0.001 * item[1][0] for item in nested_it]
                self.out_1 = self.out_1 + [frame_numbers[1] + 0.001 * item[0] for item in nested_it]
                self.in_2 = self.in_2 + [frame_numbers[0] + 0.001 * item[1][1] for item in nested_it]
                # careful the len(list(nested_it)) consumes the iterator, so this has to be done last
                if self.can_split or self.can_appear:
                    list_for_split_or_appear: list = [-4] * len(list(nested_it))
                    self.out_2 = self.out_2 + list_for_split_or_appear
            else:
                self.merge_connections = connections_to_examine[2]
                self.lx2_to_d_r = ConnectionMatrixSlice(int(np.sum(column_lengths)),
                                                        int(np.sum(column_lengths) + len(self.merge_connections)))
                # handle merge nodes implicitly: from every merge node the same number of edges go in and out (2 each) 
                # -to do not need any rows to handle the nodes
                row_lengths.append(0)
                # the number of columns is defined by the number of entries in the connections tuples list
                column_lengths.append(len(self.merge_connections))
                # the names can be directly derived from the connections tuples list
                column_names += [str(frame_numbers[0] + 0.001 * item[0]) + " and " +
                                 str(frame_numbers[0] + 0.001 * item[1]) + " to D and " +
                                 str(frame_numbers[1] + 0.001 * item[2])
                                 for item in self.merge_connections]
                # set the in and out connections for merge transitions
                self.in_1 = self.in_1 + [frame_numbers[0] + 0.001 * item[0] for item in self.merge_connections]
                self.out_1 = self.out_1 + [frame_numbers[1] + 0.001 * item[2] for item in self.merge_connections]
                self.in_2 = self.in_2 + [frame_numbers[0] + 0.001 * item[1] for item in self.merge_connections]
                if self.can_split or self.can_appear:
                    list_for_split_or_appear: list = [-4] * len(self.merge_connections)
                    self.out_2 = self.out_2 + list_for_split_or_appear

        if can_appear:
            self.a_to_r = ConnectionMatrixSlice(int(np.sum(column_lengths)), int(np.sum(column_lengths) + n))
            column_lengths.append(n)
            column_names += ["A to " + str(frame_numbers[1] + 0.001 * i) for i in range(n)]
            # set the in and out connections for appear transitions
            self.in_1 = self.in_1 + [-3] * n
            self.out_1 = self.out_1 + list(range(n))
            if self.can_merge or self.can_disappear:
                self.in_2 = self.in_2 + [None] * n
            if self.can_split or self.can_appear:
                self.out_2 = self.out_2 + [None] * n

        if can_disappear:
            self.l_to_d = ConnectionMatrixSlice(int(np.sum(column_lengths)), int(np.sum(column_lengths) + m))
            column_lengths.append(m)
            column_names += [str(frame_numbers[0] + 0.001 * i) + " to D" for i in range(m)]
            # set the in and out connections for disappear transitions
            self.in_1 = self.in_1 + [frame_numbers[0] + 0.001 * i for i in range(m)]
            self.out_1 = self.out_1 + [-4] * m
            if self.can_merge or self.can_disappear:
                self.in_2 = self.in_2 + [None] * m
            if self.can_split or self.can_appear:
                self.out_2 = self.out_2 + [None] * m

        # make the self.in_1 and self.out_1 (and if given, self_in_2 and self.out_2)
        self.in_1 = np.array(self.in_1, dtype=np.float)  # using np.float, as there is no integer nan/None
        self.out_1 = np.array(self.out_1, dtype=np.float)  # using np.float, as there is no integer nan/None

        if self.can_merge or self.can_disappear:
            self.in_2 = np.array(self.in_2, dtype=np.float)  # using np.float, as there is no integer nan/None
        if self.can_split or self.can_appear:
            self.out_2 = np.array(self.out_2, dtype=np.float)  # using np.float, as there is no integer nan/None

        # calculate the number of rows and columns
        self.row_len = int(np.sum(row_lengths))
        self.col_len = int(np.sum(column_lengths))

        # store the column and row names
        self.row_names = row_names
        self.column_names = column_names

        # initialise the actual connection matrix
        self.matrix = np.zeros(shape=(self.row_len, self.col_len), dtype=np.int64)

        """ create edges between the nodes, first: all edges that always exist """
        # TP to Li, TP to A, Ri to TM, D to TM, Li to Ri

        # create 1 edge from TP to all Li nodes with |1| if this is the first ConnectionMatrix in a series
        if self.is_first_connection_matrix:
            # out of TP
            self.matrix[self.t_plus.slice, self.t_plus_to_l.slice] = - np.ones(shape=(1, m), dtype=np.int64)
            self.matrix[self.l.slice, self.t_plus_to_l.slice] = + np.identity(m, dtype=np.int64)  # into L

        # create 1 edge from TP to A (appear) with |N|
        self.matrix[self.t_plus.slice, self.t_plus_to_a.slice] = - n  # out of TP
        self.matrix[self.a.slice, self.t_plus_to_a.slice] = + n  # into A

        # create 1 edge from all Ri nodes to TM with |1| if this is the last ConnectionMatrix in a series
        if self.is_last_connection_matrix:
            self.matrix[self.r.slice, self.r_to_t_minus.slice] = - np.identity(n, dtype=np.int64)  # out of Ri
            # into TM
            self.matrix[self.t_minus.slice, self.r_to_t_minus.slice] = + np.ones(shape=(1, n), dtype=np.int64)

        # create 1 edge from D (disappear) to TM with |M|
        self.matrix[self.d.slice, self.d_to_t_minus.slice] = - m  # out of D
        self.matrix[self.t_minus.slice, self.d_to_t_minus.slice] = + m  # into TM

        if not self.connections_given:
            # connect all Li nodes to all Ri nodes with |1|
            for i in range(m):  # for every Li
                self.matrix[self.l.start + i, self.l_to_r.start + i * n:self.l_to_r.start + (i + 1) * n] = - np.ones(
                    shape=(1, n), dtype=np.int64)  # out of Li
                self.matrix[self.r.slice, self.l_to_r.start + i * n:self.l_to_r.start + (i + 1) * n] \
                    = + np.identity(n, dtype=np.int64)  # into R
        else:
            # only connect the Li nodes to the Ri nodes specified in the list
            lsl = len(self.l_to_r_connections[0])  # length of a sublist
            for i, con_list_i in enumerate(self.l_to_r_connections):
                self.matrix[self.l.start + i, self.l_to_r.start + i * lsl:self.l_to_r.start + (i + 1) * lsl] = \
                    - np.ones(shape=(1, lsl), dtype=np.int64)  # out of Li
                for j, con in enumerate(con_list_i):
                    self.matrix[self.r.start + con, self.l_to_r.start + i * lsl + j] = 1  # into R

        # create 1 edge from A (appear) to D (disappear) with |M|
        self.matrix[self.a.slice, self.a_to_d.slice] = - m
        self.matrix[self.d.slice, self.a_to_d.slice] = + m

        # if the objects can split:
        # connect each (implicit) split node to one L node and the A (appear) node as input
        # connect it coupled to two different R nodes
        if can_split:
            if not self.connections_given:
                all_combinations_in_matrix_form_split = get_all_combinations_between_two_values_in_matrix_form(n)
                self.matrix[self.a.slice, self.a_l_to_rx2.slice] = - np.ones(shape=(1, m * kg(n - 1)),
                                                                             dtype=np.int64)  # out of A
                for i in range(m):
                    val_row = self.l.start + i
                    start_col = self.a_l_to_rx2.start + i * kg(n - 1)
                    end_col = start_col + kg(n - 1)
                    self.matrix[val_row, start_col:end_col] = - np.ones(shape=(1, kg(n - 1)),
                                                                        dtype=np.int64)  # out of L
                    self.matrix[self.r.slice, start_col:end_col] = \
                        + all_combinations_in_matrix_form_split  # into two Rs
            # loop over all [i,j,k] tuples in the split_connections list (Li -to Rj + Rk) and create the connections
            else:
                self.matrix[self.a.slice, self.a_l_to_rx2.slice] = - np.ones(shape=(1, len(self.split_connections)),
                                                                             dtype=np.int64)  # out of A
                for con_index, con in enumerate(self.split_connections):
                    val_col = self.a_l_to_rx2.start + con_index
                    self.matrix[self.l.start + con[0], val_col] = - 1  # out of Li
                    self.matrix[self.r.start + con[1], val_col] = + 1  # into two Rj
                    self.matrix[self.r.start + con[2], val_col] = + 1  # into two Rk

        # if the objects can merge:
        # connect each (implicit) merge node coupled to two different L nodes as input
        # connect it to one R node and the D (disappear) as output
        if can_merge:
            if not self.connections_given:
                all_combinations_in_matrix_form_merge = get_all_combinations_between_two_values_in_matrix_form(m)
                self.matrix[self.d.slice, self.lx2_to_d_r.slice] = + np.ones(shape=(1, n * kg(m - 1)),
                                                                             dtype=np.int64)  # into D
                for i in range(n):
                    val_row = self.r.start + i
                    start_col = self.lx2_to_d_r.start + i * kg(m - 1)
                    end_col = start_col + kg(m - 1)
                    self.matrix[val_row, start_col:end_col] = + np.ones(shape=(1, kg(m - 1)), dtype=np.int64)  # into R
                    self.matrix[self.l.slice, start_col:end_col] = \
                        - all_combinations_in_matrix_form_merge  # out of two Ls
            # loop over all [i,j,k] tuples in the merge_connections list (Li + Lj -to Rk) and create the connections
            else:
                self.matrix[self.d.slice, self.lx2_to_d_r.slice] = + np.ones(shape=(1, len(self.merge_connections)),
                                                                             dtype=np.int64)  # into D
                for con_index, con in enumerate(self.merge_connections):
                    val_col = self.lx2_to_d_r.start + con_index
                    self.matrix[self.l.start + con[0], val_col] = - 1  # out of Li
                    self.matrix[self.l.start + con[1], val_col] = - 1  # out of Lj
                    self.matrix[self.r.start + con[2], val_col] = + 1  # into Rk

        # if the objects can appear:
        # connect the (A) appear node to each R node
        if can_appear:
            self.matrix[self.a.slice, self.a_to_r.slice] = - np.ones(shape=(1, n), dtype=np.int64)  # out of A
            self.matrix[self.r.slice, self.a_to_r.slice] = + np.identity(n, dtype=np.int64)  # into R

        # if the objects can disappear:
        # connect each L node to the (D) disappear node
        if can_appear:
            self.matrix[self.l.slice, self.l_to_d.slice] = - np.identity(m, dtype=np.int64)  # out of L
            self.matrix[self.d.slice, self.l_to_d.slice] = + np.ones(shape=(1, m), dtype=np.int64)  # into D

        # define the corresponding sum of flow vector b of size number of nodes x 1 that is zero,
        # except for the source (-M-N) and the target vector (M+N)
        self.b = np.zeros(int(np.sum(row_lengths)), dtype=np.int64)
        self.tot_flow = m + n
        self.b[0] = - self.tot_flow  # the flow out of T+ is - tot_flow
        self.b[1] = + self.tot_flow  # the flow into T+ is + tot_flow
