import numpy as np
import itertools


# find local maxima in 3d array and return their coordinates
# inputs: array: a 3d numpy array in which local maxima are to be found
#         mask: a mask of the same shape as the array indicating the regions of interest for local maxima
# outputs: list_of_local_max_points: a list containing points of local maxima
def find_local_max_3d(array: np.array, mask: np.array, get_edge_maxima: bool = True, dist_to_nn: str = 'sqrt 2',
                      number_of_max: int = 100):
    # make an array where the borders are filled with twos, and the corners with 4s, so edge maxima are also included,
    # if edge maxima should be included
    if get_edge_maxima:
        local_max_array = np.ones(shape=array.shape, dtype=int) * 2
        local_max_array[1:-1, 1:-1, 1:-1] = 0
        for corner_combination in itertools.product([0, -1], repeat=3):
            local_max_array[corner_combination] = 4
    # if edge maxima are not included, get an array with zeros only
    else:
        local_max_array = np.zeros(shape=array.shape, dtype=int)

    # compare the values of each point in the array to the values of the pixels around it and
    # add 1 to the local max array, if the value at the centre pixel is indeed greater
    if dist_to_nn == '1':
        offsets = [[1, 0, 0], [0, 1, 0], [0, 0, 1], [-1, 0, 0], [0, -1, 0], [0, 0, -1]]
    elif dist_to_nn == 'sqrt 2':
        offsets = np.array(np.meshgrid([0, 1, -1], [0, 1, -1], [0, 1, -1])).T.reshape(-1, 3)
    else:
        raise ValueError("The dist_to_nn_variable must be either '1' or 'sqrt two' for considering " \
                         "6 / 26 nearest neighbours in 3D")

    # get the start and stop slicing indices for a shift delta 
    def get_st_sp(delta):
        if delta > 0:
            start_a = delta
            stop_a = None
            start_s = 0
            stop_s = -delta
        elif delta < 0:
            start_a = 0
            stop_a = delta
            start_s = -delta
            stop_s = None
        else:
            start_a, start_s = 0, 0
            stop_a, stop_s = None, None

        return start_a, stop_a, start_s, stop_s

    for dx, dy, dz in offsets:
        x_st_a, x_sp_a, x_st_s, x_sp_s = get_st_sp(dx)
        y_st_a, y_sp_a, y_st_s, y_sp_s = get_st_sp(dy)
        z_st_a, z_sp_a, z_st_s, z_sp_s = get_st_sp(dz)

        local_max_array[x_st_a:x_sp_a, y_st_a:y_sp_a, z_st_a:z_sp_a] += \
            (array[x_st_a:x_sp_a, y_st_a:y_sp_a, z_st_a:z_sp_a] >=
             array[x_st_s:x_sp_s, y_st_s:y_sp_s, z_st_s:z_sp_s]).astype(int) + \
            (array[x_st_a:x_sp_a, y_st_a:y_sp_a, z_st_a:z_sp_a] >
             array[x_st_s:x_sp_s, y_st_s:y_sp_s, z_st_s:z_sp_s]).astype(int)

    # make sure none of the local maxima are in the masked area
    local_max_array[~np.array(mask, dtype=bool)] = 0
    # mark all points as local maxima that are greater than all of their nearest neighbours
    if dist_to_nn == '1':
        local_max_booleans = local_max_array > 11

    elif dist_to_nn == 'sqrt 2':
        local_max_booleans = local_max_array > 51
    else:
        raise ValueError("The dist_to_nn_variable must be either '1' or 'sqrt two' for considering "
                         "6 / 26 nearest neighbours in 3D")

    local_max_points = np.nonzero(local_max_booleans)

    print(len(local_max_points[0]))

    # only return the 'number_of_max' points with the maximum intensity
    if len(local_max_points[0]) > number_of_max:
        print("getting rid of some local max points")
        intensities = array[local_max_points]
        arg_sort_max = np.argsort(intensities)
        local_max_points = (np.transpose(local_max_points)[arg_sort_max][-number_of_max:])[::-1]
    else:
        local_max_points = np.column_stack(local_max_points)[::-1]

    return local_max_points


if __name__ == "__main__":
    # create a random sample array of "size"^3
    size = 10
    array_1 = np.random.rand(size, size, size)
    # create a sample mask, that is zero in the row at x=0, y=0
    mask_1 = np.ones(shape=(size, size, size), dtype=bool)
    mask_1[0, 0, :] = 0

    # find the local maxima in the sample array
    local_max_pts = find_local_max_3d(array_1, mask_1, dist_to_nn='sqrt 2')
    local_max_pts_six_nn = find_local_max_3d(array_1, mask_1, dist_to_nn='1')

    # display the sample array and the local max
    import napari

    with napari.gui_qt():
        viewer = napari.Viewer()
        viewer.dims.ndisplay = 3  # open the viewer in the 3D view
        # noinspection PyTypeChecker
        viewer.add_image(array_1,
                         colormap='magenta',
                         opacity=0.6)
        viewer.add_points(local_max_pts,
                          size=1)
        viewer.add_points(local_max_pts_six_nn,
                          size=1,
                          face_color='red')
