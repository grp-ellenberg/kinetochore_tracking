# read in the data from a configuration file as specified for Judith's data

import shutil
import numpy as np
import pandas as pd
import read_in_new_skimage as ri
import sys
import skimage.measure


# a class to store all parameters
class Parameters:
    pass


# function to choose a file from a dictionary using a graphical user interface
def gui_file_name(directory=None):
    from qtpy import QtWidgets
    import sys
    # create a window (Qt Application)
    _ = QtWidgets.QApplication(sys.argv)
    # select a file via a dialog and return the file name
    if directory is None:
        directory = './'

    file_name = QtWidgets.QFileDialog.getOpenFileName(None, "Select data file...",
                                                      directory, filter="All files (*);; SM Files (*.sm)")
    # return the file name
    return file_name[0]


def read_in_config_file(config_file):
    print("The config file is: " + str(config_file))

    # create one object where all parameters can be stored
    p = Parameters()

    # save the standard jupyter lab output as the standard output
    p.standard_output_jupyter = sys.stdout

    # create a dictionary from the configuration file
    config_dict = ri.read_in_config(config_file)

    ####################################################################################################
    #                               read the config file                                               #
    ####################################################################################################

    # get all parameters where data is stored/ to be stored

    # create a dict of data parameters only
    p.data_parameters = config_dict["data parameters"]

    # get the data folder, where the input data is stored, and the analysis folder, 
    # where the output data is stored
    p.analysis_folder = p.data_parameters["analysis folder"]

    # copy the configurations file to the save folder
    shutil.copy2(config_file, p.analysis_folder + 'config_file_workflow.yml')

    p.data_folder = p.data_parameters["data folder"]

    # get the file name conventions
    p.excel_filename = p.data_parameters["excel filename"]
    p.excel_tracking_column_x_pos = p.data_parameters["excel tracking column name x"]
    p.excel_tracking_column_y_pos = p.data_parameters["excel tracking column name y"]
    p.excel_tracking_column_z_pos = p.data_parameters["excel tracking column name z"]
    p.metadata_filename = p.data_parameters["metadata filename"]
    p.microscopy_data_filename_pre_index = p.data_parameters["microscopy data filename pre index"]
    p.microscopy_data_filename_post_index = p.data_parameters["microscopy data filename post index"]
    p.microscopy_data_file_extension = p.data_parameters["microscopy data file extension"]
    p.log_file_registration = p.data_parameters["log file registration"]
    p.log_file_exclusion_sets = p.data_parameters["log file exclusion sets"]
    p.log_file_min_cost_flow = p.data_parameters["log file minimum cost flow"]

    # get all metadata parameters and read in the metadata

    # create a dict of metadata parameters only
    p.metadata_parameters = config_dict["metadata parameters"]

    # get the physical size of one pixel (resolution) in x,y and z
    p.phys_size_x = p.metadata_parameters["phys_size_x"]
    p.phys_size_y = p.metadata_parameters["phys_size_y"]
    p.phys_size_z = p.metadata_parameters["phys_size_z"]

    # get the unit of the resolution in x,y and z
    p.size_unit_x = p.metadata_parameters["size_unit_x"]
    p.size_unit_y = p.metadata_parameters["size_unit_y"]
    p.size_unit_z = p.metadata_parameters["size_unit_z"]

    # read in the metadata
    p.metadata_path = p.data_folder + p.metadata_filename + ".ome.tiff"

    # get the ROI parameters and define the ROI
    # create a dictionary for all region of interest related parameters
    p.roi_parameters = config_dict["roi parameters"]
    # get the offset, a parameter by which the ROI can easily manipulated (made > or <)
    p.roi_offset = p.roi_parameters["roi offset"]

    # get all time_points to be examined
    p.time_points = p.roi_parameters["time points"]

    ####################################################################################################
    #                                read in and rearrange this data                                   #
    ####################################################################################################
    # extract the metadata using a self-written function (returns a dictionary)
    p.metadata = ri.get_metadata_ometiff(p.metadata_path)
    p.metadata["phys_size_x"] = p.phys_size_x
    p.metadata["phys_size_y"] = p.phys_size_y
    p.metadata["phys_size_z"] = p.phys_size_z
    p.metadata["x_size_unit"] = p.size_unit_x
    p.metadata["y_size_unit"] = p.size_unit_y
    p.metadata["z_size_unit"] = p.size_unit_z
    # get the ROI from the parameters read in above:
    # get the minima and maxima of the bounding box of the region of interest in x,y and z
    p.roi_xmin, p.roi_xmax = p.roi_parameters["roi xmin"] - p.roi_offset, p.roi_parameters["roi xmax"] + p.roi_offset
    p.roi_ymin, p.roi_ymax = p.roi_parameters["roi ymin"] - p.roi_offset, p.roi_parameters["roi ymax"] + p.roi_offset
    p.roi_zmin, p.roi_zmax = p.roi_parameters["roi zmin"] - p.roi_offset, p.roi_parameters["roi zmax"] + p.roi_offset
    # define the ROI as a np slice
    p.roi = np.s_[p.roi_xmin:p.roi_xmax,
            p.roi_ymin:p.roi_ymax,
            p.roi_zmin:p.roi_zmax]

    # read in the tracking from the Excel, if an Excel filename was given
    if p.excel_filename is not None:
        # create pandas data frame from Excel-Data
        p.tracking_excel_path = p.data_folder + p.excel_filename
        p.tracking_data_df = pd.read_excel(p.tracking_excel_path)

        # get minimum and maximum tracking frame
        p.min_tracking_frame = int(p.tracking_data_df["Frame"].min(skipna=True))
        print("Excel: Tracking starts in frame " + str(p.min_tracking_frame))
        p.max_tracking_frame = int(p.tracking_data_df["Frame"].max(skipna=True))
        print("Excel: Tracking ends in frame " + str(p.max_tracking_frame))

        # get positions from data, their fra and the track number
        p.frames = p.tracking_data_df["Frame"].to_numpy()
        p.track_indices = p.tracking_data_df["Track"].to_numpy()
        p.pair_indices = p.tracking_data_df["Pair"].to_numpy()
        # take into account,that only the ROI will be processed and labelling starts with 0 in python
        p.x_COM = p.tracking_data_df[p.excel_tracking_column_x_pos] - p.roi_xmin
        p.y_COM = p.tracking_data_df[p.excel_tracking_column_y_pos] - p.roi_ymin
        p.z_COM = p.tracking_data_df[p.excel_tracking_column_z_pos] - p.roi_zmin - 1
        p.x_COM, p.y_COM, p.z_COM = p.x_COM.to_numpy(), p.y_COM.to_numpy(), p.z_COM.to_numpy()
    else:
        p.x_COM, p.y_COM, p.z_COM = None, None, None

    # read in the model and get its ROI:
    p.model_parameters = config_dict["model parameters"]
    # get the data of the kinetochore model, if any
    p.model_data_file = p.model_parameters["model data"]
    p.model_middle_file = p.model_parameters["middle of model"]
    p.model_upsampling_factor = p.model_parameters["model upsampling factor"]
    # read in the model from the .npy file, if a filename was given
    if p.model_data_file is not None:
        # read in the actual model
        p.model_array = np.load(p.analysis_folder + p.model_data_file)
        # read in the array containing the middle of the model
        p.model_middle = np.load(p.analysis_folder + p.model_middle_file)
    else:
        p.model_array, p.model_middle = None, None
    # get the parameters of the ROI of the model
    p.model_roi = p.model_parameters["model roi"]
    p.model_roi_xmin, p.model_roi_xmax = p.model_roi["roi xmin"], p.model_roi["roi xmax"]
    p.model_roi_ymin, p.model_roi_ymax = p.model_roi["roi ymin"], p.model_roi["roi ymax"]
    p.model_roi_zmin, p.model_roi_zmax = p.model_roi["roi zmin"], p.model_roi["roi zmax"]

    # prepare the model image
    # crop it to the region of interest
    p.model_roi = p.model_array[p.model_roi_xmin:p.model_roi_xmax,
                  p.model_roi_ymin:p.model_roi_ymax,
                  p.model_roi_zmin:p.model_roi_zmax]
    # calculate the middle of the ROI from the middle of the model
    p.model_middle_roi = p.model_middle - [p.model_roi_xmin, p.model_roi_ymin, p.model_roi_zmin]
    # downsample the model such that the image resolution and the model resolution are the same
    p.model_roi_downsampled = skimage.measure.block_reduce(p.model_roi, (p.model_upsampling_factor,
                                                                         p.model_upsampling_factor,
                                                                         p.model_upsampling_factor),
                                                           np.mean)
    # calculate the middle of the downsampled model (sub-integer precision)
    p.model_middle_downsampled = p.model_middle_roi / p.model_upsampling_factor

    # read in the constraints for moving, splitting and merging
    # get the tracking parameters
    p.tracking_parameters = config_dict["tracking parameters"]

    # read in the number of points that were expected
    p.number_expected = p.tracking_parameters["number expected"]

    # read in the frame number of the anaphase onset
    p.anaphase_onset = p.tracking_parameters["anaphase onset"]

    # read in the constraints for moving, splitting and merging (the n nearest neighbours that will be considered
    # to be possible candidates)
    p.constr_moving = p.tracking_parameters["constraint moving"]
    p.constr_split_merge = p.tracking_parameters["constraint split or merge"]

    # read in the penalty for (dis)appear, split and merge events
    p.dis_appear_penalty = p.tracking_parameters["disappear penalty"]
    p.split_merge_penalty = p.tracking_parameters["split merge penalty"]

    # read in the weights for the various distance measures
    p.distance_measure_weights = [p.tracking_parameters["w_eu_dist"],
                                  p.tracking_parameters["w_int_diff"],
                                  p.tracking_parameters["w_size_diff"],
                                  p.tracking_parameters["w_voronoy_star"],
                                  p.tracking_parameters["w_reverse_size"],
                                  p.tracking_parameters["w_parallel_movement"]]

    # read in, whether the distance measures are used for transitions
    p.use_for_transition = [p.tracking_parameters["eu_dist_trans"],
                            p.tracking_parameters["int_diff_trans"],
                            p.tracking_parameters["size_diff_trans"],
                            p.tracking_parameters["voronoy_star_trans"],
                            p.tracking_parameters["reverse_size_trans"],
                            p.tracking_parameters["parallel_movement_trans"]]

    # read in, whether the distance measures are used for splitting/merging
    p.use_for_split_merge = [p.tracking_parameters["eu_dist_split_merge"],
                             p.tracking_parameters["int_diff_split_merge"],
                             p.tracking_parameters["size_diff_split_merge"],
                             p.tracking_parameters["voronoy_star_split_merge"],
                             p.tracking_parameters["reverse_size_split_merge"],
                             p.tracking_parameters["parallel_movement_split_merge"]]

    # read in, whether the distance measures are used for (dis)appear events
    p.use_for_dis_appear = [p.tracking_parameters["eu_dist_dis_appear"],
                            p.tracking_parameters["int_diff_dis_appear"],
                            p.tracking_parameters["size_diff_dis_appear"],
                            p.tracking_parameters["voronoy_star_dis_appear"],
                            p.tracking_parameters["reverse_size_dis_appear"],
                            p.tracking_parameters["parallel_movement_dis_appear"]]

    p.only_use_before_anaphase_onset = [p.tracking_parameters["eu_dist_only_use_before_anaphase_onset"],
                                        p.tracking_parameters["int_diff_only_use_before_anaphase_onset"],
                                        p.tracking_parameters["size_diff_only_use_before_anaphase_onset"],
                                        p.tracking_parameters["voronoy_star_only_use_before_anaphase_onset"],
                                        p.tracking_parameters["reverse_size_only_use_before_anaphase_onset"],
                                        p.tracking_parameters["parallel_movement_only_use_before_anaphase_onset"]]

    p.can_split = p.tracking_parameters["can_split"]
    p.can_merge = p.tracking_parameters["can_merge"]
    p.can_appear = p.tracking_parameters["can_appear"]
    p.can_disappear = p.tracking_parameters["can_disappear"]

    return p
