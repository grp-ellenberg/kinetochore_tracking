import point_functions as pf
import numpy as np
import SimpleITK as sitk
import numpy.linalg
import pandas as pd
import skimage.measure
import matplotlib.pyplot as plt


# create a marker image from a list or a np.array of points,
# inputs: points: list/np.array of points that should be used as markers
#         image: an image which has the same shape as the desired output
# output: marker_img: an image of the same size and the input image, that
#                     has integer value labels at every of the input points
def get_marker_img_from_list_of_points(points, image):
    # create an image of the desired shape
    marker_img = np.zeros(shape=image.shape, dtype=int)
    # make sure all points are given in the integer format (convert them)
    points = points.astype(int)
    # set the value at each point to # of point + 1
    for i, point in enumerate(points):
        try:
            marker_img[point[0], point[1], point[2]] = i + 1
        except IndexError:
            print(points)
            print(marker_img.shape)
            print(point[0], point[1], point[2])
    return marker_img


# get a watershed from marker points, possibly within a mask that is smaller than some threshold
# inputs: height_image: np.array, the image on which the watershed should be performed 
#                       (convention: that the marker points should be local maxima)
#         points: np.array/list containing the points at which the watershed should be started
#         threshold: int/float of a value
# outputs: watershed_image: np.array, label image resulting from the watershed of the input
#          image seeded at the points given in the input, masked such that only values that 
#          are greater than the threshold in the input image are considered
def get_watershed_from_image_within_mask(height_image, points, threshold):
    # create a marker image from the list/array of points
    marker_image = get_marker_img_from_list_of_points(points, height_image)

    # convert the height image and the marker image to the SITK format
    sitk_height_image = sitk.GetImageFromArray(-height_image)
    sitk_marker_image = sitk.GetImageFromArray(marker_image)

    # perform a watershed on the image using the SimpleITK library
    watershed = sitk.MorphologicalWatershedFromMarkers(sitk_height_image, sitk_marker_image)

    # mask the image, such that only pixels in the height image greater
    # than the threshold are considered
    thresh_img = sitk_height_image < threshold
    # make sure the markers are always marked as "True"
    # thresh_img = np.logical_or(thresh_img, marker_image)
    # create a label image from the watershed
    watershed = sitk.Mask(watershed, sitk.Cast(thresh_img, watershed.GetPixelID()))

    # return the watershed label image in a np.array format
    watershed_image = sitk.GetArrayFromImage(watershed)

    print("maximum of watershed label image")
    print(np.max(watershed_image))
    print("unique sorted watershed image values")
    print(np.sort(np.unique(watershed_image)))

    return watershed_image


# get statistics on the intensity image and label image
# inputs: label_img: np.array, the label image to be examined
#         intensity_img: np.array, the intensity image from which the label image was created
# outputs: stats_list: list of tuples with different properties (# pixels, elongation, flatness, perimeter, min and 
#                      max oriented bounding box size, roundness, intensity mean and intensity std) for each object
#                      in the label image
#          stats_names: the names of the properties in each tuple
def calculate_statistics_from_label_and_intensity_image(label_img, intensity_img):
    # convert the label image and the intensity image to the SITK format
    sitk_label_img = sitk.GetImageFromArray(label_img)
    sitk_intensity_img = sitk.GetImageFromArray(intensity_img)

    # use the label shape statistics filter on the label image to get an object from which
    # some parameters can be extracted easily
    shape_stats = sitk.LabelShapeStatisticsImageFilter()
    shape_stats.ComputeOrientedBoundingBoxOn()
    shape_stats.Execute(sitk_label_img)

    # use the intensity statistics image filter to get an object from which further 
    # parameters can be extracted
    intensity_stats = sitk.LabelIntensityStatisticsImageFilter()
    intensity_stats.Execute(sitk_label_img, sitk_intensity_img)

    # extract statistics: 
    # from the shape stats object: the number of pixels, the elongation and flatness of the objects,
    # the max and min bounding box size and roundness 
    # from the intensity stats object: The mean and std of the intensity

    # more parameters can be found in the docs: 
    # Shape Statistics:
    # https://simpleitk.org/doxygen/latest/html/classitk_1_1simple_1_1LabelShapeStatisticsImageFilter.html
    # Intensity Statistic:
    # https://simpleitk.org/doxygen/latest/html/classitk_1_1simple_1_1LabelIntensityStatisticsImageFilter.html

    no_pixels = [shape_stats.GetNumberOfPixels(i) for i in shape_stats.GetLabels()]
    elongation = [shape_stats.GetElongation(i) for i in shape_stats.GetLabels()]
    flatness = [shape_stats.GetFlatness(i) for i in shape_stats.GetLabels()]
    perimeter = [shape_stats.GetPerimeter(i) for i in shape_stats.GetLabels()]
    min_bbox_size = [shape_stats.GetOrientedBoundingBoxSize(i)[0] for i in shape_stats.GetLabels()]
    max_bbox_size = [shape_stats.GetOrientedBoundingBoxSize(i)[2] for i in shape_stats.GetLabels()]
    roundness = [shape_stats.GetRoundness(i) for i in shape_stats.GetLabels()]
    intensity_mean = [intensity_stats.GetMean(i) for i in shape_stats.GetLabels()]
    intensity_std = [intensity_stats.GetStandardDeviation(i) for i in shape_stats.GetLabels()]

    # create a tuple for every object with all of these statistics
    stats_list = [(no_pixels[i],
                   elongation[i],
                   flatness[i],
                   perimeter[i],
                   min_bbox_size[i],
                   max_bbox_size[i],
                   roundness[i],
                   intensity_mean[i],
                   intensity_std[i]) for i in range(len(no_pixels))]

    # name the properties extracted above for the dataframe (and plotting, if display=True)
    stats_names = ["No of Pixels",
                   "Elongation",
                   "Flatness",
                   "Perimeter",
                   "Oriented Bounding Box Minimum Size",
                   "Oriented Bounding Box Maximum Size",
                   "Roundness",
                   "Intensity Mean",
                   "Intensity Standard Deviation"]

    stats_labels = shape_stats.GetLabels()

    return (stats_list, stats_names, stats_labels,
            no_pixels, elongation, flatness, perimeter, min_bbox_size, max_bbox_size, roundness,
            intensity_mean, intensity_std)


# analyse the labelled objects in a label image
# inputs: label_img: np.array, the label image to be examined
#         intensity_img: np.array, the intensity image from which the label image was created
#         display (optional): bool, whether or not plots on the statistics should be created
#         frame_number (optional): integer used for storing the plot, if None, the plot is not saved
#         analysis_folder (optional): folder in which the plot is saved
# outputs: stats_df: a pandas dataframe containing the statistics calculated from the label image
#          principal_axes: a list of the principal axes of the objects
# using code from: (03.05.20)
# http://insightsoftwareconsortium.github.io/SimpleITK-Notebooks/Python_html/35_Segmentation_Shape_Analysis.html

def analyse_label_img(label_img, intensity_img, display=False, frame_no=None, analysis_folder=None,
                      return_sizes_from_watershed=False):
    stats_list, stats_names, stats_labels, sizes_from_watershed = \
        calculate_statistics_from_label_and_intensity_image(label_img, intensity_img)[0:4]

    # Create the pandas data frame of the statistics above
    stats_df = pd.DataFrame(data=stats_list, index=stats_labels, columns=stats_names)

    # if the input 'display' is True, create a series of plots displaying the different 
    # measures defined above as a histogram and function of the size (number of pixels) of each object
    if display:

        # create subplots for all of these properties
        fig, axes = plt.subplots(nrows=len(stats_names), ncols=2, figsize=(6, 4 * len(stats_names)))
        # switch of the first plot
        axes[0, 0].axis('off')

        # plot the histogram of the size distribution
        stats_df.loc[:, stats_names[0]].plot.hist(ax=axes[0, 1], bins=25)
        axes[0, 1].set_xlabel(stats_names[0])
        axes[0, 1].xaxis.set_label_position("top")

        # for all of the other properties, plot a histogram of the distribution of 
        # that property across the labelled objects, then plot it as a function of the number of pixels
        for i in range(1, len(stats_names)):
            c = stats_names[i]
            # for each property (column) plot the histogram with 20 bins each
            bar = stats_df.loc[:, [c]].plot.hist(ax=axes[i, 0], bins=20, orientation='horizontal', legend=False)
            bar.set_ylabel(stats_df.loc[:, [c]].columns.values[0])
            # create a scatter plot of the property in that column over the size of the object
            scatter = stats_df.plot.scatter(ax=axes[i, 1], y=c, x=stats_names[0])

            # adjust the labels
            # the two plots (histogram and scatter plot) share their y-axis, so remove one label
            scatter.set_ylabel('')
            # remove axis labels from all plots except the last (they all share the labels)
            if i < len(stats_names) - 1:
                bar.set_xlabel('')
                scatter.set_xlabel('')

        # adjust the spacing between plot columns and use the 'tight-layout' format
        plt.subplots_adjust(wspace=0.4)
        plt.tight_layout()

        # if a frame number is given, save the figure
        if frame_no is not None:
            plt.savefig(analysis_folder + 'analysis_of_watershed_objects_frame_' + str(frame_no) + '.png')

        # display the plot
        plt.show()

    # if only the sizes should be returned, return the sizes, otherwise return the whole dataframe
    if return_sizes_from_watershed:
        return sizes_from_watershed
    else:
        return stats_df


# calculate the principal axes of each object in a label image
# input: label_img: np.array, label image containing the object(s) to be examined
# output: list of the principal axes of each object
def get_principal_axes_from_label_image(label_img):
    # convert the label image to the SITK format
    sitk_label_img = sitk.GetImageFromArray(label_img)

    # use the label shape statistics filter on the label image to get an object from 
    # which the principal axes can be extracted easily
    shape_stats = sitk.LabelShapeStatisticsImageFilter()
    shape_stats.ComputeOrientedBoundingBoxOn()
    shape_stats.Execute(sitk_label_img)

    # get a list of all principal axes
    labels = shape_stats.GetLabels()
    principal_axes_list = [shape_stats.GetPrincipalAxes(i) for i in labels]

    return labels, principal_axes_list


# calculate the (normalised) eigenvectors of the inertia tensor corresponding to the minimum and maximum
# eigenvalue from a label image from a region as given by one entry of the return of the the skimage.regionprops 
# function
# inputs: region: one region (indexed from regionprops[i] as given by skimage.measure.regionprops function for a 
#                 label image)
# outputs: min_eigenvec: the eigenvector corresponding to the minimum eigenvalue of the inertia tensor
#          max_eigenvec: the eigenvector corresponding to the maximum eigenvalue of the inertia tensor
def calculate_min_and_max_inertia_vectors_from_region(region):
    # calculate the eigenvecs and eigenvals of their
    # inertia tensor and get the eigenvectors corresponding to the max/min eigenvalue

    # use the skimage.measure package to calculate the inertia-tensor
    # of each labelled region
    inertia_tensor = region.inertia_tensor
    # calculate the eigenvalues and eigenvectors of the inertia tensor
    eigenvals, eigenvecs = numpy.linalg.eig(inertia_tensor)
    # get the minimum and maximum (careful, the notation of numpy.linalg.eig,
    # is such that the column v[:,i] is the eigenvector corresponding to the 
    # eigenvalue w[i] ) 
    min_eigenvec = eigenvecs[:, np.argmin(eigenvals)]
    max_eigenvec = eigenvecs[:, np.argmax(eigenvals)]

    # return both, the eigenvectors corresponding to the minimum and to the
    # maximum eigenvalue of the inertia tensor
    return min_eigenvec, max_eigenvec


# get the axis of a 3D rotation from the corresponding rotation matrix
# inputs: rotation matrix: np.array, 3x3 rotation matrix 
# outputs: rotation axis: np.array, 3 entry rotation vector (real valued)
def get_rotation_axis_from_rotation_matrix(rotation_matrix):
    # check if the matrix input is a rotation matrix (the matrix product of the rotation matrix with it's transpose
    # is a unit matrix and the determinant is 1)
    assert np.allclose(rotation_matrix.dot(
        numpy.matrix.transpose(rotation_matrix)), np.identity(3)), "The rotation matrix is not a unit matrix"
    assert np.isclose(numpy.linalg.det(rotation_matrix), 1), "The determinant of the rotation matrix is not 1"

    # get the eigenvectors and eigenvalues of the rotation matrix
    eigenvals, eigenvecs = numpy.linalg.eig(rotation_matrix)

    # get the eigenvector corresponding to the eigenvalue 1, also check if the axis is real
    direction_of_rotation = None
    for index, eigenvalue in enumerate(eigenvals):
        if np.isclose(eigenvalue, 1.0):
            # doc: The normalized (unit “length”) eigenvectors, such that the column v[:,i] 
            # is the eigenvector corresponding to the eigenvalue w[i].
            direction_of_rotation = eigenvecs[:, index]
            assert np.allclose(direction_of_rotation.imag, 0), "The rotation axis is not real (in R^3)"
            direction_of_rotation = direction_of_rotation.real

    if direction_of_rotation is None:
        raise ValueError("The direction of rotation is ill defined, no eigenvector is close to one")

    # return the direction of rotation
    return direction_of_rotation


# get vectors in the format used by napari from their directions and starting points
# inputs: directions: list/np.array with 3 entries in for each index [i], the directions of
#                     the output vectors
#         centres: list/np.array with 3 entries in for each index [i], the centres of
#                     the output vectors (their starting points)
#         stretch: factor by which the vectors are elongated
# output: vectors: np.array of shape 2,3 for each entry in the directions/centres input
def get_vectors(directions, centres, stretch=None):
    # create output array of desired size
    vectors = np.zeros(shape=(len(directions), 2, 3))
    # input the directions and centres of the points in the correct positions
    vectors[:, 1, :] = np.asarray(directions)
    if stretch is not None:
        vectors[:, 1, :] = stretch * vectors[:, 1, :]
    vectors[:, 0, :] = np.asarray(centres)
    # return the array of vectors
    return np.asarray(vectors)


# from a label image and a list of points within these labels, get objects that 
# are bigger than a certain size_threshold, the points within these big objects
# and the direction of the eigenvector of the inertia tensor corresponding to the 
# minimum eigenvalue (the principal axes)
def get_big_objects_and_min_eval_inertia_evec_from_label_image(label_image, points_list, size_threshold):
    # calculate the region properties using the skimage.measure module
    regionprops = skimage.measure.regionprops(label_image)

    # create empty lists to store the labels of the big objects, the points
    # within them, the corresponding inertia vectors and major axis lengths
    big_obj_labels, big_obj_points, big_obj_min_inertia_vecs, big_obj_major_axis_lengths = [], [], [], []

    # go through all regions, calculate the eigenvecs and eigenvals of their
    # inertia tensor and get the eigenvectors corresponding to the max/min
    # eigenvalue, if a list of labels is given, only store the 'relevant' ones
    for region in regionprops:
        if region.area > size_threshold:
            big_obj_labels.append(region.label)
            big_obj_points.append(points_list[region.label - 1])
            big_obj_major_axis_lengths.append(region.major_axis_length)
            big_obj_min_inertia_vecs.append(calculate_min_and_max_inertia_vectors_from_region(region)[0])

    # create numpy arrays for the lists containing arrays
    big_obj_min_inertia_vecs = np.asarray(big_obj_min_inertia_vecs)
    big_obj_points = np.asarray(big_obj_points)

    return big_obj_labels, big_obj_points, big_obj_min_inertia_vecs, big_obj_major_axis_lengths


# split big objects in two and create two points within the object along the principal axis.
# The size of the objects is determined by a watershed starting from seed points (list_of_points) up to a threshold,
# if a ws_threshold is given. Objects are considered big, if they contain > size_threshold pixels.
# If display_label_image_stats is True, some properties of the label image, such as the size of the objects,
# their mean intensity, etc. will be calculated and plotted (see function analyse_label_img ).
# If a frame number is given, the plot will be saved in the analysis folder
# inputs: intensity_image: np.array, intensity image on which a watershed will be performed to determine which objects
#                          are too big and need to be split in two
#         list_of_points: np.array/list of np.arrays containing points that are used for the watershed
#         ws_threshold: float/int, threshold up to which the watershed 'basins' should be filled
#         size_threshold: threshold above which an object is considered to be 'big'
#         display_label_image_stats (optional): bool, whether or not the statistics of the label image objects (
#                                               resulting from the watershed) should be displayed this essentially
#                                                calls the 'analyse_label_image' function, the default value is False
#         frame_no (optional): number of the frame that is processed (this is only used to save the label image
#                              statistics), if None is given (default), the plots are not saved.
#                              The plotting is only executed if 'display_label_image_stats' is 'True'
#         analysis_folder (optional): str, the folder in which the plot (if a frame_no is given and
#                                     display_label_image_stats is True) is saved
#         return_sizes_from_watershed (optional): bool, whether the sizes of the watershed should be returned or not
def get_additional_points_from_big_objects(intensity_image, list_of_points, ws_threshold=None, size_threshold=50,
                                           display_label_image_stats=False,
                                           frame_no=None, analysis_folder=None, return_sizes_from_watershed=False):
    # create a watershed using the list of points as seeds until the threshold level is reached
    intensity_img_ws = get_watershed_from_image_within_mask(intensity_image, list_of_points, threshold=-ws_threshold)

    # if the statistics of the objects within the label image should be displayed,
    # call the analysis function (explanations see above)
    # if a frame number is given, the plot will be saved
    if display_label_image_stats:
        sizes_from_watershed = analyse_label_img(intensity_img_ws, intensity_image,
                                                 display=True, frame_no=frame_no,
                                                 analysis_folder=analysis_folder,
                                                 return_sizes_from_watershed=return_sizes_from_watershed)
    else:
        sizes_from_watershed = None

    # get the labels of big objects, the points within these objects, the principal axes (eigenvecs corresponding
    # to the minimum eigenvalue of the inertia tensor) and the major axis length of these objects
    # big objects are all objects with a number of pixels greater than the size_threshold in the watershed
    big_obj_labels, big_obj_points, big_obj_min_inertia_vecs, big_obj_major_axis_lengths = \
        get_big_objects_and_min_eval_inertia_evec_from_label_image(intensity_img_ws, list_of_points, size_threshold)

    print("Found " + str(len(big_obj_labels)) + " big objects that will be split in two.")

    if len(big_obj_labels) > 0:

        # get additional points by splitting the big object along its principal axis
        additional_points_in_big_obj_labels = []
        for i in range(len(big_obj_labels)):
            # use a numerical factor to shrink the distance between the points within the object (in range [0,1])
            numerical_factor = 0.8
            # add two points by an offset going in positive/negative direction along the principal axes from the
            # 'original' point as a 'base value' a 1/4 of the major axis length is chosen
            offset = numerical_factor * big_obj_major_axis_lengths[i] / 4 * big_obj_min_inertia_vecs[i]
            additional_points_in_big_obj_labels.append(big_obj_points[i] + offset)
            additional_points_in_big_obj_labels.append(big_obj_points[i] - offset)

        # use the integer values of the newly created points instead
        additional_points_in_big_obj_labels = np.round(additional_points_in_big_obj_labels).astype(int)

        # remove the 'old big object points' from the list_of_points
        new_list_of_points = pf.multidim_set_diff(big_obj_points, list_of_points)

        # add the 'new big object points' instead
        new_list_of_points = pf.multidim_union(new_list_of_points, additional_points_in_big_obj_labels)

    else:
        new_list_of_points = list_of_points

    # return the sizes of watershed additionally to the points, if desired, otherwise only return the points
    if return_sizes_from_watershed:
        return new_list_of_points, sizes_from_watershed
    else:
        return new_list_of_points


# split objects in an image given a list of points, a watershed threshold and an intensity image
def split_given_objects(complete_points_list, split_points_list, intensity_image, watershed_threshold):

    # create a watershed using the list of points as seeds until the threshold level is reached
    label_img_ws = get_watershed_from_image_within_mask(intensity_image, split_points_list,
                                                        threshold=-watershed_threshold)

    # if the list of points is empty, return it like that
    if len(split_points_list) == 0:
        return complete_points_list

    # calculate the region properties using the skimage.measure module
    regionprops = skimage.measure.regionprops(label_img_ws)

    # create empty lists to store the labels of the big objects, the points within them, the corresponding inertia
    # vectors and major axis lengths
    big_obj_labels, big_obj_points, big_obj_min_inertia_vecs, big_obj_major_axis_lengths = [], [], [], []

    # go through all regions, calculate the eigenvecs and eigenvals of their inertia tensor and get the eigenvectors
    # corresponding to the max/min eigenvalue
    for region in regionprops:
        big_obj_labels.append(region.label)
        big_obj_points.append(split_points_list[region.label - 1])
        big_obj_major_axis_lengths.append(region.major_axis_length)
        big_obj_min_inertia_vecs.append(calculate_min_and_max_inertia_vectors_from_region(region)[0])

    # create numpy arrays for the lists containing arrays
    big_obj_min_inertia_vecs = np.asarray(big_obj_min_inertia_vecs)
    big_obj_points = np.asarray(big_obj_points)

    # get additional points by splitting the big object along its principal axis
    additional_points_in_big_obj_labels = []
    for i in range(len(big_obj_labels)):
        # use a numerical factor to shrink the distance between the points within the object (in range [0,1])
        numerical_factor = 0.8
        # add two points by an offset going in positive/negative direction along the principal axes from the
        # 'original' point as a 'base value' a 1/4 of the major axis length is chosen
        offset = numerical_factor * big_obj_major_axis_lengths[i] / 4 * big_obj_min_inertia_vecs[i]
        additional_points_in_big_obj_labels.append(big_obj_points[i] + offset)
        additional_points_in_big_obj_labels.append(big_obj_points[i] - offset)

    # use the integer values of the newly created points instead
    additional_points_in_big_obj_labels = np.round(additional_points_in_big_obj_labels).astype(int)

    # remove the 'old big object points' from the list_of_points
    new_list_of_points = pf.multidim_set_diff(big_obj_points, complete_points_list)

    # add the 'new big object points' instead
    new_list_of_points = pf.multidim_union(new_list_of_points, additional_points_in_big_obj_labels)

    print("now have " + str(len(new_list_of_points)) + "points")
    return new_list_of_points
