from interact_with_segmentation_threshs_with_points_object import get_nd_points_layer_napari
from display_outcome_tracking_napari import add_images_to_viewer

import napari
import numpy as np

from qtpy.QtWidgets import QLabel, QVBoxLayout, QGroupBox, QComboBox


# change the colour of the tracking vector in the viewer depending on different attributes that were selected by the
# user in the combo-box
# viewer: napari Viewer object containing a 'vectors transition "x"' layers, where "x" stands for the frame number
#         value: the value of the combo-box from "add_switch_to_highlight_transition_cost_or_faithfulness",where the
#                number corresponds to the index in the vector properties list
#         points_properties_list: a list with all the names of the properties the points layer has as strings
def point_colour_combo_box_callback(viewer, value, points_im_1_and_2_props_keys_list):
    property_name = points_im_1_and_2_props_keys_list[value]
    for layer in viewer.layers:
        if layer.name.startswith("points 1"):
            layer.face_color = property_name
        elif layer.name.startswith("points 2"):
            layer.face_color = property_name
    return


def add_a_switch_to_highlight_pairs_based_on_local_association(viewer, points_im_1_properties):
    # create a Combo-Box to change the highlighting of the points layers
    point_style_combo_box = QComboBox()

    # add all possible options to the box
    point_im_1_and_2_props_keys_list = list(points_im_1_properties.keys())
    # get all pair indices
    point_im_1_and_2_props_keys_list = [props_key for props_key in point_im_1_and_2_props_keys_list
                                        if props_key.startswith("display_colour")]
    point_style_combo_box.addItems(point_im_1_and_2_props_keys_list)

    # Connect the dropdown selection to its callback function to change the vector style dependent on the selection
    # noinspection PyUnresolvedReferences
    point_style_combo_box.currentIndexChanged.connect(lambda value=point_style_combo_box:
                                                      point_colour_combo_box_callback(viewer,
                                                                                      value,
                                                                                      point_im_1_and_2_props_keys_list))

    # create a pyqt-box for placing all PyQt-objects
    box = QVBoxLayout()
    box.addWidget(QLabel('Point colour based on: '))  # add a label to the QSlider
    box.addWidget(point_style_combo_box)

    # use a group box to add the objects collected in the QVBoxLayout object (no title)
    group_box = QGroupBox('')
    # set the layout
    group_box.setLayout(box)
    # add the widget to the napari viewer, on the right side
    viewer.window.add_dock_widget(group_box, name='Point colour based on: ', area='right')

    return viewer


def display_outliers(outliers_df, point_objects, i_lpp, good_connections=None):
    number_of_frames = int(outliers_df["time_index"].max()) + 1
    number_of_points = int(len(outliers_df["x_pos_ROI"]) / number_of_frames)

    # get all points from the outliers dataframe, their pair and tuple indices and create a dictionary with these values
    # as needed for the napari viewer
    points = np.array([outliers_df["x_pos_ROI"], outliers_df["y_pos_ROI"], outliers_df["z_pos_ROI"]]).T
    points = points.reshape((number_of_frames, number_of_points, 3))

    # get the global (non-batch wise) pair and tuple indices of these points to add to the properties
    pair_index = np.array(outliers_df["pair_index"])
    tuple_index = np.array(outliers_df["tuple_index"])
    pair_display_colour = np.copy(pair_index)
    pair_display_colour[pair_display_colour < 0] = -1
    object_index = np.array(outliers_df["object_index"])

    points_im_1, points_im_2 = points[:-1], points[1:]
    points_im_1_properties = {'object_index': object_index[:-number_of_points],
                              'pair_index': pair_index[:-number_of_points],
                              'tuple_index': tuple_index[:-number_of_points],
                              'display_colour': pair_display_colour[:-number_of_points]}
    points_im_2_properties = {'object_index': object_index[number_of_points:],
                              'pair_index': pair_index[number_of_points:],
                              'tuple_index': tuple_index[number_of_points:],
                              'display_colour': pair_display_colour[number_of_points:]}
    text_points = {
        'text': '{object_index}: {pair_index}.{tuple_index}',
        'size': 4,
        'color': 'white',
        'translation': np.array([-0.5, 0, 0]),
        'visible': False
    }

    # get the tuple and pair indices for all batches
    for j, (batch_range, batch_name) in enumerate(zip(i_lpp.batch_ranges, i_lpp.batch_names)):
        # read the pair and tuple indices to np arrays from the dataframe
        pair_ix_batch = np.nan_to_num(np.array(outliers_df["pair_index" + batch_name], dtype=float))
        tuple_ix_batch = np.nan_to_num(np.array(outliers_df["tuple_index" + batch_name], dtype=float))
        display_col_batch = np.copy(pair_ix_batch)
        display_col_batch[display_col_batch < 0] = -1
        # add the pair and tuple indices to the respective napari properties dictionary
        points_im_1_properties['pair_index' + batch_name] = pair_ix_batch[:-number_of_points]
        points_im_1_properties['tuple_index' + batch_name] = tuple_ix_batch[:-number_of_points]
        points_im_1_properties['display_colour' + batch_name] = display_col_batch[:-number_of_points]
        points_im_2_properties['pair_index' + batch_name] = pair_ix_batch[number_of_points:]
        points_im_2_properties['tuple_index' + batch_name] = tuple_ix_batch[number_of_points:]
        points_im_2_properties['display_colour' + batch_name] = display_col_batch[number_of_points:]

    # get all vectors from the outliers dataframe, their pair and tuple indices and whether or not they are faulty
    # and create a properties dictionary as needed for the napari viewer
    vectors = np.array([outliers_df["x_vector"], outliers_df["y_vector"], outliers_df["z_vector"]]).T
    vectors = vectors.reshape((number_of_frames, number_of_points, 3))[0:-1]

    vectors_for_napari = np.array([get_nd_points_layer_napari(points_im_1), get_nd_points_layer_napari(vectors)])
    vectors_for_napari = np.einsum('ijk->jik', vectors_for_napari)

    is_faulty_connection = outliers_df["faulty connection (combined batches)"][:-number_of_points]
    vector_properties = {'faulty_connection': is_faulty_connection}

    # if corrected connections are given to the function, also display the updated tracks
    if good_connections is not None:
        # sort the dataframe by the time_index, pair_index and tuple_index for all objects
        outliers_df.sort_values(by=["time_index", "object_index"], inplace=True)
        points = np.array([outliers_df["x_pos_ROI"], outliers_df["y_pos_ROI"], outliers_df["z_pos_ROI"]]).T
        points = points.reshape((number_of_frames, number_of_points, 3))
        # get the points in the respective frames and their connection vectors, also get the information which frames
        # they are
        points_1a = np.array([points[connection[0]][connection[1][0]] for connection in good_connections])
        points_1b = np.array([points[connection[0]][connection[2][0]] for connection in good_connections])
        points_2a = np.array([points[connection[0] + 1][connection[1][1]] for connection in good_connections])
        points_2b = np.array([points[connection[0] + 1][connection[2][1]] for connection in good_connections])
        frames = np.array([connection[0] for connection in good_connections])
        pair_indices_random = np.random.randint(0, high=100, size=len(points_1a), dtype=int)
        pairs = list(pair_indices_random) + list(pair_indices_random)
        good_connections_properties = {'pair_indices': pairs}

        vecs = np.append(points_2a - points_1a, points_2b - points_1b, axis=0)
        points = np.append(points_1a, points_1b, axis=0)
        frames = np.append(frames, frames, axis=0)

        import sys
        sys.stdout = sys.__stdout__
        # create a np array with the correct shape for napari
        good_connections_vectors_napari = np.zeros((len(good_connections)*2, 2, 4), dtype=np.float64)
        # add the data above to the napari array in the correct order
        good_connections_vectors_napari[:, 0, 0] = frames
        good_connections_vectors_napari[:, 1, 0] = frames
        good_connections_vectors_napari[:, 0, 1:] = points
        good_connections_vectors_napari[:, 1, 1:] = vecs

    else:
        good_connections_vectors_napari, good_connections_properties = None, None

    with napari.gui_qt():
        viewer = napari.Viewer(title="Incorrect tracks", axis_labels=["frame", "z", "y", "x"])
        viewer.dims.ndisplay = 3  # open the viewer in the 3D view

        # add a drop-down menu which can be used to change the colour-labels of the points from global to batch
        # pair-indices
        add_a_switch_to_highlight_pairs_based_on_local_association(viewer, points_im_1_properties)

        # add the images and points and tracking vectors to the napari viewer
        add_images_to_viewer(viewer, point_objects)

        viewer.add_points(
            get_nd_points_layer_napari(points_im_1),
            properties=points_im_1_properties,
            face_color='display_colour',
            face_colormap='nipy_spectral',
            # edge_color_cycle=['black', 'green', 'red'],
            edge_color='black',
            text=text_points,
            size=1,
            symbol='ring',
            name='points 1')

        # add the points from image2, the colours are the same as for image1, but the symbol
        # used for points in the second image is a disk/circle
        viewer.add_points(
            get_nd_points_layer_napari(points_im_2),
            properties=points_im_2_properties,
            face_color='display_colour',
            face_colormap='nipy_spectral',
            # edge_color_cycle=['black', 'green', 'red'],
            edge_color='black',  # 'tuple_index',
            text=text_points,
            size=1,
            symbol='disc',
            name='points 2')

        # add the points from image2, the colours are the same as for image1, but the symbol
        # used for points in the second image is a disk/circle
        viewer.add_vectors(
            vectors_for_napari,
            properties=vector_properties,
            edge_color_cycle=['green', 'red'],
            edge_color='faulty_connection',
            name='vectors')

        # if corrected connections are given to the function, also display the updated tracks
        if good_connections is not None:
            viewer.add_vectors(
                good_connections_vectors_napari,
                edge_colormap='nipy_spectral',
                edge_color='pair_indices',
                properties=good_connections_properties,
                name='corrected vectors')
    return good_connections_vectors_napari
