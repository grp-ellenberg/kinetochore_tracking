import numpy as np
from tqdm import tqdm
from itertools import chain


class AltTracksCluster:

    def __init__(self, depth_to_go, unique_index):
        self.orig_objects = [[] for _ in range(depth_to_go + 1)]
        self.orig_costs = [[] for _ in range(depth_to_go)]
        self.alt_objects = [[] for _ in range(depth_to_go + 1)]
        self.alt_costs = [[] for _ in range(depth_to_go)]
        self.success = False
        self.unique_index = unique_index
        self.alt_index = unique_index - 1
        self.depth_to_go = depth_to_go


# find differences between the "original" main interpretation of the linear programming problem and "alternative"
# solutions found with different costs vectors and check which ones are "better"
# inputs: pandas_df: a pandas_df containing both the interpretation of the i_lpp and alternative i_lpps and their
#                    batch-wise interpretation before anaphase onset
#         i_lpp: the output of the "interpret_lpp" function that interprets the output of a the minimum costs flow
#                (lpp) with the "main" costs
#         alt_ilpps: list with i_lpps of the "alternative costs"
#         p: the parameters objects created from the read-in
# outputs: i_lpp: the updated i_lpp which now contains some tracks from the alternative tracking outcomes, if
#                 they were deemed to be better based on the pair-wise tracking analysis on the batches
def find_better_tracks_using_alternatives(pandas_df, i_lpp, alt_i_lpps, p):
    unique_vecs_list, unique_indices_list, rows_with_alternative_vectors = \
        find_columns_with_differing_vectors_in_pandas_df(pandas_df)
    i_lpp = find_clusters(pandas_df, alt_i_lpps, p, i_lpp, rows_with_alternative_vectors, unique_vecs_list,
                          unique_indices_list)
    return i_lpp


def find_columns_with_differing_vectors_in_pandas_df(pandas_df):
    # first: find all columns in which the vectors are not the same
    # 1) get all columns with vectors that are not the same in x, y or z
    for dimension in ['x', 'y', 'z']:
        vector_filter_for_dim = [col for col in pandas_df if col.startswith(dimension + '_vector')]
        individual_vec_differs = [pandas_df[vector_filter] != pandas_df[dimension + '_vector'] for vector_filter in
                                  vector_filter_for_dim[1:]]
        pandas_df["some vec differs in " + dimension] = np.array(individual_vec_differs).any(axis=0).astype(bool)
    # 2) only get those that are not the same in all dimensions
    vector_filter_for_differing_vectors = [col for col in pandas_df if col.startswith("some vec differs in ")]
    pandas_df["vec differs"] = pandas_df[vector_filter_for_differing_vectors].any(axis=1).astype(bool)

    # for the vectors that are not the same: change the vector to the alternative vector and then try if the
    # pair tracking distance gets better, if yes, accept the change
    # 1) get the rows with vectors that have differences
    rows_with_alternative_vectors = pandas_df.loc[pandas_df["vec differs"] == True]
    # 2) get the vectors in these segments
    vectors_for_dim = []
    for dimension in ["x", "y", "z"]:
        vector_filter_for_dim = [col for col in pandas_df if col.startswith(dimension + '_vector')]
        vectors_for_dim.append(rows_with_alternative_vectors[vector_filter_for_dim].values)
    # vectors sorted by their dimension, number and alternative number [dim, index, alternative]
    vectors_for_dim = np.array(vectors_for_dim)
    # get the unique vectors for every alternative track
    unique_vecs_list, unique_indices_list = [], []
    for i in range(vectors_for_dim.shape[1]):
        unique_vecs, unique_indices = np.unique(vectors_for_dim[:, i, :], axis=1, return_index=True)
        unique_vecs_list.append(unique_vecs)
        unique_indices_list.append(unique_indices)
    return unique_vecs_list, unique_indices_list, rows_with_alternative_vectors


# get the batch name corresponding to the "closest match of pair indices batch" from a time point
def get_batch_name_and_frame_no_from_time_point_and_parameters(time_point, p):
    # get the time-index and the name of the batch where this time-index is in the middle
    if 2 <= time_point <= p.anaphase_onset - 1 - min(p.time_points) - 2:
        time_offset_first, time_offset_second = 2, 2
        batch_no = time_point - 2
    elif time_point == 0:
        time_offset_first, time_offset_second = 0, 4
        batch_no = time_point
    elif time_point == 1:
        time_offset_first, time_offset_second = 1, 3
        batch_no = time_point - 1
    elif time_point == p.anaphase_onset - 1 - min(p.time_points):
        time_offset_first, time_offset_second = 4, 0
        batch_no = time_point - 3
    elif time_point == p.anaphase_onset - 1 - min(p.time_points) - 1:
        time_offset_first, time_offset_second = 3, 1
        batch_no = time_point - 4
    else:
        raise ValueError("This case is not possible to reach in theory.")
    frame_number = min(p.time_points) + time_point
    # construct the batch name
    batch_name = "pair_plus_pair_vec_diff_batch_" + str(int(batch_no)) + "_frames_" + str(int(
        frame_number - time_offset_first)) + "-" + str(int(frame_number + time_offset_second))
    return batch_name, frame_number


def get_costs_for_transition(pandas_df, backward_obj, column_batch_name, alt_ix=None):
    # get the row in question
    pandas_df_column = "object_index_is"
    if alt_ix is not None:
        pandas_df_column += "_alt" + str(alt_ix)
    backward_obj_row = pandas_df[pandas_df[pandas_df_column] == backward_obj]
    # if no unique_ix is given (or it is set to None), the column name is the batch_name, otherwise, append it
    if alt_ix is not None:
        column_batch_name += "_alt" + str(alt_ix)
    # get the costs from the row in the pandas dataframe
    cost = backward_obj_row[column_batch_name].values[0]
    print("Did find cost")
    return cost


def get_forward_object_and_add_to_cluster(backward_obj, depth_to_go, forward_dict, alt_tracks_cluster,
                                          column_batch_names, pandas_df):
    forward_obj = None
    alt_tracks_cluster.orig_objects[0].append(backward_obj)
    for k in range(depth_to_go):
        forward_obj = forward_dict[backward_obj]
        alt_tracks_cluster.orig_costs[k].append(get_costs_for_transition(pandas_df, backward_obj,
                                                                         column_batch_names[k], None))
        backward_obj = forward_obj

        alt_tracks_cluster.orig_objects[k + 1].append(forward_obj)
    return alt_tracks_cluster, forward_obj


def get_backward_obj_and_add_to_cluster(forward_obj, depth_to_go, backward_dict, alt_tracks_cluster,
                                        column_batch_names, pandas_df, alt_ix):
    alt_tracks_cluster.alt_objects[depth_to_go].append(forward_obj)
    backward_obj = None
    for k in reversed(range(depth_to_go)):
        backward_obj = backward_dict[forward_obj]
        alt_tracks_cluster.alt_costs[k].append(get_costs_for_transition(pandas_df, backward_obj,
                                                                        column_batch_names[k], alt_ix))
        forward_obj = backward_obj
        alt_tracks_cluster.alt_objects[k].append(backward_obj)
    return alt_tracks_cluster, backward_obj


# search for clusters in differences in the tracking outcome by going forward in the solution and backward in the
# alternative solution given the tracks until the first object is found again (a loop/cluster is complete). If
# alternative objects have been chosen, the cluster is expanded in time, except if this is not possible, as this
# is already the first frame or after anaphase onset.
def search_for_cluster_given_starting_object(time_point, unique_index,
                                             starting_backward_object,
                                             dict_forward, dict_backward, dict_backward_given_alt,
                                             forward_depth, backward_depth, p, pandas_df,
                                             max_depth=10):
    if forward_depth + backward_depth >= max_depth:
        print("Depth too big")
        return None

    # the index of the alternative is the unique_index -1
    alt_ix = unique_index - 1

    # if the time point is too close to the anaphase onset, the difference measures used to determine which
    # track is better are none existent here, as they are based on the difference in pair movement,
    # which does not make sense after anaphase onset
    if time_point >= p.anaphase_onset - min(p.time_points) - 3:
        print("beyond anaphase onset")
        return None
    if time_point is None or np.isnan(time_point):
        print("Time point is none")
        return None

    # get the depth to go in each step
    depth_to_go = forward_depth + backward_depth - 1

    # get the best "batch" to use for the difference measures given the time point
    column_batch_names, frame_numbers = [], []
    for k in range(depth_to_go):
        column_batch_name, frame_number = get_batch_name_and_frame_no_from_time_point_and_parameters(time_point + k, p)
        column_batch_names.append(column_batch_name)
        frame_numbers.append(frame_number)

    # create empty lists for the clusters and their respective costs
    alt_tracks_cluster = AltTracksCluster(depth_to_go, unique_index)
    alt_tracks_cluster.orig_objects[0].append(starting_backward_object)
    alt_tracks_cluster.orig_costs[0].append(get_costs_for_transition(pandas_df, starting_backward_object,
                                                                     column_batch_names[0]))
    try:
        alt_tracks_cluster.alt_costs[0].append(get_costs_for_transition(pandas_df, starting_backward_object,
                                                                        column_batch_names[0], alt_ix))
    except IndexError:
        print("Index Error Can't find costs for starting object")
        # only decrease the "time_point", if the resulting time point is still in the possible range of time_points
        if time_point == 0:
            return None
        print("starting object already not in problem --> increase backward depth and choose new starting object")
        # in this case the alternative path chose another object that is not part of the "main solution" at all
        # prior to the current step --> get a new starting object one frame back, increase the backward depth and rerun
        new_starting_object = dict_backward[starting_backward_object]
        backward_depth += 1
        time_point -= 1
        alt_tracks_cluster = search_for_cluster_given_starting_object(time_point, unique_index,
                                                                      new_starting_object,
                                                                      dict_forward, dict_backward,
                                                                      dict_backward_given_alt,
                                                                      forward_depth, backward_depth, p, pandas_df)
        return alt_tracks_cluster

    backward_obj = None

    first_iteration = True

    while backward_obj != starting_backward_object:

        if first_iteration:
            backward_obj = starting_backward_object
            first_iteration = False

        try:
            alt_tracks_cluster, forward_obj = get_forward_object_and_add_to_cluster(backward_obj, depth_to_go,
                                                                                    dict_forward, alt_tracks_cluster,
                                                                                    column_batch_names, pandas_df)
        except KeyError:
            print("Can't find backward object in the forwards dict")
            # only decrease the "time_point", if the resulting time point is still in the possible range of time_points
            if time_point == 0:
                print("Can't decrease backward depth")
                return None
            print("So increasing the backward depth")
            # in this case the alternative path chose another object that is not part of the "main solution" at all
            # prior to the current step --> get a new starting object one frame back, increase the backward depth
            # and rerun
            backward_depth += 1
            time_point -= 1
            new_starting_object = dict_backward[starting_backward_object]
            # in this case the alternative path chose another object that is not part of the "main solution" at all
            # prior to the current step --> increase the backward depth and rerun
            alt_tracks_cluster = search_for_cluster_given_starting_object(time_point, unique_index,
                                                                          new_starting_object,
                                                                          dict_forward, dict_backward,
                                                                          dict_backward_given_alt,
                                                                          forward_depth, backward_depth, p, pandas_df)
            return alt_tracks_cluster

        try:
            # get the object corresponding to the alternative track in the "first frame"
            # careful: as the unique indices start with 0 being the standard track, to get to the
            # alternative tracks 1 has to be subtracted
            alt_tracks_cluster, backward_obj = get_backward_obj_and_add_to_cluster(forward_obj, depth_to_go,
                                                                                   dict_backward_given_alt,
                                                                                   alt_tracks_cluster,
                                                                                   column_batch_names,
                                                                                   pandas_df, alt_ix)
        except KeyError:
            print("can't find forward object in the backwards dict, so increasing the forward depth")
            # in this case the alternative path chose another object that is not part of the "main solution" at all
            # prior to the current step --> increase the forward depth and rerun
            forward_depth += 1

            if time_point + forward_depth >= p.anaphase_onset - min(p.time_points) - 2:
                print("beyond anaphase onset")
                return None

            alt_tracks_cluster = search_for_cluster_given_starting_object(time_point, unique_index,
                                                                          starting_backward_object,
                                                                          dict_forward, dict_backward,
                                                                          dict_backward_given_alt,
                                                                          forward_depth, backward_depth, p, pandas_df)
            return alt_tracks_cluster
    alt_tracks_cluster.success = True

    return alt_tracks_cluster


# take floating point values from a pandas dataframe and convert them into a dictionary of strings
# inputs: col_1: pandas column that should be used as keys
#         col_2: pandas column that should be used as values
#         precision: the precision to which the floats are rounded before stored in the dict, the default is 3
# output: alt_dict: dictionary of strings where the keys came from col_1 and the values from col_2, the precision
#                   of the string floats is given by the precision parameter
def get_from_pd_columns(col_1, col_2):
    alt_dict = dict(zip(col_1, col_2))
    return alt_dict


# find clusters that belong together (clusters are alternative decisions for traces that are correlated, e.g.
# if you have the connections 0.0 -> 1.0 and 0.1 -> 1.1 in one track and in the alternative
# track you have 0.0 -> 1.1 and 0.1 -> 1.0, you can not simply just choose one of the alternative connections
# as otherwise you have "loose ends" or objects that are chosen "twice")
def find_clusters(pandas_df, loaded_alt_i_lpps, p, i_lpp, rows_with_alternative_vectors, unique_vecs_list,
                  unique_indices_list):
    # 1) create dictionaries to go back and forwards in time for the object indices for the "original" and the
    # "alternative" tracking solutions:
    dict_forward = get_from_pd_columns(pandas_df["object_index_is"], pandas_df["object_index_to"])
    dict_backward = get_from_pd_columns(pandas_df["object_index_is"], pandas_df["object_index_from"])

    alt_dicts_forward, alt_dicts_backward = [], []
    for j, _ in enumerate(loaded_alt_i_lpps):
        alt_dicts_forward.append(get_from_pd_columns(pandas_df["object_index_is_alt" + str(j)],
                                 pandas_df["object_index_to_alt" + str(j)]))

        alt_dicts_backward.append(get_from_pd_columns(pandas_df["object_index_is_alt" + str(j)],
                                  pandas_df["object_index_from_alt" + str(j)]))

    # 2) for the unique vecs (that do not consist of nans (alternative point chosen, not just alternative vector)),
    # find clusters
    clusters = []
    for row_index_unique_vecs_list, (unique_vecs_starter, unique_indices_starter) in \
            tqdm(enumerate(zip(unique_vecs_list, unique_indices_list)), total=len(unique_vecs_list)):

        # vectors that differ can be different in several alternative tracking approaches, thus for every index that
        # differs, do the cluster search independently
        for unique_index in unique_indices_starter:
            # skip for index 0, which corresponds to the "main/basic" tracking and not the alternatives
            if unique_index == 0:
                continue

            # get the time point
            time_point = rows_with_alternative_vectors["time_index"].iloc[row_index_unique_vecs_list]

            # define the "forward and backward depth" i.e. the amount of steps you have to go
            # forward/backward to find an interconnected path
            forward_depth, backward_depth = 1, 1

            # get the object with which the iteration is started and add it to the cluster
            starting_backward_object = rows_with_alternative_vectors["object_index_is"].iloc[row_index_unique_vecs_list]

            alt_tracks_cluster = search_for_cluster_given_starting_object(time_point, unique_index,
                                                                          starting_backward_object,
                                                                          dict_forward, dict_backward,
                                                                          alt_dicts_backward[unique_index - 1],
                                                                          forward_depth, backward_depth,
                                                                          p, pandas_df)
            clusters.append(alt_tracks_cluster)

    # get unique clusters
    sorted_clusters_orig, sorted_clusters_alt = [], []
    unique_clusters = []
    for cluster in clusters:
        if cluster is not None and cluster.success:
            new_sorted_cluster_orig = [sorted(cluster_list) for cluster_list in cluster.orig_objects]
            new_sorted_cluster_orig = list(chain(*new_sorted_cluster_orig))
            new_sorted_cluster_alt = [sorted(cluster_list) for cluster_list in cluster.alt_objects]
            new_sorted_cluster_alt = list(chain(*new_sorted_cluster_alt))
            array_equal_in = [np.array_equal(new_sorted_cluster_orig, sorted_cluster, equal_nan=False)
                              for sorted_cluster in sorted_clusters_orig]
            array_equal_out = [np.array_equal(new_sorted_cluster_alt, sorted_cluster, equal_nan=False)
                               for sorted_cluster in sorted_clusters_alt]
            if not (np.any(array_equal_in) and np.any(array_equal_out)):
                sorted_clusters_orig.append(new_sorted_cluster_orig)
                sorted_clusters_alt.append(new_sorted_cluster_alt)
                unique_clusters.append(cluster)

    # get all connections where a replacement "makes sense" i.e. the clusters where the total cost of the
    # alternative is lower than the total cost of the original cost
    unique_clusters_with_lower_costs = []
    for cluster in unique_clusters:
        # if the costs of the alternative cluster are smaller than the original, accept the alternative tracks
        alt_costs = sum([sum(costs) for costs in cluster.alt_costs])
        orig_costs = sum([sum(costs) for costs in cluster.orig_costs])
        cluster.costs_difference = alt_costs - orig_costs
        if cluster.costs_difference < 0:
            unique_clusters_with_lower_costs.append(cluster)

    # check for possible clashes between unique clusters:
    # 1) get all objects that will be replaced
    objects_that_will_be_replaced = []
    for cluster in unique_clusters_with_lower_costs:
        cluster.objects_to_be_replaced = []
        cluster.more_than_two_different_objects_chosen = False
        # for every step that was done, change the connections from the first to second frame in the ilpp
        for depth_to_go_index in range(cluster.depth_to_go):
            list_of_new_1st_objects = cluster.alt_objects[depth_to_go_index]
            list_of_new_2nd_objects = cluster.alt_objects[depth_to_go_index + 1]
            list_of_old_1st_objects = cluster.orig_objects[depth_to_go_index]
            list_of_old_2nd_objects = cluster.orig_objects[depth_to_go_index + 1]
            old_only_2nd_objects = set(list_of_old_2nd_objects) - set(list_of_new_2nd_objects)
            old_only_1st_objects = set(list_of_old_1st_objects) - set(list_of_new_1st_objects)
            if len(old_only_2nd_objects) == 1:
                objects_that_will_be_replaced.append(list(old_only_2nd_objects)[0])
                cluster.objects_to_be_replaced.append(list(old_only_2nd_objects)[0])
            elif len(old_only_2nd_objects) > 1:
                cluster.more_than_two_different_objects_chosen = True
            if len(old_only_1st_objects) == 1:
                objects_that_will_be_replaced.append(list(old_only_1st_objects)[0])
                cluster.objects_to_be_replaced.append(list(old_only_1st_objects)[0])
            elif len(old_only_1st_objects) > 1:
                cluster.more_than_two_different_objects_chosen = True
    # 2) for objects that should be replaced in several alternatives: get the best alternative
    unique_replacements, counts = np.unique(objects_that_will_be_replaced, return_counts=True)
    for unique_replacement, count in zip(unique_replacements, counts):
        costs_for_replacement = []
        if count > 2:  # every count is made twice (in the 1st and 2nd objects)
            # get all clusters with this object and their costs:
            for cluster in unique_clusters_with_lower_costs:
                if unique_replacement in cluster.objects_to_be_replaced:
                    costs_for_replacement.append(cluster.alt_costs)
                cluster.cheapest_alternative = True
            for cluster in unique_clusters_with_lower_costs:
                if unique_replacement in cluster.objects_to_be_replaced:
                    if cluster.alt_costs == min(costs_for_replacement):
                        cluster.cheapest_alternative = True
                    else:
                        cluster.cheapest_alternative = False

    # replace the connections in the original lpp with alternative lpp connections, if the cost of these is less
    # (cluster-wise)
    for cluster in unique_clusters_with_lower_costs:
        if not cluster.more_than_two_different_objects_chosen and \
                cluster.cheapest_alternative:
            # for every step that was done, change the connections from the first to second frame in the ilpp
            for depth_to_go_index in range(cluster.depth_to_go):
                list_of_new_1st_objects = cluster.alt_objects[depth_to_go_index]
                list_of_new_2nd_objects = cluster.alt_objects[depth_to_go_index + 1]
                list_of_old_1st_objects = cluster.orig_objects[depth_to_go_index]
                list_of_old_2nd_objects = cluster.orig_objects[depth_to_go_index + 1]
                i_lpp.change_connection_in_ilpp(list_of_new_1st_objects, list_of_new_2nd_objects,
                                                list_of_old_1st_objects, list_of_old_2nd_objects,
                                                loaded_alt_i_lpps[cluster.alt_index], p)

    # get an array with the sorted points for further calculations
    i_lpp.get_sorted_points_array()
    # get the pair and tuple dictionary for the linear programming problem
    i_lpp.get_pair_and_tuple_dict(p, weight_factor_pair_finding=2.)

    return i_lpp
