import math
import copy
import numpy as np
from scipy import spatial
from os_functions import try_creating_folder
import matplotlib.pyplot as plt
from scipy.optimize import linear_sum_assignment
from scipy.spatial.distance import pdist, squareform

plt.style.use('dark_background')


# class to contain the interpretation of the linear programming problem --> the tracking association
# The attributes of an instance of the LppInterpretation are:
# points_im_1_list: list of lists containing a np.array with coordinates, the points which were chosen in the input
#                   image of each transition
# points_im_2_list: list of lists containing a np.array with coordinates, the points which were chosen in the output
#                   image of each transition
# points_im_1_colour_list: list of lists of integers/floats, the tracking labels for each point in the input image
#                          of each transition
# points_im_2_colour_list: list of lists of integers/floats, the tracking labels for each point in the output image
#                           of each transition
# points_im_1_is_split_list: list of lists of integers, labels indicating whether the resulting point is the result
#                            of a split event in this frame (either from a previous step (2) or from the linear
#                            optimisation (1)) or not (0) for each point in the input image of each transition
# points_im_2_is_split_list: list of lists of integers, labels indicating whether the resulting point is the result
#                            of a split event in this frame (either from a previous step (2) or from the linear
#                            optimisation (1)) or not (0) for each point in the output image of each transition
# text_points_im_1_list: text containing the objects point-object ID for each chosen point in the first image
# text_points_im_2_list: text containing the objects point-object ID for each chosen point in the second image
# sorted_points: contains the points in each frame sorted by their object number, the indexes are
#                [time_point, object_number, spatial_dimension]
# argsorted_points: Indices that were used to sort the points_list such that it results in the sorted_points. These
#                   can be used to sort other lists of the same shape (e.g. the colour list, is split list, etc.)
# difference_vectors: the difference between two points that have been associated in neighbouring frames.
#                     the indices are [time_point(start), object_number, spatial_dimension]. So this array
#                     is len(time_point) - 1 long, because there is 1 transition less then there are time
#                     points
class LppInterpretation:
    def __init__(self, points_im_1_list, points_im_2_list,
                 points_im_1_colour_list, points_im_2_colour_list,
                 points_im_1_is_split_list, points_im_2_is_split_list,
                 text_points_im_1_list, text_points_im_2_list,
                 vectors_list, costs_list, specific_costs_list,
                 current_colour):

        # attributes that are specific for each point
        self.points_im_1_list, self.points_im_2_list = points_im_1_list, points_im_2_list
        self.points_im_1_colour_list, self.points_im_2_colour_list = points_im_1_colour_list, points_im_2_colour_list
        self.points_im_1_is_split_list = points_im_1_is_split_list
        self.points_im_2_is_split_list = points_im_2_is_split_list
        self.text_points_im_1_list = text_points_im_1_list
        self.text_points_im_2_list = text_points_im_2_list

        # attributes that are automatically created from the points when creating the point specific attributes
        self.vectors_list = vectors_list

        # attributes that are tracking outcome specific
        self.costs_list = costs_list
        self.specific_costs_list = specific_costs_list

        # attribute needed to add new objects
        self.current_colour = current_colour

        # attributes that are specific for the set of points
        self.argsorted_points, self.sorted_points = None, None
        self.sorted_points_array, self.sorted_by_object_points = None, None
        self.pair_dictionary, self.tuple_dictionary = None, None
        self.unique_real_pairs, self.euclidean_distance_matrix = None, None
        self.real_pairs, self.not_real_pairs = None, None

        # attribute indicating that connection was changed from lpp interpretation
        self.is_changed_connection = None
        self.points_removed = []

    def __str__(self):
        return_string = "Linear Programming Problem Interpretation Object with the following attributes: \n"
        return_string += str(self.__dict__.keys())[11:-2]
        return_string += "\n For more information on these read the 'interpret.lpp.py'-file"
        return return_string

    # sort the points lists, to use slicing/indices to get object more easily and get the difference vectors
    # outputs: euclidean_distance_matrix: contains the euclidean distance between the start and end point of
    #                                     each difference vector, where a difference vector connects associated
    #                                     points in neighbouring time frames (see difference vector)
    #          sorted_points_list: contains the points in each frame sorted by their object number, the indexes are
    #                              [time_point, object_number, spatial_dimension]
    #          difference_vectors: the difference between two points that have been associated in neighbouring frames.
    #                              the indices are [time_point(start), object_number, spatial_dimension]. So this array
    #                              is len(time_point) - 1 long, because there is 1 transition less then there are time
    #                              points
    def get_sorted_points_list_difference_vectors_and_distance_matrix(self):
        sorted_points_list, difference_vectors, argsorted_points = \
            get_sorted_points_and_vectors(self.points_im_1_list, self.points_im_1_colour_list,
                                          self.points_im_2_list, self.points_im_2_colour_list)

        # for every distance vector, calculate its length (using the euclidean distance between start and end point
        # of the vector)
        euclidean_distance_matrix = np.sqrt(np.sum(np.square(difference_vectors), axis=2))

        # store the argsorted points and the sorted points in the LppInterpretation object
        self.argsorted_points = argsorted_points
        self.sorted_points = sorted_points_list

        return euclidean_distance_matrix, sorted_points_list, difference_vectors

    # get the sorted points array from the points lists
    def get_sorted_points_array(self):
        # from the interpretation of the linear programming problem results, get the distance between each of the
        # associated points (the distance an object moved between two frames), the matrix indicating these distances and
        # the sorted list of points
        # also add the .argsorted_points feature to the i_lpp which can be used to sort further properties with the
        # same shape as the points, e.g. the costs
        self.euclidean_distance_matrix, sorted_points_list, _ = \
            self.get_sorted_points_list_difference_vectors_and_distance_matrix()
        self.sorted_points_array = np.array(sorted_points_list)

    # get a pair and tuple dictionary from the sorted points array and parameters
    # inputs: p: the parameters object
    #         weight_factor_pair_finding: float, a factor determining how much the vector difference for a pair
    #                                     matters in pair finding, compared to the absolute distance between two
    #                                     kinetochores that were paired
    def get_pair_and_tuple_dict(self, p, weight_factor_pair_finding):
        if self.sorted_points_array is None:
            self.get_sorted_points_array()

        # create a sorted points array from the sorted points list and use it to find pairs from the tracking info
        self.real_pairs, self.not_real_pairs, self.sorted_by_object_points = \
            find_pairs(self.sorted_points_array[:, :], p, weight_factor=weight_factor_pair_finding)

        print("Found " + str(len(self.real_pairs)) + " real pairs and " + str(len(self.not_real_pairs)) +
              " objects that can not be matched")

        # create a pair dictionary in which every object gets assigned a pair number
        pair_dictionary = dict()
        tuple_index_dictionary = dict()
        # create unique pairs and save them to the i_lpp
        self.unique_real_pairs = list(set([tuple(sorted(t)) for t in self.real_pairs]))
        # for every unique pair, set the dictionary entry of the pair to be the pair number (number of the tuple)
        for i, pair in enumerate(self.unique_real_pairs):
            pair_dictionary[pair[0]], pair_dictionary[pair[1]] = i, i
            tuple_index_dictionary[pair[0]], tuple_index_dictionary[pair[1]] = 1, 2

        # add the quantities computed above to the linear programming problem interpretation
        self.pair_dictionary = pair_dictionary
        self.tuple_dictionary = tuple_index_dictionary

    # given a list of strings and the correct time index, get the indices of these objects in all of the lists.
    # if any of the "old_to_new" dictionaries are filled, use them to convert objects that only exist in an
    # alternative interpretation
    def get_indices_of_objects_from_list_of_strings(self, list_of_strings, time_index, from_im: int):
        if from_im == 1:
            text_points = self.text_points_im_1_list
            dict_old_to_new = self.dict_old_to_new_1st
        elif from_im == 2:
            text_points = self.text_points_im_2_list
            dict_old_to_new = self.dict_old_to_new_2nd
        else:
            raise (ValueError, "from_im has to be either 1 or 2")
        indices_list = [text_points[time_index].index(dict_old_to_new[obj]) if obj in dict_old_to_new
                        else text_points[time_index].index(obj) for obj in list_of_strings]
        return indices_list

    # replace a single point from the lpp using an alternative lpp given the time-index of relevant frames,
    # the name of the new object and old object
    # inputs: alt_ilpp: an alternative lpp object (!= self)
    #         time_index: the relevant time-slice in which the objects are located
    #         new_object_name: the name of the object that should be replaced
    #         old_object_name: object that should be removed when adding the new object at it's place
    def replace_single_point_using_alt_ilpp(self, alt_ilpp, time_index, new_object_name, old_object_name):

        # only replace the object, if it hasn't been replaced before
        if old_object_name not in self.points_removed:

            # 1) get the index of the old object in the i_lpp & replace the old text with the new text
            # 1.1) in the first image
            ix_old_im_1 = self.text_points_im_1_list[time_index].index(old_object_name)
            self.text_points_im_1_list[time_index][ix_old_im_1] = new_object_name
            # 1.2) in the second image
            if time_index - 1 >= 0:
                ix_old_im_2 = self.text_points_im_2_list[time_index - 1].index(old_object_name)
                self.text_points_im_2_list[time_index - 1][ix_old_im_2] = new_object_name
            # 2) get the index of the new object in the alt_ilpp to get the point and replace the old point with
            #    the new
            # 2.1) get the new point
            ix_new_in_alt = alt_ilpp.text_points_im_1_list[time_index].index(new_object_name)
            new_point = alt_ilpp.points_im_1_list[time_index][ix_new_in_alt]
            # 3) replace the old point with the new point
            self.points_im_1_list[time_index][ix_old_im_1] = new_point
            # 1.2) in the second image
            if time_index - 1 >= 0:
                self.points_im_2_list[time_index - 1][ix_old_im_2] = new_point

            self.points_removed.append(old_object_name)
        return

    # change a connection in the interpretation --> updating points_im_1/2_colour_list, vectors_list, costs_list, and
    # specific_costs_list. If "final_update" is set to True, the new argsorted_points, sorted_points,
    # sorted_points_array, sorted_by_object_points, pair_dictionary, tuple_dictionary, unique_real_pairs, real_pairs,
    # euclidean_distance_matrix and not_real_pairs will be calculated as well.
    # inputs: list_of_new_1st_objects
    #         list_of_new_2nd_objects:
    #         alt_ilpp: The interpretation of the alternative linear programming problem solution (also an instance of
    #                   LppInterpretation
    #         p: the parameters object
    #         final_update: only if final_update is True the parameters associated with the complete set of connections
    #                       will be updated
    #         weight_factor_pair_finding: only has to be specified for the final_update, float, a factor determining
    #                                     how much the vector difference for a pair matters in pair finding, compared
    #                                     to the absolute distance between two kinetochores that were paired
    def change_connection_in_ilpp(self, list_of_new_1st_objects, list_of_new_2nd_objects,
                                  list_of_old_1st_objects, list_of_old_2nd_objects,
                                  alt_ilpp, p, final_update=False, weight_factor_pair_finding=None):

        for l1, l2 in zip(list_of_new_1st_objects, list_of_new_2nd_objects):
            print(str(l1) + " --> " + str(l2))

        # get the time index of the objects in the first frame and check if it is the first or last frame (for these
        # exceptional cases apply, so booleans with information on whether this is the first or last frame are
        # created here
        time_index_1st_objects = math.floor(list_of_new_1st_objects[0]) - np.min(p.time_points)

        print("Time index " + str(time_index_1st_objects))

        is_first_frame, is_last_frame = False, False
        if time_index_1st_objects == 0:
            is_first_frame = True
        if time_index_1st_objects - (np.max(p.time_points) - np.min(p.time_points)) == 0:
            is_last_frame = True
        # set the variables that will not be defined in this case to None
        indices_im_1_of_new_2nd_objects, indices_im_2_of_new_1st_objects = None, None
        colours_im_1_of_new_2nd_objects, colours_im_2_of_new_1st_objects = None, None

        # if it does not exist, yet: add an attribute to the ilpp (self) that keeps track of changes
        if not (hasattr(self, 'is_changed_connection')) or (self.is_changed_connection is None):
            self.is_changed_connection = [[False for _ in list_of_obj] for list_of_obj in self.text_points_im_1_list]

        """
        """

        # check if the old and new objects are a perfect intersection (i.e. only the connections have changed) or
        # if new objects have been found
        new_only_1st_objects = set(list_of_new_1st_objects) - set(list_of_old_1st_objects)
        new_only_2nd_objects = set(list_of_new_2nd_objects) - set(list_of_old_2nd_objects)
        old_only_2nd_objects = set(list_of_old_2nd_objects) - set(list_of_new_2nd_objects)
        old_only_1st_objects = set(list_of_old_1st_objects) - set(list_of_new_1st_objects)

        if not len(new_only_1st_objects) == 0:
            print(new_only_1st_objects)
        if not len(new_only_2nd_objects) == 0:
            print(new_only_2nd_objects)

        # if objects have been changed: find the old objects that no longer are in the list
        if len(new_only_1st_objects) != 0:

            assert(len(new_only_1st_objects) == len(old_only_1st_objects))
            # if only one new object is found, it is obvious which old element should be replaced
            if len(new_only_1st_objects) == 1:
                # replace the old object by the new one
                new_object_name, old_object_name = list(new_only_1st_objects)[0], list(old_only_1st_objects)[0]

                self.replace_single_point_using_alt_ilpp(alt_ilpp, time_index_1st_objects,
                                                         new_object_name, old_object_name)
            else:
                return

        if len(new_only_2nd_objects) != 0:
            old_only_2nd_objects = set(list_of_old_2nd_objects) - set(list_of_new_2nd_objects)
            if len(new_only_2nd_objects) == 1:
                # replace the old object by the new one
                new_object_name, old_object_name = list(new_only_2nd_objects)[0], list(old_only_2nd_objects)[0]

                self.replace_single_point_using_alt_ilpp(alt_ilpp, time_index_1st_objects+1,
                                                         new_object_name, old_object_name)
            else:
                return
            assert(len(new_only_2nd_objects) == len(old_only_2nd_objects))

        # get the indices in the text_points_im_1/2_list list of the objects
        try:
            indices_im_1_of_new_1st_objects = [self.text_points_im_1_list[time_index_1st_objects].index(obj)
                                               for obj in list_of_new_1st_objects]
        except ValueError:
            print("Can't make change as object was removed before")
            return
        self.is_changed_connection[time_index_1st_objects] = \
            [self.is_changed_connection[time_index_1st_objects][index]
             if index not in indices_im_1_of_new_1st_objects else True
             for index in range(len(self.text_points_im_1_list[time_index_1st_objects]))]
        if not is_last_frame:
            indices_im_1_of_new_2nd_objects = [self.text_points_im_1_list[time_index_1st_objects+1].index(obj)
                                               for obj in list_of_new_2nd_objects]
        indices_im_2_of_new_2nd_objects = [self.text_points_im_2_list[time_index_1st_objects].index(obj)
                                           for obj in list_of_new_2nd_objects]
        if not is_first_frame:
            indices_im_2_of_new_1st_objects = [self.text_points_im_2_list[time_index_1st_objects-1].index(obj)
                                               for obj in list_of_new_1st_objects]
        # update the colour list
        # 1) get the colours of the objects in both sets of images
        colours_im_1_of_new_1st_objects = [self.points_im_1_colour_list[time_index_1st_objects][i]
                                           for i in indices_im_1_of_new_1st_objects]

        if not is_last_frame:
            colours_im_1_of_new_2nd_objects = [self.points_im_1_colour_list[time_index_1st_objects+1][i]
                                               for i in indices_im_1_of_new_2nd_objects]
        colours_im_2_of_new_2nd_objects = [self.points_im_2_colour_list[time_index_1st_objects][i]
                                           for i in indices_im_2_of_new_2nd_objects]
        if not is_first_frame:
            colours_im_2_of_new_1st_objects = [self.points_im_2_colour_list[time_index_1st_objects-1][i]
                                               for i in indices_im_2_of_new_1st_objects]
        # 2) create a dictionary translating the "old" colour into the new one
        colour_dict_old_to_new = None
        if not is_last_frame:
            colour_dict_old_to_new = dict(zip(colours_im_1_of_new_2nd_objects, colours_im_1_of_new_1st_objects))
        if not is_first_frame:
            colour_dict_old_to_new_im2 = dict(zip(colours_im_2_of_new_2nd_objects, colours_im_2_of_new_1st_objects))
            if not is_last_frame:
                assert colour_dict_old_to_new_im2 == colour_dict_old_to_new, "Dicts are not equal"
            else:
                colour_dict_old_to_new = colour_dict_old_to_new_im2

        # 3) Loop over the colours: for all frames after the time_index of the first objects:
        #    apply the change in "colour"/tracking index
        new_points_im_1_colour_list = copy.deepcopy(self.points_im_1_colour_list)
        new_points_im_2_colour_list = copy.deepcopy(self.points_im_2_colour_list)
        print("replacing all objects starting with " + str(time_index_1st_objects) + "in first im")

        print(time_index_1st_objects)

        for i in range(time_index_1st_objects+1, len(new_points_im_1_colour_list)):
            new_points_im_1_colour_list[i] = [colour_dict_old_to_new[colour_im_1]
                                              if colour_im_1 in colour_dict_old_to_new else colour_im_1
                                              for colour_im_1 in new_points_im_1_colour_list[i]]
        for i in range(time_index_1st_objects, len(new_points_im_2_colour_list)):
            new_points_im_2_colour_list[i] = [colour_dict_old_to_new[colour_im_2]
                                              if colour_im_2 in colour_dict_old_to_new else colour_im_2
                                              for colour_im_2 in new_points_im_2_colour_list[i]]
        self.points_im_1_colour_list = new_points_im_1_colour_list
        self.points_im_2_colour_list = new_points_im_2_colour_list

        # update the vectors list for the time-step in question
        for ix_im_1_new_1st_obj, ix_im_1_new_2nd_obj in \
                zip(indices_im_1_of_new_1st_objects, indices_im_1_of_new_2nd_objects):
            # get the starting and end point of the vector (the point going "into the connection and "out of the
            # connection)
            point_in = self.points_im_1_list[time_index_1st_objects][ix_im_1_new_1st_obj]
            point_out = self.points_im_1_list[time_index_1st_objects+1][ix_im_1_new_2nd_obj]
            # update the vectors-list
            self.vectors_list[time_index_1st_objects][ix_im_1_new_1st_obj] = [point_in, point_out - point_in]

        # update the costs list and specific costs list
        for obj_ix, obj in zip(indices_im_1_of_new_1st_objects, list_of_new_1st_objects):
            # 1) get the index in the lists of the alternative tracking_outcome
            alt_obj_ix = alt_ilpp.text_points_im_1_list[time_index_1st_objects].index(obj)
            # 2) replace the costs
            self.costs_list[time_index_1st_objects][obj_ix] = alt_ilpp.costs_list[time_index_1st_objects][alt_obj_ix]
            # 3) replace the specific costs with the alternative specific costs for the respective transitions
            # 3.1) get the keys of the specific costs in the i_lpp and the ones of the alt_ilpp (alternative i_lpp),
            #      and sort them depending on whether they are in both costs dictionaries or only one of both
            #      dictionaries
            set_keys = set([key for key in self.specific_costs_list])
            set_alt_keys = set([key for key in alt_ilpp.specific_costs_list])
            common_keys = set_keys.intersection(set_alt_keys)
            keys_only_ilpp = set_keys - set_alt_keys
            keys_only_alt_ilpp = set_alt_keys - set_keys
            # 3.2) Add the specific costs to the ilpp
            # 3.2.1) for specific costs that were used for both linear programming problem: simply match the costs
            for key in common_keys:
                self.specific_costs_list[key][time_index_1st_objects][obj_ix] = \
                    alt_ilpp.specific_costs_list[key][time_index_1st_objects][alt_obj_ix]
            # 3.2.2) for specific costs that only exist in the lpp but exist no more: set the value to None
            for key in keys_only_ilpp:
                self.specific_costs_list[key][time_index_1st_objects][obj_ix] = None
            # 3.2.3) for specific costs that only exist in the alternative ilpp, add lists that default to None
            #        for all values except for the new values (Note that once that if new alternative vectors with
            #        these keys are added, this list already exists and this case no longer applies, but the
            #        "common_keys" applies case instead)
            for key in keys_only_alt_ilpp:
                self.specific_costs_list[key] = []
                for time_ix in range(np.max(p.time_points) - np.min(p.time_points)):
                    self.specific_costs_list[key].append([None for _ in range(len(self.points_im_1_list[time_ix]))])
                self.specific_costs_list[key][time_index_1st_objects][obj_ix] = \
                    alt_ilpp.specific_costs_list[key][time_index_1st_objects][alt_obj_ix]

        # if this is the final update and all connections have been sorted out, also update the new argsorted_points,
        # sorted_points, sorted_points_array, sorted_by_object_points, pair_dictionary, tuple_dictionary,
        # unique_real_pairs, real_pairs, euclidean_distance_matrix and not_real_pairs
        if final_update:
            # get an array with the sorted points for further calculations
            self.get_sorted_points_array()
            # get the pair and tuple dictionary for the linear programming problem
            self.get_pair_and_tuple_dict(p, weight_factor_pair_finding)
        return


# get the sorted points list and difference vectors from a list of points and their tracking indices for both frames
# inputs: points_im_1_list: list of lists containing a np.array with coordinates, the points which were chosen in the
#                           input image of each transition
#         points_im_2_list: list of lists containing a np.array with coordinates, the points which were chosen in the
#                           output image of each transition
#         points_im_1_tracking_indices_list: list of lists of integers/floats, the tracking labels for each point in
#                                            the input image of each transition
#         points_im_2_tracking_indices_list: list of lists of integers/floats, the tracking labels for each point in the
#                                            output image of each transition
# outputs: sorted_points_list: contains the points in each frame sorted by their object number, the indexes are
#                              [time_point, object_number, spatial_dimension]
#          difference_vectors: the difference between two points that have been associated in neighbouring frames.
#                              the indices are [time_point(start), object_number, spatial_dimension]. So this array
#                              is len(time_point) - 1 long, because there is 1 transition less then there are time
#                              points
def get_sorted_points_and_vectors(points_im_1_list, points_im_1_tracking_indices_list,
                                  points_im_2_list, points_im_2_tracking_indices_list):
    # create an empty list to store the sorted points lists
    sorted_points_list = []

    # create an empty list to store the argsorted keys, with which the points or other lists that have the same
    # shape/size as the points can be sorted (e.g. the costs)
    argsorted_keys_list = []

    # len of first manual list
    len_of_first_list = None

    # create a points list for all images
    points_all_im_list = points_im_1_list + [points_im_2_list[-1]]
    colours_all_im_list = points_im_1_tracking_indices_list + [points_im_2_tracking_indices_list[-1]]

    # for every pair of points and their colours/ tracking numbers in the first image, sort
    # the points by their colours/ tracking numbers
    for points_list, colour_list in zip(points_all_im_list, colours_all_im_list):

        points_list = list(points_list)
        colour_list = list(colour_list)
        # make sure even the last lists, where not all points were tracked are displayed
        if len_of_first_list is None:
            len_of_first_list = len(points_list)

        while len(points_list) < len_of_first_list:
            points_list.append(np.array([0., 0, 0], dtype=float))
            colour_list.append(len(points_list) + 100)

        # create an argsorted list, containing the indices which would sort the colour_list
        argsorted_keys = np.argsort(colour_list)
        # sort the points using these keys and add them to the sorted points list
        sorted_points_list.append(np.array(points_list)[argsorted_keys])
        argsorted_keys_list.append(argsorted_keys)

    # turn the sorted_points_list into a numpy array, the indices correspond to
    # [time_point, object_number, spatial_dimension]
    sorted_points_array = np.array(sorted_points_list)

    # calculate the distance between associated points along the time-axis, such that the vector showing the
    # movement between
    # associated points in contained in the difference vectors
    difference_vectors = np.diff(sorted_points_array, n=1, axis=0)

    return sorted_points_list, difference_vectors, argsorted_keys_list


# plot the distances between two associated points given in the euclidean_distance_matrix over the
# time_points, save the plot in the analysis folder and display the plot if the display-option is
# chosen (display=True)
# inputs: time_points
def plot_tracking_distances_over_time_points(time_points, euclidean_distance_matrix, p, display=False):
    # if the folder "Tracking_distance_plots/" does not exist yet in the analysis folder, create a folder in which the
    # tracking distance plots are stored
    p.tracking_distance_plot_folder = p.analysis_folder + "Tracking_distance_plots/"
    try_creating_folder(p.tracking_distance_plot_folder)

    # create x-values in between the time_points, as the transitions are happening in-between frames
    x_values = [time_point - 0.5 for time_point in time_points[1:]]

    ax = None

    for i in range(len(euclidean_distance_matrix[0, :])):

        # get the euclidean distances for a single time-step
        euclidean_distances = euclidean_distance_matrix[:, i]

        # create a new plot every 10 frames
        if i % 10 == 0:
            fig, ax = plt.subplots(1, 1, figsize=(8, 4))

        # plot the euclidean distance for every object
        ax.plot(x_values, euclidean_distances, 'o', label="object" + str(i))

        # show the old plot every ten frames
        if i % 10 == 9:

            # Put a legend to the right of the current axis
            # Shrink current axis by 20%
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
            ax.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), ncol=1)

            # add a title
            plt.title("Distance between associated points over transition")

            # save the figure
            plt.savefig(p.tracking_outcome_folder + "Points " + str(i - 9) + "_to_" + str(i) + ".png")

            # show the figure, if the display option is chosen
            if display:
                plt.show()
            else:
                plt.close()
    return


# plot the costs for each pair of objects as a stacked bar plot over the frame number
def plot_pair_of_costs(i_lpp, sorted_points_df, p, create_plots=True):
    import seaborn as sns

    # get the names of all columns that are considered costs
    keys_of_costs = [key for key in i_lpp.specific_costs_list]
    keys_of_costs.append("pair_vec_diff")
    keys_of_costs.append("pair_dist")

    # get the maximum costs
    sum_of_costs = np.zeros(sorted_points_df.shape[0])
    for key in keys_of_costs:
        sum_of_costs += sorted_points_df[key]
    max_sum_of_costs = max(sum_of_costs)

    # get the sum of the pair distance plus the pair vector distance from the
    sorted_points_df["pair_plus_pair_vec_diff"] = sorted_points_df["pair_dist"] + sorted_points_df["pair_vec_diff"]

    # get the upper whisker of the boxplot of the pair plus pair vec difference
    upper_quartile = np.percentile(sorted_points_df["pair_plus_pair_vec_diff"], 75)
    lower_quartile = np.percentile(sorted_points_df["pair_plus_pair_vec_diff"], 25)
    inter_quartile = upper_quartile - lower_quartile
    upper_whisker_value = upper_quartile + 1.5 * inter_quartile

    # get all unique pair indices
    pair_indices = sorted_points_df.pair_index.unique()

    # create a histogram of the "pair difference plots" including a box-diagram
    f, (ax_box, ax_hist) = plt.subplots(2, sharex="all", gridspec_kw={"height_ratios": (.3, .7)})

    sns.boxplot(x=sorted_points_df["pair_plus_pair_vec_diff"], ax=ax_box)
    sns.histplot(sorted_points_df["pair_plus_pair_vec_diff"], ax=ax_hist)

    ax_box.set(yticks=[])
    sns.despine(ax=ax_hist)
    sns.despine(ax=ax_box, left=True)

    sns.set(style="ticks")
    sns.set_theme(style="darkgrid")
    f.savefig(p.analysis_folder + "Tracking_outcome/pair_costs_histogram_and_boxplot.png")
    plt.style.use('dark_background')

    # create plots showing the costs for each pair in the tracking
    if create_plots:
        faulty_connection_slices = sorted_points_df['faulty_connection'] == 1
        for pair_index in pair_indices:
            # create bar-plots of transition costs for each pair of objects over the frame number
            ax = plot_clustered_stacked(sorted_points_df, columns_to_plot=keys_of_costs,
                                        column_to_group_by="tuple_index", column_to_use_as_x_index="time_index",
                                        column_to_select_by="pair_index", value_to_select=pair_index,
                                        labels=keys_of_costs,
                                        title="Stacked costs for transitions of Pair " + str(pair_index))
            plt.tight_layout()
            ax.set_ylim(bottom=0, top=max_sum_of_costs)

            plt.savefig(p.analysis_folder + "Tracking_outcome/total_costs_for_pair_" + str(pair_index) + ".png",
                        bbox_inches='tight')
            plt.close()

            # create bar-plots of transition costs for each pair of objects over the frame number
            ax = plot_clustered_stacked(sorted_points_df, columns_to_plot=["pair_vec_diff", "pair_dist"],
                                        column_to_group_by="tuple_index", column_to_use_as_x_index="time_index",
                                        column_to_select_by="pair_index", value_to_select=pair_index,
                                        labels=["vector difference", "inter-pair distance"],
                                        title="Stacked costs for transitions of Pair " + str(pair_index))
            plt.tight_layout()
            # set the y-limits to the maximum sum of costs, so all plots share them and are comparable
            ax.set_ylim(bottom=0, top=max_sum_of_costs)
            # add a horizontal line with the upper whisker value to the plot
            ax.axhline(y=upper_whisker_value, xmin=0, xmax=plt.xlim()[1])
            # add a vertical line with the incorrect connection candidate to the plot, if there is one
            pair_slice = sorted_points_df['pair_index'] == pair_index
            if sorted_points_df.loc[pair_slice, 'is faulty connection'].any():
                # get the time of failure, by using the correct pair slice and taking only "faulty_connection
                # candidate" value sas there are two objects in a pair, the first entry is chosen, if there are several
                time_of_failure = sorted_points_df.loc[pair_slice & faulty_connection_slices, 'time_index'].iloc[0]
                ax.axvline(x=time_of_failure, ymin=0, ymax=max_sum_of_costs)

            # save the plot and clear the 'canvas'
            plt.savefig(p.analysis_folder + "Tracking_outcome/pair_costs_for_pair_" + str(pair_index) + ".png",
                        bbox_inches='tight')
            plt.close()

    return sorted_points_df


# create a clustered stacked bar-plot, where
def plot_clustered_stacked(df, columns_to_plot,
                           column_to_group_by, column_to_use_as_x_index,
                           column_to_select_by, value_to_select, hatching_labels=None,
                           labels=None, title="multiple stacked bar plot", hatching_pattern="/", **kwargs):
    # only get the selected portion of the dataframe
    df = df[df[column_to_select_by] == value_to_select]

    # number objects per group
    n_groups = len(df.groupby(column_to_group_by))
    n_col = len(columns_to_plot)
    n_ind = int(len(df.index) / n_groups)
    ax = plt.subplot(111)

    sub_df = None
    column_to_group_by_vals = df[column_to_group_by].unique()

    for value_to_group_by in column_to_group_by_vals:  # for each data frame

        sub_df = df[df[column_to_group_by] == value_to_group_by]

        ax = sub_df.plot(y=columns_to_plot,
                         kind="bar",
                         linewidth=0,
                         stacked=True,
                         ax=ax,
                         legend=False,
                         grid=False,
                         **kwargs)  # make bar plots
    if labels is None:
        handles, labels = ax.get_legend_handles_labels()  # get the handles we want to modify
    else:
        handles, _ = ax.get_legend_handles_labels()
    for i in range(0, n_groups * n_col, n_col):  # len(h) = n_col * n_df
        for j, pa in enumerate(handles[i:i + n_col]):
            for rect in pa.patches:  # for each index
                rect.set_x(rect.get_x() + 1 / float(n_groups + 1) * i / float(n_col))
                rect.set_hatch(hatching_pattern * int(i / n_col))  # edited part
                rect.set_width(1 / float(n_groups + 1))

    ax.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_groups + 1)) / 2.)
    ax.set_xticklabels(sub_df[column_to_use_as_x_index], rotation=0)
    if title is not None:
        ax.set_title(title)

    # Add invisible data to add another legend
    n = []
    if hatching_labels is None:
        hatching_labels_was_none = True
        hatching_labels = []
    else:
        hatching_labels_was_none = False
    for i in range(n_groups):
        if hatching_labels_was_none:
            hatching_labels.append("object " + str(i))
        n.append(ax.bar(0, 0, color="gray", hatch=hatching_pattern * i))

    # add a legend with the colours corresponding to the data
    l1 = ax.legend(handles[:n_col], labels[:n_col], loc=[1.01, 0.5])
    ax.add_artist(l1)
    # add a second legend with the hatches
    plt.legend(n, hatching_labels, loc=[1.01, 0.1])

    return ax


# adds a legend with number_of_columns columns to the given axis of a plot in the top centre of a plot
# inputs: ax: the axis (matplotlib.pyplot axis object of a matplotlib figure) to which the legend should be added
#         number_of_columns: integer, the number of columns the legend is supposed to have
# output: ax: the input axis, but with the legend added
def add_legend_at_centre_top_of_plot(ax, number_of_columns):
    # shrink current axis size by 10% at the bottom
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.15,
                     box.width, box.height * 0.85])

    # Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.0),
              fancybox=False, shadow=False, ncol=number_of_columns, facecolor='inherit', edgecolor='inherit')

    return ax


# plot the coordinates of each pair of objects over the time_points in three separate plots for each
# object (x, y and z coordinates)
# inputs: sorted_by_object_points: the points of the objects in a form where the first entry corresponds to the object,
#                                  the second to the frame number (time) and the third to the dimension (x,y,z)
#         pairs: tuples of integers indicating which of the objects form pairs
#         time_points: list or np.array, the name of the frames that were observed and are now examined
#         p: an instance of the Parameters class containing all of the parameters specified in the input file, such as
#            folder names
def plot_pairwise_distance(sorted_by_object_points, pairs, time_points, p):

    # get rid of 'double tuples' (e.g. if there is (1,3) and (3,1) only one of them is left, because they
    # contain the same information)
    unique_pairs = list(set(pairs))

    # loop over all pairs
    for pair_index, pair in enumerate(unique_pairs):

        # get the first and the second object from the list of points
        object_1 = sorted_by_object_points[pair[0]]
        object_2 = sorted_by_object_points[pair[1]]

        # get their coordinates
        coords_list = [object_1[:, 0], object_1[:, 1], object_1[:, 2],
                       object_2[:, 0], object_2[:, 1], object_2[:, 2]]
        # plot the x coordinates in blue, the y-coordinates in green and the z-coordinates in red
        # plot the first object as a full circle and the second object as a circle filled with white
        edge_colour_list = ["blue", "green", "red", "blue", "green", "red"]
        face_colour_list = ["blue", "green", "red", "white", "white", "white"]
        # assign the names to the objects
        name_list = ["x1", "y1", "z1", "x2", "y2", "z2"]

        # create a plot with a subplot for each of the dimensions
        fig, ax = plt.subplots(3, 1, figsize=(7, 10))
        # create an axis counter, that keeps track of the dimension plotted
        ax_counter = 0

        # loop over the sets of coordinates, colours and names to create the plots
        for coordinates, f_colour, e_colour, name in zip(coords_list, face_colour_list, edge_colour_list, name_list):
            # plot the coordinates in one dimension with the given colours, labels and coordinates
            ax[ax_counter].scatter(time_points, coordinates, marker="o", facecolors=f_colour, edgecolors=e_colour,
                                   label=name)
            # add a legend for this point
            ax[ax_counter] = add_legend_at_centre_top_of_plot(ax[ax_counter], len(coords_list))
            # set the limits to correspond to the maximum dimensions in this direction (the min and max volume where
            # points where found)
            ax[ax_counter].set_ylim(np.min(sorted_by_object_points[:, :, ax_counter]) - 10,
                                    np.max(sorted_by_object_points[:, :, ax_counter]) + 10)
            ax[ax_counter].set_xlabel("Frame Number")
            ax[ax_counter].set_ylabel("Coordinates")
            # increase the axis counter to keep track of the position
            ax_counter = (ax_counter + 1) % 3

        # add a title to the plot
        pair_name = "Pair " + str(pair_index)
        fig.suptitle(pair_name)
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        plt.savefig(p.tracking_pair_plot_folder + pair_name + ".png")
        plt.savefig(p.tracking_pair_plot_folder + pair_name + "_low_res.png", dpi=40)
        plt.close()

    return


# find pairs of two objects moving in a similar fashion closely together
# inputs: sorted_points_array: numpy array, the indices correspond to [time_point, object_number, spatial_dimension] for
#                              each object
#         time_points: list or np.array, the name of the frames that were observed and are now examined
#         p: an instance of the Parameters class containing all of the parameters specified in the input file, such as
#            folder names
#         weight_factor: how the difference in spatial position VS in movement should be weighted, the total spatial
#                        position difference is weighted as 1, the total difference in the movement vectors between two
#                        points is scaled by the 'weight_factor'
#         display_real_pairs: bool indicating whether or not the plot of 'real pairs' (see "outputs") coordinates
#                             over the frame number should be displayed (it is created and stored in the analysis folder
#                             in the "Tracking_Pair_Plots" folder anyway
#         display_not_real_pairs: bool indicating whether or not the plot of 'not real pairs' (see "outputs")
#                                 coordinates over the frame number should be displayed (it is created and stored in
#                                 the analysis folder in the "Tracking_Pair_Plots" folder anyway
# outputs: real_pairs: tracks for which for every object the partner most closely associated with associates this object
#                      most closest (e.g. object 1 is closest to object 3 and object 3 is also closest to object 1)
#          not_real_pairs: tracks for which one object is most closely associated with another object, which is in
#                          turn associated most closely with a third object. (e.g. object 3 is closest to object 1,
#                          but object 1 is closest to object 5)
def find_pairs(sorted_points_array, p, weight_factor=1.):
    # get the differences between the points locations themselves

    # get the object differences in a form where the first entry corresponds to the object, the second to the frame
    # number (time)
    # and the third to the dimension (x,y,z)
    sorted_by_object_points = (np.swapaxes(sorted_points_array, 0, 1))
    # only take the time steps until anaphase onset as a measure, if there is a point of anaphase onset
    if p.anaphase_onset is not None:
        sorted_by_object_points_until_anaphase = sorted_by_object_points[:, :p.anaphase_onset]
    else:
        sorted_by_object_points_until_anaphase = sorted_by_object_points
    # flatten the spatial dimensions, as they will be treated equally for the difference measure
    sorted_by_object_points_flat_spatial_dimensions = \
        sorted_by_object_points_until_anaphase.reshape(sorted_by_object_points_until_anaphase.shape[0],
                                                       sorted_by_object_points_until_anaphase.shape[1] *
                                                       sorted_by_object_points_until_anaphase.shape[2])
    # get a difference matrix with the pairwise euclidean distance between all direction vectors of all time points
    points_difference_matrix = squareform(pdist(sorted_by_object_points_flat_spatial_dimensions, 'euclidean'))
    # add the maximum to the diagonal of the difference matrix to make sure the objects are not assigned to themselves
    points_difference_matrix += np.max(points_difference_matrix) * np.identity(points_difference_matrix.shape[0])

    # add the difference matrix between the tracked points direction vectors
    sorted_by_object_direction_vectors = np.diff(sorted_by_object_points_until_anaphase, n=1, axis=1)
    sorted_by_object_direction_vectors_flat_spatial_dimensions = \
        sorted_by_object_direction_vectors.reshape(sorted_by_object_direction_vectors.shape[0],
                                                   sorted_by_object_direction_vectors.shape[1] *
                                                   sorted_by_object_direction_vectors.shape[2])
    # get a difference matrix with the pairwise euclidean distance between all direction vectors of all time points
    direction_vectors_difference_matrix = squareform(
        pdist(sorted_by_object_direction_vectors_flat_spatial_dimensions, 'euclidean'))
    # add the maximum to the diagonal of the difference matrix to make sure the objects are not assigned to themselves
    direction_vectors_difference_matrix += np.max(direction_vectors_difference_matrix) * np.identity(
        direction_vectors_difference_matrix.shape[0])

    # create a distance matrix adding the direction differences and the spatial distances
    difference_matrix = points_difference_matrix + weight_factor * direction_vectors_difference_matrix

    # sign the linear assignment problem, where each object has to be assigned to one other object
    solution_to_linear_sum_assignment = linear_sum_assignment(difference_matrix)

    # get the pairs from the linear sum assignment in tuple form
    pairs = [(first_val, second_val) for first_val, second_val in zip(solution_to_linear_sum_assignment[0],
                                                                      solution_to_linear_sum_assignment[1])]
    # get the reverse version of each pair
    reverse_pairs = [(second_val, first_val) for first_val, second_val in zip(solution_to_linear_sum_assignment[0],
                                                                              solution_to_linear_sum_assignment[1])]

    # check if the reverse version is already contained in 'pairs' to make sure for every object associated with its
    # partner the reverse is also True (e.g. object 1 is closest to object 3 and object 3 is also closest to object 1),
    # for the "real_pairs" this is True, for the "not_real_pairs" the objects are not associated with their partner in
    # both ways (e.g. object 3 is closest to object 1, but object 1 is closest to object 5)
    real_pairs = [pair for pair in pairs if pair in reverse_pairs]
    not_real_pairs = [pair for pair in pairs if pair not in reverse_pairs]

    return real_pairs, not_real_pairs, sorted_by_object_points


# compare a list of vectors (vl1) to its closest pendant in another list of vectors(vl2) using a KDTree
# if the difference is bigger than some number num_diff, return a one in its position 1, otherwise return 0
# inputs: vl1: list or array of vectors (shape: N, 2, D [list of N vectors, where the start and stop points of
#              dimension D are stored separately)
#         vl2: list or array of vectors (shape: M, 2, D [list of M vectors, where the start and stop points of
#              dimension D are stored separately)
#         num_diff: the difference above which a 1 should be returned
#         plot: boolean, whether or not a plot should be shown for each of the differences (can help to adjust num_diff)
# output: has_no_close_pendant_list: a list of length N containing boolean values indicating if the difference between
#         a vector in vl1 and its closest pendant in vl2 (according to the KDTree algorithm) are further apart than
#         num_diff or not
def compare_vector_in_vl1_to_vector_in_vl2(vl1, vl2, num_diff=3, plot=False):
    # reshape the vectors list to feed it to the KDTree
    vector_list_1, vector_list_2 = np.array(vl1), np.array(vl2)

    old_shape_1, old_shape_2 = vector_list_1.shape[1], vector_list_1.shape[2]
    vector_list_1 = np.reshape(vector_list_1,
                               newshape=(vector_list_1.shape[0], old_shape_1 * old_shape_2))
    vector_list_2 = np.reshape(vector_list_2,
                               newshape=(vector_list_2.shape[0], old_shape_1 * old_shape_2))

    tree = spatial.KDTree(vector_list_2)
    has_no_close_pendant_list = []
    for vector in vector_list_1:
        query_result = tree.query(vector)
        if query_result[0] > num_diff:
            has_no_close_pendant_list.append(1)
        else:
            has_no_close_pendant_list.append(0)

    if plot:
        query_results_measure = []
        for vector in vector_list_1:
            query_results_measure.append(tree.query(vector)[0])
        plt.scatter(range(len(query_results_measure)), query_results_measure)
        plt.show()

    return has_no_close_pendant_list


# create a distance plot between the manual and the automated tracking points
# inputs: sorted_automated_points: points found by the automated tracking as a numpy array in the form
#         [time_point, object_number, spatial_dimension]
#         sorted_manual_points: points found by manual tracking as a numpy array in the form
#         [time_point, object_number, spatial_dimension]
def plot_distance_between_manual_and_automated_tracking_points(sorted_automated_points, sorted_manual_points, p,
                                                               display=False):
    from scipy.spatial.distance import cdist
    corresponding_object_no_dict = {}
    for i, automated_point_first_frame in enumerate(sorted_automated_points[0]):
        corresponding_object_number = np.argmin(cdist(np.array([automated_point_first_frame]), sorted_manual_points[0]))
        corresponding_object_no_dict[i] = corresponding_object_number

    # create a plot of object distances over the frame number
    plt.close()
    fig, ax = plt.subplots(2, 1, figsize=(20, 8), gridspec_kw={'height_ratios': [4, 1]})
    first_distance_bigger_one_point_five_max_first_frame = []
    correct_tracks = 0
    total_number_of_objects = sorted_automated_points.shape[1]
    total_number_of_time_points = sorted_manual_points.shape[0]

    # get the distances in the first frame and 1.5 times their maximum value
    distances_in_first_frame = []
    for object_no in range(total_number_of_objects):
        distances_in_first_frame.append(np.diag(cdist(sorted_automated_points[0, object_no, :].reshape(1, -1),
                                                      sorted_manual_points[0,
                                                      corresponding_object_no_dict[object_no], :].reshape(1, -1)))[0])
    one_point_five_max_first_frame = 1.5 * float(np.max(distances_in_first_frame))

    # for every object, find the distance between the automated nad the manual point and the first frame where
    # the object is lost
    for object_no in range(total_number_of_objects):
        # calculate the distance between the automatically found point and its equivalent (only look at the diagonal
        # entries of the cdist, as only distances at the same time step matter
        obj_distances = np.diag(cdist(sorted_automated_points[:, object_no, :],
                                      sorted_manual_points[:, corresponding_object_no_dict[object_no], :]))
        # find the frames where the distance is bigger than 1.5*max_distance_in_first_frame (if any)
        where_obj_dist_bigger_one_point_five_max_first_frame = np.where(obj_distances >
                                                                        one_point_five_max_first_frame)[0]
        # if there are any frames where the distance between the automatically found object and the manually tracked
        # point, add this first frame to the 'first_distance_bigger-on_point_five_max_first_frame' list
        if where_obj_dist_bigger_one_point_five_max_first_frame.size > 0:
            first_distance_bigger_one_point_five_max_first_frame.append(
                np.min(where_obj_dist_bigger_one_point_five_max_first_frame))
        # otherwise, conclude that this is a correct track and append the value -1 instead to the
        # 'first_distance_bigger-on_point_five_max_first_frame' list
        else:
            correct_tracks += 1
            first_distance_bigger_one_point_five_max_first_frame.append(-1)
        ax[0].plot(obj_distances, label="object" + str(object_no))
    # Put a legend to the right of the current axis
    # Shrink current axis by 20%
    box = ax[0].get_position()
    ax[0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax[0].legend(loc='center left', bbox_to_anchor=(1.0, 0.5), ncol=4)

    # add a title
    ax[0].title.set_text("Distance between automated and manual points over transition")

    # add a bar plot below with the number of correct tracks

    # create green and red bars to indicate how many objects have been tracked correctly
    correct_bar = ax[1].barh(0, int(correct_tracks), color='#007B0B', height=0.2, edgecolor='white',
                             label="correct tracks")  # green bar
    # red bar
    incorrect_bar = ax[1].barh(0, int(total_number_of_objects - correct_tracks), color='#AF0F0F',
                               left=int(correct_tracks), height=0.2, edgecolor='white',
                               label="incorrect tracks")  # red bar
    for bar_obj in [correct_bar, incorrect_bar]:
        height, width = bar_obj[0].get_height(), bar_obj[0].get_width()
        ax[1].text(bar_obj[0].get_x() + width / 2, bar_obj[0].get_y() + height / 2, str(int(width)),
                   ha='center', color='white')
    # add labels to the plot
    box = ax[1].get_position()
    ax[1].set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax[1].legend(loc='center left', bbox_to_anchor=(1.0, 0.5), ncol=1)
    ax[1].axis("off")

    # save the figure
    plt.savefig(p.tracking_outcome_folder + "dist_auto_to_manual_points.png")
    # show the figure, if the display option is chosen
    plt.close()

    # create a histogram frames where the distance is bigger than 1.5 times the maximum distance in the first frame
    # for the first time, i.e. a histogram in which frame the objects are lost
    plt.hist(first_distance_bigger_one_point_five_max_first_frame, bins=range(total_number_of_time_points))
    plt.title("first distance bigger than 1.5x max distance in first frame")
    plt.savefig(p.tracking_distance_plot_folder + "dist_smaller_1p5_times_max_dist_first_frame.png")
    # show the figure, if the display option is chosen
    if display:
        plt.show()
    else:
        plt.close()

    return first_distance_bigger_one_point_five_max_first_frame


# use the outputs of the pulp minimisation step to assign points to their tracks in a sensible manner
# inputs: ins_and_outs: the inputs and outputs for each connection that was added to the linear programming problem
#                        (e.g. a connection between object 3 at time point 50 and object 7 at time point 51 would be
#                         50.003 --> 51.007, so 50.003 would be the input and 51.007 the output). Split events
#                         allow for two outputs, merge events for two inputs.
#         time_points: list or np.array, the name of the frames that were observed and are now examined
#         lpp: the pulp linear programming problem which was created and solved before and now contains the solution
#         pulp_var_dict: a dictionary containing the pulp variable names for the connections and the names in the form
#                        as stated above to help interpreting the problem (pulp changes some strings such as + or -, so
#                        this approach is chosen to keep track of the linear programming problem more accurately) and
#                        the costs for each connection
#         points_1_list: list of lists containing the points in the input image for each transition
#         points_2_list: list of lists containing the points in the output image for each transition
#         points_1_is_split_list: booleans indicating if this point is the result of a split event in an earlier
#                                 processing step, with the same shape as points_1_list
#         points_2_is_split_list: booleans indicating if this point is the result of a split event in an earlier
#                                 processing step, with the same shape as points_2_list
# outputs: i: instance of the LppInterpretation class, attributes: see at the top of this file
def interpret_the_lpp_results(ins_and_outs, time_points, lpp, pulp_dict,
                              points_1_list, points_2_list, points_1_is_split_list, points_2_is_split_list):
    # create flat versions of the inputs and outputs and find all of the points that were chosen at
    # some point
    in_1, in_2, out_1, out_2 = ins_and_outs
    flat_in_1, flat_in_2 = [i for sublist in in_1 for i in sublist], [i for sublist in in_2 for i in sublist]
    flat_out_1, flat_out_2 = [i for sublist in out_1 for i in sublist], [i for sublist in out_2 for i in sublist]
    chosen_ins_1, chosen_ins_2, chosen_outs_1, chosen_outs_2 = [], [], [], []
    chosen_costs = []
    chosen_specific_costs = {}
    for key in pulp_dict["detailed_cost"]:
        chosen_specific_costs[key] = []

    # create a dictionary to assign colours to all objects
    colour_dict = {}

    may_have_second_in, may_have_second_out = False, False
    current_colour = None
    second_object_number_in, chosen_in_2, second_object_number_out, chosen_out_2 = None, None, None, None

    # get the indices of the rows corresponding to the PulP linear programming variables
    for variable in lpp.variables():
        # if the variable was chosen and is part of the problem's solution, find out which input point and output
        # point it belongs to
        if variable.varValue is not None and variable.varValue > 0:
            # find the corresponding index (be aware that PulP converts 'signs' such as underscores, plus and minus
            # signs to spaces, so only one of them can be used in the variable names and they have to be replaced
            # for matching).
            # This is done using the pulp variable dictionary that was created with the linear programming problem
            # to keep track of the variables more easily.
            index = pulp_dict["index"][variable.name.replace("_", " ")]
            # find the input point corresponding to the index
            chosen_ins_1.append(flat_in_1[index])
            # if this is the result of a merge event, find the second input point
            if len(flat_in_2) > 0:  # check if merges are possible at all
                may_have_second_in = True
                chosen_ins_2.append(flat_in_2[index])  # find the potential second input point
            # find the output point corresponding to the index
            chosen_outs_1.append(flat_out_1[index])
            # if this is the result of a split event, find the second output point
            if len(flat_out_2) > 0:  # check if splits are possible at all
                may_have_second_out = True
                chosen_outs_2.append(flat_out_2[index])  # find the potential second output point
            # add the costs of this transition to the costs list:
            chosen_costs.append(pulp_dict["cost"][variable.name.replace("_", " ")])
            for key in pulp_dict["detailed_cost"]:
                chosen_specific_costs[key].append(pulp_dict["detailed_cost"][key][variable.name.replace("_", " ")])

    # sort the chosen ins by their number, such that the first frames come first and the tracking indices can be
    # matched in the correct time order
    argsorted_chosen_ins = np.argsort(chosen_ins_1)
    chosen_ins_1 = np.array(chosen_ins_1)[argsorted_chosen_ins]
    chosen_outs_1 = np.array(chosen_outs_1)[argsorted_chosen_ins]
    if may_have_second_in:
        chosen_ins_2 = np.array(chosen_ins_2)[argsorted_chosen_ins]
    if may_have_second_out:
        chosen_outs_2 = np.array(chosen_outs_2)[argsorted_chosen_ins]
    # sort the costs (total and specific)
    chosen_costs = np.array(chosen_costs)[argsorted_chosen_ins]
    for key in pulp_dict["detailed_cost"]:
        chosen_specific_costs[key] = np.array(chosen_specific_costs[key])[argsorted_chosen_ins]

    # create lists to store the values of the points in each image, their tracking index / colour and an indicator
    # whether they are the result of a split event or not and vectors connecting the input to the output point
    points_im_1_list = [[] for _ in range(len(time_points) - 1)]
    points_im_2_list = [[] for _ in range(len(time_points) - 1)]

    text_points_im_1_list = [[] for _ in range(len(time_points) - 1)]
    text_points_im_2_list = [[] for _ in range(len(time_points) - 1)]

    points_im_1_colour_list = [[] for _ in range(len(time_points) - 1)]
    points_im_2_colour_list = [[] for _ in range(len(time_points) - 1)]

    points_im_1_is_split_list = [[] for _ in range(len(time_points) - 1)]
    points_im_2_is_split_list = [[] for _ in range(len(time_points) - 1)]

    vectors_list = [[] for _ in range(len(time_points) - 1)]
    costs_list = [[] for _ in range(len(time_points) - 1)]
    specific_costs_list = {}
    for key in pulp_dict["detailed_cost"]:
        specific_costs_list[key] = [[] for _ in range(len(time_points) - 1)]

    # create a counter that counts the object in the first frame (the initial object colour / tracking index)
    object_counter_first_frame = 0

    # loop over the pairs of chosen points and get the relations between them
    for i, (chosen_in_1, chosen_out_1, cost_for_transition) in \
            enumerate(zip(chosen_ins_1, chosen_outs_1, chosen_costs)):

        # get the frame number and object number of each object from its label
        frame_number_in, object_number_in = int(chosen_in_1), round((chosen_in_1 % 1) * 1000)
        frame_number_out, object_number_out = int(chosen_out_1), round((chosen_out_1 % 1) * 1000)

        # if merge events are possible (if there can be a second input), get the second input point, if possible
        if may_have_second_in:
            print("may have second input")
            chosen_in_2 = chosen_ins_2[i]
            # check if it not None and not a float 'nan' (second part of the logical expression)
            if chosen_in_2 is not None and not chosen_in_2 != chosen_in_2:
                second_object_number_in = round((chosen_in_2 % 1) * 1000)
            else:
                second_object_number_in = None

        # if split events are possible (if there can be a second output), get the second output point, if possible
        if may_have_second_out:
            print("can have second output")
            chosen_out_2 = chosen_outs_2[i]
            # check if it not None and not a float 'nan' (second part of the logical expression)
            if chosen_out_2 is not None and not chosen_out_2 != chosen_out_2:
                second_object_number_out = round((chosen_out_2 % 1) * 1000)
            else:
                second_object_number_out = None

        # if this is not a split, merge, disappear or appear event
        if frame_number_in > 0 and frame_number_out > 0:

            # get the frame index of the ingoing and outgoing frame
            frame_index_in, frame_index_out = time_points.index(frame_number_in), time_points.index(frame_number_out)

            # if this is not the first frame, look into the colour/ tracking index dictionary to find the corresponding
            # tracking index/ colour from the previous frame
            if not frame_number_in == min(time_points):
                try:
                    current_colour = colour_dict[chosen_in_1]
                except KeyError:
                    print("Cannot find the matching tracking index / colour for the object " + str(chosen_in_1) +
                          " in the following dictionary " + str(colour_dict))
                    print(frame_number_in)
                    print(time_points)
                    raise KeyError("Key error while looking for current colour in the colour dictionary.")
            # if this is the first frame, set the colour/ tracking index to be an integer and increase the object
            # counter by 1
            else:
                current_colour = object_counter_first_frame
                object_counter_first_frame += 1

            # add a dictionary entry to the tracking index/ colour dictionary for the output object
            colour_dict[chosen_out_1] = current_colour

            # find the corresponding points and split indices and append them to the points list for the respective
            # frame. Also keep track of the tracking indices/ colours by adding them to their list and vectors
            # connecting each input object with each output object
            try:
                # for the input object
                point_in = points_1_list[frame_index_in][object_number_in]
                is_split_index_in = points_1_is_split_list[frame_index_in][object_number_in]
                points_im_1_list[frame_index_in].append(point_in)
                points_im_1_is_split_list[frame_index_in].append(is_split_index_in)
                points_im_1_colour_list[frame_index_in].append(current_colour)
                text_points_im_1_list[frame_index_in].append(chosen_in_1)

                # for output object
                point_out = points_2_list[frame_index_in][object_number_out]
                points_im_2_list[frame_index_in].append(point_out)
                is_split_index_out = points_2_is_split_list[frame_index_in][object_number_out]
                points_im_2_is_split_list[frame_index_in].append(is_split_index_out)
                points_im_2_colour_list[frame_index_in].append(current_colour)
                text_points_im_2_list[frame_index_in].append(chosen_out_1)

                # create vectors pointing from corresponding points to one another
                vectors_list[frame_index_in].append([point_in, point_out - point_in])
                # store the associated costs
                costs_list[frame_index_in].append(cost_for_transition)
                for key in pulp_dict["detailed_cost"]:
                    specific_cost = chosen_specific_costs[key][i]
                    specific_costs_list[key][frame_index_in].append(specific_cost)

                # if this is the result of a merge, also add the second input object to the list as well as the
                # corresponding vector
                if second_object_number_in is not None and second_object_number_in != 0:
                    second_point_in = points_1_list[frame_index_in][second_object_number_in]
                    second_is_split_index_in = points_1_is_split_list[frame_index_in][second_object_number_in]
                    points_im_1_list[frame_index_in].append(second_point_in)
                    points_im_1_is_split_list[frame_index_in].append(second_is_split_index_in)
                    points_im_1_colour_list[frame_index_in].append(current_colour)
                    text_points_im_1_list[frame_index_in].append(chosen_in_2)
                    vectors_list[frame_index_in].append([second_point_in, point_out - second_point_in])

                # if this is the result of a split, also add the second input object to the list, make sure the
                # objects resulting from this object have a similar, but slightly distinct 'color'/ index by increasing
                # the counter by 0.1. Also add the other associated properties (e.g. the vector) to the corresponding
                # lists
                if second_object_number_out is not None and second_object_number_out != 0:
                    second_point_out = points_2_list[frame_index_in][second_object_number_out]
                    second_is_split_index_out = points_2_is_split_list[frame_index_in][second_object_number_out]
                    points_im_2_list[frame_index_in].append(second_point_out)
                    points_im_2_is_split_list[frame_index_in].append(second_is_split_index_out)
                    points_im_2_colour_list[frame_index_in].append(current_colour)
                    text_points_im_2_list[frame_index_in].append(chosen_out_2)
                    vectors_list[frame_index_in].append([point_in, second_point_out - point_in])
                    colour_dict[chosen_out_2] = current_colour + 0.1

            # if for some reason the corresponding input/ output objects can not be found in the respective frames,
            # raise an IndexError and print some information
            except IndexError:
                print("frame index in: " + str(frame_index_in))
                print("frame index out: " + str(frame_index_out))
                print("object number in: " + str(object_number_in))
                print("object number out: " + str(object_number_out))
                print("len of points 1: " + str(len(points_1_list[frame_index_in])))
                print("len of points 2: " + str(len(points_2_list[frame_index_in])))
                print("len of im 1 is split: " + str(len(points_1_is_split_list[frame_index_in])))
                print("len of im 2 is split: " + str(len(points_2_is_split_list[frame_index_in])))
                raise IndexError("An IndexError occurred when indexing the points using the" +
                                 "frame number and the object number")

    i = LppInterpretation(points_im_1_list, points_im_2_list,
                          points_im_1_colour_list, points_im_2_colour_list,
                          points_im_1_is_split_list, points_im_2_is_split_list,
                          text_points_im_1_list, text_points_im_2_list,
                          vectors_list, costs_list, specific_costs_list,
                          current_colour)

    return i


# display the outcome of a single tracking step in a napari viewer from the information in the connection matrix,
# the solution matrix and the two images
# inputs: lpp: the linear programming problem, as defined in 'solve_min_cost_flow_with_exclusion_sets'
#         point_objects: a list of DetectedObjectsList which contain e.g. the detected object centres for each frame and
#                        information on these points
#         ins_and_outs: each connection has its own column in the linear programming problem, the ins and outs define
#                       which objects are at the input and which objects are at the output of some connection
#         points_1_list: a nested list containing all points that were given to the lpp corresponding to the input
#                        frame for each transition
#         points_2_list: a nested list containing all points that were given to the lpp corresponding to the output
#                        frame for each transition
#         points_1_is_split_list: a list of the same shape as points_1_list that for each point indicates whether (2)
#                                 or not (0) it is the result of splitting one of the earlier detected objects
#         points_2_is_split_list: a list of the same shape as points_2_list that for each point indicates whether (2)
#                                 or not (0) it is the result of splitting one of the earlier detected objects
#         pulp_var_dict: a dictionary relating the pulp variable names to their indices in the solution vector list
#         time_points: list or np.array, the name of the frames that were observed and are now examined
#         display: whether or not to display the results using napari
def interpret_lpp(lpp, ins_and_outs,
                  points_1_list, points_2_list,
                  points_1_is_split_list, points_2_is_split_list,
                  pulp_dict, p, weight_factor_pair_finding=1.0):
    # ToDo: Add interpretation of appear and disappear (also vectors for displaying)
    # create basic lists with the points in each frame from the output of the linear programming problem
    i_lpp = interpret_the_lpp_results(ins_and_outs, p.time_points, lpp, pulp_dict,
                                      points_1_list, points_2_list,
                                      points_1_is_split_list, points_2_is_split_list)

    # get an array with the sorted points for further calculations
    i_lpp.get_sorted_points_array()

    # get the pair and tuple dictionary for the linear programming problem
    i_lpp.get_pair_and_tuple_dict(p, weight_factor_pair_finding)
    return i_lpp


# create plots helping to interpret the linear programming problem
def plot_tracking_outcome(i_lpp, p, time_points):
    p.tracking_outcome_folder = p.analysis_folder + "Tracking_Outcome_Plots/"
    try_creating_folder(p.tracking_outcome_folder)

    # plot a histogram of the costs
    flat_costs_list = [cost for sublist in i_lpp.costs_list for cost in sublist]
    plt.hist(flat_costs_list)
    plt.title("histogram of costs for transitions")
    plt.savefig(p.tracking_outcome_folder + "histogram_transition_costs.png")

    # plot the tracking distances over the time points given to the problem
    plot_tracking_distances_over_time_points(p.time_points[:], i_lpp.euclidean_distance_matrix[:, :], p)

    # if the display option is chosen for the real_pairs or not_real_pairs, print a quick reminder which of them is
    # displayed and plot the coordinates of each object in the pair over the frame number
    plot_pairwise_distance(i_lpp.sorted_by_object_points, i_lpp.real_pairs, time_points, p)
    plot_pairwise_distance(i_lpp.sorted_by_object_points, i_lpp.not_real_pairs, time_points, p)

    return
