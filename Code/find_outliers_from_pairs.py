from interpret_lpp import find_pairs

from get_connections_for_connection_matrix import get_n_nearest_neighbours_of_each_obj_in_next_image
from os_functions import try_creating_folder

import numpy as np
import sys
import pandas as pd
from tqdm import tqdm

from scipy.spatial.distance import cdist
from scipy.stats import mode
import matplotlib.pyplot as plt


# reshape a list of vectors such that it can be interpreted as a n-dimensional vectors-layer
# by the napari viewer
# input: vectors_list: list of np.array that contains points
# output: vectors_nd: np.array that can be interpreted as n-dimensional points layer by napari
def get_nd_vectors_layer_napari(vectors_list):
    vectors_nd = np.concatenate(
        [np.concatenate((np.full((len(pts), 1), i), pts), axis=1) for i, pts in enumerate(vectors_list)],
        axis=0,
    )
    return vectors_nd


# extend the pair and tuple dicts to account for the fact that some objects can't be matched
# create extended pair dictionary that links "non-pairs" to -1
# inputs: initial_pair_dict: the initial pair dictionary, where some elements do not have an entry, if they are not in a
#                            pair
#         initial_tuple_dict: the initial tuple dictionary, where some elements do not have an entry, if they are not in
#                             a pair
#         p: parameters object (see read_in_config_file)
# outputs: extended_pair_dict: the updated pair dictionary, where points that do not belong to a pair yield a -1
#          extended_tuple_dict: the updated tuple dictionary, where points that do not belong to a pair yield a -1
def extend_pair_and_tuple_dict(initial_pair_dict, initial_tuple_dict, p):
    extended_pair_dict, extended_tuple_dict = initial_pair_dict, initial_tuple_dict
    added_key = -1
    for i in range(p.number_expected):
        try:
            initial_pair_dict[i], initial_tuple_dict[i] = initial_pair_dict[i], initial_tuple_dict[i]
        except KeyError:
            extended_pair_dict[i], extended_tuple_dict[i] = added_key, -1
            added_key -= 1
    return extended_pair_dict, extended_tuple_dict


# create an excel and a pandas dataframe of the linear programming problem solution
def create_excel_with_results(i_lpp, p, excel_name="points_excel.xlsx", weight_factor_pair_finding=1.0, batch_size=10):
    # create a dictionary with an empty list for "costs" and all "specific costs" and the pair indices for the local,
    # batch analysis, this dictionary will later be converted into a pandas dataframe to export it as an .xslx file
    sorted_dict = {"time_index": [], "pair_index": [], "tuple_index": [],
                   "object_index": [], "object_index_to": [], "object_index_is": [], "object_index_from": [],
                   "x_pos_ROI": [], "y_pos_ROI": [], "z_pos_ROI": [],
                   "x_pos_global": [], "y_pos_global": [], "z_pos_global": [],
                   "x_vector": [], "y_vector": [], "z_vector": [],
                   "costs": []}
    for key in i_lpp.specific_costs_list:
        sorted_dict[key] = []

    # find pairs in batches of 10
    print("finding pairs in batches")
    i_lpp = find_pairs_in_batches_for_reconstruction(i_lpp, p, batch_size,
                                                     weight_factor_pair_finding=weight_factor_pair_finding)
    # add dictionary keys for the batch dicts to the sorted dictionary
    for j, batch_name in enumerate(i_lpp.batch_names):
        sorted_dict["pair_index" + batch_name], sorted_dict["tuple_index" + batch_name] = [], []

    # extend the pair and tuple dict such that they also link to objects that are not in any pair are linked to negative
    # numbers
    i_lpp.extended_pair_dict, i_lpp.extended_tuple_dict = extend_pair_and_tuple_dict(i_lpp.pair_dictionary,
                                                                                     i_lpp.tuple_dictionary, p)

    # first sort all points and costs by their tracking indices
    for i, argsorted_points in enumerate(i_lpp.argsorted_points[:-1]):

        # get the length of the current costs/points list
        length = len(i_lpp.costs_list[i])

        # add the time index to the dictionary
        sorted_dict["time_index"].append([i] * length)
        # add the object index to the list
        sorted_dict["object_index"].append(list(range(0, length)))

        # add the pair and tuple indices to the sorted_points list
        sorted_dict["pair_index"].append([i_lpp.extended_pair_dict[obj] for obj in list(range(0, length))])
        sorted_dict["tuple_index"].append([i_lpp.extended_tuple_dict[obj] for obj in list(range(0, length))])

        # add the local, batch pair indices to the sorted_points_list
        for j, (batch_pair_dict, batch_tuple_dict, batch_name, batch_range) in enumerate(
                zip(i_lpp.extended_batch_pair_dicts, i_lpp.extended_batch_tuple_dicts, i_lpp.batch_names,
                    i_lpp.batch_ranges)):
            # only find the pair and tuple indices for each batch, if the time_point is within the batch range:
            if batch_range[0] <= min(p.time_points) + i <= batch_range[-1]:
                sorted_dict["pair_index" + batch_name].append([batch_pair_dict[obj]
                                                               for obj in list(range(0, length))])
                sorted_dict["tuple_index" + batch_name].append([batch_tuple_dict[obj]
                                                                for obj in list(range(0, length))])
            else:
                sorted_dict["pair_index" + batch_name].append([None] * length)
                sorted_dict["tuple_index" + batch_name].append([None] * length)

        # add the sorted points for each coordinate to the dictionary
        # (indices [time_point, object_number, spatial_dimension])
        sorted_dict["x_pos_ROI"].append(i_lpp.sorted_points_array[i, :, 0])
        sorted_dict["y_pos_ROI"].append(i_lpp.sorted_points_array[i, :, 1])
        sorted_dict["z_pos_ROI"].append(i_lpp.sorted_points_array[i, :, 2])

        # add the global position of the point (with taking into account the ROI)
        # to the dictionary
        sorted_dict["x_pos_global"].append(i_lpp.sorted_points_array[i, :, 0] + p.roi_xmin)
        sorted_dict["y_pos_global"].append(i_lpp.sorted_points_array[i, :, 0] + p.roi_ymin)
        sorted_dict["z_pos_global"].append(i_lpp.sorted_points_array[i, :, 0] + p.roi_zmin)

        # resort the argsorted points then find the vectors of movement by subtracting the spatial positions in
        # adjacent frames
        sorted_by_object_points_at_i = i_lpp.sorted_points_array[i, :, :]
        try:
            sorted_by_object_points_at_i_plus_1 = i_lpp.sorted_points_array[i + 1, :, :]
            vectors = sorted_by_object_points_at_i_plus_1 - sorted_by_object_points_at_i
        # set the vector for the last time slot to 0
        except IndexError:
            vectors = np.zeros(shape=sorted_by_object_points_at_i.shape)
        sorted_dict["x_vector"].append(vectors[:, 0])
        sorted_dict["y_vector"].append(vectors[:, 1])
        sorted_dict["z_vector"].append(vectors[:, 2])

        # sort the costs using the argsorted points
        sorted_costs = np.array(i_lpp.costs_list[i])[argsorted_points]
        sorted_dict["costs"].append(sorted_costs)
        # sort all the special using the argsorted points
        for key in i_lpp.specific_costs_list:
            specific_costs_sorted = np.array(i_lpp.specific_costs_list[key][i])[argsorted_points]
            sorted_dict[key].append(specific_costs_sorted)

        # sort the in and outgoing objects using the argsorted points
        sorted_object_from = np.array(i_lpp.text_points_im_1_list[i])[argsorted_points]
        sorted_object_to = np.array(i_lpp.text_points_im_2_list[i])[argsorted_points]
        sorted_dict["object_index_is"].append(sorted_object_from)
        sorted_dict["object_index_to"].append(sorted_object_to)
        sorted_dict["object_index_from"].append([None]*len(sorted_object_to))

    # flatten the lists to create a pandas dataframe
    for key in sorted_dict:
        sorted_dict[key] = np.array(sorted_dict[key]).flatten()

    # create a pandas dataframe from the dictionary of lists
    sorted_points_df = pd.DataFrame.from_dict(sorted_dict)

    # add the "object index from" to the df
    sorted_points_df["object_index_from"] = [sorted_points_df["object_index_is"].values[
                                                 np.where(sorted_points_df['object_index_to'] ==
                                                          sorted_points_df["object_index_is"].iloc[i])[0]][0]
                                             if len(np.where(sorted_points_df['object_index_to'] ==
                                                             sorted_points_df["object_index_is"].iloc[i])[0]) != 0
                                             else None
                                             for i in range(len(sorted_points_df["object_index_to"]))
                                             ]

    # sort the dataframe by the time_index, pair_index and tuple_index for all objects
    sorted_points_df.sort_values(by=["time_index", "pair_index", "tuple_index"], inplace=True)

    # save the outcome as an .xlsx file in a "Tracking_outcome" folder
    try_creating_folder(p.analysis_folder + "Tracking_outcome")
    sorted_points_df.to_excel(p.analysis_folder + "Tracking_outcome/" + excel_name)

    return sorted_points_df, i_lpp


# find pairs batch-wise: take a batch of "batch_size" frames of the image and find pairs in the tracks of these batches.
# For each batch create a pair-dictionary, tuple-dictionary and also store the information which batch these
# pairs were found in. Return the i_lpp with all of these new additions.
# The batches overlap, meaning e.g. if the batch size is 5, and there are 10 frames before anaphase (0 to 9),
# there will be 6 batches: [0-4, 1-5, 2-6, 3-7, 4-8, 5-9])
# inputs: i_lpp: Linear Programming problem interpretation object (see interpret_lpp)
#         p: parameters object (see read_in_config_file)
#         batch_size: integer, the size of the batches used for finding pairs
#         weight_factor_pair_finding: the balance between spatial closeness and moving in the same way (until anaphase)
#                                     (see find_pairs in interpret_lpp)
# output: i_lpp: Linear Programming problem interpretation object (see interpret_lpp) with the additional information
#                explained above
def find_pairs_in_batches_for_reconstruction(i_lpp, p, batch_size, weight_factor_pair_finding=2.):
    # add the batch_size and weight factor to the i_lpp object
    i_lpp.batch_size, i_lpp.weight_factor_pair_finding = batch_size, weight_factor_pair_finding
    i_lpp.batch_pair_dicts, i_lpp.batch_tuple_dicts = [], []
    i_lpp.extended_batch_pair_dicts, i_lpp.extended_batch_tuple_dicts = [], []
    i_lpp.batch_ranges, i_lpp.batch_names = [], []

    # find pairs in batches of the tracking sequence
    for time_point, frame_no in enumerate(range(min(p.time_points), max(p.time_points) - batch_size)):
        if frame_no + batch_size < p.anaphase_onset:
            i_lpp.batch_ranges.append(range(frame_no, frame_no + batch_size))
            i_lpp.batch_names.append("_batch_" + str(time_point) + "_frames_" + str(frame_no) + "-" +
                                     str(frame_no + batch_size - 1))
            # get the subset corresponding to the batch by using sorted points array which
            # is sorted by [time_point, object_number, spatial_dimension]
            sorted_points_array_batch = i_lpp.sorted_points_array[time_point:time_point + batch_size]
            # find the pairs in the batch
            real_pairs, _, _ = find_pairs(sorted_points_array_batch, p, weight_factor=weight_factor_pair_finding)

            # create a pair dictionary in which every object gets assigned a pair number
            pair_dictionary = dict()
            tuple_index_dictionary = dict()
            # create unique pairs
            unique_real_pairs = list(set([tuple(sorted(t)) for t in real_pairs]))
            # for every unique pair, set the dictionary entry of the pair to be the pair number (number of the tuple)
            for j, pair in enumerate(unique_real_pairs):
                pair_dictionary[pair[0]], pair_dictionary[pair[1]] = j, j
                tuple_index_dictionary[pair[0]], tuple_index_dictionary[pair[1]] = 1, 2

            # add the dictionaries for each batch to the list in the i_lpp
            i_lpp.batch_pair_dicts.append(pair_dictionary)
            i_lpp.batch_tuple_dicts.append(tuple_index_dictionary)

            # extend the batch tuple dictionaries such that they also link to objects that are not in any pair to -1
            extended_pair_dict, extended_tuple_dict = extend_pair_and_tuple_dict(pair_dictionary,
                                                                                 tuple_index_dictionary, p)
            # add the extended dictionaries for each batch to the list in the i_lpp
            i_lpp.extended_batch_pair_dicts.append(extended_pair_dict)
            i_lpp.extended_batch_tuple_dicts.append(extended_tuple_dict)
        else:
            print("batch from " + str(frame_no) + " to " + str(frame_no + batch_size - 1) +
                  " is later than anaphase onset in frame " + str(p.anaphase_onset))
    return i_lpp


# loop over the connected tracks to find pairs, then identify outliers using the pair-information and the fact
# that kinetochores move in parallel prior to anaphase:
# Loop over batches of "batch_size" of the sorted spatial information in the "sorted_list_of_points" to locally identify
# pairs of kinetochores. Then use the fact that they should move in pairs prior to anaphase to check the connections

def loop_over_tracks_to_find_outliers_using_pair_info(sorted_points_df, i_lpp, p, no_partner_penalty=2.):
    print("looping over tracks batch-wise to find outliers using the pair information")

    batch_names = ["_batch_" + str(j) + "_frames_" + str(batch_range[0]) + "-" + str(batch_range[-1])
                   for j, batch_range in enumerate(i_lpp.batch_ranges)]
    # first add all new columns to the dataframe
    column_list = ["pair_diff_" + column_name + batch_name
                   for column_name in ["x_vector", "y_vector", "z_vector", "x_pos_ROI", "y_pos_ROI", "z_pos_ROI"]
                   for batch_name in batch_names]
    column_list = column_list + ["pair_vec_diff" + batch_name for batch_name in batch_names]
    column_list = column_list + ["pair_dist" + batch_name for batch_name in batch_names]
    column_list = column_list + ["pair_plus_pair_vec_diff" + batch_name for batch_name in batch_names]

    sorted_points_df.reindex(columns=[*sorted_points_df.columns.tolist(), *column_list], fill_value=float('nan'))

    # loop over the batches to find the pairs and pair vectors for each batch
    for j, (batch_pair_dict, batch_tuple_dict, batch_name, batch_range) in tqdm(enumerate(
            zip(i_lpp.batch_pair_dicts, i_lpp.batch_tuple_dicts, i_lpp.batch_names, i_lpp.batch_ranges)),
            total=len(i_lpp.batch_pair_dicts)):

        # get the relevant time slices
        min_t_ix, max_t_ix = batch_range[0] - min(p.time_points), batch_range[-1] - min(p.time_points)
        relevant_time_ixs = (min_t_ix <= sorted_points_df["time_index"]) & (sorted_points_df["time_index"] <= max_t_ix)

        # only look at the "true pairs" i.e. where the batch pair index is non-negative
        only_true_pairs = sorted_points_df["pair_index" + batch_name] >= 0

        # the relevant indices where the pair distance should be calculated are the time-indices in question where
        # pairs could be identified
        rel_t_and_true_pair = relevant_time_ixs & only_true_pairs
        rel_t_and_no_true_pair = relevant_time_ixs & ~ only_true_pairs

        # for places where no pair could be identified, set the pair dist to the penalty used when no pair is found,
        # all of these will be above the "upper whisker" and identified as faulty
        sorted_points_df.loc[rel_t_and_no_true_pair, "pair_plus_pair_vec_diff" + batch_name] = no_partner_penalty

        # sort the dataframe by the time_index, pair_index and tuple_index for all objects
        sorted_points_df.sort_values(by=["time_index", "pair_index" + batch_name, "tuple_index" + batch_name],
                                     inplace=True)

        # get the distance between the vectors and loci of each pair in this sorting
        for column_name in ["x_vector", "y_vector", "z_vector", "x_pos_ROI", "y_pos_ROI", "z_pos_ROI"]:
            # Get the difference between each row of the dataframe row x - row x+1 and then "reverse" row x - row x -1
            column = sorted_points_df[column_name]
            diff_correct_order = column.loc[rel_t_and_true_pair] - column.loc[rel_t_and_true_pair].shift(-1)
            diff_reverse_order = column.loc[rel_t_and_true_pair] - column.loc[rel_t_and_true_pair].shift(1)

            # "correct order difference" for every second object
            pair_difference = diff_correct_order
            pair_difference.iloc[1::2] = diff_reverse_order

            # add these distances to the dataframe
            sorted_points_df.loc[rel_t_and_true_pair, "pair_diff_" + column_name + batch_name] = pair_difference

        # add the total distance to the dataframe
        sorted_points_df.loc[rel_t_and_true_pair, "pair_vec_diff" + batch_name] = \
            np.sqrt(np.square(sorted_points_df.loc[rel_t_and_true_pair, "pair_diff_" + "x_vector" + batch_name]) +
                    np.square(sorted_points_df.loc[rel_t_and_true_pair, "pair_diff_" + "y_vector" + batch_name]) +
                    np.square(sorted_points_df.loc[rel_t_and_true_pair, "pair_diff_" + "z_vector" + batch_name]))

        sorted_points_df.loc[rel_t_and_true_pair, "pair_dist" + batch_name] = \
            np.sqrt(np.square(sorted_points_df.loc[rel_t_and_true_pair, "pair_diff_" + "x_pos_ROI" + batch_name]) +
                    np.square(sorted_points_df.loc[rel_t_and_true_pair, "pair_diff_" + "y_pos_ROI" + batch_name]) +
                    np.square(sorted_points_df.loc[rel_t_and_true_pair, "pair_diff_" + "z_pos_ROI" + batch_name]))

        # add a normalised version of the total distance to the dataframe
        sorted_points_df.loc[rel_t_and_true_pair, "pair_vec_diff" + batch_name] = \
            sorted_points_df.loc[rel_t_and_true_pair, "pair_vec_diff" + batch_name] \
            / np.max(sorted_points_df.loc[rel_t_and_true_pair, "pair_vec_diff" + batch_name])
        sorted_points_df.loc[rel_t_and_true_pair, "pair_dist" + batch_name] = \
            sorted_points_df.loc[rel_t_and_true_pair, "pair_dist" + batch_name] / \
            np.max(sorted_points_df.loc[rel_t_and_true_pair, "pair_dist" + batch_name])

        pair_indices = sorted_points_df.pair_index.unique()

        # get the sum of the pair distance plus the pair vector distance from the
        sorted_points_df.loc[rel_t_and_true_pair, "pair_plus_pair_vec_diff" + batch_name] = \
            sorted_points_df.loc[rel_t_and_true_pair, "pair_dist" + batch_name] + \
            sorted_points_df.loc[rel_t_and_true_pair, "pair_vec_diff" + batch_name]

        # get the upper whisker of the boxplot of the pair plus pair vec difference
        upper_quartile = np.percentile(sorted_points_df.loc[rel_t_and_true_pair,
                                                            "pair_plus_pair_vec_diff" + batch_name],
                                       75)
        lower_quartile = np.percentile(sorted_points_df.loc[rel_t_and_true_pair,
                                                            "pair_plus_pair_vec_diff" + batch_name],
                                       25)
        inter_quartile = upper_quartile - lower_quartile
        upper_whisker_value = upper_quartile + 1.5 * inter_quartile

        sorted_points_df = identify_non_pair_outliers(sorted_points_df, pair_indices, upper_whisker_value,
                                                      batch_name=batch_name)

    # for each point, check all batches whether or not this is an outlier
    # get a list the "faulty connection column" of all batches
    column_names_faulty = [column_name for column_name in sorted_points_df.columns.tolist()
                           if column_name.startswith('faulty connection')]
    # convert these columns to numpy arrays, so the numpy .reduce() function works for the logical or
    list_of_faulty_column_np_arrays = [sorted_points_df[column_name_faulty].values for column_name_faulty in
                                       column_names_faulty]
    # using a logical or find all faulty connections
    is_faulty_connection_all = np.logical_or.reduce(list_of_faulty_column_np_arrays, axis=0)
    # add the faulty connections as information to the dataframe
    sorted_points_df["faulty connection (combined batches)"] = is_faulty_connection_all
    print("Found " + str(np.sum(is_faulty_connection_all)) + " connections that will be revisited. That is "
          + str(int(100 * np.sum(is_faulty_connection_all) / len(sorted_points_df))) + " % of all connections.")

    # sort the dataframe by the time_index, pair_index and tuple_index over all frames before saving
    sorted_points_df.sort_values(by=["time_index", "pair_index", "tuple_index"], inplace=True)

    # save the results as an excel file
    print("Saving to csv file (can be found in analysis folder + 'Tracking_outcome' folder as 'outliers_csv.csv')")
    sorted_points_df.to_csv(p.analysis_folder + "Tracking_outcome/" + "outliers_csv.csv", decimal='.', sep=';',
                            float_format='%.3f')
    return sorted_points_df, i_lpp


# identify outliers that don't move in pairs
def identify_non_pair_outliers(sorted_points_df, pair_indices, upper_whisker_value, batch_name=""):
    # create three empty columns to store the indicators for faulty connections
    sorted_points_df['pair has faulty connection' + batch_name] = np.nan
    sorted_points_df['good before (not after)' + batch_name] = np.nan
    sorted_points_df['faulty connection' + batch_name] = np.nan

    sorted_points_df['faulty connection' + batch_name] = sorted_points_df['pair_plus_pair_vec_diff' + batch_name] \
        > upper_whisker_value

    # get the slice with all faulty connections, ! do not replace "==1" with "is True" !
    is_faulty_connection_slice = sorted_points_df['faulty connection' + batch_name] == 1
    for pair_index in pair_indices:
        # get the slice corresponding to the pair
        pair_slice = sorted_points_df['pair_index' + batch_name] == pair_index
        # check if any of the connections for that pair is faulty
        bool_error_in_pair = sorted_points_df.loc[pair_slice, 'faulty connection' + batch_name].any()
        # set the "obj" has faulty connection to True, if there was an error in the pair (keep in mind: the error
        # is based on the pair difference, so the connections of both objects have been labeled as faulty, even though
        # one most likely is not
        sorted_points_df.loc[pair_slice, 'pair has faulty connection' + batch_name] = bool_error_in_pair
        if bool_error_in_pair:
            # find out whether the mean of the time values before or after the faulty connection
            # 1) find the time-indices of all faulty connections
            time_indices_faulty_for_pair = sorted_points_df.loc[pair_slice & is_faulty_connection_slice, 'time_index']

            # 2) get the minimum and maximum of the faulty indices
            min_faulty_t_index = min(time_indices_faulty_for_pair)
            max_faulty_t_index = max(time_indices_faulty_for_pair)
            # 3) get the mean 'pair_plus_pair_vec_diff' before the min faulty index and the mean after the max faulty
            # index
            time_before_slice = sorted_points_df['time_index'] < min_faulty_t_index
            time_after_slice = sorted_points_df['time_index'] > max_faulty_t_index
            mean_pair_plus_pair_vec_diff_before_first_faulty = \
                np.mean(sorted_points_df.loc[pair_slice & time_before_slice, "pair_plus_pair_vec_diff" + batch_name])
            mean_pair_plus_pair_vec_diff_after_last_faulty = \
                np.mean(sorted_points_df.loc[pair_slice & time_after_slice, "pair_plus_pair_vec_diff" + batch_name])
            # 4) if the mean before is bigger than the mean after, set the "is good before" to False, otherwise set
            #    it to True
            if np.isnan(mean_pair_plus_pair_vec_diff_before_first_faulty):
                time_index_slice = sorted_points_df['time_index'] == max_faulty_t_index
            elif np.isnan(mean_pair_plus_pair_vec_diff_after_last_faulty):
                time_index_slice = sorted_points_df['time_index'] == min_faulty_t_index
            elif mean_pair_plus_pair_vec_diff_before_first_faulty > mean_pair_plus_pair_vec_diff_after_last_faulty:
                sorted_points_df.loc[pair_slice, 'good before (not after)' + batch_name] = False
                time_index_slice = sorted_points_df['time_index'] == max_faulty_t_index
            else:
                sorted_points_df.loc[pair_slice, 'good before (not after)' + batch_name] = True
                time_index_slice = sorted_points_df['time_index'] == min_faulty_t_index
            sorted_points_df.loc[pair_slice & time_index_slice, 'faulty connection' + batch_name] = True
    return sorted_points_df


# for every connection that was identified as faulty in the outliers df, try to find connections where the kinetochore
# moves in the same direction as its "partner" (the kinetochore in the same pair)
def correct_tracks_using_the_outliers_df(outliers_df, p, plot=False):

    # use a log-file to print everything happening
    old_stdout = sys.stdout
    try_creating_folder(p.analysis_folder + "Tracking_outcome")
    sys.stdout = open(p.analysis_folder + "Tracking_outcome/" + "correcting_tracks.txt", 'w')

    # get the number of frames and number of points to be able to reshape the points
    number_of_frames = int(outliers_df["time_index"].max()) + 1
    number_of_points = int(len(outliers_df["x_pos_ROI"]) / number_of_frames)

    # get all points from the outliers dataframe, their pair and tuple indices and create a dictionary with these values
    # as needed for the napari viewer, sort the points by time_index and object index
    # sort the dataframe by the time_index, pair_index and tuple_index for all objects
    outliers_df.sort_values(by=["time_index", "object_index"], inplace=True)
    points = np.array([outliers_df["x_pos_ROI"], outliers_df["y_pos_ROI"], outliers_df["z_pos_ROI"]]).T
    points = points.reshape((number_of_frames, number_of_points, 3))
    outliers_df.sort_values(by=["time_index", "pair_index", "tuple_index"], inplace=True)

    # get the indices of all tracks flagged as incorrect
    faulty_connection_rows_df = outliers_df[outliers_df["faulty connection (combined batches)"] == 1]
    faulty_connection_object_indices = faulty_connection_rows_df["object_index"]
    faulty_connection_frames = faulty_connection_rows_df["time_index"]

    # get the names of all columns with the faulty connection information for the batches
    filter_col = [col for col in outliers_df if col.startswith('pair_index_batch_')]
    pair_indices_faulty_connections_batches = faulty_connection_rows_df[filter_col]

    # create a list of proposals for change
    good_connections = []

    # loop over all faulty connections
    for i, (faulty_connection_object_index, faulty_connection_frame_index) \
            in enumerate(zip(faulty_connection_object_indices, faulty_connection_frames)):
        print("faulty connection no " + str(i) + " in frame " + str(faulty_connection_frame_index))
        # get all possible partners for this object: first get all pair indices for all batches
        pair_indices_for_faulty_connection = pair_indices_faulty_connections_batches.iloc[i]

        # get the column names of the batches in which a connection was marked as faulty and the indices of the pair
        # that was marked as faulty
        all_column_names = pair_indices_for_faulty_connection.index
        pair_indices_for_batch = [pair_indices_for_faulty_connection[column_name] for column_name in all_column_names]
        column_names = [column_name for j, column_name in enumerate(all_column_names)
                        if pair_indices_for_batch[j] is not None]
        pair_indices_for_batch = [pair_index_for_batch for pair_index_for_batch in pair_indices_for_batch
                                  if pair_index_for_batch is not None]

        # check if all pair indices are negative, if so there was no "partner" assigned to object with the faulty
        # connection, then check neighbouring batches and see if there is a fitting connection
        while (np.array(pair_indices_for_batch) < 0).all():
            print("found faulty connection corresponding to an object that was not identified to belong to a pair "
                  "in any of the batches, so checking for its assignment in neighbouring batches.")
            # get the list of all column names
            all_column_names = list(all_column_names)
            # create a list for new batch column names, pair_indices in these batches and the direction in which the
            # assignment has taken place
            column_names_new, pair_indices_new = [], []

            # try checking the next or the last batch
            # 1) first get the first and the last column given at the moment
            if len(column_names) > 1:
                current_column_name_index_first = list(all_column_names).index(column_names[0])
                current_column_name_index_last = current_column_name_index_first
            elif len(column_names) == 1:
                current_column_name_index_first = all_column_names.index(column_names[0])
                current_column_name_index_last = all_column_names.index(column_names[-1])
            else:
                break

            # then check if in the batches next to the batches checked before, the object in question has a partner
            # and get the partners pair indices in these batches

            if 0 < current_column_name_index_first - 1:
                column_before = all_column_names[current_column_name_index_first - 1]
                row_of_faulty_conn_first = faulty_connection_rows_df[
                    faulty_connection_rows_df[column_names[0]] == pair_indices_for_batch[0]]
                pair_indices_in_last_batch = row_of_faulty_conn_first[column_before]
                pair_index_in_last_batch = [pair_index_in_last_batch for pair_index_in_last_batch in
                                            pair_indices_in_last_batch if pair_index_in_last_batch is not None][0]
                if pair_index_in_last_batch is not None:
                    pair_indices_new.append(pair_index_in_last_batch)
                    column_names_new.append(column_before)

            if current_column_name_index_last + 1 < len(all_column_names):
                row_of_faulty_conn_last = faulty_connection_rows_df[
                    faulty_connection_rows_df[column_names[-1]] == pair_indices_for_batch[0]]
                column_after = all_column_names[current_column_name_index_last + 1]
                pair_indices_in_next_batch = row_of_faulty_conn_last[column_after]
                try:
                    pair_index_in_next_batch = [pair_index_in_next_batch for pair_index_in_next_batch in
                                                pair_indices_in_next_batch if pair_index_in_next_batch is not None][0]
                    if pair_index_in_next_batch is not None:
                        pair_indices_new.append(pair_index_in_next_batch)
                        column_names_new.append(column_after)
                except IndexError:
                    pass

            column_names = column_names_new
            pair_indices_for_batch = pair_indices_new

        # if there there were same batches were partners were assigned and some where they weren't, remove the ones
        # where no partners were assigned
        if (np.array(pair_indices_for_batch) < 0).any():
            print("some pair indices smaller 0: Removing them")
            # in this case, remove those that are negative
            column_names = [column_name for j, column_name in enumerate(column_names)
                            if pair_indices_for_batch[j] > 0]
            pair_indices_for_batch = [pair_index_for_batch for pair_index_for_batch in pair_indices_for_batch
                                      if pair_index_for_batch > 0]
        else:
            print("no pair indices smaller 0")
        # get the object indices of the partner(s)
        # 1) get all objects with the same pair_index in the respective pair-index column of the batch in which
        # the connection was marked as faulty
        partners_for_batches = [np.array(outliers_df[outliers_df[column_name]
                                                     == pair_index_for_batch]["object_index"])
                                for pair_index_for_batch, column_name in
                                zip(pair_indices_for_batch, column_names)]
        # 2) remove the faulty object itself to find its partner
        # remove empty lists from "partners_for_batches"
        partners_for_batches = [np_array for np_array in partners_for_batches if np_array.size != 0]
        partners_for_batches_obj_indices = [partner_for_batches[np.where(partner_for_batches !=
                                                                         faulty_connection_object_index)][0]
                                            for partner_for_batches in partners_for_batches]
        # check if partners were assigned in more than one batch
        if len(partners_for_batches_obj_indices) > 1:
            print("more than one pair index assigned")
            # in this case, check if the pair indices correspond to the same partner

            # check if all pair indices are equal
            if partners_for_batches_obj_indices.count(partners_for_batches_obj_indices[0]) == \
                    len(partners_for_batches_obj_indices):
                partner_object_index = partners_for_batches_obj_indices[0]
                print("all pair indices equal :)")
            else:
                # if they are not equal, use the most frequent value:
                partner_object_index = mode(partners_for_batches_obj_indices)[0][0]
                print("Some pair indices differ!")
        elif len(partners_for_batches_obj_indices) == 1:
            partner_object_index = partners_for_batches_obj_indices[0]
        else:
            print("continue, because no partners could not be identified")
            continue

        # for both the point and its partner, get the 10 closest objects in the next frame
        # get nn_move nearest neighbours in next image (these are the possible LR transitions)
        next_im_neighbours = \
            get_n_nearest_neighbours_of_each_obj_in_next_image(points[faulty_connection_frame_index + 1],
                                                               points[faulty_connection_frame_index],
                                                               10)
        closest_points_to_faulty_conn_indices = next_im_neighbours[faulty_connection_object_index]
        closest_points_to_partner_indices = next_im_neighbours[partner_object_index]

        # find the vectors corresponding to these transitions
        # 1) get the points of the faulty_connection and its partner
        faulty_conn_point = points[faulty_connection_frame_index + 1][faulty_connection_object_index]
        faulty_conn_partner_point = points[faulty_connection_frame_index + 1][partner_object_index]
        # 2) get the array of points closest to them
        closest_points_to_faulty_conn = np.take(points[faulty_connection_frame_index],
                                                indices=closest_points_to_faulty_conn_indices, axis=0)
        closest_points_to_partner = np.take(points[faulty_connection_frame_index],
                                            indices=closest_points_to_partner_indices, axis=0)
        # 3) calculate the vectors
        vectors_faulty_conn_point = faulty_conn_point - closest_points_to_faulty_conn
        vectors_faulty_conn_partner = faulty_conn_partner_point - closest_points_to_partner

        # get the minimum and argmin of the vectors
        min_vectors, argmin_vectors = [], []
        min_angle_difference, min_vector_difference = [], []
        vector_lengths = []

        for direction_vector in vectors_faulty_conn_point:
            vector_length = np.linalg.norm(direction_vector)
            direction_vector = np.array([direction_vector])  # make direction vector "2d" for cdist function
            # find the connections which are most parallel (analogous to the distance measure for
            # "parallel_movement_for_transition")
            # get the distance in ending points
            eu_distance_between_direction_vecs = cdist(direction_vector, vectors_faulty_conn_partner)
            # get the distance in direction
            cos_distance_between_direction_vecs = cdist(direction_vector, vectors_faulty_conn_partner,
                                                        metric='cosine')
            # to get a measure that does permit for larger movement, where differences in euclidean distance
            # might be bigger, but also allows for objects to not move much and maybe move slightly in opposite
            # directions, multiply these two measures with each other
            total_distance_between_direction_vecs = eu_distance_between_direction_vecs * \
                cos_distance_between_direction_vecs
            # get the minimum distance between direction vectors as a cost measure
            min_vectors.append(np.min(total_distance_between_direction_vecs))
            argmin_vectors.append(np.argmin(total_distance_between_direction_vecs))
            min_angle_difference.append(np.min(cos_distance_between_direction_vecs))
            min_vector_difference.append(np.min(eu_distance_between_direction_vecs))
            # get the norm of the minimum vector and add it to the vector lengths
            vector_length += np.linalg.norm(vectors_faulty_conn_partner[
                                                np.argmin(eu_distance_between_direction_vecs)])
            vector_lengths.append(vector_length)
        sum_vec_len_vec_diff_norm = np.array(vector_lengths) + np.array(min_vector_difference)
        sum_vec_len_vec_diff_norm = sum_vec_len_vec_diff_norm / max(sum_vec_len_vec_diff_norm)
        if plot:
            plt.plot(min_vectors / max(min_vectors), label='product')
            plt.plot(min_vector_difference / max(min_vector_difference), label='vector diff')
            plt.plot(vector_lengths / max(vector_lengths), label='vector lengths (+)')
            plt.plot(sum_vec_len_vec_diff_norm, 'r-', label='prod vec len vec diff')
            plt.plot([np.argmin(sum_vec_len_vec_diff_norm)], [np.min(sum_vec_len_vec_diff_norm)], 'ro')
            plt.legend()
            plt.show()
        index_of_good_connection_to_faulty_object = np.argmin(sum_vec_len_vec_diff_norm)
        number_of_good_connection_to_faulty_object = closest_points_to_faulty_conn_indices[
            index_of_good_connection_to_faulty_object]
        index_of_good_connection_to_partner = argmin_vectors[int(index_of_good_connection_to_faulty_object)]
        number_of_good_connection_to_partner = closest_points_to_partner_indices[
            index_of_good_connection_to_partner]
        good_connections.append((faulty_connection_frame_index,
                                 (number_of_good_connection_to_faulty_object, faulty_connection_object_index),
                                 (number_of_good_connection_to_partner, partner_object_index)))

    print(good_connections)

    sys.stdout = old_stdout
    return good_connections
