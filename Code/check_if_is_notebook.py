

# check if the script is executed in a Jupyter notebook or a 'normal' python script
# source: https://stackoverflow.com/questions/15411967/how-can-i-check-if-code-is-executed-in-the-ipython-notebook
#         answer by: Gustavo Bezerra 06.02.20
def is_notebook():
    try:
        shell = get_ipython().__class__.__name__ # noqa
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter notebook or qt console
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False      # Probably standard Python interpreter
