# IMPORTS

# 2D plots and simple 3D plots
import matplotlib.pyplot as plt

# import self-written functions
import read_in as ri
import label_image_functions as lif
import projection_and_analysis as pa

# specific modules
import os
import numpy as np
from skimage import measure
import pandas as pd
import skimage.io
import skimage.draw
import skimage.transform
import vispy.io as visio
import sys
import shutil

import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=UserWarning)
    import napari

# use dark pyplot style
plt.style.use('dark_background')


# a class to store all parameters
class Parameters:

    # make all variables None at the beginning
    def __init__(self):

        self.metadata = None
        self.analysis_folder, self.save_folder, self.atlas_construction_log_file_name = None, None, None
        self.data_folder, self.output_excel_name = None, None
        self.excel_filename, self.excel_path = None, None
        self.microscopy_data_filename_pre_index, self.microscopy_data_filename_post_index = None, None
        self.microscopy_data_file_extension, self.roi = None, None
        self.ellipsoid_size_tuple = None
        self.model_upsampling_factor, self.bounding_box_buffer, self.upsampling_buffer_scaling = None, None, None
        self.roi_xmin, self.roi_ymin, self.roi_zmin = None, None, None
        self.roi_xmax, self.roi_ymax, self.roi_zmax = None, None, None
        self.observed_frames = None
        self.corresponding_npy_file_name_before_index, self.corresponding_npy_file_name_after_index = None, None
        self.phys_size_x, self.phys_size_y, self.phys_size_z = None, None, None
        self.observed_frames, self.frames, self.track_indices = None, None, None
        self.x_COM, self.y_COM, self.z_COM = None, None, None
        self.model_data_output_name, self.middle_of_model_output_name = None, None

        return


# class to store the values corresponding to the good 'non-touching' objects
class NonTouchingObjects:

    def __init__(self):
        # create empty lists to store the data

        self.tracking_labels_list = []

        self.params_parabola_list = []
        self.params_gauss_list = []
        self.intensity_img_list = []
        self.bbox_list = []

        self.max_int_list = []
        self.mean_int_list = []
        self.min_int_list = []

        self.corresponding_labels_list = []
        self.corresponding_frames_list = []


def read_in_atlas_construction_config_file(configuration_file):

    print("The config file is: " + str(configuration_file))

    # create one object where all parameters can be stored
    p = Parameters()

    # create a dictionary from the configuration file
    config_dict = ri.read_in_config(configuration_file)

    # create a dictionary with all the data storage parameters
    data_parameters_dict = config_dict["data parameters"]

    # read in the folder and file names
    p.analysis_folder, p.data_folder = data_parameters_dict["analysis folder"], data_parameters_dict["data folder"]
    p.save_folder = data_parameters_dict["save folder"]
    p.microscopy_data_filename_pre_index = data_parameters_dict["microscopy data filename pre index"]
    p.microscopy_data_filename_post_index = data_parameters_dict["microscopy data filename post index"]
    p.microscopy_data_file_extension = data_parameters_dict["microscopy data file extension"]

    # read in the output file names
    p.atlas_construction_log_file_name = data_parameters_dict["atlas construction log file name"]
    p.model_data_output_name = data_parameters_dict["model data output name"]
    p.middle_of_model_output_name = data_parameters_dict["middle of model output name"]
    p.output_excel_name = data_parameters_dict["output excel name"]

    # read in the Excel file specifications
    # (alternative 1 to specify data, otherwise these are all None)
    p.excel_filename = data_parameters_dict["excel filename"]
    if p.excel_filename == "None":
        p.excel_filename = None
    else:
        p.excel_path = p.data_folder + p.excel_filename
    p.excel_tracking_column_x_pos = data_parameters_dict["excel tracking column name x"]
    p.excel_tracking_column_y_pos = data_parameters_dict["excel tracking column name y"]
    p.excel_tracking_column_z_pos = data_parameters_dict["excel tracking column name z"]

    # read in the names of observed frames and points in them
    # (alternative 2 to specify data, otherwise these are all None)
    p.observed_frames = data_parameters_dict["observed frames"]
    p.corresponding_npy_file_name_before_index = data_parameters_dict["corresponding npy file name before index"]
    p.corresponding_npy_file_name_after_index = data_parameters_dict["corresponding npy file name after index"]

    # read in the model specifications
    model_specification_dict = config_dict["model specifications"]
    p.ellipsoid_size_tuple = model_specification_dict["ellipsoid size"]
    p.model_upsampling_factor = model_specification_dict["model upsampling factor"]
    p.bounding_box_buffer = model_specification_dict["bounding box buffer"]
    p.upsampling_buffer_scaling = model_specification_dict["upsampling buffer scaling"]

    # read in the image region of interest specifications
    roi_parameters = config_dict["roi parameters"]
    roi_offset = roi_parameters["roi offset"]
    # get the minima and maxima of the bounding box of the region of interest in x,y and z
    p.roi_xmin, p.roi_xmax = roi_parameters["roi xmin"] - roi_offset, roi_parameters["roi xmax"] + roi_offset
    p.roi_ymin, p.roi_ymax = roi_parameters["roi ymin"] - roi_offset, roi_parameters["roi ymax"] + roi_offset
    p.roi_zmin, p.roi_zmax = roi_parameters["roi zmin"] - roi_offset, roi_parameters["roi zmax"] + roi_offset
    # define the ROI as a np slice
    p.roi = np.s_[p.roi_xmin:p.roi_xmax,
                  p.roi_ymin:p.roi_ymax,
                  p.roi_zmin:p.roi_zmax]

    # get all metadata parameters and read in the metadata
    # create a dict of metadata parameters only
    p.metadata_parameters = config_dict["metadata parameters"]

    # get the physical size of one pixel (resolution) in x,y and z
    p.phys_size_x = p.metadata_parameters["phys_size_x"]
    p.phys_size_y = p.metadata_parameters["phys_size_y"]
    p.phys_size_z = p.metadata_parameters["phys_size_z"]

    # get the unit of the resolution in x,y and z
    p.size_unit_x = p.metadata_parameters["size_unit_x"]
    p.size_unit_y = p.metadata_parameters["size_unit_y"]
    p.size_unit_z = p.metadata_parameters["size_unit_z"]

    # read in the tracking from the Excel, if an Excel filename was given
    if p.excel_filename is not None:
        # create pandas data frame from Excel-Data
        p.tracking_excel_path = p.data_folder + p.excel_filename
        p.tracking_data_df = pd.read_excel(p.tracking_excel_path)

        # get minimum and maximum tracking frame
        p.min_tracking_frame = int(p.tracking_data_df["Frame"].min(skipna=True))
        print("Excel: Tracking starts in frame " + str(p.min_tracking_frame))
        p.max_tracking_frame = int(p.tracking_data_df["Frame"].max(skipna=True))
        print("Excel: Tracking ends in frame " + str(p.max_tracking_frame))

        # get positions from data, their fra and the track number
        p.frames = p.tracking_data_df["Frame"].to_numpy()
        p.track_indices = p.tracking_data_df["Track"].to_numpy()
        p.pair_indices = p.tracking_data_df["Pair"].to_numpy()
        # take into account,that only the ROI will be processed and labelling starts with 0 in python
        p.x_COM = p.tracking_data_df[p.excel_tracking_column_x_pos] - p.roi_xmin
        p.y_COM = p.tracking_data_df[p.excel_tracking_column_y_pos] - p.roi_ymin
        p.z_COM = p.tracking_data_df[p.excel_tracking_column_z_pos] - p.roi_zmin - 1
        p.x_COM, p.y_COM, p.z_COM = p.x_COM.to_numpy(), p.y_COM.to_numpy(), p.z_COM.to_numpy()
        p.observed_frames = list(range(p.min_tracking_frame, p.max_tracking_frame))
    else:
        p.x_COM, p.y_COM, p.z_COM = None, None, None
    return p


def read_in_point_locations_from_list_of_numpy_files(p):

    # get the x, y and z centre of mass from the saved points and return them
    points_list = []
    for frame_number in p.observed_frames:
        # read in the observed points from npy files stored in the analysis folder
        points = np.load(p.analysis_folder + p.corresponding_npy_file_name_before_index + str(frame_number) +
                         p.corresponding_npy_file_name_after_index)
        points_list.append(points)
    return points_list


def create_mask_with_ellipsoid_sized_windows_at_points(points_in_frame, ellipsoid_size_tuple, size_of_image):
    # define size of ellipsoid cut-out regions
    ellipsoid_a, ellipsoid_b, ellipsoid_c = ellipsoid_size_tuple
    # create ellipsoids at all points
    ellipsoid = skimage.draw.ellipsoid(a=ellipsoid_a, b=ellipsoid_b, c=ellipsoid_c)
    ellipsoid_size = np.sum(ellipsoid)
    mask = lif.create_mask_by_applying_small_mask_to_many_points(ellipsoid, points_in_frame, size_of_image)
    label_ellipsoid = skimage.measure.label(mask, connectivity=1, background=0)
    return label_ellipsoid, ellipsoid_size


def display_original_image_with_ellipsoids(orig_image_xyz_roi, points_in_frame, label_ellipsoid, p, frame_no):
    # create a napari gui_qt context
    with napari.gui_qt():
        # open a napari viewer in 3D and add axis labels for the x and y directions
        viewer = napari.Viewer()
        viewer.dims.ndisplay = 3
        viewer.dims.set_axis_label(0, 'x')
        viewer.dims.set_axis_label(1, 'y')

        scale_from_parameters = (p.phys_size_x, p.phys_size_y, p.phys_size_z)

        # add the microscopy data
        viewer.add_image(orig_image_xyz_roi,
                         colormap='turbo',
                         name='original image',
                         opacity=1.0,
                         scale=scale_from_parameters)

        # add the points in the respective frame
        viewer.add_points(points_in_frame,
                          name='points com',
                          size=1,
                          opacity=0.7,
                          scale=scale_from_parameters)

        # add the labels for the respective points
        viewer.add_labels(label_ellipsoid, name='label image from ellipsoids', scale=scale_from_parameters)

        # create and save a screenshot to the analysis folder
        screen = viewer.screenshot()
        visio.write_png(p.save_folder + 'screenshot_napari_points_label_image_from_ellipsoid_frame' + str(frame_no)
                        + '.png', screen)
        return


def get_the_fit_parameters_for_non_touching_objects_in_ellipsoid_mask(region_props, ellipsoid_size, nto, frame_number,
                                                                      has_excel, points_in_frame, track_index_in_frame):
    # for each object: if it has the same size as the ellipsoid (if it does not 'touch') any nearest neighbours:
    # get it's parameters

    for obj in region_props:
        if int(obj.area) == ellipsoid_size:

            # get the objects properties: the intensity image, bounding box, mean, max and min intensity
            intensity_img_of_obj = obj.intensity_image
            bbox_of_obj = obj.bbox
            max_int = obj.max_intensity
            mean_int = obj.mean_intensity
            min_int = obj.min_intensity

            # if there is an Excel file also providing manual tracking results get the corresponding manual
            # tracking index
            if has_excel:
                # create an indicator whether this is a 'good object' --> the corresponding manual tracking index
                # can be found
                is_good_object, good_point_index = False, None
                # track the number of objects within the bounding box, if there are more than two objects, skip both
                # of them and delete the previously stored parameters
                number_tracker = 0
                for point_index, point in enumerate(points_in_frame):
                    # if a point is within the bounding box of the current object
                    if bbox_of_obj[0] < point[0] < bbox_of_obj[0 + 3] \
                            and bbox_of_obj[1] < point[1] < bbox_of_obj[1 + 3] \
                            and bbox_of_obj[2] < point[2] < bbox_of_obj[2 + 3]:
                        # append its tracking label to the list of tracking labels
                        if number_tracker == 0:
                            number_tracker += 1
                            good_point_index = point_index
                            is_good_object = True
                        # if there are more than one point in the list, remove the last object from the tracking list
                        else:
                            print("Found alternative point" + str(point) + " in obj " + str(obj.label) + " in frame "
                                  + str(frame_number) + "--> ignoring both points")
                            is_good_object = False
                if is_good_object:
                    assert (good_point_index is not None), \
                        "Some of the tests for 'is_good_object' went wrong, the index of the good points should not " \
                        "be None! Check the get_the_fit_parameters_for_non_touching_objects_in_ellipsoid_mask " \
                        "function in the kinetochore_atlas_construction.py file"
                    nto.tracking_labels_list.append(track_index_in_frame[good_point_index])
            else:
                is_good_object = True

            # try fitting a gaussian and a parabola to the intensity distribution within the bounding box of the 'good
            # objects', if the fit is impossible, skip the object

            try:
                obj_params_parabola = pa.get_model_parameters_from_object(intensity_img_of_obj, scale_factor=None)
                obj_params_gauss = pa.get_model_parameters_from_object(intensity_img_of_obj, fit='gauss',
                                                                       scale_factor=None)
            except ValueError:
                print('Can not fit object ' + str(obj.label) + ' --> skipping it')
                is_good_object = False
                obj_params_gauss, obj_params_parabola = None, None

            # if the object is a 'good object' add its values to the non-touching objects list
            if is_good_object:
                # add the label and frame information to the 'good' objects labels
                nto.corresponding_labels_list.append(obj.label)
                nto.corresponding_frames_list.append(frame_number)

                # add the intensity image and bounding box to the list of intensity images
                nto.intensity_img_list.append(intensity_img_of_obj)
                nto.bbox_list.append(bbox_of_obj)

                # add some interpreted intensity information to the list
                nto.max_int_list.append(max_int)
                nto.min_int_list.append(min_int)
                nto.mean_int_list.append(mean_int)

                # add the fitting information to the list
                nto.params_parabola_list.append(obj_params_parabola)
                nto.params_gauss_list.append(obj_params_gauss)

    return nto


def create_a_data_frame_with_info_on_objects_used(nto):
    # create a variable to store the new length of all lists, to reshape the numpy arrays correctly in the following
    # steps
    new_len = len(nto.corresponding_labels_list)

    # create numpy arrays from the lists created by the loop over all objects
    na_frames = np.reshape(np.array(nto.corresponding_frames_list), newshape=(new_len, 1))
    na_labels = np.reshape(np.array(nto.corresponding_labels_list), newshape=(new_len, 1))
    if len(nto.tracking_labels_list) != 0:
        na_tracking_labels = np.reshape(np.array(nto.tracking_labels_list), newshape=(new_len, 1))
    else:
        na_tracking_labels = np.reshape(np.array([False] * new_len), newshape=(new_len, 1))

    na_min_int = np.reshape(np.array(nto.min_int_list), newshape=(new_len, 1))
    na_mean_int = np.reshape(np.array(nto.mean_int_list), newshape=(new_len, 1))
    na_max_int = np.reshape(np.array(nto.max_int_list), newshape=(new_len, 1))

    na_parabola = np.reshape(np.array(nto.params_parabola_list), newshape=(new_len, 9))
    na_gauss = np.reshape(np.array(nto.params_gauss_list), newshape=(new_len, 9))
    na_bbox = np.reshape(np.array(nto.bbox_list), newshape=(new_len, 6))

    # create column labels
    column_labels = ['frames', 'python_label', 'manual_tracking_label',
                     'pax', 'pbx', 'pcx', 'pay', 'pby', 'pcy', 'paz', 'pbz', 'pcz',
                     'gax', 'gmux', 'gsigmax', 'gay', 'gmuy', 'gsigmay', 'gaz', 'gmuz', 'gsigmaz',
                     'bb_x_min', 'bb_y_min', 'bb_z_min', 'bb_x_max', 'bb_y_max', 'bb_z_max',
                     'min_int', 'mean_int', 'max_int']

    # create a pandas dataframe with all of this information
    df_kinetochor_analysis_all = pd.DataFrame(np.concatenate((na_frames, na_labels, na_tracking_labels,
                                                              na_parabola,
                                                              na_gauss,
                                                              na_bbox,
                                                              na_min_int, na_mean_int, na_max_int), axis=1),
                                              columns=column_labels)

    return df_kinetochor_analysis_all, na_tracking_labels


def create_plots_of_fitting_results(df_kinetochor_analysis_all, p):
    # create plots with correlated parabola parameters
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='pax', y='pay', color='red',
                                         title='Parabola amplitude in x- and y-direction')
    ax.figure.savefig(p.save_folder + 'a_par_x_and_y.png')
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='pax', y='paz', color='blue',
                                         title='Parabola amplitude in x- and z-direction')
    ax.figure.savefig(p.save_folder + 'a_par_x_and_z.png')
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='pcx', y='pcy', color='green',
                                         title='Parabola offset in x- and y-direction')
    ax.figure.savefig(p.save_folder + 'c_par_x_and_y.png')
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='pcx', y='pcz', color='yellow',
                                         title='Parabola offset in x- and z-direction')
    ax.figure.savefig(p.save_folder + 'c_par_x_and_z.png')

    # create plots with correlated gauss parameters
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='gax', y='gay', color='red',
                                         title='Gauss amplitude in x- and y-direction')
    ax.figure.savefig(p.save_folder + 'a_gau_x_and_y.png')
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='gax', y='gaz', color='blue',
                                         title='Gauss amplitude in x- and z-direction')
    ax.figure.savefig(p.save_folder + 'a_gau_x_and_z.png')
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='gsigmax', y='gsigmay', color='green',
                                         title='Gauss sigma in x- and y-direction')
    ax.figure.savefig(p.save_folder + 'sigma_gau_x_and_y.png')
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='gsigmax', y='gsigmaz', color='yellow',
                                         title='Gauss sigma in x- and z-direction')
    ax.figure.savefig(p.save_folder + 'sigma_gau_x_and_z.png')

    # create plots with the correlated minimum, mean and maximum intensity
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='min_int', y='max_int', color='orange',
                                         title='Max int over min int')
    ax.figure.savefig(p.save_folder + 'max_int_over_min_int.png')
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='mean_int', y='max_int', color='purple',
                                         title='Max int over mean int')
    ax.figure.savefig(p.save_folder + 'max_int_over_mean_int.png')
    ax = df_kinetochor_analysis_all.plot(kind='scatter', x='mean_int', y='min_int', color='gray',
                                         title='Min int over mean int')
    ax.figure.savefig(p.save_folder + 'min_int_over_mean_int.png')
    return


def create_plot_of_20_unique_objects_each_with_their_mean_max_and_min_intensity(df_kinetochor_analysis_all,
                                                                                na_tracking_labels, p,
                                                                                observed_frames):
    # get the unique tracking labels
    unique_tracked_obj = np.unique(na_tracking_labels)

    # create 4 separate plots with 20 objects each
    for k in range(0, 4):

        # create a 5x4 size canvas for the plots that share their x and y axis
        fig, ax_obj_label = plt.subplots(nrows=5, ncols=4, figsize=(20, 10), sharex='all', sharey='all')

        # add the x-and y-axis labels
        fig.text(0.5, 0.04, 'frame number', ha='center')
        fig.text(0.04, 0.5, 'intensity (a.u.)', va='center', rotation='vertical')

        # for every unique tracking label, get its mean max and min int and plot it over the frame number
        for i, tracking_label in enumerate(unique_tracked_obj[0 + k * 20:20 + k * 20]):
            object_only = df_kinetochor_analysis_all['manual_tracking_label'] == tracking_label
            df_kinetochor_analysis_all[object_only].plot(kind='scatter', x='frames', y='mean_int',
                                                         ax=ax_obj_label[i // 4, i % 4],
                                                         color='seagreen', marker='x')
            df_kinetochor_analysis_all[object_only].plot(kind='scatter', x='frames', y='min_int',
                                                         ax=ax_obj_label[i // 4, i % 4],
                                                         color='mediumspringgreen', marker='x')
            df_kinetochor_analysis_all[object_only].plot(kind='scatter', x='frames', y='max_int',
                                                         ax=ax_obj_label[i // 4, i % 4],
                                                         color='lightseagreen', marker='x')
            # set the x and y limits for the axis to be the same
            ax_obj_label[i // 4, i % 4].set_xlim(min(observed_frames), max(observed_frames))
            ax_obj_label[i // 4, i % 4].set_ylim(0, 500)
            ax_obj_label[i // 4, i % 4].set_xlabel(None)
            ax_obj_label[i // 4, i % 4].set_ylabel(None)

        plt.savefig(p.save_folder + '20_unique_objects_' + str(k) + '.png')
        return


def create_plot_of_mean_max_and_min_intensity_and_variance_over_frame(df_kinetochor_analysis_all, p, observed_frames):
    # create empty lists to store the results
    var_min, var_max, var_mean = [], [], []
    means_min, means_max, means_mean = [], [], []
    tracking_frame_list = []

    # get the mean and standard deviation for the three quantities (mean, max and min intensity)
    for j in observed_frames:
        frame_only = df_kinetochor_analysis_all['frames'] == j
        means_min.append(df_kinetochor_analysis_all[frame_only]['min_int'].mean())
        var_min.append(df_kinetochor_analysis_all[frame_only]['min_int'].std())
        means_mean.append(df_kinetochor_analysis_all[frame_only]['mean_int'].mean())
        var_mean.append(df_kinetochor_analysis_all[frame_only]['mean_int'].std())
        means_max.append(df_kinetochor_analysis_all[frame_only]['max_int'].mean())
        var_max.append(df_kinetochor_analysis_all[frame_only]['max_int'].std())
        tracking_frame_list.append(j)

    # create a figure to plot the results
    fig, ax = plt.subplots(figsize=(8, 5))

    # create an error bar plot with the min, mean and maximum intensity over each frame
    ax.errorbar(tracking_frame_list, means_max, yerr=var_max, color='lightseagreen', label='max')
    ax.errorbar(tracking_frame_list, means_mean, yerr=var_mean, color='mediumspringgreen', label='mean')
    ax.errorbar(tracking_frame_list, means_min, yerr=var_min, color='seagreen', label='min')

    # label the axes and the plot, add a legend
    ax.set_xlabel('frame number')
    ax.set_ylabel('intensity (a.u.)')
    ax.set_title('Average over objects and STD over frame number \n of minimum, mean and maximum')
    ax.legend(loc='upper center', bbox_to_anchor=(1.1, 0.6), ncol=1)

    # save the figure
    plt.savefig(p.save_folder + 'average_over_frame_mean_max_min.png')

    return


# up-sample image and calculate average over extracted kinetochores
def calculate_average_over_kinetochores(p, df_kinetochor_analysis_all, observed_frames):

    # get the parameters from the model
    upsampling_factor = int(p.model_upsampling_factor)
    bb_buffer = int(p.bounding_box_buffer)
    upsampling_buffer = int(p.upsampling_buffer_scaling) * upsampling_factor

    # get the bbox values for each object from the df and use it to calculate the bounding box size (it is the
    # same for all objects)
    bb_x_max, bb_x_min = df_kinetochor_analysis_all['bb_x_max'], df_kinetochor_analysis_all['bb_x_min']
    bb_y_max, bb_y_min = df_kinetochor_analysis_all['bb_y_max'], df_kinetochor_analysis_all['bb_y_min']
    bb_z_max, bb_z_min = df_kinetochor_analysis_all['bb_z_max'], df_kinetochor_analysis_all['bb_z_min']
    bbox_size = [int(bb_x_max[0] - bb_x_min[0]), int(bb_y_max[0] - bb_y_min[0]), int(bb_z_max[0] - bb_z_min[0])]

    # get the middle values from the fitting for each object in each dimension from the df
    x_middle, y_middle, z_middle = (df_kinetochor_analysis_all['gmux'], df_kinetochor_analysis_all['gmuy'],
                                    df_kinetochor_analysis_all['gmuz'])

    # get the frame number for each object from the df
    frame = df_kinetochor_analysis_all['frames']

    # it is better if the new image size is an uneven number, because in that case there is a 'clear' centre
    new_image_size = [int((j + 2 * bb_buffer) * upsampling_factor + 2 * upsampling_buffer) for j in bbox_size]

    mean_img = np.zeros(shape=new_image_size, dtype=float)
    amount_averaging = 0
    middle_of_mean_img = None

    for i, frame_no in enumerate(observed_frames):

        # get the region of interest of the microscopy data
        orig_image_xyz_roi = ri.load_image_from_memory(frame_no, p.data_folder, p.microscopy_data_filename_pre_index,
                                                       p.microscopy_data_filename_post_index,
                                                       p.microscopy_data_file_extension, p.roi)

        # set lowest value to zero
        orig_image_xyz_roi -= np.min(orig_image_xyz_roi)
        offset = np.min(orig_image_xyz_roi)
        print("image offset that was subtracted: " + str(offset))

        frame_bool = np.where(frame == frame_no)

        for index in frame_bool[0]:
            bbox_indices = np.s_[int(bb_x_min[index] - bb_buffer): int(bb_x_max[index] + bb_buffer),
                                 int(bb_y_min[index] - bb_buffer): int(bb_y_max[index] + bb_buffer),
                                 int(bb_z_min[index] - bb_buffer): int(bb_z_max[index] + bb_buffer)]

            bbox_image = orig_image_xyz_roi[bbox_indices]

            # upsample image
            desired_shape = tuple(i * upsampling_factor for i in bbox_image.shape)
            bbox_image_upsampled = skimage.transform.resize(bbox_image, order=0, output_shape=desired_shape,
                                                            preserve_range=True, anti_aliasing=False)

            width_upsampled_bbox = [i for i in bbox_image_upsampled.shape]

            middle_of_mean_img = np.array([int(i / 2) for i in mean_img.shape])

            middle_of_bb_old_coordinates = np.array([x_middle[index], y_middle[index], z_middle[index]])
            middle_of_bb_new_coordinates = (middle_of_bb_old_coordinates + bb_buffer) * upsampling_factor
            middle_of_bb_new_coordinates = np.round(middle_of_bb_new_coordinates).astype(int)

            start_indices = middle_of_mean_img - middle_of_bb_new_coordinates
            stop_indices = middle_of_mean_img - middle_of_bb_new_coordinates + width_upsampled_bbox

            indices_for_placing_bbox = np.s_[start_indices[0]:stop_indices[0],
                                             start_indices[1]:stop_indices[1],
                                             start_indices[2]:stop_indices[2]]

            mean_img[indices_for_placing_bbox] += bbox_image_upsampled
            amount_averaging += 1

    mean_img /= amount_averaging

    print('Calculated the average over ' + str(amount_averaging) + ' kinetochores')

    return mean_img, middle_of_mean_img


# display the kinetochore atlas
def display_the_atlas(mean_img):
    with napari.gui_qt():
        viewer = napari.Viewer()
        viewer.dims.ndisplay = 3
        # noinspection PyTypeChecker
        viewer.add_image(mean_img,
                         colormap='turbo',
                         name='mean kinetochore image')
    return


def kinetochore_atlas_construction(atlas_construction_parameters_file, display=False):

    p = read_in_atlas_construction_config_file(atlas_construction_parameters_file)

    # create a save file
    try:
        print(p.save_folder)
        os.mkdir(p.save_folder)
    except OSError:
        print("Creation of the directory %s failed. It might already exist." % p.save_folder)
    else:
        print("Successfully created the directory %s " % p.save_folder)

    # copy the configurations file to the save folder
    shutil.copy2(atlas_construction_parameters_file, p.save_folder + 'config_file_atlas_construction.yml')

    # save everything that is printed to a log-file
    old_stdout = sys.stdout
    sys.stdout = open(p.save_folder + p.atlas_construction_log_file_name, 'w')

    # depending on the method used to store the points (either excel or .npy files), read them in

    # read in from an excel file
    if p.excel_filename is not None:
        has_excel = True
        # get the centre of mass for each point, their track indices and corresponding frame
        observed_frames, x_com, y_com, z_com = p.observed_frames, p.x_COM, p.y_COM, p.z_COM
        track_indices, frames = p.track_indices, p.frames
        points_list = None

    # read in from .npy files
    else:
        has_excel = False
        # get the names of the observed frames, the corresponding centre of mass for each point and the list of points
        # in each frame
        points_list = read_in_point_locations_from_list_of_numpy_files(p)
        observed_frames = p.observed_frames
        track_indices, frames, x_com, y_com, z_com = None, None, None, None, None

    # create an object in which the parameters of the good 'non-touching' objects that are not in a conglomerate
    # are stored. Objects are defined as non-touching if the ellipsoids at their centre points do not touch
    nto = NonTouchingObjects()

    # loop over all observed frames and get the parameters from the objects in this frame
    for i, frame_no in enumerate(observed_frames):

        if has_excel:
            # get information on position of points and their label in that frame
            list_indices_of_frame = np.where(frames == frame_no + 1)
            track_index_in_frame = track_indices[list_indices_of_frame]

            x_com_frame, y_com_frame, z_com_frame = (x_com[list_indices_of_frame], y_com[list_indices_of_frame],
                                                     z_com[list_indices_of_frame])

            points_in_frame = np.array([x_com_frame, y_com_frame, z_com_frame]).T

        else:
            list_indices_of_frame, track_index_in_frame = None, None
            points_in_frame = points_list[i]

        orig_image_xyz_roi = ri.load_image_from_memory(frame_no, p.data_folder, p.microscopy_data_filename_pre_index,
                                                       p.microscopy_data_filename_post_index,
                                                       p.microscopy_data_file_extension, p.roi)

        # set lowest value to zero
        orig_image_xyz_roi -= np.min(orig_image_xyz_roi)

        label_ellipsoid, ellipsoid_size = create_mask_with_ellipsoid_sized_windows_at_points(points_in_frame,
                                                                                             p.ellipsoid_size_tuple,
                                                                                             orig_image_xyz_roi)

        # if 'display' is activated: show the original image, the points of the centre of mass tracked by Judith and
        # the labelled ellipsoids
        if display:
            display_original_image_with_ellipsoids(orig_image_xyz_roi, points_in_frame, label_ellipsoid, p, frame_no)

        # get the properties of the regions in the label image
        region_props = skimage.measure.regionprops(label_ellipsoid, intensity_image=orig_image_xyz_roi)

        # add the fitting parameters for the non-touching objects for which the ellipsoid masks where created
        # using the region properties
        nto = get_the_fit_parameters_for_non_touching_objects_in_ellipsoid_mask(region_props, ellipsoid_size, nto,
                                                                                frame_no, has_excel, points_in_frame,
                                                                                track_index_in_frame)

    # create a pandas dataframe with all the information stored in the NonTouchingObjects object before and save
    # it as an excel file
    df_kinetochor_analysis_all, na_tracking_labels = create_a_data_frame_with_info_on_objects_used(nto)
    with pd.ExcelWriter(p.save_folder + p.output_excel_name) as output_excel:
        df_kinetochor_analysis_all.to_excel(output_excel,
                                            sheet_name='atlas data kinetochores',
                                            float_format='%.6f')

    # create and save plots with two of the fitting parameters correlated
    create_plots_of_fitting_results(df_kinetochor_analysis_all, p)
    # create a plot of the mean, max and minimum intensity and their variance over the frame number
    create_plot_of_mean_max_and_min_intensity_and_variance_over_frame(df_kinetochor_analysis_all, p,
                                                                      observed_frames)
    # if the manual tracking indices are known, create a plots for every object, showing its intensity statistics
    # (mean, min and max intensity)
    if has_excel:
        create_plot_of_20_unique_objects_each_with_their_mean_max_and_min_intensity(df_kinetochor_analysis_all,
                                                                                    na_tracking_labels, p,
                                                                                    observed_frames)

    # calculate the mean kinetochore and the middle of the mean kinetochore and save each as a .npy file
    # if the display option is activated, display the model
    mean_img, middle_of_mean_img = calculate_average_over_kinetochores(p, df_kinetochor_analysis_all, observed_frames)
    np.save(p.save_folder + p.model_data_output_name, mean_img)
    np.save(p.save_folder + p.middle_of_model_output_name, middle_of_mean_img)
    if display:
        display_the_atlas(mean_img)

    sys.stdout = old_stdout

    return


if __name__ == '__main__':
    # config_file = "config_judith_old_data_atlas_construction_new_computer.yml"
    config_file = "config_judith_new_data_atlas_construction_second_iteration_new_computer.yml"
    kinetochore_atlas_construction(config_file, display=True)
