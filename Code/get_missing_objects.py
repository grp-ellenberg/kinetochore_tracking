from solve_min_cost_flow import solve_min_flow_eq_linear_programming
from connection_matrix_with_exclusion_sets import ConnectionMatrix
from get_connections_for_connection_matrix import get_possible_connections
from distance_measures import CostVector
import split_big_objects as sbo
import yaml
import get_threshold_values_for_segmentation as gtv


# find missing objects from a points of subsequent frames that have a specified number of points
# given a list of the points in each frame and the number of points expected:
# inputs: points_lists: list or np.array of a list/array containing the points (3D) that were found in each frame
#         time_points: list containing the time points (the frame numbers) to consider for tracking
#         config_file: the corresponding configuration (.yml) file
def find_missing_objects(points_lists, time_points, config_file, metadata):
    # get the parameters needed from the configuration file and read them into variables
    config_dict = ri.read_in_config(config_file)

    # create a dict of data parameters only
    data_parameters = config_dict["data parameters"]

    # get the data folder, where the input data is stored, and the analysis folder,
    # where the output data is stored
    data_folder = data_parameters["data folder"]
    # get the file name conventions
    microscopy_data_filename_pre_index = data_parameters["microscopy data filename pre index"]
    microscopy_data_filename_post_index = data_parameters["microscopy data filename post index"]
    microscopy_data_file_extension = data_parameters["microscopy data file extension"]

    # get the tracking parameters
    tracking_parameters = config_dict["tracking parameters"]

    # get the number of points expected
    number_of_points_expected = tracking_parameters["number expected"]

    # read in the constraints for moving, splitting and merging
    constr_moving = tracking_parameters["constraint moving"]
    constr_split_merge = tracking_parameters["constraint split or merge"]

    # read in the penalty for (dis)appear, split and merge events
    dis_appear_penalty = tracking_parameters["disappear penalty"]
    split_merge_penalty = tracking_parameters["split merge penalty"]

    # read in the weights for the various distance measures
    distance_measure_weights = [tracking_parameters["w_eu_dist"],
                                tracking_parameters["w_int_diff"],
                                tracking_parameters["w_size_diff"],
                                tracking_parameters["w_voronoy_star"],
                                tracking_parameters["w_reverse_size"]]

    # read in, whether the distance measures are used for transitions
    use_for_transition = [tracking_parameters["eu_dist_trans"],
                          tracking_parameters["int_diff_trans"],
                          tracking_parameters["size_diff_trans"],
                          tracking_parameters["voronoy_star_trans"],
                          tracking_parameters["reverse_size_trans"]]

    # read in, whether the distance measures are used for splitting/merging
    use_for_split_merge = [tracking_parameters["eu_dist_split_merge"],
                           tracking_parameters["int_diff_split_merge"],
                           tracking_parameters["size_diff_split_merge"],
                           tracking_parameters["voronoy_star_split_merge"],
                           tracking_parameters["reverse_size_split_merge"]]

    # read in, whether the distance measures are used for (dis)appear events
    use_for_dis_appear = [tracking_parameters["eu_dist_dis_appear"],
                          tracking_parameters["int_diff_dis_appear"],
                          tracking_parameters["size_diff_dis_appear"],
                          tracking_parameters["voronoy_star_dis_appear"],
                          tracking_parameters["reverse_size_dis_appear"]]

    # get the region of interest from the configuration file
    roi_parameters = config_dict["roi parameters"]
    # get the offset, a parameter by which the ROI can easily manipulated (made > or <)
    roi_offset = roi_parameters["roi offset"]
    # get the ROI from the parameters read in above:
    # get the minima and maxima of the bounding box of the region of interest in x,y and z
    roi_xmin, roi_xmax = roi_parameters["roi xmin"] - roi_offset, roi_parameters["roi xmax"] + roi_offset
    roi_ymin, roi_ymax = roi_parameters["roi ymin"] - roi_offset, roi_parameters["roi ymax"] + roi_offset
    roi_zmin, roi_zmax = roi_parameters["roi zmin"] - roi_offset, roi_parameters["roi zmax"] + roi_offset
    # define the ROI as a np slice
    roi = np.s_[roi_xmin:roi_xmax, roi_ymin:roi_ymax, roi_zmin:roi_zmax]

    # find all frames in which all points were found, add min(time_points)-1 and max(time_points)+1 to the list,
    # to make sure all frames are reached when later iterating between the frames where all points have been found
    frames_with_all_points = [np.min(time_points) - 1]
    for i, points in enumerate(points_lists):
        if len(points) == number_of_points_expected:
            frames_with_all_points.append(time_points[i])
    frames_with_all_points.append(np.max(time_points) + 1)

    print("The frames with all points are: ")
    print(frames_with_all_points)

    # for every frame with all points, "go" in both directions and find the missing objects in the adjacent frame by
    # identifying the object(s) which, when split in two, will yield the missing point
    for i in range(len(frames_with_all_points) - 2):
        # first go backwards (this is why -1 is included min(time_points)-1
        frame_with_all_points = frames_with_all_points[i + 1]
        frame_before, frame_after = frames_with_all_points[i], frames_with_all_points[i + 2]

        for j in range(frame_with_all_points, frame_before, -1):
            print("transition between frame " + str(j) + " and frame " + str(j - 1))
            fixed_points = points_lists[time_points.index(j)]
            moving_points = points_lists[time_points.index(j - 1)]

            fixed_image = ri.load_image_from_memory(j, data_folder,
                                                    microscopy_data_filename_pre_index,
                                                    microscopy_data_filename_post_index,
                                                    microscopy_data_file_extension,
                                                    roi)
            moving_image = ri.load_image_from_memory(j - 1, data_folder,
                                                     microscopy_data_filename_pre_index,
                                                     microscopy_data_filename_post_index,
                                                     microscopy_data_file_extension,
                                                     roi)

            # test minimum flow solver for real images
            # first get possible transitions

            possible_l_r_transitions, possible_splits, possible_merges = get_possible_connections(fixed_points,
                                                                                                  moving_points,
                                                                                                  constr_moving,
                                                                                                  constr_split_merge)

            can_split, can_merge, can_appear, can_disappear = True, True, False, False

            # print out how many possible events there are
            print("# possible splits: " + str(len(possible_splits)))
            print("# possible merges: " + str(len(possible_merges)))
            print("# possible moves:  " + str(len(possible_l_r_transitions) * len(possible_l_r_transitions[0])))

            # create a connection matrix and a cost vector with the connections defined above
            m = ConnectionMatrix(len(fixed_points), len(moving_points), can_split, can_merge, can_appear, can_disappear,
                                 (possible_l_r_transitions, possible_splits, possible_merges))


            cv = CostVector(fixed_points, moving_points, fixed_image, moving_image, m, metadata,
                            dis_appear_penalty=dis_appear_penalty, split_merge_penalty=split_merge_penalty,
                            distance_measure_weights=distance_measure_weights,
                            use_dist_measure_for_transition=use_for_transition,
                            use_dist_measure_for_split_and_merge=use_for_split_merge,
                            use_dist_measure_for_appear_disappear=use_for_dis_appear)

            print("Solving linear programming problem ...")

            sol_vec = solve_min_flow_eq_linear_programming(m, cv)

            for k, val in enumerate(list(m.matrix[m.d.slice, :][0])):
                if val != 0 and np.abs(sol_vec[k]) > 0.01:
                    print(str(m.column_names[k]) + ": " + str(val * sol_vec[k]))

            # display_outcome_tracking(m, sol_vec, fixed_points, moving_points,
            #                          fixed_image, moving_image, possible_merges, possible_splits,
            #                          constr_moving, display=False)
            # get all merged points
            merged_points = []

            for possible_merge, val in zip(possible_merges, sol_vec[m.lx2_to_d_r.slice]):
                if val > 0:
                    print("val: " + str(val))
                    merged_points.append(moving_points[possible_merge[2]])

            # convert to a numpy array (needed for the split big objects input)
            merged_points = np.array(merged_points)

            # load the thresholds from the .yml file
            with open(analysis_folder + 'automatic_segmentation_thresholds.yml', 'r') as stream:
                thresholds_dict = yaml.load(stream, Loader=yaml.Loader)

            # get thresholds in frame taking into account bleaching depending on time point/frame number
            new_t_dict = gtv.get_thresholds_after_considering_bleaching(time_points.index(j - 1), time_points,
                                                                        thresholds_dict["slope_bleaching"],
                                                                        (thresholds_dict["max_points_init"],
                                                                         thresholds_dict["double_thresholding_init"],
                                                                         thresholds_dict["watershed_intensity_init"]),
                                                                        return_dict_instead=True
                                                                        )
            # calculate the updated points separating the objects that 'merge' and store them in the points_lists
            new_points = sbo.split_given_objects(moving_points, merged_points, moving_image, new_t_dict["watershed_intensity"])
            print(new_points)
            points_lists[time_points.index(j - 1)] = new_points
            print("..........................................")


if __name__ == "__main__":

    import numpy as np
    import pandas as pd
    import read_in as ri

    # define the configuration file
    config_file = "C:/Users/janaw/Documents/Master-Arbeit/Project/Code/config_judith_old_data_new_computer.yml"
    # config_file = "C:/Users/Jana/Documents/Code_14_08/config_judith_old_data_office.yml"
    # config_file = "/home/jana/Documents/EMBL/Master-Arbeit/Code/config_judith_old_data.yml"
    config_dict = ri.read_in_config(config_file)

    ####################################################################################################
    ###################################### read the config file ########################################
    ####################################################################################################

    # get all parameters where data is stored/ to be stored

    # create a dict of data parameters only
    data_parameters = config_dict["data parameters"]

    # get the data folder, where the input data is stored, and the analysis folder,
    # where the output data is stored
    analysis_folder = data_parameters["analysis folder"]
    data_folder = data_parameters["data folder"]

    # get the file name conventions
    excel_filename = data_parameters["excel filename"]
    excel_tracking_column_x_pos = data_parameters["excel tracking column name x"]
    excel_tracking_column_y_pos = data_parameters["excel tracking column name y"]
    excel_tracking_column_z_pos = data_parameters["excel tracking column name z"]
    metadata_filename = data_parameters["metadata filename"]
    microscopy_data_filename_pre_index = data_parameters["microscopy data filename pre index"]
    microscopy_data_filename_post_index = data_parameters["microscopy data filename post index"]
    microscopy_data_file_extension = data_parameters["microscopy data file extension"]

    # get all metadata parameters and read in the metadata

    # create a dict of metadata parameters only
    metadata_parameters = config_dict["metadata parameters"]

    # get the physical size of one pixel (resolution) in x,y and z
    phys_size_x = metadata_parameters["phys_size_x"]
    phys_size_y = metadata_parameters["phys_size_y"]
    phys_size_z = metadata_parameters["phys_size_z"]

    # get the unit of the resolution in x,y and z
    size_unit_x = metadata_parameters["size_unit_x"]
    size_unit_y = metadata_parameters["size_unit_y"]
    size_unit_z = metadata_parameters["size_unit_z"]

    # read in the metadata
    metadata_path = data_folder + metadata_filename + ".ome.tiff"

    # get the ROI parameters and define the ROI
    # create a dictionary for all region of interest related parameters
    roi_parameters = config_dict["roi parameters"]
    # get the offset, a parameter by which the ROI can easily manipulated (made > or <)
    roi_offset = roi_parameters["roi offset"]

    # get all time_points to be examined
    time_points = roi_parameters["time points"]

    ####################################################################################################
    ################################ read in and rearrange this data ###################################
    ####################################################################################################

    # extract the metadata using a self-written function (returns a dictionary)
    metadata = ri.get_metadata_ometiff(metadata_path)
    metadata["phys_size_x"] = phys_size_x
    metadata["phys_size_y"] = phys_size_y
    metadata["phys_size_z"] = phys_size_z
    metadata["x_size_unit"] = size_unit_x
    metadata["y_size_unit"] = size_unit_y
    metadata["z_size_unit"] = size_unit_z

    # get the ROI from the parameters read in above:
    # get the minima and maxima of the bounding box of the region of interest in x,y and z
    roi_xmin, roi_xmax = roi_parameters["roi xmin"] - roi_offset, roi_parameters["roi xmax"] + roi_offset
    roi_ymin, roi_ymax = roi_parameters["roi ymin"] - roi_offset, roi_parameters["roi ymax"] + roi_offset
    roi_zmin, roi_zmax = roi_parameters["roi zmin"] - roi_offset, roi_parameters["roi zmax"] + roi_offset
    # define the ROI as a np slice
    roi = np.s_[roi_xmin:roi_xmax, roi_ymin:roi_ymax, roi_zmin:roi_zmax]

    # read in the tracking from the Excel, if an Excel filename was given
    if excel_filename is not None:
        # create pandas data frame from Excel-Data
        tracking_excel_path = data_folder + excel_filename
        tracking_data_df = pd.read_excel(tracking_excel_path)

        # get minimum and maximum tracking frame
        min_tracking_frame = int(tracking_data_df["Frame"].min(skipna=True))
        print("Excel: Tracking starts in frame " + str(min_tracking_frame))
        max_tracking_frame = int(tracking_data_df["Frame"].max(skipna=True))
        print("Excel: Tracking ends in frame " + str(max_tracking_frame))

        # get positions from data, their frame and the track number
        frames = tracking_data_df["Frame"].to_numpy()
        track_indices = tracking_data_df["Track"].to_numpy()
        pair_indices = tracking_data_df["Pair"].to_numpy()
        # take into account,that only the ROI will be processed and labelling starts with 0 in python
        x_COM = tracking_data_df[excel_tracking_column_x_pos] - roi_xmin
        y_COM = tracking_data_df[excel_tracking_column_y_pos] - roi_ymin
        z_COM = tracking_data_df[excel_tracking_column_z_pos] - roi_zmin - 1
        x_COM, y_COM, z_COM = x_COM.to_numpy(), y_COM.to_numpy(), z_COM.to_numpy()
    else:
        x_COM, y_COM, z_COM = None, None, None

    all_points_split = []
    for time_point in time_points:
        try:
            all_points_split.append(
                np.load(analysis_folder + "new_automatic_points_in_frame_" + str(time_point) + ".npy"))
        except FileNotFoundError:
            all_points_split.append(
                np.load(analysis_folder + "automatic_points_in_frame_" + str(time_point) + ".npy"))

    find_missing_objects(all_points_split, time_points, config_file, metadata)
