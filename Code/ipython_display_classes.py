import matplotlib.pyplot as plt
from display_functions import new_cmap_diverging_at_zero
import numpy as np
import ipywidgets as ipyw
import scipy.ndimage.filters


class ImageSlicesAndPointViewer:

    def __init__(self, imgs, points=None, metadata_var=None, is_natural_units=True, figsize=(8, 6), cmin=0, cmax=0,
                 cmap='viridis'):  # Jana: change default cmap from plasma to viridis
        # volume = 3D image stack
        self.v = None
        self.is_natural_units = is_natural_units
        self.figsize = figsize
        self.x_label = None
        self.y_label = None
        self.title = None
        self.vol = []
        self.fig = None
        self.ax = None
        self.extent = None
        self.view = None
        self.metadata = metadata_var
        self.orient = {"y-z": [1, 2, 0], "z-x": [2, 0, 1], "x-y": [0, 1, 2]}
        if cmap == 'diverging':
            self.cmap = new_cmap_diverging_at_zero(cmin, cmax)
            self.is_diverging = True
            self.cmin = cmin
            self.cmax = cmax
        else:
            self.cmap = plt.get_cmap(cmap)
            self.is_diverging = False

        self.imgs = imgs
        self.num_imgs = len(imgs)

        self.points = points
        if self.points is not None:
            self.num_points = len(points)

        # Call to select slice plane
        ipyw.interact(self.view_selection, view=ipyw.RadioButtons(
            options=['x-y', 'y-z', 'z-x'], value='x-y',
            description='Slice plane selection:', disabled=False,
            style={'description_width': 'initial'}))

    def view_selection(self, view):
        # Transpose the volume to orient according to the slice plane selection

        self.view = view
        for i in range(self.num_imgs):
            self.vol.append(np.transpose(self.imgs[i], axes=self.orient[self.view]))
        max_z = self.vol[0].shape[self.orient[self.view][2]] - 1

        # Set the corresponding extent in real units
        if self.metadata is not None:
            extent_x = self.imgs[0].shape[0] * self.metadata['phys_size_x']
            extent_y = self.imgs[0].shape[1] * self.metadata['phys_size_y']

            try:
                extent_z = self.imgs[0].shape[2] * self.metadata['new_phys_size_z']

            except KeyError:
                extent_z = self.imgs[0].shape[2] * self.metadata['phys_size_z']
                print("Assume this not interpolated --> old size z for representation")

        else:
            extent_x, extent_y, extent_z = self.imgs[0].shape[0], self.imgs[0].shape[1], self.imgs[0].shape[2]

        extent_dir = {"y-z": [extent_y, extent_z], "z-x": [extent_z, extent_x], "x-y": [extent_x, extent_y]}
        self.extent = extent_dir[view]

        # Jana: Use labels for the plot's axes and a title, also tell user in what direction the slices are selected
        labels = {"y-z": ["y", "z"], "z-x": ["z", "x"], "x-y": ["x", "y"]}
        slice_sel = {"y-z": "x", "z-x": "y", "x-y": "z"}
        self.x_label = labels[self.view][0] + "-axis"
        self.y_label = labels[self.view][1] + "-axis"
        self.title = "View of " + view + "-slice"

        # Call to view a slice within the selected slice plane
        # Jana: tell user in what direction the slices are selected
        ipyw.interact(self.plot_slice,
                      z=ipyw.IntSlider(min=0, max=max_z, step=1, continuous_update=False,
                                       description=f'Select {slice_sel[self.view]}-slice:'))

    def plot_slice(self, z):

        x_vals = []
        y_vals = []
        colors = ['green', 'red', 'yellow', 'blue', 'black', 'white', 'purple', 'orange']

        if self.points is not None:
            for i, point_set in enumerate(self.points):
                x_vals.append([])
                y_vals.append([])
                for point in point_set:
                    print(point)
                    if np.round(point[self.orient[self.view][2]]) == z:
                        x_vals[i].append(point[self.orient[self.view][0]] + 0.5)
                        y_vals[i].append(point[self.orient[self.view][1]] - 0.5)
                        print('found point')
                if not x_vals[i]:
                    print('found no points')

        print(x_vals)
        print(y_vals)

        # Plot slice for the given plane and slice
        self.fig, self.ax = plt.subplots(ncols=self.num_imgs, sharey='row', figsize=self.figsize)

        # set make the figures background translucent
        self.fig.patch.set_facecolor('#E0E0E0')
        self.fig.patch.set_alpha(0.0)

        if self.is_diverging:
            self.v[0] = self.cmin
            self.v[1] = self.cmax

        if self.num_imgs == 1:

            self.ax.imshow(self.vol[0][:, :, z], cmap=self.cmap, origin='lower',
                           vmin=np.min(self.imgs[0]), vmax=np.max(self.imgs[0]),
                           extent=[-0.5, self.extent[0] - 0.5, -0.5, self.extent[1] - 0.5])

            if self.points is not None:
                for j in range(len(self.points)):
                    self.ax.plot(x_vals[j], y_vals[j], marker='o', markersize=10, color=colors[j])

        else:

            for i in range(self.num_imgs):
                self.ax[i].imshow(self.vol[i][:, :, z], cmap=self.cmap, origin='lower',
                                  vmin=np.min(self.imgs[i]), vmax=np.max(self.imgs[i]),
                                  extent=[-0.5, self.extent[0] - 0.5, -0.5, self.extent[1] - 0.5])

                if self.points is not None:
                    for j in range(len(self.points)):
                        self.ax[i].plot(x_vals[j], y_vals[j], marker='o', markersize=10, color=colors[j])

        plt.xlabel(self.x_label)
        plt.ylabel(self.y_label)
        plt.title(self.title)


# image slice viewer
class ImageSliceViewer3D:
    """
    ImageSliceViewer3D is for viewing volumetric image slices in jupyter or
    ipython notebooks.

    User can interactively change the slice plane selection for the image and
    the slice plane being viewed.

    Arguments:
    Volume = 3D input image
    figsize = default(8,8), to set the size of the figure
    cmap = default('plasma'), string for the matplotlib colormap. You can find
    more matplotlib colormaps on the following link: https://matplotlib.org/users/colormaps.html
    Jana: source: https://github.com/mohakpatel/ImageSliceViewer3D/blob/master/ImageSliceViewer3D.ipynb 24.01.2019

    """

    def __init__(self, volume, metadata_var=None, is_natural_units=True, figsize=(8, 6), cmin=0, cmax=0,
                 cmap='viridis'):  # Jana: change default cmap from plasma to viridis
        # volume = 3D image stack
        self.is_natural_units = is_natural_units
        self.volume = volume
        self.figsize = figsize
        self.x_label = None
        self.y_label = None
        self.title = None
        self.vol = None
        self.fig = None
        self.extent = None
        self.metadata = metadata_var
        if cmap == 'diverging':
            self.cmap = new_cmap_diverging_at_zero(cmin, cmax)
            self.is_diverging = True
            self.cmin = cmin
            self.cmax = cmax
        else:
            self.cmap = plt.get_cmap(cmap)
            self.is_diverging = False
        self.v = [np.min(self.volume), np.max(self.volume)]

        # Call to select slice plane
        ipyw.interact(self.view_selection, view=ipyw.RadioButtons(
            options=['x-y', 'y-z', 'z-x'], value='x-y',
            description='Slice plane selection:', disabled=False,
            style={'description_width': 'initial'}))

    def view_selection(self, view):
        # Transpose the volume to orient according to the slice plane selection
        orient = {"y-z": [1, 2, 0], "z-x": [2, 0, 1], "x-y": [0, 1, 2]}
        self.vol = np.transpose(self.volume, axes=orient[view])
        max_z = self.vol.shape[2] - 1

        # Set the corresponding extent in real units
        if self.metadata is not None:
            extent_x = self.volume.shape[0] * self.metadata['phys_size_x']
            extent_y = self.volume.shape[1] * self.metadata['phys_size_y']

            try:
                extent_z = self.volume.shape[2] * self.metadata['new_phys_size_z']

            except KeyError:
                extent_z = self.volume.shape[2] * self.metadata['phys_size_z']
                print("Assume this not interpolated --> old size z for representation")

        else:
            extent_x, extent_y, extent_z = self.volume.shape[0], self.volume.shape[1], self.volume.shape[2]

        extent_dir = {"y-z": [extent_y, extent_z], "z-x": [extent_z, extent_x], "x-y": [extent_x, extent_y]}
        self.extent = extent_dir[view]

        # Jana: Use labels for the plot's axes and a title, also tell user in what direction the slices are selected
        labels = {"y-z": ["y", "z"], "z-x": ["z", "x"], "x-y": ["x", "y"]}
        slice_sel = {"y-z": "x", "z-x": "y", "x-y": "z"}
        self.x_label = labels[view][0] + "-axis"
        self.y_label = labels[view][1] + "-axis"
        self.title = "View of " + view + "-slice"

        # Call to view a slice within the selected slice plane
        # Jana: tell user in what direction the slices are selected
        ipyw.interact(self.plot_slice,
                      z=ipyw.IntSlider(min=0, max=max_z, step=1, continuous_update=False,
                                       description=f'Select {slice_sel[view]}-slice:'))

    def plot_slice(self, z):
        # Plot slice for the given plane and slice
        self.fig = plt.figure(figsize=self.figsize)

        # set make the figures background translucent
        self.fig.patch.set_facecolor('#E0E0E0')
        self.fig.patch.set_alpha(0.0)

        if self.is_diverging:
            self.v[0] = self.cmin
            self.v[1] = self.cmax

        if self.is_natural_units:
            plt.imshow(self.vol[:, :, z], cmap=self.cmap,
                       vmin=self.v[0], vmax=self.v[1], extent=[0, self.extent[0], 0, self.extent[1]])

        else:
            plt.imshow(self.vol[:, :, z], cmap=self.cmap,
                       vmin=self.v[0], vmax=self.v[1])

        # Jana: add color bar (scale) to the plot, label the axes and add a title
        plt.colorbar()
        plt.xlabel(self.x_label)
        plt.ylabel(self.y_label)
        plt.title(self.title)


class GaussianBlurSliceViewer3D:
    """
    ImageSliceViewer3D is for viewing volumetric image slices in jupyter or
    ipython notebooks.

    User can interactively change the slice plane selection for the image and
    the slice plane being viewed.

    Arguments:
    Volume = 3D input image
    figsize = default(8,8), to set the size of the figure
    cmap = default('plasma'), string for the matplotlib colormap. You can find
    more matplotlib colormaps on the following link:
    https://matplotlib.org/users/colormaps.html
    Jana: source: https://github.com/mohakpatel/ImageSliceViewer3D/blob/master/ImageSliceViewer3D.ipynb 24.01.2019

    """

    def __init__(self, volume, metadata_var, figsize=(16, 10), cmap='viridis'):
        # volume = 3D image stack
        self.gauss_volume = volume
        self.init_volume = volume
        self.thresh_volume = volume
        self.figsize = figsize
        self.cmap = cmap
        self.v = [np.min(volume), np.max(volume)]
        self.sigma = 0
        self.slice = 0
        self.view = 'x-y'
        self.thresh = 0
        self.gauss_vol = None
        self.init_vol = None
        self.thresh_vol = None
        self.x_label = None
        self.y_label = None
        self.title = None
        self.metadata = metadata_var

        # Jana: Call to blur the image with a Gaussian blur
        ipyw.interact(self.choose_sigma, sigma=ipyw.FloatSlider(min=0, max=10, step=0.33, continuous_update=False,
                                                                description=f'1. Select sigma-value: ',
                                                                style={'description_width': 'initial'}))

    def choose_sigma(self, sigma):
        self.sigma = sigma

        # Jana: Apply Gaussian filter to image
        self.gauss_volume = scipy.ndimage.filters.gaussian_filter(self.init_volume, sigma=self.sigma)

        # Jana: Possibility to set a threshold
        ipyw.interact(self.set_threshold,
                      thresh=ipyw.FloatSlider(min=0, max=np.max(self.gauss_volume), step=0.2, continuous_update=False,
                                              description=f'3. Select a threshold:',
                                              style={'description_width': 'initial'},
                                              value=self.thresh))

    def set_threshold(self, thresh):
        self.thresh = thresh

        self.thresh_volume = self.gauss_volume > thresh

        # Call to select slice plane
        ipyw.interact(self.view_selection, view=ipyw.RadioButtons(
            options=['x-y', 'y-z', 'z-x'], value='x-y',
            description=f'2. Select slice plane:', disabled=False,
            style={'description_width': 'initial'}))

    def view_selection(self, view):
        self.view = view
        # Transpose the volume to orient according to the slice plane selection
        orient = {"y-z": [1, 2, 0], "z-x": [2, 0, 1], "x-y": [0, 1, 2]}
        self.gauss_vol = np.transpose(self.gauss_volume, orient[self.view])
        self.init_vol = np.transpose(self.init_volume, orient[self.view])
        self.thresh_vol = np.transpose(self.thresh_volume, orient[self.view])

        # Jana: Use labels for the plot's axes and a title, also tell user in what direction the slices are selected
        labels = {"y-z": ["y", "z"], "z-x": ["z", "x"], "x-y": ["x", "y"]}
        slice_sel = {"y-z": "x", "z-x": "y", "x-y": "z"}
        self.x_label = labels[self.view][0] + "-axis"
        self.y_label = labels[self.view][1] + "-axis"
        self.title = "View of " + self.view + "-slice (blurred and original)"

        # Call to view a slice within the selected slice plane
        # Jana: tell user in what direction the slices are selected, add initial value
        max_z = self.gauss_vol.shape[2] - 1
        ipyw.interact(self.plot_slice,
                      z=ipyw.IntSlider(min=0, max=max_z, step=1, continuous_update=False,
                                       description=f'3. Select {slice_sel[self.view]}-slice:',
                                       style={'description_width': 'initial'}, value=self.slice))

    def plot_slice(self, z):
        self.slice = z

        extent_x = float(self.metadata["size_x"]) * self.metadata['phys_size_x']
        extent_y = float(self.metadata["size_y"]) * self.metadata['phys_size_y']

        # Plot slice for the given plane and slice
        fig, axs = plt.subplots(nrows=3, ncols=3, figsize=self.figsize)
        fig.subplots_adjust(left=0.02, bottom=0.06, right=0.95, top=0.94, wspace=0.05)
        fig.suptitle(self.title)

        im_0 = axs[0, 0].imshow(self.init_vol[:, :, self.slice], cmap=plt.get_cmap(self.cmap),
                                vmin=self.v[0], vmax=self.v[1], extent=[0, extent_x, 0, extent_y])

        # Jana: add color bar (scale) to the plot, label the axes and add a title
        axs[0, 0].set_xlabel(self.x_label)
        axs[0, 0].set_ylabel(self.y_label)
        axs[0, 0].set_title('Original')
        fig.colorbar(im_0, ax=axs[0, 0])

        # Jana: Do the same for a the Gaussian blurred image

        im_1 = axs[0, 1].imshow(self.gauss_vol[:, :, self.slice], cmap=plt.get_cmap(self.cmap),
                                vmin=self.v[0], vmax=self.v[1], extent=[0, extent_x, 0, extent_y])
        axs[0, 1].set_xlabel(self.x_label)
        axs[0, 1].set_ylabel(self.y_label)
        axs[0, 1].set_title('Blurred')
        fig.colorbar(im_1, ax=axs[0, 1])

        # Jana: Do the same for a the thresholded blurred image
        im_1 = axs[0, 2].imshow(self.thresh_vol[:, :, self.slice], cmap=plt.get_cmap(self.cmap),
                                vmin=0, vmax=1, extent=[0, extent_x, 0, extent_y])
        axs[0, 2].set_xlabel(self.x_label)
        axs[0, 2].set_ylabel(self.y_label)
        axs[0, 2].set_title('Thresholded')
        fig.colorbar(im_1, ax=axs[0, 2])

        # Jana: Add histogram

        gs = axs[1, 1].get_gridspec()

        axs[1, 0].remove()
        axs[1, 1].remove()
        axs[1, 2].remove()
        ax_row1 = fig.add_subplot(gs[1, :])

        init_hist = self.init_volume.flatten()
        blur_hist = self.gauss_volume.flatten()

        ax_row1.hist(blur_hist, bins=256, range=(1, max(init_hist.max(), blur_hist.max())),
                     color='blue', alpha=0.5, histtype='stepfilled', density=True, label='blurred')

        ax_row1.hist(init_hist, bins=256, range=(1, max(init_hist.max(), blur_hist.max())),
                     color='red', alpha=0.5, histtype='stepfilled', density=True, label='original')

        ax_row1.axvline(self.thresh, color='k', linestyle='dashed', linewidth=1, label='threshold')

        ax_row1.set_ylim((10 ** (-10), 1))
        ax_row1.set_yscale('log', nonposy='clip')

        ax_row1.legend(loc='upper right')

        # Jana: Add histogram non-log scale

        gs = axs[1, 1].get_gridspec()

        axs[2, 0].remove()
        axs[2, 1].remove()
        axs[2, 2].remove()
        ax_row2 = fig.add_subplot(gs[2, :])

        ax_row2.hist(blur_hist, bins=256, range=(1, max(init_hist.max(), blur_hist.max())),
                     color='blue', alpha=0.5, histtype='stepfilled', density=True, label='blurred')

        ax_row2.hist(init_hist, bins=256, range=(1, max(init_hist.max(), blur_hist.max())),
                     color='red', alpha=0.5, histtype='stepfilled', density=True, label='original')

        ax_row2.axvline(self.thresh, color='k', linestyle='dashed', linewidth=1, label='threshold')

        ax_row2.legend(loc='upper right')

        ipyw.interact(self.export_values,
                      export_true=ipyw.ToggleButton(
                          value=False,
                          description='Export Values',
                          disabled=False,
                          button_style='success',  # 'success', 'info', 'warning', 'danger' or ''
                          tooltip='Description',
                          icon='check'  # (FontAwesome names without the `fa-` prefix)
                      ))

    def export_values(self, export_true):
        if export_true:
            return self.sigma, self.thresh, self.slice
