import numpy as np
from scipy.spatial.distance import cdist as scipy_cdist
import itertools
import warnings
from scipy.spatial import Voronoi
import point_object as po
global w


# define the distance such, that it respects the microscopy settings (using a weighting factor w)
def cdist(coords_1, coords_2, weights, metric='euclidean'):
    return scipy_cdist(coords_1, coords_2, metric=metric, w=weights)


# create a class for each distance measure, so the functions/ inputs are
# bundled in one instance and can be looped over
# inputs for init:
#   name: str, name of the distance measure
#   detected_objects_list 1/2: detected objects list (object) in first/second of the two image
#   weight: float, factor by which this measure will be weighted relative to other measures
#   dist_transition_fct: function returning the distance between objects, if given both
#                        the inputs_1 and the inputs_2 (this is the distance between objects
#                        in different images)
#                        distances are calculated between all pairs and returned in matrix form
#   dist_split_or_merge: get the distance between two objects from one image to one object from the other
#                        image. For splitting: there two objects are in the second image, one object in the first image,
#                        for merging: there is one object in the first image, two in the second image.
#                        only return the values for a list of input values
#   distance_to_empty_fct: get the distance to an 'empty' object (that is not in the image, yet)
#                          for all objects in one image, given the input for that image
#                          return a list of distances
class DistanceMeasure:
    def __init__(self, name, inputs_1, inputs_2, weight,
                 dist_transition_fct, dist_split_or_merge_fct, distance_to_empty_fct,
                 use_for_transition, use_for_split_and_merge, use_for_dis_appear, only_use_before_anaphase_onset,
                 penalty_for_single_point):
        self.name = name
        self.inputs_1 = inputs_1
        self.inputs_2 = inputs_2
        self.weight = weight
        self.dist_transition_fct = dist_transition_fct
        self.dist_split_or_merge_fct = dist_split_or_merge_fct
        self.distance_to_empty_fct = distance_to_empty_fct
        self.use_for_transition = use_for_transition,
        self.use_for_split_and_merge = use_for_split_and_merge,
        self.use_for_dis_appear = use_for_dis_appear
        self.only_use_before_anaphase_onset = only_use_before_anaphase_onset,
        self.penalty_for_single_point = penalty_for_single_point


""" functions for defining the 'intensity difference' as a measure of distance"""


# get the intensity difference between each object in the 'detected_objects_list_1' and each object in the
# 'detected_objects_list_2', and return it in matrix form
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a intensities attribute
#         (intensities: list of intensities of the image at the centre of the objects found in each image)
# outputs: intensity_difference: np.array in matrix form containing the intensity_difference between all pairs of points
#          in the detected_objects_list_1 and detected_objects_list_2
def intensity_distance_for_transition(detected_objects_list_1, detected_objects_list_2, _):
    ints_1, ints_2 = detected_objects_list_1.centre_intensities, detected_objects_list_2.centre_intensities
    intensity_difference = np.abs(np.subtract.outer(ints_1, ints_2))
    return intensity_difference


# for a split of merge event: assume the resulting single has an intensity close to the mean of the two
# splitting/merging objects
# inputs: detected_objects_list_im_2x_obj: the detected_objects_list object in the image where there are 2 objects
#                                          (first image for merge, second image for split)
#         detected_objects_list_im_1x_obj: the detected_objects_list object in the image where there is only 1 object
#                                          (second image for merge, first image for split)
def intensity_distance_for_split_or_merge(detected_objects_list_im_2x_obj, detected_objects_list_2_im_1x_obj, _):
    ints_im_2x = detected_objects_list_im_2x_obj.centre_intensities
    ints_im_1x = detected_objects_list_2_im_1x_obj.centre_intensities
    # get the average between 2 intensities in the image with 2 objects:
    # 1) create an array with all possible combinations of intensities:
    ints_im_2x_combinations = np.array(np.meshgrid(ints_im_2x, ints_im_2x)).T
    # 2) calculate the mean between all possible combinations
    av_ints = np.mean(ints_im_2x_combinations, axis=2)
    # calculate the difference between these averages and the objects in the intensities in the image with only one
    # object
    intensity_difference_tensor = np.abs(np.subtract.outer(av_ints, ints_im_1x))
    return intensity_difference_tensor


# get the difference between the intensities in an image and the mean intensity of the other image, as the 'empty'
# object is considered to have the mean intensity
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a intensities attribute
#         (intensities: list of intensities of the image at the centre of the objects found in each image)
def intensity_distance_to_empty_object(detected_objects_list_1, detected_objects_list_2):
    ints_image = detected_objects_list_1.centre_intensities
    ints_other_image = detected_objects_list_2.centre_intensities
    intensity_difference = np.abs(ints_image - np.mean(ints_other_image))
    return intensity_difference


""" functions for defining 'Euclidean distance' as a measure of distance"""


# get the Euclidean distance between each object in the 'detected_objects_list_1' and each object in the
# 'detected_objects_list_2', and return it in matrix form
def euclidean_distance_transition(detected_objects_list_1, detected_objects_list_2, _):
    coords_1 = detected_objects_list_1.coordinates  # resolution_corrected_coordinates
    coords_2 = detected_objects_list_2.coordinates  # resolution_corrected_coordinates
    distance = cdist(coords_1, coords_2, w)  # metric="euclidean"
    print("euclidean distance shape")
    print(distance.shape)
    return distance


# for a split of merge event: assume the resulting single object is 'in between' the two merging object or objects
# resulting from the split
# inputs: detected_objects_list_im_2x_obj: the detected_objects_list object in the image where there are 2 objects
#                                          (first image for merge, second image for split)
#         detected_objects_list_im_1x_obj: the detected_objects_list object in the image where there is only 1 object
#                                          (second image for merge, first image for split)
def euclidean_distance_for_split_or_merge(detected_objects_list_im_2x_obj, detected_objects_list_2_im_1x_obj, _):
    # create np arrays from the lists of np.arrays of the coordinates
    coords_im_2x = np.stack(detected_objects_list_im_2x_obj.coordinates)  # resolution_corrected_coordinates
    coords_im_1x = np.stack(detected_objects_list_2_im_1x_obj.coordinates)  # resolution_corrected_coordinates
    # get the average between 2 coordinates in the image with 2 objects:
    # 1) create an array with all possible combinations of coordinates in each dimension such that all combinations of
    # (x_i, x_j), (y_i, y_j) etc. are explored
    coords_x = np.array(np.meshgrid(coords_im_2x[:, 0], coords_im_2x[:, 0])).T
    coords_y = np.array(np.meshgrid(coords_im_2x[:, 1], coords_im_2x[:, 1])).T
    coords_z = np.array(np.meshgrid(coords_im_2x[:, 2], coords_im_2x[:, 2])).T
    # 2) calculate the mean between all possible combinations in each dimension
    coords_x_mean = np.mean(coords_x, axis=2)
    coords_y_mean = np.mean(coords_y, axis=2)
    coords_z_mean = np.mean(coords_z, axis=2)
    # 3) rearrange the averages, such that av_coords[i,j] gives the average between coords_im_2x[i] and coords_im_2x[j]
    av_coords = np.stack((coords_x_mean, coords_y_mean, coords_z_mean), axis=2)
    # calculate the difference between these averages and the objects in the intensities in the image with only one
    # object
    euclidean_distance_tensor = np.zeros((len(coords_im_2x), len(coords_im_2x), len(coords_im_1x)))
    for i in range(len(coords_im_2x)):
        euclidean_distance_tensor[i, :, :] = cdist(av_coords[i, :, :], coords_im_1x, w)  # metric="euclidean"
    # penalise splits or merges of objects that are further apart in the same image
    distance_same_im = cdist(coords_im_2x, coords_im_2x, w)  # metric="euclidean"
    for i in range(len(coords_im_2x)):
        for j in range(len(coords_im_2x)):
            euclidean_distance_tensor[i, j, :] *= distance_same_im[i, j] * 10 / np.max(distance_same_im)
    return euclidean_distance_tensor


# get euclidean distance to image border (an object can appear in the image from the image border, thus an 'empty
# object' is thought of as being at the image border)
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a coordinates and an im_shape
#         attribute, only the first object is used for the calculations
#         (coordinates: list of np.arrays containing the coordinates of the points) and the im_shape (output
#         of np.shape, tuple of image size)
# dist: list of distances, same length as the points list, containing the distances of each of the points to the
#       image border
def euclidean_distance_to_empty_object(detected_objects_list_1, _):
    # unpack the inputs
    coords = detected_objects_list_1.coordinates  # resolution_corrected_coordinates
    im_shape = detected_objects_list_1.im_shape  # resolution_corrected_im_shape
    # get the distance to the "upper" border for each axis (x,y,z)
    dist_upper_border = np.multiply.outer(np.ones((len(coords))), im_shape) - coords
    # the distance to the lower border, is just the point itself
    dist_lower_border = coords
    # the distance to all sides is the contains both, the distances to the upper and the lower borders of the
    # image
    dist_to_all_sides = np.concatenate((dist_upper_border, dist_lower_border), axis=1)
    # the minimum distance for a point within a rectangular box to the box is just the minimum of the distance to the
    # left, right, upper, lower, front, back side
    dist = np.min(dist_to_all_sides, axis=1)
    return dist


""" functions for defining 'object size' as a measure of distance"""


# get the size difference between each object in the 'detected_objects_list_1' and each object in the
# 'detected_objects_list_2', and return it in matrix form
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a sizes attribute
#         (sizes: list of sizes [volume] of the objects in pixels)
# outputs: size_difference: np.array in matrix form containing the size_difference between all pairs of points
#          in the detected_objects_list_1 and detected_objects_list_2
def size_distance_for_transition(detected_objects_list_1, detected_objects_list_2, _):
    print(len(detected_objects_list_1.watershed_sizes))
    print(len(detected_objects_list_2.watershed_sizes))
    sizes_1, sizes_2 = detected_objects_list_1.watershed_sizes, detected_objects_list_2.watershed_sizes
    size_difference = np.abs(np.subtract.outer(sizes_1, sizes_2))
    print("size difference shape")
    print(size_difference.shape)
    return size_difference


# for a split of merge event: assume the single object is as big as the two split/merging objects
# together.
# inputs: detected_objects_list_im_2x_obj: the detected_objects_list object in the image where there are 2 objects
#                                          (first image for merge, second image for split)
#         detected_objects_list_im_1x_obj: the detected_objects_list object in the image where there is only 1 object
#                                          (second image for merge, first image for split)
def size_distance_for_split_or_merge(detected_objects_list_im_2x_obj, detected_objects_list_im_1x_obj, _):
    sizes_im_2x = detected_objects_list_im_2x_obj.watershed_sizes
    sizes_im_1x = detected_objects_list_im_1x_obj.watershed_sizes
    # get the average between 2 intensities in the image with 2 objects:
    # 1) create an array with all possible combinations of intensities:
    sizes_im_2x_combinations = np.array(np.meshgrid(sizes_im_2x, sizes_im_2x)).T
    # 2) calculate the sum of all possible combinations
    sum_sizes = np.sum(sizes_im_2x_combinations, axis=2)
    # calculate the difference between these averages and the objects in the intensities in the image with only one
    # object
    size_difference_tensor = np.abs(np.subtract.outer(sum_sizes, sizes_im_1x))
    return size_difference_tensor


# get the difference between the sizes in an image and the mean size of the other image, as the 'empty'
# object is considered to have the mean size
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a "sizes" attribute
#         (sizes: list of the sizes [volume] of the objects in pixels)
def size_distance_to_empty_object(detected_objects_list_1, detected_objects_list_2):
    sizes_image = detected_objects_list_1.watershed_sizes
    sizes_other_image = detected_objects_list_2.watershed_sizes
    size_difference = np.abs(sizes_image - np.mean(sizes_other_image))
    return size_difference


""" functions for defining 'mean minimum distance of Voronoy star's ends' as a measure of distance"""


# Compare vectors that are part of the voronoy tessellation for a certain point (the so called 'voronoy star' of the
# point) to another voronoy tessellation:
# the distance between all 'arms' of star 1 and star 2 is calculated and their minimum is taken, then the mean of
# these minima is defined as the distance of two voronoy stars.
# To make it symmetric, the minima are calculated along both axes (the minimum distance of every arm in the first
# star to every arm in the second star and vice versa). The resulting 1D numpy arrays are combined before calculating
# the mean.
def mean_dist_to_nearest_extremity_in_voronoy_star(voronoy_star_1, voronoy_star_2):
    voronoy_star_1, voronoy_star_2 = np.array(voronoy_star_1), np.array(voronoy_star_2)
    # get the distances between the end points of each voronoy star
    distances = cdist(voronoy_star_1, voronoy_star_2, w)
    # calculate the minimum along each axis
    nearest_neighbour_dist_for_each_voronoy_star_arm_12 = np.min(distances, axis=1)
    nearest_neighbour_dist_for_each_voronoy_star_arm_21 = np.min(distances, axis=0)
    # concatenate the 1D numpy arrays with the minima
    nn_dist_for_each_voronoy_star_arm = np.concatenate((nearest_neighbour_dist_for_each_voronoy_star_arm_12,
                                                        nearest_neighbour_dist_for_each_voronoy_star_arm_21))
    # get the mean nearest neighbour distance between the two voronoy stars
    mean_nn_dist = np.mean(nn_dist_for_each_voronoy_star_arm)
    return mean_nn_dist


# for a point in an image, get the corresponding point at the image border with the minimum distance to this point
# inputs: coordinates_image: list of coordinate values for each point (3D np.arrays)
#         image_shape: shape attribute of the image
# output: image_border_coordinates: list of coordinate values (same length as 'coordinates_image) of the closest
#         point on the image border for each point in 'coordinates_image)
def get_nearest_points_on_image_border_list(coordinates_image, image_shape):
    # get the 'border point' for each of the objects in the first image

    # get the distance to the "upper" border for each axis (x,y,z)
    dist_upper_border = np.multiply.outer(np.ones((len(coordinates_image))), image_shape) - coordinates_image
    # the distance to the lower border, is just the point itself
    dist_lower_border = coordinates_image
    # the distance to all sides is the contains both, the distances to the upper and the lower borders of the
    # image
    dist_to_all_sides = np.concatenate((dist_upper_border, dist_lower_border), axis=1)
    # the argument of the minimum distance for a point within a rectangular box to the box is just the argument of the
    # minimum of the distance to the left, right, upper, lower, front, back side
    dist_arg = np.argmin(dist_to_all_sides, axis=1)

    # want to get the coordinates of the point at the image border with the minimum distance from the original point,
    # so first create an empty array (all zeros)
    image_border_coordinates = np.zeros((len(coordinates_image), 3), dtype=float)

    # for every point in the original image
    for i, point in enumerate(coordinates_image):
        # copy the point
        image_border_coordinates[i] = point
        # if the minimal distance is to the lower border, set value at the position of minimal distance (dist_arg[i]%3)
        # to 0
        if dist_arg[i] // 3 == 1:
            image_border_coordinates[i, dist_arg[i] % 3] = 0
        # else, the minimal distance is to the upper border, so set value of the position of minimal distance equal to
        # the maximum possible value in that direction (shape in the direction -1, as counting starts from 0)
        else:
            image_border_coordinates[i, dist_arg[i] % 3] = image_shape[dist_arg[i] % 3] - 1
    return image_border_coordinates


# get the size difference between each object in the 'detected_objects_list_1' and each object in the
# 'detected_objects_list_2', and return it in matrix form
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a
#         voronoy_stars attribute
#         (voronoy_stars: list vectors/arms belonging to each voronoy star)
# outputs: size_difference: np.array in matrix form containing the voronoy_star_difference between all pairs of points
#          in the detected_objects_list_1 and detected_objects_list_2
def voronoy_star_distance_for_transition(detected_objects_list_1, detected_objects_list_2, possible_transitions):
    voronoy_stars_1 = detected_objects_list_1.voronoy_stars  # resolution_corrected_voronoy_stars
    voronoy_stars_2 = detected_objects_list_2.voronoy_stars  # resolution_corrected_voronoy_stars
    # create a np.array to store the differences between voronoy stars
    voronoy_star_difference = np.zeros(shape=(detected_objects_list_1.length, detected_objects_list_2.length),
                                       dtype=float)
    # loop over pairs of voronoy stars (one from each image) to get the mean difference between the nearest extremity
    # of one to the other
    if possible_transitions is None:
        for i, j in itertools.product(range(detected_objects_list_1.length), range(detected_objects_list_2.length)):
            voronoy_star_difference[i, j] = mean_dist_to_nearest_extremity_in_voronoy_star(voronoy_stars_1[i],
                                                                                           voronoy_stars_2[j])
    else:
        for i in range(len(possible_transitions)):
            for j in possible_transitions[i]:
                voronoy_star_difference[i, j] = mean_dist_to_nearest_extremity_in_voronoy_star(voronoy_stars_1[i],
                                                                                               voronoy_stars_2[j])
    return voronoy_star_difference


# for a split of merge event: assume the single object gets all the 'connection' from the two split/merging objects
# inputs: detected_objects_list_im_2x_obj: the detected_objects_list object in the image where there are 2 objects
#                                          (first image for merge, second image for split)
#         detected_objects_list_im_1x_obj: the detected_objects_list object in the image where there is only 1 object
#                                          (second image for merge, first image for split)
def voronoy_star_distance_for_split_or_merge(detected_objects_list_im_2x_obj, detected_objects_list_im_1x_obj,
                                             **kwargs):
    if "possible_transitions" in kwargs:
        possible_transitions = kwargs['possible_transitions']
        if "is_merge" in kwargs:
            is_merge = kwargs['is_merge']
        else:
            raise ValueError("If possible_transitions are given to the 'voronoy_star_distance_for_split_or_merge,"
                             "also the 'is_merge' argument must be defined.")
    else:
        possible_transitions, is_merge = None, None

    vs_im_2x = detected_objects_list_im_2x_obj.voronoy_stars  # resolution_corrected_voronoy_stars
    vs_im_1x = detected_objects_list_im_1x_obj.voronoy_stars  # resolution_corrected_voronoy_stars

    # for all combinations, create an array with the difference
    voronoy_star_difference_tensor = np.zeros(shape=(detected_objects_list_im_2x_obj.length,
                                                     detected_objects_list_im_2x_obj.length,
                                                     detected_objects_list_im_1x_obj.length),
                                              dtype=float)

    # loop over pairs of voronoy stars (one from each image) to get the mean difference between the nearest extremity
    # of one to the other
    if possible_transitions is None:
        for i, k in itertools.product(range(detected_objects_list_im_2x_obj.length),
                                      range(detected_objects_list_im_1x_obj.length)):
            for j in range(i):
                # combine the voronoy stars in the image with two objects (by concatenating the lists containing
                # the vectors
                combi_vs_i_j = vs_im_2x[i] + vs_im_2x[j]
                # compare the merged voronoy star to the voronoy star k in the image with one object only
                dist_ijk = mean_dist_to_nearest_extremity_in_voronoy_star(combi_vs_i_j, vs_im_1x[k])
                # put the result in the corresponding places in the matrix
                voronoy_star_difference_tensor[i, j, k], voronoy_star_difference_tensor[j, i, k] = dist_ijk, dist_ijk
    else:
        for t in possible_transitions:
            if is_merge:
                i, j, k = t[0], t[1], t[2]
            else:
                i, j, k = t[1], t[2], t[0]
            # combine the voronoy stars in the image with two objects (by concatenating the lists containing the vectors
            combi_vs_i_j = vs_im_2x[i] + vs_im_2x[j]
            # compare the merged voronoy star to the voronoy star k in the image with one object only
            dist_ijk = mean_dist_to_nearest_extremity_in_voronoy_star(combi_vs_i_j, vs_im_1x[k])
            # put the result in the corresponding places in the matrix
            voronoy_star_difference_tensor[i, j, k], voronoy_star_difference_tensor[j, i, k] = dist_ijk, dist_ijk
    return voronoy_star_difference_tensor


# get the difference between the voronoy stars in an image and a voronoy star at the border in the second image,
# where the border point is the closest point to the star's coordinates in the first image
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists
# outputs: distance between a voronoy star of a detected object in detected_objects_lists_1, to a voronoy star from
#          the point that is closest to the image border, measured from the coordinates of the point in
#          'detected_objects_lists_1', where the voronoy star is constructed with the other objects in
#          'detected_objects_lists_2'
def voronoy_star_distance_to_empty_object(detected_objects_list_1, detected_objects_list_2):
    # parameters needed from the first image
    voronoy_stars_image = detected_objects_list_1.voronoy_stars  # resolution_corrected_voronoy_stars
    coordinates_image = detected_objects_list_1.coordinates  # resolution_corrected_coordinates
    image_shape = detected_objects_list_1.im_shape  # resolution_corrected_im_shape
    coordinates_on_border_image = get_nearest_points_on_image_border_list(coordinates_image, image_shape)

    # parameter needed from the second image
    coordinates_second_image = detected_objects_list_2.coordinates  # resolution_corrected_coordinates

    # create a np array to store the distances
    voronoy_star_difference = np.zeros(detected_objects_list_1.length, dtype=float)

    for i, voronoy_star in enumerate(voronoy_stars_image):
        # add the object on the image border the list with the coordinates of the second image
        coordinates_second_image_with_border_object = coordinates_second_image.copy().tolist()
        coordinates_second_image_with_border_object.append(coordinates_on_border_image[i])
        # calculate the Voronoy Stars for every object, the voronoy star contains all vectors pointing 'away' from this
        # object that are part of the Voronoy tessellation in the second image
        # create a scipy.spatial "Voronoy" object
        voronoy_second_image = Voronoi(coordinates_second_image_with_border_object)
        # get the 'ridge_tuples': the tuples of points which connections are part of the Voronoy tessellation
        ridge_tuples_voronoy_second_image = voronoy_second_image.ridge_points
        # get the voronoy tessellation of the last point --> the appended point on the image border
        # get all tuples that contain the current last point:
        # 1) get all tuples that contain the last point in the first position
        tuple_list_containing_point_i = [ridge_tuple for ridge_tuple in ridge_tuples_voronoy_second_image
                                         if ridge_tuple[0] == len(coordinates_second_image_with_border_object) - 1]
        # 2) get all tuples that contain the last point at the second position and reverse them before adding them
        tuple_list_containing_point_i += [[ridge_tuple[1], ridge_tuple[0]] for ridge_tuple in
                                          ridge_tuples_voronoy_second_image
                                          if ridge_tuple[1] == len(coordinates_second_image_with_border_object) - 1]
        # create every voronoy star by looping over all connection with this point
        border_star = []
        for tuple_containing_point_i in tuple_list_containing_point_i:
            # calculate the vector pointing from the current point to their neighbours, if the connection
            # is part of the Voronoy tessellation
            diff = (coordinates_second_image_with_border_object[tuple_containing_point_i[1]]
                    - coordinates_second_image_with_border_object[tuple_containing_point_i[0]])
            border_star.append(diff)
        voronoy_star_difference[i] = mean_dist_to_nearest_extremity_in_voronoy_star(border_star, voronoy_star)

    return voronoy_star_difference


""" define reverse area as a cost - used as indicator whether or not splitting is plausible"""


# for a split of merge event: assume that it is more likely for an object to be the result of a merge or the
# start
# inputs: detected_objects_list_im_2x_obj: the detected_objects_list object in the image where there are 2 objects
#                                          (first image for merge, second image for split)
#         detected_objects_list_im_1x_obj: the detected_objects_list object in the image where there is only 1 object
#                                          (second image for merge, first image for split)
def reverse_size_for_split_or_merge(detected_objects_list_im_2x_obj, detected_objects_list_im_1x_obj, _):
    len_sizes_im_2x = detected_objects_list_im_2x_obj.length
    sizes_im_1x = detected_objects_list_im_1x_obj.watershed_sizes

    # create a tensor to store the values of reverse area for each possible split/merge event
    reverse_size_tensor = np.zeros((len_sizes_im_2x, len_sizes_im_2x, len(sizes_im_1x)), dtype=float)
    # store the reverse area of the single object for each event
    for i, size in enumerate(sizes_im_1x):
        reverse_size_tensor[:, :, i] = 1 / size
    return reverse_size_tensor


# the reverse area only makes sense for split or merge events, otherwise consider to use 'area difference',
# if the function is nonetheless called for a transition event, return a vector with zeros only and raise a warning
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a sizes attribute
#         (sizes: list of sizes [volume] of the objects in pixels)
# outputs: reverse_size: np.array with zeros only, as reverse size only makes sense as a measure for a merging/
#                        splitting event
def reverse_size_for_transition(detected_objects_list_1, detected_objects_list_2, _):
    len_sizes_1, len_sizes_2 = detected_objects_list_1.length, detected_objects_list_2.length
    reverse_size = np.zeros((len_sizes_1, len_sizes_2), dtype=float)
    warnings.warn("The 'reverse size' distance measure only makes sense for a split or merge event, it will "
                  "be zero for every transition event. Consider setting the 'used for transition' value to False when"
                  "calling the 'CostVector' constructor.", UserWarning)
    return reverse_size


# the reverse area only makes sense for split or merge events, otherwise consider to use 'area difference',
# if the function is nonetheless called for a (dis)appearance event, return a vector with zeros only and raise a warning
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a "sizes" attribute
#         (sizes: list of the sizes [volume] of the objects in pixels)
# outputs: reverse_size: np.array with zeros only, as reverse size only makes sense as a measure for a merging/
#                        splitting event
def reverse_size_to_empty_object(detected_objects_list_1, _):
    len_sizes_image = detected_objects_list_1.length
    reverse_size = np.zeros(len_sizes_image, dtype=float)
    warnings.warn("The 'reverse size' distance measure only makes sense for a split or merge event, it will "
                  "be zero for every transition event. Consider setting the 'used for empty' value to False when"
                  "calling the 'CostVector' constructor.", UserWarning)
    return reverse_size


""" define parallel movement as a cost - used to make movement in parallel for close by objects more plausible"""


# the movement in parallel only makes sense for a transition, otherwise it is zero
# inputs: detected_objects_list_im_2x_obj: the detected_objects_list object in the image where there are 2 objects
#                                          (first image for merge, second image for split)
#         detected_objects_list_im_1x_obj: the detected_objects_list object in the image where there is only 1 object
#                                          (second image for merge, first image for split)
# outputs: moves_in_parallel_tensor: np.array with zeros only, as reverse size only makes sense as a measure for a
#                                    transition event
def parallel_movement_for_split_or_merge(detected_objects_list_im_2x_obj, detected_objects_list_im_1x_obj, _):
    len_sizes_im_2x = detected_objects_list_im_2x_obj.length
    sizes_im_1x = detected_objects_list_im_1x_obj.watershed_sizes

    warnings.warn("The 'parallel movement' distance measure only makes sense for objects that do not "
                  "split and move pairwise. This will not affect split or merge probabilities. Please"
                  " make sure your used cost functions make sense!", UserWarning)

    # create an empty tensor with zeros only to return to the main function
    moves_in_parallel_tensor = np.zeros((len_sizes_im_2x, len_sizes_im_2x, len(sizes_im_1x)), dtype=float)
    return moves_in_parallel_tensor


# get a cost for how 'parallel' a movement of an object is compared to its closest neighbours
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a sizes attribute
#         (sizes: list of sizes [volume] of the objects in pixels)
# outputs: matrix with a measure for similarity of a transition vector for the transition i,j compared to the
#          the 'closest match' of a transition vector of the five closest objects in the first frame.
#          As a measure of similarity the product of the euclidean distance and the cosine distance of the vectors
#          is used to ensure larger movements, which should be fairly parallel but might have a bigger euclidean
#          distance, are possible, as well as smaller movements, which might be anti-parallel, but have a small
#          euclidean distance.
#          Only the costs for possible transitions are computed
def parallel_movement_for_transition(detected_objects_list_1, detected_objects_list_2, possible_transitions):
    # create a np.array to store the movement_in_parallel_measure
    parallel_movement_transition = np.zeros(shape=(detected_objects_list_1.length, detected_objects_list_2.length),
                                            dtype=float)

    # get the coordinates of the objects in the two frames
    coords_1 = detected_objects_list_1.coordinates  # resolution_corrected_coordinates
    coords_2 = detected_objects_list_2.coordinates  # resolution_corrected_coordinates

    # get the distance between each object in frame 1 and all other objects (including itself)
    distance = cdist(coords_1, coords_1, w)
    # get the 6 minimum distance objects (exclude the object itself)
    objs_with_min_distance_matrix = distance.argsort(axis=0)[1:6 + 1, :]

    # if the possible_transitions are not specified, get the according indices over which the iteration should be made
    if possible_transitions is None:
        possible_transitions = itertools.product(range(detected_objects_list_1.length),
                                                 range(detected_objects_list_2.length))

    # get the direction vector for each transition and the respective starting indices
    direction_vectors = np.array([coords_2[t[1]] - coords_1[t[0]] for t in possible_transitions])
    starting_indices = np.array([t[0] for t in possible_transitions])

    # for every possible transition, get the most similar direction vector of a close by connection and favour
    # transitions with similar connection vectors close by
    for transition, direction_vector, starting_index in zip(possible_transitions, direction_vectors, starting_indices):
        starting_indices_of_close_by_objs = objs_with_min_distance_matrix[:, starting_index]
        direction_vecs_of_close_by_objs = np.array([direction_vectors[i] for i in range(len(direction_vectors))
                                                    if starting_indices[i] in starting_indices_of_close_by_objs])
        # get the distance in ending points
        eu_distance_between_direction_vecs = cdist(direction_vector, direction_vecs_of_close_by_objs, w)
        # get the distance in direction
        cos_distance_between_direction_vecs = cdist(direction_vector, direction_vecs_of_close_by_objs, w,
                                                    metric='cosine')
        # to get a measure that does permit for larger movement, where differences in euclidean distance might be
        # bigger, but also allows for objects to not move much and maybe move slightly in opposite directions,
        # multiply these two measures with each other
        total_distance_between_direction_vecs = eu_distance_between_direction_vecs * cos_distance_between_direction_vecs
        # get the minimum distance between direction vectors as a cost measure
        min_distance_between_direction_vecs = np.min(total_distance_between_direction_vecs)
        # set the cost for each transition to correspond to the measure
        parallel_movement_transition[transition[0], transition[1]] = min_distance_between_direction_vecs

    return parallel_movement_transition


# the movement in parallel only makes sense for a transition, otherwise it is zero
# if the function is nonetheless called for a (dis)appearance event, return a vector with zeros only and raise a warning
# inputs: detected_objects_lists_1/2: instances of DetectedObjectsLists having both a "sizes" attribute
#         (sizes: list of the sizes [volume] of the objects in pixels)
# outputs: parallel_movement_to_empty: np.array with zeros only, as reverse size only makes sense as a measure for a
#                                      transition event
def parallel_movement_to_empty_object(detected_objects_list_1, _):
    len_sizes_image = detected_objects_list_1.length
    parallel_movement_to_empty = np.zeros(len_sizes_image, dtype=float)
    warnings.warn("The 'parallel movement' distance measure only makes sense for objects that do not "
                  "split and move pairwise. This will not affect probabilities associated with an empty object. "
                  "Please make sure your used cost functions make sense!", UserWarning)
    return parallel_movement_to_empty


# the CostVector containing the costs for all defined interactions
class CostVector:
    def __init__(self, pts_1, pts_2, im_1, im_2, connection_matrix, metadata,
                 indicator_split_list_1=None,
                 indicator_split_list_2=None,
                 dis_appear_penalty=None, split_merge_penalty=None,
                 distance_measure_weights=None,
                 use_dist_measure_for_transition=None,
                 use_dist_measure_for_split_and_merge=None,
                 use_dist_measure_for_appear_disappear=None,
                 only_use_before_anaphase_onset=None,
                 is_before_anaphase_onset=True,
                 watershed_sizes_1=None, watershed_sizes_2=None):

        # if no weights etc. are given, set them to reasonable values and use all measures for every type of transition
        if distance_measure_weights is None:
            distance_measure_weights = [0.7, 0.05, 0.05, 0.2, 0.02, 0]
        if use_dist_measure_for_transition is None:
            use_dist_measure_for_transition = [True, True, True, True, False, False]
        if use_dist_measure_for_split_and_merge is None:
            use_dist_measure_for_split_and_merge = [True, True, True, True, True, False]
        if use_dist_measure_for_appear_disappear is None:
            use_dist_measure_for_appear_disappear = [True, True, True, True, False, False]
        if only_use_before_anaphase_onset is None:
            only_use_before_anaphase_onset = [False, False, False, False, False, True]

        # store the connection_matrix in a shorter variable, so the
        # expressions do not get to long
        m = connection_matrix

        # store the individual values of the costs in a dictionary
        self.costs_dict = {}

        # get the number of nodes on the left and on the right
        no_ls = len(pts_1)
        no_rs = len(pts_2)

        # initiate a vector with the correct length for each of the both
        # measures: euclidean distance and intensity difference
        self.val = np.zeros(connection_matrix.col_len, dtype=np.float64)

        # create a list of all distance measures
        self.distance_measures = []

        # define the distance measures with the pre-defined functions, if
        # they are passed to the init function, if None is passed for dist_measures,
        # all are taken
        det_obj_1 = po.DetectedObjectsList(pts_1, im_1, metadata, watershed_sizes=watershed_sizes_1)
        det_obj_2 = po.DetectedObjectsList(pts_2, im_2, metadata, watershed_sizes=watershed_sizes_2)

        # get the physical distances as weights for all euclidean distances computed from the metadata
        phys_size_x = metadata["phys_size_x"]
        phys_size_y = metadata["phys_size_y"]
        phys_size_z = metadata["phys_size_z"]
        global w
        w = [phys_size_x, phys_size_y, phys_size_z]

        # euclidean distance
        if distance_measure_weights[0] > 0:
            print("Euclidean distance used as similarity measure")
            eu_dist = DistanceMeasure("euclidean distance",
                                      det_obj_1, det_obj_2,
                                      distance_measure_weights[0],
                                      euclidean_distance_transition,
                                      euclidean_distance_for_split_or_merge,
                                      euclidean_distance_to_empty_object,
                                      use_dist_measure_for_transition[0],
                                      use_dist_measure_for_split_and_merge[0],
                                      use_dist_measure_for_appear_disappear[0],
                                      only_use_before_anaphase_onset[0],
                                      False)
            self.distance_measures.append(eu_dist)

        # intensity difference
        if distance_measure_weights[1] > 0:
            print("Intensity difference used as similarity measure")
            int_dif = DistanceMeasure("intensity difference",
                                      det_obj_1, det_obj_2,
                                      distance_measure_weights[1],
                                      intensity_distance_for_transition,
                                      intensity_distance_for_split_or_merge,
                                      intensity_distance_to_empty_object,
                                      use_dist_measure_for_transition[1],
                                      use_dist_measure_for_split_and_merge[1],
                                      use_dist_measure_for_appear_disappear[1],
                                      only_use_before_anaphase_onset[1],
                                      False)
            self.distance_measures.append(int_dif)

        # size difference
        if distance_measure_weights[2] > 0:
            print("Size difference used as similarity measure")
            size_dif = DistanceMeasure("area difference",
                                       det_obj_1, det_obj_2,
                                       distance_measure_weights[2],
                                       size_distance_for_transition,
                                       size_distance_for_split_or_merge,
                                       size_distance_to_empty_object,
                                       use_dist_measure_for_transition[2],
                                       use_dist_measure_for_split_and_merge[2],
                                       use_dist_measure_for_appear_disappear[2],
                                       only_use_before_anaphase_onset[2],
                                       False)
            self.distance_measures.append(size_dif)

            det_obj_1.get_sizes_of_objects_at_points_from_image(im_1)
            det_obj_2.get_sizes_of_objects_at_points_from_image(im_2)

        # Voronoy star end to end mean nearest neighbour difference
        if distance_measure_weights[3] > 0:
            print("Voronoy star mean nearest neighbour ends difference used as similarity measure")
            voronoy_dif = DistanceMeasure("voronoy difference",
                                          det_obj_1, det_obj_2,
                                          distance_measure_weights[3],
                                          voronoy_star_distance_for_transition,
                                          voronoy_star_distance_for_split_or_merge,
                                          voronoy_star_distance_to_empty_object,
                                          use_dist_measure_for_transition[3],
                                          use_dist_measure_for_split_and_merge[3],
                                          use_dist_measure_for_appear_disappear[3],
                                          only_use_before_anaphase_onset[3],
                                          False)
            self.distance_measures.append(voronoy_dif)

            det_obj_1.get_voronoy_star_points_list()
            det_obj_2.get_voronoy_star_points_list()

        # reverse size
        if distance_measure_weights[4] > 0:
            print("Reverse size difference used as similarity measure")
            rev_size = DistanceMeasure("reverse size",
                                       det_obj_1, det_obj_2,
                                       distance_measure_weights[4],
                                       reverse_size_for_transition,
                                       reverse_size_for_split_or_merge,
                                       reverse_size_to_empty_object,
                                       use_dist_measure_for_transition[4],
                                       use_dist_measure_for_split_and_merge[4],
                                       use_dist_measure_for_appear_disappear[4],
                                       only_use_before_anaphase_onset[4],
                                       True)
            self.distance_measures.append(rev_size)

            # movement in parallel
            if distance_measure_weights[5] > 0:
                print("Movement in parallel used as similarity measure")
                parallel_move = DistanceMeasure("movement in parallel",
                                                det_obj_1, det_obj_2,
                                                distance_measure_weights[5],
                                                parallel_movement_for_transition,
                                                parallel_movement_for_split_or_merge,
                                                parallel_movement_to_empty_object,
                                                use_dist_measure_for_transition[5],
                                                use_dist_measure_for_split_and_merge[5],
                                                use_dist_measure_for_appear_disappear[5],
                                                only_use_before_anaphase_onset[5],
                                                False)
                self.distance_measures.append(parallel_move)

            # if the sizes have not yet been added as an attribute to the detected object, add them
            if det_obj_1.watershed_sizes is None or det_obj_2.watershed_sizes is None or \
                    len(det_obj_1.watershed_sizes) == 0 or \
                    len(det_obj_2.watershed_sizes) == 0:
                det_obj_1.get_sizes_of_objects_at_points_from_image(im_1)
                det_obj_2.get_sizes_of_objects_at_points_from_image(im_2)

        # loop over all distance measures
        for dm in self.distance_measures:

            if is_before_anaphase_onset or not dm.only_use_before_anaphase_onset[0]:

                dm_val = np.zeros(connection_matrix.col_len, dtype=np.float64)

                # if the distance measure is used to calculate the values for a transition
                if dm.use_for_transition:

                    # get the distance between one L and one R object in matrix form
                    # as a cost for the association between L_i and R_j
                    if not m.connections_given:
                        dist_lr_matrix = dm.dist_transition_fct(dm.inputs_1, dm.inputs_2)
                        dm_val[m.l_to_r.slice] = dist_lr_matrix.flatten()
                    else:
                        dist_lr_matrix = dm.dist_transition_fct(dm.inputs_1, dm.inputs_2, m.l_to_r_connections)
                        dist_lr_list = []

                        print(dm.dist_transition_fct.__name__)

                        for i in range(len(m.l_to_r_connections)):
                            for j in m.l_to_r_connections[i]:
                                dist_lr_list.append(dist_lr_matrix[i][j])
                        dm_val[m.l_to_r.slice] = dist_lr_list

                # get the distance to an 'empty object' if the object can appear or disappear from the image border
                # and the distance measure is used to estimate the costs of a disappear/appear event
                if connection_matrix.can_disappear and dm.use_for_dis_appear:
                    # get the distance between the Ls and the their corresponding
                    # empty equivalents as cost for L -> D
                    empty_1 = dm.distance_to_empty_fct(dm.inputs_1, dm.inputs_2)
                    dm_val[m.l_to_d.slice] = empty_1
                    # add the penalty for appearing/disappearing

                if connection_matrix.can_appear and dm.use_for_dis_appear:
                    # get the distance between the Rs and their corresponding
                    # empty equivalents as cost for A -> R
                    empty_2 = dm.distance_to_empty_fct(dm.inputs_2, dm.inputs_1)
                    dm_val[m.a_to_r.slice] = empty_2
                    # add the penalty for appearing/disappearing

                print("split and merge")

                # get the mean intensity and mean position of the objects, if the object can split or merge
                # and the current distance measure is used to estimate the costs of split/merge events
                if connection_matrix.can_merge and dm.use_for_split_and_merge:

                    # merging: compare two Ls to one R
                    # create a list to store all values
                    merging_dist_list = []
                    if not m.connections_given:
                        # calculate intensity distance for merging L_i + L_j --> R_k = merge_dist[i,j,k]
                        # (symmetric in i,j)
                        merge_dist = dm.dist_split_or_merge_fct(dm.inputs_1, dm.inputs_2)
                        for k in range(no_rs):  # for all Rs
                            for i in range(no_ls - 1):  # for all pairs of Ls
                                # want to create loop over all pairs of Ls, so fix first one (i) and
                                # then check all second ones greater than this one
                                for j in range(i + 1, no_ls):
                                    merging_dist_list += [merge_dist[i, j, k]]

                    else:
                        # calculate intensity distance for merging L_i + L_j --> R_k = merge_dist[i,j,k]
                        # (symmetric in i,j)
                        merge_dist = dm.dist_split_or_merge_fct(dm.inputs_1, dm.inputs_2,
                                                                possible_transitions=m.merge_connections, is_merge=True)
                        # add the costs for all merges given in the list of merge_connections (m.merge_connections),
                        # tuples are given in a list with [[i_0, j_0, k_0], [i_1, j_1, k_1], ... [i_n, j_n, k_n]]
                        # where Li + Lj --> Rk
                        for merge_tuple in m.merge_connections:
                            merging_dist_list += [merge_dist[merge_tuple[0], merge_tuple[1], merge_tuple[2]]]
                    # store the distances in the dm_val array, as the merging_dist_list is a nested list, remove
                    # the nesting first and convert it into a numpy array
                    dm_val[m.lx2_to_d_r.slice] = np.array(merging_dist_list)

                if connection_matrix.can_split and dm.use_for_split_and_merge:
                    # splitting: compare one L to two Rs
                    # create a list to store all values
                    splitting_dist_list = []
                    if not m.connections_given:
                        # calculate intensity distance for splitting L_k --> R_i + R_j = split_dist[i,j,k]
                        # (symmetric in i,j)
                        split_dist = dm.dist_split_or_merge_fct(dm.inputs_2, dm.inputs_1, None)
                        for k in range(no_ls):  # for all Rs
                            # want to create loop over all pairs of Rs, so fix first one (i) and
                            # then check all second ones greater than this one
                            for i in range(no_rs - 1):  # for all pairs of Rs
                                for j in range(i + 1, no_rs):
                                    splitting_dist_list += [split_dist[k, i, j]]
                    else:
                        # calculate intensity distance for splitting L_k --> R_i + R_j = split_dist[i,j,k]
                        # (symmetric in i,j)
                        split_dist = dm.dist_split_or_merge_fct(dm.inputs_2, dm.inputs_1,
                                                                possible_transitions=m.split_connections,
                                                                is_merge=False)
                        # add the costs for all merges given in the list of split_connections (m.split_connections),
                        # tuples are given in a list with [[i_0, j_0, k_0], [i_1, j_1, k_1], ... [i_n, j_n, k_n]]
                        # where Li --> Rj + Rk
                        for split_tuple in m.split_connections:
                            splitting_dist_list += [split_dist[split_tuple[1], split_tuple[2], split_tuple[0]]]

                            # store the distances in the dm_val array, as the merging_dist_list is a nested list, remove
                    # the nesting first and convert it into a numpy array
                    dm_val[m.a_l_to_rx2.slice] = np.array(splitting_dist_list)

                    # add penalties for choosing individual points that are the result of a split or merge of existing
                    # objects in exclusion sets

                    # make the connections corresponding to a split of the points_1 more expensive, if the weight is
                    # used as a penalty for a single point
                    if indicator_split_list_1 is not None and dm.penalty_for_single_point:
                        for i, split_list_entry in indicator_split_list_1:
                            # for the events corresponding to a split, add an additional penalty for object size
                            if split_list_entry == 2:
                                penalty = 1 / dm.inputs_1.watershed_sizes[i]
                                dm_val[m.in_1 == pts_1[i]] += penalty
                                dm_val[m.in_2 == pts_1[i]] += penalty

                    if indicator_split_list_2 is not None and dm.penalty_for_single_point:
                        for i, split_list_entry in indicator_split_list_2:
                            # for the events corresponding to a split, add an additional penalty for object size
                            if split_list_entry == 2:
                                penalty = 1 / dm.inputs_1.watershed_sizes[i]
                                dm_val[m.in_1 == pts_1[i]] += penalty
                                dm_val[m.in_2 == pts_1[i]] += penalty

                # rescale the weight, such that the lowest non-zero value equals 0.01 and the maximum equals 1 to
                # make sure all weights are on the same order of magnitude and the differences are comparable
                non_zero_dm = dm_val != 0
                dm_val[non_zero_dm] -= np.min(dm_val[non_zero_dm])
                dm_val[non_zero_dm] *= 0.99 / np.max(dm_val)
                dm_val[non_zero_dm] += 0.01
                self.val += dm.weight * dm_val

                # add the value of this specific cost to the costs dictionary
                self.costs_dict[dm.name] = dm.weight * dm_val

            # add penalties for appearing, disappearing, etc.
            if connection_matrix.can_appear and dis_appear_penalty is not None:
                self.val[m.a_to_r.slice] += dis_appear_penalty

            if connection_matrix.can_disappear and dis_appear_penalty is not None:
                self.val[m.l_to_d.slice] += dis_appear_penalty

            if connection_matrix.can_merge and split_merge_penalty is not None:
                self.val[m.lx2_to_d_r.slice] += split_merge_penalty

            if connection_matrix.can_split and split_merge_penalty is not None:
                self.val[m.a_l_to_rx2.slice] += split_merge_penalty

    def __str__(self):
        return str(self.val)
