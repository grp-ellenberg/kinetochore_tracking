import napari
import numpy as np
from interact_with_segmentation_threshs_with_points_object import get_nd_points_layer_napari
from termcolor import colored
from interpret_lpp import get_sorted_points_and_vectors, compare_vector_in_vl1_to_vector_in_vl2, \
    plot_distance_between_manual_and_automated_tracking_points
import matplotlib.pyplot as plt
from qtpy.QtCore import Qt
from qtpy.QtWidgets import QSlider, QLabel, QVBoxLayout, QGroupBox, QComboBox
from qtpy.QtGui import QPixmap

plt.style.use('dark_background')


# Define a callback that will take the value of the slider and the viewer and highlight the selected
# pair by making the points bigger
# inputs: viewer: Napari viewer instance that contains the points layers
#         slider_value: number of the 'pair_index' that should be highlighted
#         picture: instance of QPixmap in which the corresponding pair plot should be displayed
#         p: parameters that were given as input in the parameters file to find the plot in the analysis folder
def highlight_pair_callback(viewer, slider_value, picture, p):
    for layer in viewer.layers:
        if layer.name.startswith("points"):
            if slider_value >= 0:
                pair_index = layer.properties["pair_index"]
                pair_index_equals_value = pair_index == slider_value
                pixmap = QPixmap(p.tracking_pair_plot_folder +
                                 "Pair " + str(slider_value) + "_low_res.png")
                pixmap.scaledToWidth(pixmap.width() / 100)
                picture.setPixmap(pixmap)
                layer.size = 0.5 + pair_index_equals_value * 1.5
            else:
                layer.size = 1
    return


# create a slider to then highlight selected pairs in the viewer, label it, so the user knows what to do. By sliding
# the slider, a different pair is highlighted. If it is pulled to the very left (-2) no pair will be highlighted.
# A pair is highlighted by making both points belonging to it bigger in every frame.
# pair by making the points bigger
# inputs: viewer: Napari Viewer instance that contains the points layers
#         unique_real_paris: tuples of objects that were paired before ("unique" meaning if objects 1 and 3 form a pair,
#                            only one of the tuples (1, 3), (3, 1) are included, "real" meaning only such tuples are
#                            considered, where object x -> y and object y -> x, where "->" means "is most similar to".
#                            if x -> z, z -> y, y -> z, the objects are not considered to have a 'partner'.
#         p: parameters that were given as input in the parameters file to find the plot in the analysis folder
# outputs: viewer: the updated napari Viewer.
def add_a_slider_to_highlight_a_pair_to_the_viewer(viewer, unique_real_pairs, p):
    # create a horizontal QSlider that is added to the napari viewer
    highlight_pair_slider = QSlider(Qt.Horizontal)
    highlight_pair_slider.setMinimum(-1)
    highlight_pair_slider.setMaximum(len(unique_real_pairs) - 1)
    highlight_pair_slider.setSingleStep(1)

    # add a picture of the current plot with the distance maps
    pixmap = QPixmap(p.tracking_pair_plot_folder + "Pair " + str(0) + "_low_res.png")
    pixmap.scaledToWidth(pixmap.width() / 100)
    picture = QLabel()
    picture.setPixmap(pixmap)
    picture.resize(pixmap.width() / 100, pixmap.height() / 100)
    picture.show()

    # add a label to the QSlider
    highlight_pair_label = QLabel('Highlight Pair Number: ')

    # Connect the slider to its callback function to highlight pairs
    # noinspection PyUnresolvedReferences
    highlight_pair_slider.valueChanged[int].connect(
        lambda value=highlight_pair_slider: highlight_pair_callback(viewer, value, picture, p))

    # create a pyqt-box for placing all PyQt-objects
    box = QVBoxLayout()
    box.addWidget(highlight_pair_label)
    box.addWidget(highlight_pair_slider)
    box.addWidget(picture)

    # use a group box to add the objects collected in the QVBoxLayout object (no title)
    group_box = QGroupBox('')
    # set the layout
    group_box.setLayout(box)
    group_box.resize(pixmap.width() / 100, pixmap.height() / 100)
    # add the widget to the napari viewer, on the right side
    viewer.window.add_dock_widget(group_box, name='highlight pair number', area='right')

    return viewer


# change the colour of the tracking vector in the viewer depending on different attributes that were selected by the
# user in the combo-box
# viewer: napari Viewer object containing a 'vectors transition "x"' layers, where "x" stands for the frame number
#         value: the value of the combo-box from "add_switch_to_highlight_transition_cost_or_faithfulness",where the
#                number corresponds to the index in the vector properties list
#         vector_properties_list: a list with all the names of the properties the vector layer has as strings
def combo_box_callback(viewer, value, vector_properties_list):
    for layer in viewer.layers:
        if layer.name.startswith("vectors transition"):
            layer.edge_color = vector_properties_list[value]
    return


def add_switch_to_highlight_transition_cost_or_faithfulness(viewer, vector_properties_list):
    # create a Combo-Box to change the highlighting of the vectors layers
    vector_style_combo_box = QComboBox()
    vector_style_combo_box.addItems(vector_properties_list)

    # add a label to the QSlider
    vector_style_label = QLabel('Vector Style Based on: ')

    # Connect the dropdown selection to its callback function to change the vector style dependent on the selection
    # noinspection PyUnresolvedReferences
    vector_style_combo_box.currentIndexChanged.connect(lambda value=vector_style_combo_box:
                                                       combo_box_callback(viewer, value, vector_properties_list))

    # create a pyqt-box for placing all PyQt-objects
    box = QVBoxLayout()
    box.addWidget(vector_style_label)
    box.addWidget(vector_style_combo_box)

    # use a group box to add the objects collected in the QVBoxLayout object (no title)
    group_box = QGroupBox('')
    # set the layout
    group_box.setLayout(box)
    # add the widget to the napari viewer, on the right side
    viewer.window.add_dock_widget(group_box, name='vector style based on', area='right')

    return viewer


def add_key_binding_for_getting_next_frame_to_napari_viewer(viewer):
    @viewer.bind_key('n')
    def next_vector_and_image(_viewer_):
        """Increment dimensions slider and make the next vectors layer visible."""
        axis = _viewer_.window.qt_viewer.dims.last_used
        if axis is not None:
            cur_point = _viewer_.dims.point[axis]
            axis_range = _viewer_.dims.range[axis]
            new_point = np.clip(
                cur_point + axis_range[2],
                axis_range[0],
                axis_range[1] - axis_range[2],
            )
            _viewer_.dims.set_point(axis, new_point)
            _viewer_.status = "Incremented the dimension"
            for layer in _viewer_.layers:
                if layer.name.startswith(("vectors transition ",
                                          "manual vectors transition ")):  # double parentheses needed!
                    layer.visible = False
            for layer in _viewer_.layers:
                if layer.name in ["vectors transition " + str(cur_point + axis_range[2]),
                                  "manual vectors transition " + str(cur_point + axis_range[2])]:
                    layer.visible = True
                    layer.opacity = 1
                elif layer.name in ["vectors transition " + str(cur_point + axis_range[2] - 1),
                                    "vectors transition " + str(cur_point + axis_range[2] + 1),
                                    "manual vectors transition " + str(cur_point + axis_range[2] - 1),
                                    "manual vectors transition " + str(cur_point + axis_range[2] + 1)]:
                    layer.visible = True
                    layer.opacity = 0.1

    return viewer


def add_key_binding_for_getting_last_frame_to_napari_viewer(viewer):
    @viewer.bind_key('l')
    def last_vectors_and_image(_viewer_):
        """Decrement dimensions slider and make the last vectors layer visible."""
        axis = _viewer_.window.qt_viewer.dims.last_used
        if axis is not None:
            cur_point = _viewer_.dims.point[axis]
            axis_range = _viewer_.dims.range[axis]
            new_point = np.clip(
                cur_point - axis_range[2],
                axis_range[0],
                axis_range[1] - axis_range[2],
            )
            _viewer_.dims.set_point(axis, new_point)
            _viewer_.status = "Decremented the dimension"
            for layer in _viewer_.layers:
                if layer.name.startswith(("vectors transition ",
                                          "manual vectors transition ")):  # double parentheses needed!
                    layer.visible = False
            for layer in _viewer_.layers:
                if layer.name in ["vectors transition " + str(cur_point - axis_range[2]),
                                  "manual vectors transition " + str(cur_point - axis_range[2])]:
                    layer.visible = True
                    layer.opacity = 1
                elif layer.name in ["vectors transition " + str(cur_point - axis_range[2] - 1),
                                    "vectors transition " + str(cur_point - axis_range[2] + 1),
                                    "manual vectors transition " + str(cur_point - axis_range[2] - 1),
                                    "manual vectors transition " + str(cur_point - axis_range[2] + 1)]:
                    layer.visible = True
                    layer.opacity = 0.1

    return viewer


# adds a legend with number_of_columns columns to the given axis of a plot in the top centre of a plot
# inputs: ax: the axis (matplotlib.pyplot axis object of a matplotlib figure) to which the legend should be added
#         number_of_columns: integer, the number of columns the legend is supposed to have
# output: ax: the input axis, but with the legend added
def add_legend_at_centre_top_of_plot(ax, number_of_columns):
    # shrink current axis size by 10% at the bottom
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.15,
                     box.width, box.height * 0.85])

    # Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.0),
              fancybox=False, shadow=False, ncol=number_of_columns, facecolor='inherit', edgecolor='inherit')

    return ax


# add two images to the napari viewer for each transition
# inputs: viewer: instance of napari.Viewer(), the viewer to which the images should be added
#         point_object: list of instances of the PointObject class, also containing the microscopy data for each frame
def add_images_to_viewer(viewer, point_objects):
    im_1_list = [po.microscopy_data for po in point_objects[:-1]]
    im_2_list = [po.microscopy_data for po in point_objects[1:]]

    # add both images, in red (image 1) and green (image 2)
    # noinspection PyTypeChecker
    viewer.add_image(np.array(im_1_list),
                     colormap='green',
                     name='image 1',
                     opacity=1.0)
    # noinspection PyTypeChecker
    viewer.add_image(np.array(im_2_list),
                     colormap='red',
                     name='image 2',
                     opacity=0.6)
    return


# add the point from the tracking to the napari viewer for each transition
# give the points in both images their properties
# inputs: viewer: instance of napari.Viewer(), the viewer to which the points should be added
#         i_lpp: custom class object of LppInterpretation (see top of this script) containing the interpretation 
#                of the linear programming problem, e.g. which points were chosen, what object they belong to, 
#                if they are the result of a split event at some point, etc.
#         pair_dictionary: dictionary sorting the pairs, such that each object belongs to a pair
#         tuple_index_dictionary: dictionary giving the objects in each pair the suffixes 1 or 2, depending on which
#                                 object they belong to
#         max_colour: the maximum value of the tracking indices/colours to normalise for it
def add_points_to_viewer(viewer, i_lpp, pair_dictionary, tuple_index_dictionary, max_colour):
    # get all properties in a 'flat form' so they can be interpreted well by the napari viewer

    # the colours/ tracking indices
    flat_colour_list_im_1 = [col for col_lists in i_lpp.points_im_1_colour_list for col in col_lists]
    flat_colour_list_im_2 = [col for col_lists in i_lpp.points_im_2_colour_list for col in col_lists]

    # the indicators when and where a point was split (in the segmentation or tracking step)
    flat_split_indicators_im_1 = [ind for points_1_list in i_lpp.points_im_1_is_split_list for ind in
                                  points_1_list]
    flat_split_indicators_im_2 = [ind for points_1_list in i_lpp.points_im_2_is_split_list for ind in
                                  points_1_list]

    # the text containing the names from the segmentation
    flat_text_list_im_1 = [name for name_lists in i_lpp.text_points_im_1_list for name in name_lists]
    flat_text_list_im_2 = [name for name_lists in i_lpp.text_points_im_2_list for name in name_lists]

    # the pair index indicating which pair the point belongs to, and the normalised version
    flat_pair_index_list_im_1 = [pair_dictionary[colour] if colour in pair_dictionary.keys()
                                 else -0.1 for colour in flat_colour_list_im_1]
    flat_pair_index_list_im_2 = [pair_dictionary[colour] if colour in pair_dictionary.keys()
                                 else -0.1 for colour in flat_colour_list_im_2]
    flat_norm_pair_index_list_im_1 = [pair_dictionary[colour] / max_colour if colour in pair_dictionary.keys()
                                      else -0.1 for colour in flat_colour_list_im_1]
    flat_norm_pair_index_list_im_2 = [pair_dictionary[colour] / max_colour if colour in pair_dictionary.keys()
                                      else -0.1 for colour in flat_colour_list_im_2]

    # the tuple index indicating whether an object is the first or second object of a pair
    flat_tuple_index_list_im_1 = [tuple_index_dictionary[colour] if colour in pair_dictionary.keys()
                                  else -1 for colour in flat_colour_list_im_1]
    flat_tuple_index_list_im_2 = [tuple_index_dictionary[colour] if colour in pair_dictionary.keys()
                                  else -1 for colour in flat_colour_list_im_2]

    # create a dictionary of all of these properties to pass it to the napari viewer
    points_im_1_properties = {'number': flat_colour_list_im_1, 'is_split': flat_split_indicators_im_1,
                              'name': flat_text_list_im_1, 'pair_index': flat_pair_index_list_im_1,
                              'norm_pair_index': flat_norm_pair_index_list_im_1,
                              'tuple_index': flat_tuple_index_list_im_1}
    points_im_2_properties = {'number': flat_colour_list_im_2, 'is_merged': flat_split_indicators_im_2,
                              'name': flat_text_list_im_2, 'pair_index': flat_pair_index_list_im_2,
                              'norm_pair_index': flat_norm_pair_index_list_im_2,
                              'tuple_index': flat_tuple_index_list_im_2}

    # create a text label for each point
    # The label of the format is pair_number.pair_index: frame_number.object_number. For example, if the label is
    # "20.1: 81.012" that means this is object 1 of pair 20, it was segmented as point 12 in frame 81.
    # The pair_index can only be 1 or 2
    text = {
        'text': '{pair_index}.{tuple_index}: {name}',
        'size': 4,
        'color': 'white',
        'translation': np.array([-2, 0, 0]),
        'visible': False
    }

    # add the points from image1, the face colours are set according to their 'colors_l' list,
    # the edges are white if the object is split and black otherwise,
    # the symbol used for the points from the first image is a dot

    viewer.add_points(
        get_nd_points_layer_napari(i_lpp.points_im_1_list),
        properties=points_im_1_properties,
        face_color='norm_pair_index',  # 'number',
        face_colormap='nipy_spectral',
        edge_color='is_split',
        edge_color_cycle=['black', 'white', 'magenta'],
        size=1,
        text=text,
        symbol='ring',
        name='points 1')

    # add the points from image2, the colours are the same as for image1, but the symbol
    # used for points in the second image is a disk/circle
    viewer.add_points(
        get_nd_points_layer_napari(i_lpp.points_im_2_list),
        properties=points_im_2_properties,
        face_color='norm_pair_index',  # 'number',
        face_colormap='nipy_spectral',
        edge_color='is_merged',
        edge_color_cycle=['black', 'white', 'magenta'],
        size=1,
        text=text,
        symbol='disc',
        name='points 2')
    return


# add the vectors from the tracking to the napari viewer for each transition
# give the vectors in both images their properties
# inputs: viewer: instance of napari.Viewer(), the viewer to which the points should be added
#         i_lpp: custom class object of LppInterpretation (see top of this script) containing the interpretation
#                of the linear programming problem, e.g. which vectors/ connections are chosen, their costs, etc.
#         pair_dictionary: dictionary sorting the pairs, such that each object belongs to a pair
#         max_colour: the maximum value of the tracking indices/colours to normalise for it
def add_vectors_to_viewer(viewer, i_lpp, pair_dictionary, max_colour, where_dist_is_lost):
    # create a list of all properties added to simplify their display later
    vector_properties_list = []

    # create a bool to check if this is the first vector layer, because only this layer will be shown visibly.
    is_first_vector = True

    # find the maximum cost for all transitions to normalise the costs for each transition, use the costs as an
    # additional property for each vector
    flat_costs_list = [cost for sublist in i_lpp.costs_list for cost in sublist]
    max_cost = max([i for i in flat_costs_list if i is not None])

    # for all specific costs, find their maximum
    max_value_dict = {}
    for key in i_lpp.specific_costs_list:
        flat_spec_costs_list = [cost for sublist in i_lpp.specific_costs_list[key] for cost in sublist]
        max_value_dict[key] = max([i for i in flat_spec_costs_list if i is not None])

    # add a layer of vectors pointing from every point in points1 to its corresponding point in points2
    for i, (vectors, first_point_colour, costs_list) in enumerate(zip(i_lpp.vectors_list,
                                                                      i_lpp.points_im_1_colour_list, i_lpp.costs_list)):

        # get the pair indices for each vector in the transition, to colour the vector according to which pair it
        # belongs to
        vector_pair_index_list = [pair_dictionary[colour] / max_colour if colour in pair_dictionary.keys()
                                  else -0.1 for colour in first_point_colour]
        # get the costs for each vector in the transition
        vector_costs_list = [costs / max_cost for costs in costs_list]

        vector_properties = {'pair_index': vector_pair_index_list, 'transition_costs': vector_costs_list}
        if i == 0:
            vector_properties_list.append('pair_index')
            vector_properties_list.append('transition_costs')

        # mark the vectors where the manual track is lost for the first time in the 'looses_manual_tracking' property
        if where_dist_is_lost is not None:
            objects_where_tracking_is_lost = np.argwhere(np.array(where_dist_is_lost) == i + 1)
            vectors_where_tracking_is_lost = [0 if colour not in objects_where_tracking_is_lost
                                              else 0.9 for colour in first_point_colour]
            vector_properties['looses_manual_tracking'] = vectors_where_tracking_is_lost
            if i == 0:
                vector_properties_list.append('looses_manual_tracking')

        # get the specific costs for each vector in the transition and add them as possible properties
        if len(i_lpp.specific_costs_list) == len(i_lpp.vectors_list):
            for key in i_lpp.specific_costs_list:
                specific_costs_list = [spec_costs / max_value_dict[key] for spec_costs in
                                       i_lpp.specific_costs_list[key][i]]
                vector_properties[str(key)] = specific_costs_list
                if i == 0:
                    vector_properties_list.append(key)

        # add the vectors to the napari viewer using a 'rainbow' colour map and colour them according to the pair they
        # belong to, only display the first set of vectors as initial view
        viewer.add_vectors(np.array(vectors),
                           properties=vector_properties,
                           edge_width=0.5,
                           edge_color='pair_index',
                           edge_colormap='nipy_spectral',
                           name='vectors transition ' + str(i),
                           visible=is_first_vector)
        is_first_vector = False
    return vector_properties_list, viewer


# add manual points to the napari viewer, but remove +/- infinity values first and replace them with negative values
# add the point from the manual segmentation to the napari viewer for each transition
# inputs: viewer: instance of napari.Viewer(), the viewer to which the points should be added
#         manual_points_1_list: list of manual points in the first frame of each transition
#         manual_points_1_list: list of manual points in the second frame of each transition
def add_manual_points_to_viewer(viewer, manual_points_1_list, manual_points_2_list):
    # if no manual point is entered, there might be +/- infinity values, replace them by -2/-1 values
    manual_points_1_list_updated, manual_points_2_list_updated = [], []
    for mp1, mp2 in zip(manual_points_1_list, manual_points_2_list):
        mp1[np.isneginf(mp1)] = -1
        mp2[np.isneginf(mp2)] = -1
        mp1[np.isinf(mp1)] = -2
        mp2[np.isinf(mp2)] = -2
        manual_points_1_list_updated.append(mp1)
        manual_points_2_list_updated.append(mp2)
    manual_points_1_list, manual_points_2_list = manual_points_1_list_updated, manual_points_2_list_updated

    # add the points manually found in image1 in green
    viewer.add_points(get_nd_points_layer_napari(manual_points_1_list),
                      face_color='green',
                      size=1,
                      symbol='ring',
                      name='manual points 1',
                      opacity=0.4)

    # add the points manually found in image1 in green
    viewer.add_points(get_nd_points_layer_napari(manual_points_2_list),
                      face_color='red',
                      size=1,
                      symbol='disc',
                      name='manual points 2',
                      opacity=0.4)
    return


def display_outcome_tracking_napari(i_lpp, point_objects, p, display_napari=True):
    if point_objects[0].manual_points is not None:
        has_manual_points = True
        manual_points_1_list = [po.manual_points for po in point_objects[:-1]]
        manual_points_2_list = [po.manual_points for po in point_objects[1:]]
    else:
        has_manual_points, manual_points_1_list, manual_points_2_list = False, None, None

    if point_objects[0].manual_tracking_indices is not None:
        has_tracking_indices = True
        tracking_indices_1_list = [po.manual_tracking_indices for po in point_objects[:-1]]
        tracking_indices_2_list = [po.manual_tracking_indices for po in point_objects[1:]]
        sorted_manual_points, manual_vectors, _ = get_sorted_points_and_vectors(manual_points_1_list,
                                                                                tracking_indices_1_list,
                                                                                manual_points_2_list,
                                                                                tracking_indices_2_list)

        where_dist_is_lost = \
            plot_distance_between_manual_and_automated_tracking_points(i_lpp.sorted_points_array,
                                                                       np.array(sorted_manual_points),
                                                                       p, display_napari)
    else:
        has_tracking_indices, tracking_indices_1_list, tracking_indices_2_list = False, None, None
        where_dist_is_lost = None

    with napari.gui_qt():

        # create a napari viewer object
        viewer = napari.Viewer(title="Outcome tracking", axis_labels=["frame", "z", "y", "x"])
        viewer.dims.ndisplay = 3  # open the viewer in the 3D view

        # add the images and points and tracking vectors to the napari viewer
        add_images_to_viewer(viewer, point_objects)
        add_points_to_viewer(viewer, i_lpp, i_lpp.pair_dictionary, i_lpp.tuple_dictionary,
                             i_lpp.current_colour)
        vector_props_list, viewer = add_vectors_to_viewer(viewer, i_lpp, i_lpp.pair_dictionary,
                                                          i_lpp.current_colour, where_dist_is_lost)

        if has_manual_points:
            # add the manual points to the viewer (do not use the sorted points here, as they are None, if
            # no tracking indices are given as an input)
            add_manual_points_to_viewer(viewer, manual_points_1_list, manual_points_2_list)

            # if tracking indices are given, add the vectors connecting the points in red, if no similar vector
            # can be found in the automated tracking, or green, if such a vector can be found
            if has_tracking_indices:

                manual_vectors_list = [[] for _ in range((len(p.time_points) - 1))]

                for frame_index_in in range(len(p.time_points) - 1):
                    points_fixed = manual_points_1_list[frame_index_in]
                    points_moving = manual_points_2_list[frame_index_in]
                    tracking_indices_fixed = np.array(tracking_indices_1_list[frame_index_in])
                    tracking_indices_moving = np.array(tracking_indices_2_list[frame_index_in])
                    for i, point_in in enumerate(points_fixed):
                        tracking_number = tracking_indices_fixed[i]
                        corresponding_index_moving = np.where(tracking_indices_moving == tracking_number)[0]
                        if len(corresponding_index_moving) > 1:
                            print(colored("Too many indices corresponding to object" + str(tracking_number) +
                                          " in the manual tracking indices", "red"))
                        elif len(corresponding_index_moving) == 0:
                            print(colored("No corresponding object found to object " + str(tracking_number) +
                                          " in the manual tracking indices", "red"))
                        else:
                            corresponding_point_2 = points_moving[corresponding_index_moving][0]
                            manual_vectors_list[frame_index_in].append([point_in,
                                                                        corresponding_point_2 - point_in])

                is_first_vector = True
                # add a layer of vectors pointing from every point in points1 to its corresponding point in points2
                for i, m_vectors in enumerate(manual_vectors_list):  # manual vectors list
                    try:
                        has_similar_vector_in_non_manual = \
                            compare_vector_in_vl1_to_vector_in_vl2(m_vectors, i_lpp.vectors_list[i])

                        manual_vecs_properties = {'is_good': has_similar_vector_in_non_manual}

                        viewer.add_vectors(np.array(m_vectors),
                                           edge_width=0.5,
                                           properties=manual_vecs_properties,
                                           edge_color_cycle=['green', 'red'],
                                           edge_color='is_good',
                                           name='manual vectors transition ' + str(i),
                                           visible=is_first_vector)
                        is_first_vector = False
                    except OverflowError:
                        print("Overflow error when checking for similarity between manual vectors and automatic")

        # add a slider to highlight one pair at a time
        add_a_slider_to_highlight_a_pair_to_the_viewer(viewer, i_lpp.unique_real_pairs, p)

        # add a switch to highlight high cost transitions
        add_switch_to_highlight_transition_cost_or_faithfulness(viewer, vector_props_list)

        # add key bindings to the napari viewer to conveniently go through a stack of frames and activate the
        # corresponding 'movement vectors' and in a lighter colour their 'previous and next' versions automatically.
        # Forward movement is activated by pressing "N" (next) and backward movement is triggered pressing "L"
        # (last).

        add_key_binding_for_getting_last_frame_to_napari_viewer(viewer)
        add_key_binding_for_getting_next_frame_to_napari_viewer(viewer)

    return
