import scipy.ndimage as ndimage
import skimage.measure
import numpy as np
import skimage.morphology


# dilating a scipy label array by application of a maximum filter followed by setting the values greater than
# zero to their original value
# input: label image (np.array of arbitrary dimension)
# output: label image (np. array of same dim and size as input array, but with each label area being bigger
#                      (if not completely surrounded by neighbouring label areas))
def dilate_label_image(label_image):
    dilated_label_image = ndimage.maximum_filter(label_image, size=3)
    dilated_label_image[label_image != 0] = label_image[label_image != 0]
    return dilated_label_image


# remove small components from a binary mask
# inputs: binary_mask: np.array of the mask image
#         size_threshold: minimum number of pixels in connected components
# outputs: binary_mask_of_big_objects: np.array of the mask with big
#          components removed
def remove_small_components_from_binary_img(binary_mask, size_threshold):
    binary_mask = skimage.morphology.remove_small_objects(binary_mask, min_size=size_threshold, in_place=True)
    return binary_mask


# take a label image (label_img) and return an label image with only the labels that are in the "labels_list" remaining
# inputs: label_img: an integer np.array of the same size as some image, indicating labels
#         labels_list: a list of labels that should remain
# output: new_label_img: a label image of the same size as the label_img, but with only the labels in the labels_list
#         remaining (and relabeled --> natural numbers starting from 1)
def get_label_img_of_labels_list(label_img, labels_list):
    boolean_img = np.isin(label_img, labels_list, assume_unique=True)
    new_label_img = skimage.measure.label(boolean_img)
    return new_label_img


# create an uneven sized mask from any mask
# input: mask of any size
# output: mask of uneven size (such that it is symmetric around some point)
def get_uneven_sized_mask(mask):
    # first, check if the atlas has a good shape (at least one element is even)
    if any(n % 2 == 0 for n in mask.shape):
        mask_new_shape = np.zeros(shape=len(mask.shape), dtype=int)
        for i in range(len(mask.shape)):
            if mask.shape[i] % 2 == 0:
                mask_new_shape[i] = mask.shape[i] + 1
            else:
                mask_new_shape[i] = mask.shape[i]
        new_mask = np.zeros(shape=mask_new_shape, dtype=bool)
        new_mask[0:mask.shape[0], 0:mask.shape[1], 0:mask.shape[2]] = mask
        return new_mask
    else:
        return mask


# take a small mask and apply it to many points to form a bigger mask for an image with some desired shape
# inputs: small mask: a boolean np.array of a mask (will be converted to uneven size)
#         points: points in the image to which the smaller mask should be applied
#         image_with_desired_shape: image with a shape that the output mask should have
# output: big_mask: np.array with the same size as the image_with_desired shape where the small_mask has been
#                   applied to all points
def create_mask_by_applying_small_mask_to_many_points(small_mask, points, image_with_desired_shape):
    # create an uneven mask from the input mask
    small_mask = get_uneven_sized_mask(small_mask)
    # create a big np array of the same size as the image with the 'goal' shape
    big_mask = np.zeros(shape=image_with_desired_shape.shape, dtype=bool)

    # get the center point of the small mask
    small_mask_half_dim = tuple(int(np.floor(i / 2)) for i in small_mask.shape)  # ranges from 0 to 2 half_dim

    # for every point: apply the small mask
    for point in points:
        # get a temporary copy of the big mask
        big_mask_temporary = np.zeros(shape=image_with_desired_shape.shape, dtype=bool)
        # test if the whole object is in the image shape, otherwise, don't add the shape and print an error message
        try:
            st = [int(np.round(point[i]) - small_mask_half_dim[i]) for i, j in enumerate(point)]  # start values
            sp = [int(np.round(point[i]) + small_mask_half_dim[i] + 1) for i, j in enumerate(point)]  # stop values
            big_mask_temporary[st[0]:sp[0], st[1]:sp[1], st[2]:sp[2]] = small_mask
            big_mask = np.logical_or(big_mask_temporary, big_mask)
        except (OverflowError, ValueError):
            print(f'Function: create_mask_by_applying_small_mask_to_many_points in correlation_functions.py: \
                    Skipping point {point} because of an overflow or value error')
    return big_mask


# apply a series of n dilations to some binary image
# inputs: image: np.array (binary) of some binary image
#         no_dilations: the number of dilations that should be applied to this binary image
# outputs: image: the dilated binary image
def apply_n_dilations(image, no_dilations):
    for i in range(no_dilations):
        image = skimage.morphology.binary_dilation(image)
    return image


# get the part of an image that is within some mask at a certain point
# inputs: big_image: np.array of a bigger image
#         mask: mask of shape which should be extracted from the big image
#         point: point at which an image of the same shape as the mask should be extracted from the big image
# output: return_image: image of the same size as the mask, but with only values within the mask set to a non-zero value
def get_masked_part_at_point_from_big_image(big_image, mask, point):
    # get half the dimensions of the mask
    mask_half_dim = tuple(int(np.floor(i / 2)) for i in mask.shape)  # ranges from 0 to 2 half_dim

    # calculate the start and stop values from the given point and the mask dimensions (half)
    st = [p - mask_half_dim[i] for i, p in enumerate(point)]  # start values
    sp = [p + mask_half_dim[i] + 1 for i, p in enumerate(point)]  # stop values

    try:
        # slice the original image, such that the return image has the same dim as the mask
        return_image = np.copy(big_image[st[0]:sp[0], st[1]:sp[1], st[2]:sp[2]])
        return_image[np.logical_not(mask)] = 0
    # if part of the mask lies outside of the image, return the values for the image in the part that is not outside
    # of the mask and the mean value at all other places instead
    except IndexError:
        return_image = np.ones(shape=mask.shape) * np.mean(big_image)
        st_mask = [0 for _ in point]  # start values for the mask
        sp_mask = [shape for shape in mask.shape]  # stop values for the mask
        # for every dimension, check if the start/stop value in the big image is out of the range, adjust the mask
        # starts and stops and image starts and stops accordingly
        for i in range(len(point)):
            # if the start value is smaller than zero, set it to 0 and change the mask start accordingly
            if st[i] < 0:
                st_mask[i] = abs(st[i])
                st[i] = 0
            # if the stop value lies outside of the image dimensions, change the mask stop accordingly and set it to
            # the maximum possible value
            if big_image.shape[i] - sp[i] < 0:
                sp_mask[i] = sp_mask[i] - abs(sp[i] - big_image.shape[i])
                sp[i] = big_image.shape[i]
        return_image[st_mask[0]:sp_mask[0], st_mask[1]:sp_mask[1], st_mask[2]:sp_mask[2]] = \
            np.copy(big_image[st[0]:sp[0], st[1]:sp[1], st[2]:sp[2]])
        # set all the undesired values from slicing to zero
        return_image[np.logical_not(mask)] = 0

    return return_image
