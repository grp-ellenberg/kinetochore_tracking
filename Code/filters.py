# import statements
import SimpleITK as sitk
from read_in import get_image, get_metadata_ometiff
from get_3D_stack import get_xyz_image_from_5d
from scipy.ndimage.filters import uniform_filter, percentile_filter, gaussian_filter
from scipy.ndimage import median_filter
from scipy.ndimage.measurements import variance


# Gaussian filter --> smoothing, does not preserve edges
# inputs: image (nD numpy array of the image)
#         std_sigma (float, sigma value of the Gaussian to be applied to the data)
# outputs: image (nD numpy array of the image)
def gaussian_blur_xyz(orig_xyz_image, std_sigma):
    return gaussian_filter(orig_xyz_image, sigma=std_sigma)


# Lee filter
# used to remove patterns resulting from interference (e.g. speckle noise)
# https://en.wikipedia.org/wiki/Speckle_(interference) 03.02.20
# https://stackoverflow.com/questions/39785970/speckle-lee-filter-in-python 03.02.20
# inputs: image (nD numpy array of the image)
#         size (int, size of the median filter (square/n-cube with side-length filter_size))
#         time_step_var (float,
# outputs: image (nD numpy array of the image)
def lee_filter(img, size):
    img_mean = uniform_filter(img, (size, size, size))
    img_sqr_mean = uniform_filter(img**2, (size, size, size))
    img_variance = img_sqr_mean - img_mean**2

    overall_variance = variance(img)

    img_weights = img_variance / (img_variance + overall_variance)
    img_output = img_mean + img_weights * (img - img_mean)
    return img_output


# median filter
# only keep the median of cubic region around voxel with 'filter'-size --> keep edges, remove impulse noise
# inputs: image (nD numpy array of the image)
#         filter_size (int, size of the median filter (square/n-cube with side-length filter_size))
# outputs: image (nD numpy array of the image)
def median_filter_xyz(orig_xyz_image, filter_size):
    # padding around is zero --> might reduce object intensity of objects entering/leaving the frame,
    # but better for noise handling
    return median_filter(orig_xyz_image, size=filter_size, mode='constant')


# percentile filter
# only keep the xth-percentile of cubic region around voxel with 'filter'-size --> keep edges, remove impulse noise
# https://staff.fnwi.uva.nl/r.vandenboomgaard/IPCV20172018/LectureNotes/IP/LocalOperators/
# percentile filter.html 03.02.2019 (background)
# https://docs.scipy.org/doc/scipy-0.15.1/reference/generated/
# scipy.ndimage.filters.percentile_filter.html#scipy.ndimage.filters.percentile_filter 03.02.2019 (documentation)
# inputs: image (nD numpy array of the image)
#         filter_size (int, size of the percentile filter (square/n-cube with side-length filter_size))
#         percentile: (negative int: percentile at which the cut should be made (e.g. -70: use 30 percentile max to
#                      replace the value of the point in the middle)
# outputs: image (nD numpy array of the image)
def percentile_filter_xyz(orig_xyz_image, filter_size, percentile):
    # padding around is zero --> might reduce object intensity of objects entering/leaving the frame,
    # but better for noise handling
    return percentile_filter(orig_xyz_image, percentile=percentile, size=filter_size, mode='constant')


# Patch Based denoising filter for Rician noise
# inputs: image (nD numpy array of the image)
#         also_apply_grad_diff (bool, after using teh patch based denoising filter, should a gradient anisotropic
#                               diffusion image filter be applied ?, default: False)
# outputs: image (nD numpy array of the image)
def patch_based_denoising(image, also_apply_grad_diff=False):
    sitk_image = sitk.GetImageFromArray(image)
    patch_based_filter = sitk.PatchBasedDenoisingImageFilter()
    patch_based_filter.KernelBandwidthEstimationOn()
    patch_based_filter.SetNoiseModel(2)  # 2 = 'rician'
    patch_based_filter.SetNoiseModelFidelityWeight(1.0)
    patch_based_filter.SetNumberOfSamplePatches(20)
    patch_based_filter.SetPatchRadius(4)
    patch_based_filter.SetNumberOfIterations(1)
    image_sitk_output = patch_based_filter.Execute(sitk_image)
    if also_apply_grad_diff:
        image_sitk_output = sitk.Cast(sitk_image, sitk.sitkFloat32)
        gradient_anisotropic_diffusion_filter = sitk.GradientAnisotropicDiffusionImageFilter()
        gradient_anisotropic_diffusion_filter.SetTimeStep(0.01)
        gradient_anisotropic_diffusion_filter.SetNumberOfIterations(5)
        gradient_anisotropic_diffusion_filter.SetConductanceParameter(9.0)
        image_sitk_output = gradient_anisotropic_diffusion_filter.Execute(image_sitk_output)
        image_sitk_output = sitk.Cast(image_sitk_output, sitk.sitkUInt8)
    image_output = sitk.GetArrayFromImage(image_sitk_output)
    return image_output


# Curvature Anisotropic Diffusion Image Filter
# https://en.wikipedia.org/wiki/Anisotropic_diffusion 11.02.20 uses the anisotropic diffusion with the
# modified curvature diffusion equation (MCDE) as specified in SITK
# inputs: image (nD numpy array of the image)
#         iterations (int, number of iterations for the filter, the default is 10)
# outputs: image (nD numpy array of the image)
def curvature_diff_image_filter(image, iterations=10):
    print("applying curvature difference filter")
    sitk_image = sitk.GetImageFromArray(image)
    sitk_image = sitk.Cast(sitk_image, sitk.sitkFloat32)
    curvature_diff_filter = sitk.CurvatureAnisotropicDiffusionImageFilter()
    curvature_diff_filter.EstimateOptimalTimeStep(sitk_image)
    curvature_diff_filter.SetNumberOfIterations(iterations)
    curvature_diff_filter.SetConductanceParameter(9)
    image_sitk_output = curvature_diff_filter.Execute(sitk_image)
    image_sitk_output = sitk.Cast(image_sitk_output, sitk.sitkUInt8)
    image_output = sitk.GetArrayFromImage(image_sitk_output)
    return image_output


# Gradient Diffusion Image Filter
# https://en.wikipedia.org/wiki/Anisotropic_diffusion 11.02.20 uses the anisotropic diffusion with the
# the gradient function
# inputs: image (nD numpy array of the image)
#         iterations (int, number of iterations for the filter, the default is 10)
# outputs: image (nD numpy array of the image)
def grad_diff_image_filter(image, iterations=10):
    sitk_image = sitk.GetImageFromArray(image)
    sitk_image = sitk.Cast(sitk_image, sitk.sitkFloat32)
    grad_diff_filter = sitk.GradientAnisotropicDiffusionImageFilter()
    grad_diff_filter.EstimateOptimalTimeStep(sitk_image)
    grad_diff_filter.SetNumberOfIterations(iterations)
    grad_diff_filter.SetConductanceParameter(9)
    image_sitk_output = grad_diff_filter.Execute(sitk_image)
    image_sitk_output = sitk.Cast(image_sitk_output, sitk.sitkUInt8)
    image_output = sitk.GetArrayFromImage(image_sitk_output)
    return image_output


# Bilateral Image Filter - uses space and intensity difference to adjust weights for Gaussian filtering
# https://en.wikipedia.org/wiki/Bilateral_filter 11.02.20 using the bilateral operator from:
# Tomasi and Manduchi (Bilateral Filtering for Gray and ColorImages. IEEE ICCV. 1998.)
# result similar to anisotropic diffusion but the implementation is non-iterative
# any distance metric can be used for kernel smoothing the image range
# inputs: image (nD numpy array of the image)
# outputs: image (nD numpy array of the image)
def bilateral_filter(image):
    sitk_image = sitk.GetImageFromArray(image)
    sitk_image = sitk.Cast(sitk_image, sitk.sitkUInt8)
    image_sitk_output = sitk.Bilateral(sitk_image, domainSigma=1.5, rangeSigma=10.0)
    image_sitk_output = sitk.Cast(image_sitk_output, sitk.sitkUInt8)
    image_output = sitk.GetArrayFromImage(image_sitk_output)
    return image_output


if __name__ == "__main__":
    import time

    # specify the path
    analysis_folder = "C:/Users/Jana/Documents/analysis/"
    folder_path = analysis_folder + "example_data/Ome_tiff/"
    file_name = "test.ome"
    file_extension = '.tiff'

    path = folder_path + file_name + file_extension

    # read in the image

    metadata = get_metadata_ometiff(path)
    test_image = get_xyz_image_from_5d(1, 0, get_image(path), metadata)
    start_time = time.time()
    print("Start ")
    # filtered_image = percentile_filter_xyz(test_image, filter_size=2, percentile=-70)
    filtered_image = bilateral_filter(test_image)  # per iteration and 3D image: takes 26 s for range 10, 100 samples
    # filtered_image = curvature_diff_image_filter(filtered_image, iterations=5)  # per iteration, 3D image: takes 1.5s
    # filtered_image = grad_diff_image_filter(test_image) # per 3D image: takes 12 s
    # filtered_image = patch_based_denoising(test_image, also_apply_grad_diff=True)  # takes 630-1100 s per 3D image
    print("--- filtering took %s seconds ---" % (time.time() - start_time))

    # show the filtered image
    import napari
    with napari.gui_qt():
        viewer = napari.Viewer()
        viewer.add_image(filtered_image)
        viewer.add_image(test_image)
