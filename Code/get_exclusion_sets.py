import numpy as np
import sys
from get_connections_for_connection_matrix import get_possible_connections
from distance_measures_with_exclusion_sets import CostVector
from connection_matrix_with_exclusion_sets import ConnectionMatrix


# get the exclusions sets and new points list from a point_object
def get_exclusion_sets_from_points_object(point_object, time_point):

    print("getting exclusion set")

    # first add the none split points to the points list and add their split equivalents to the exclusion sets
    # single points
    chosen_non_split_points = point_object.coordinates[point_object.is_chosen & ~point_object.is_split]

    # get the split points of the chosen, split points
    split_equivalents_of_cnsp = np.array(point_object.split_points)[point_object.is_chosen & ~point_object.is_split,
                                                                    :, :]
    # reshape the points (because at this stage two points belonging to a single split are bundled together)
    reshaped_split_equivalents_of_cnsp = split_equivalents_of_cnsp.reshape(-1, *split_equivalents_of_cnsp.shape[-1:])

    # find the sizes of these points from the watersheds, and also the sizes equivalent to the split points
    chosen_non_split_points_watershed_sizes = point_object.watershed_sizes[point_object.is_chosen &
                                                                           ~point_object.is_split]
    split_equivalents_of_cnsp_sizes = np.zeros(2*len(chosen_non_split_points_watershed_sizes))
    split_equivalents_of_cnsp_sizes[::2] = 0.5*chosen_non_split_points_watershed_sizes
    split_equivalents_of_cnsp_sizes[1::2] = 0.5*chosen_non_split_points_watershed_sizes

    assert (len(chosen_non_split_points_watershed_sizes) == len(chosen_non_split_points)), \
        "Number of non-split points same as number of their watershed sizes"

    # create the exclusion sets (a split point can't be chosen at the same time as its non-split version)
    no_chosen_points = len(chosen_non_split_points)
    exclusion_sets_first_point = [[time_point + 0.001 * i, time_point + 0.001 * (no_chosen_points + 2 * i)]
                                  for i in range(no_chosen_points)]
    exclusion_sets_second_point = [[time_point + 0.001 * i, time_point + 0.001 * (no_chosen_points + (2 * i) + 1)]
                                   for i in range(no_chosen_points)]
    exclusion_sets = exclusion_sets_first_point + exclusion_sets_second_point

    assert (len(chosen_non_split_points) == len(reshaped_split_equivalents_of_cnsp)/2), \
        "Splitting results in less or more than two objects on average"

    # concatenate the two lists of points
    chosen_points_including_split_points = np.concatenate((chosen_non_split_points, reshaped_split_equivalents_of_cnsp))
    chosen_sizes_including_split_points = np.concatenate((chosen_non_split_points_watershed_sizes,
                                                          split_equivalents_of_cnsp_sizes))

    assert (len(chosen_points_including_split_points) == len(chosen_sizes_including_split_points)), \
        "Number of non-split points same as number of their watershed sizes after concatenation"

    # create an indicator for a split event
    indicator_split = [0] * len(chosen_non_split_points) + [1] * len(reshaped_split_equivalents_of_cnsp)

    if len(chosen_non_split_points) < point_object.number_expected:
        booleans_indicating_split_points = point_object.is_chosen & point_object.is_split
        split_point_indices = np.argwhere(booleans_indicating_split_points)
        for index in split_point_indices:
            chosen_points_including_split_points = np.append(chosen_points_including_split_points,
                                                             point_object.split_points[index[0]], axis=0)

            chosen_sizes_including_split_points = np.append(chosen_sizes_including_split_points,
                                                            [point_object.watershed_sizes[index[0]]/2,
                                                             point_object.watershed_sizes[index[0]]/2], axis=0)

            indicator_split += [2, 2]

            # make sure these points must be chosen
            exclusion_sets.append([time_point + 0.001 * (len(chosen_points_including_split_points)-1)])
            exclusion_sets.append([time_point + 0.001 * (len(chosen_points_including_split_points)-2)])

    assert (len(chosen_points_including_split_points) == len(chosen_sizes_including_split_points)), \
        "Number of non-split points same as number of their watershed sizes after adding split points"

    print("Got the following exclusion sets")
    print(exclusion_sets)
    return chosen_points_including_split_points, exclusion_sets, indicator_split, chosen_sizes_including_split_points


def get_exclusion_sets_cm_and_cv(point_objects, p):
    # save the parameters to a log file by setting the system standard output (the place where things are printed to)
    # to be a log file that is stored in the analysis folder.
    exclusion_sets_log_file = p.log_file_exclusion_sets
    old_stdout = sys.stdout
    sys.stdout = open(p.analysis_folder + exclusion_sets_log_file, 'w')

    # create arrays to store values
    connection_matrices, cost_vectors = [], []
    points_1_list, points_2_list = [], []
    indicator_split_list_point_1, indicator_split_list_point_2 = [], []
    sizes_1_list, sizes_2_list = [], []

    # loop  over pairs of neighbouring time points and create a 'network' of connections between them.
    # For each set of connections, define exclusion sets for conflicting hypotheses and the costs for each
    # transition.
    for i, time_point_1, time_point_2 in zip(range(len(p.time_points) - 1), p.time_points[:-1], p.time_points[1:]):

        # write a 'status' message to the log-file
        print("reading in points for frames " + str(time_point_1) + " and frame " + str(time_point_2))

        # get the points and exclusion sets in the respective frames/ at the respective time points
        points_1, exclusion_sets_1, points_1_indicator_split, sizes_1 = get_exclusion_sets_from_points_object(
            point_objects[i], time_point_1)
        points_2, exclusion_sets_2, points_2_indicator_split, sizes_2 = get_exclusion_sets_from_points_object(
            point_objects[i+1], time_point_2)
        # these points to a list where all of them are stored
        points_1_list.append(points_1)
        points_2_list.append(points_2)

        # append these indicators to a list
        indicator_split_list_point_1.append(points_1_indicator_split)
        indicator_split_list_point_2.append(points_2_indicator_split)

        # append the sizes to a list
        sizes_1_list.append(sizes_1)
        sizes_2_list.append(sizes_2)

        assert(len(sizes_1) == len(points_1)), "len watershed size list " + str(len(sizes_1)) + \
                                                               "of points_1 longer or shorter than points list" \
                                                               + str(len(points_1_indicator_split))
        assert(len(sizes_2) == len(points_2)), "len watershed size list" + str(len(sizes_2)) + \
                                                               "of points_2 longer or shorter than points list" \
                                                               + str(len(points_2_indicator_split))

        # get the possible transitions between the points of the respective frames. These transitions are limited by
        # how close the objects are (e.g. only the 10 nearest neighbours in the next frame are considered to be possible
        # end points of the transition). This behaviour is defined in the configuration file
        possible_l_r_transitions, possible_splits, possible_merges = get_possible_connections(points_1,
                                                                                              points_2,
                                                                                              p.constr_moving,
                                                                                              p.constr_split_merge)

        # save to the log file out how many possible events there are in each category
        print("# possible splits: " + str(len(possible_splits)))
        print("# possible merges: " + str(len(possible_merges)))
        print("# possible moves:  " + str(len(possible_l_r_transitions) * len(possible_l_r_transitions[0])))

        # create the connection matrix for each transition and store them in a list that includes all connection
        # matrices
        print("Creating a ConnectionMatrix")

        # make sure the connections to the sink/source are only present for the last/first connection matrix
        if time_point_1 == min(p.time_points):
            is_first_connection_matrix = True
        else:
            is_first_connection_matrix = False
        if time_point_2 == max(p.time_points):
            is_last_connection_matrix = True
        else:
            is_last_connection_matrix = False

        # create a connection matrix and a cost vector with the connections defined above
        m = ConnectionMatrix(len(points_1), len(points_2),
                             p.can_split, p.can_merge, p.can_appear, p.can_disappear,
                             (time_point_1, time_point_2),
                             (possible_l_r_transitions, possible_splits, possible_merges),
                             exclusion_sets_in=exclusion_sets_1,
                             exclusion_sets_out=exclusion_sets_2, no_expected=p.number_expected,
                             is_first_connection_matrix=is_first_connection_matrix,
                             is_last_connection_matrix=is_last_connection_matrix)
        connection_matrices.append(m)

        print(exclusion_sets_1)
        print(exclusion_sets_2)

        # calculate the costs for the respective transitions from the cost vector and store it in a list with all
        # cost vectors
        print("Getting the corresponding CostVector")

        if p.anaphase_onset is not None:
            is_before_anaphase = time_point_2 < p.anaphase_onset
        else:
            is_before_anaphase = True
            print("is before anaphase onset")

        cv = CostVector(points_1,
                        points_2,
                        point_objects[i].microscopy_data,
                        point_objects[i + 1].microscopy_data,
                        m,
                        p.metadata,
                        indicator_split_list_1=indicator_split_list_point_1,
                        indicator_split_list_2=indicator_split_list_point_2,
                        dis_appear_penalty=p.dis_appear_penalty,
                        split_merge_penalty=p.split_merge_penalty,
                        distance_measure_weights=p.distance_measure_weights,
                        use_dist_measure_for_transition=p.use_for_transition,
                        use_dist_measure_for_split_and_merge=p.use_for_split_merge,
                        use_dist_measure_for_appear_disappear=p.use_for_dis_appear,
                        only_use_before_anaphase_onset=p.only_use_before_anaphase_onset,
                        is_before_anaphase_onset=is_before_anaphase,
                        watershed_sizes_1=sizes_1, watershed_sizes_2=sizes_2
                        )

        cost_vectors.append(cv)
        print("....")

    # reset the systems standard output, so things that are printed are no longer stored in the log-files
    sys.stdout = old_stdout

    return (cost_vectors, connection_matrices, points_1_list, points_2_list,
            indicator_split_list_point_1, indicator_split_list_point_2, sizes_1_list, sizes_2_list)
