import read_in as ri
import numpy as np
import matplotlib.pyplot as plt
import warnings


# quantify the amount of bleaching by looking at the first and last time point and compare the absolute difference
# of the histograms
# input: time_points, np.array/list of all frame numbers to be investigated
#        data_folder, microscopy_data_filename_pre_index, microscopy_data_filename_post_index,
#           microscopy_data_file_extension: str, the file naming conventions
#        roi: np.slice, the region of interest of the microscopy data
#        display: bool, whether or not the histogram should be plotted
# output: bleaching_thresh: float, quantifying the bleaching between the frames (expected absolute difference between
#                           kinetochore maxima in the first and last frame)
def get_bleaching_thresholds_from_histogram(time_points,
                                            data_folder,
                                            microscopy_data_filename_pre_index,
                                            microscopy_data_filename_post_index,
                                            microscopy_data_file_extension,
                                            roi,
                                            display=False):

    # if only one frame is passed to this function, there is no bleaching, so return 0
    if np.min(time_points) == np.max(time_points):
        return 0

    # read in the microscopy data of the first and last frame
    microscopy_data_first_frame = ri.load_image_from_memory(np.min(time_points), data_folder,
                                                            microscopy_data_filename_pre_index,
                                                            microscopy_data_filename_post_index,
                                                            microscopy_data_file_extension,
                                                            roi)
    microscopy_data_last_frame = ri.load_image_from_memory(np.max(time_points), data_folder,
                                                           microscopy_data_filename_pre_index,
                                                           microscopy_data_filename_post_index,
                                                           microscopy_data_file_extension,
                                                           roi)

    # flatten the data of the first and last frame to obtain histograms with the np.histogram function
    flattened_first_frame = microscopy_data_first_frame.flatten()
    flattened_last_frame = microscopy_data_last_frame.flatten()
    # histograms of the first and last frame

    histogram_first_frame, bin_edges_first_frame = np.histogram(flattened_first_frame, bins=256, density=False,
                                                                range=(0, 600))
    histogram_last_frame, bin_edges_last_frame = np.histogram(flattened_last_frame, bins=256, density=False,
                                                              range=(0, 600))

    # threshold how much the histograms must differ relatively to each other to be considered 'different': 10%
    threshold_percentage = 0.1

    # get the relative absolute difference between the first and last frame
    # Note: this causes a RuntimeWarning, because the divide operation is done before the logical where is applied,
    #       this error can be ignored, as all values causing this warning are set to zero after this.
    histogram_diff = histogram_first_frame - histogram_last_frame
    histogram_sum = histogram_first_frame + histogram_last_frame
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=RuntimeWarning)
        histogram_diff_rel = np.abs(np.where(histogram_sum > 0, 2 * histogram_diff / histogram_sum, 0))

    # condition 1: relative difference of counts in first and last frame has to be greater than 10% to be considered
    #              to be caused by bleaching

    first_condition_where_histogram_diff_rel_greater_thresh = np.where(histogram_diff_rel > threshold_percentage)

    # get the values of all histogram parts that fulfill this first condition
    edges_where_one_condition = bin_edges_first_frame[first_condition_where_histogram_diff_rel_greater_thresh]
    first_frame_where_first_condition = histogram_first_frame[first_condition_where_histogram_diff_rel_greater_thresh]
    last_frame_where_first_condition = histogram_last_frame[first_condition_where_histogram_diff_rel_greater_thresh]

    # condition 2: to not take into account fluctuations in the low intensity (non-fluorescent data) regime, only 
    #              take values greater than the mean in the first frame
    second_condition_where_bin_values_greater_mean = np.where(edges_where_one_condition >
                                                              np.mean(microscopy_data_first_frame))

    # get the values of all histogram parts that fulfill this second condition
    edges_where_both_conditions = edges_where_one_condition[second_condition_where_bin_values_greater_mean]
    first_frame_where_both_conditions = \
        first_frame_where_first_condition[second_condition_where_bin_values_greater_mean]
    last_frame_where_both_conditions = last_frame_where_first_condition[second_condition_where_bin_values_greater_mean]

    # get the mean of these 'bleaching effected regimes' in the first and last frame
    sum_first_frame = np.sum(edges_where_both_conditions * first_frame_where_both_conditions)
    sum_last_frame = np.sum(edges_where_both_conditions * last_frame_where_both_conditions)
    first_frame_mean = sum_first_frame / np.sum(first_frame_where_both_conditions)
    last_frame_mean = sum_last_frame / np.sum(last_frame_where_both_conditions)

    # get the absolute difference between those to means
    diff_first_and_last_frame_where_both_conditions_mean = first_frame_mean - last_frame_mean

    # the bleaching threshold is 2.5 (empirical factor) times this absolute difference
    bleaching_thresh = 2.5 * diff_first_and_last_frame_where_both_conditions_mean

    # if the display variable is set to 'True' create a two histograms (log-scale and absolute scale) of the first
    # frame, the last frame and their absolute difference
    if display:
        fig, axs = plt.subplots(nrows=2, figsize=(20, 10))
        ax_row1, ax_row2 = axs[0], axs[1]
        ax_row1.set_ylim((10 ** (-10), 10 ** 7))
        ax_row1.set_yscale('log', nonposy='clip')
        for ax in axs:
            ax.set_xlim(0, 600)
            ax.bar(x=bin_edges_first_frame[:-1], height=histogram_first_frame, color='red', alpha=0.5,
                   label='first frame')
            ax.bar(x=bin_edges_last_frame[:-1], height=histogram_last_frame, color='blue', alpha=0.5,
                   label='last frame')
            ax.bar(x=bin_edges_last_frame[:-1], height=histogram_diff, color='green', alpha=0.8,
                   label='difference fist and last frame')
            ax.axhline(threshold_percentage, color='blue')
            ax.legend(loc='upper right')
        plt.show()

    return bleaching_thresh


# use the first and second moment of the intensity distribution to get thresholds for segmentation of kinetochores
# idea: the background dominates the image, the intensity distribution of the background should always be
#       roughly the same compared ot the data
# input: microscopy_data_of_frame: np.array with the intensity image as captured by the microscope
# output: three thresholds at 4.5, 8.3 and 14.2 std of the intensity distribution
def get_intensity_thresholds_from_intensity_distribution_moments(microscopy_data_of_frame):
    # get the mean and the standard deviation of the data
    mean = np.mean(microscopy_data_of_frame)
    std = np.std(microscopy_data_of_frame)

    # calculate the values as mentioned above, the constants are determined heuristically
    thresh_double_thresh = mean + 4.5 * std
    thresh_max_init = mean + 8.3 * std
    thresh_watershed = mean + 14.2 * std

    return thresh_max_init, thresh_double_thresh, thresh_watershed


# match the histograms of two input pictures: the "picture_to_be_transformed"'s values
# will be changed linearly, such that its histogram matches the "template_picture's" histogram.
# The boolean value "return_function_only" can be set to True, if only the mapping function should
# be returned.
# inputs: picture_to_be_transformed: np.array(), picture that will be changed so its histogram
#                                    matches the "template_picture"'s histogram
#         template_picture: np.array(), picture with a histogram of the desired shape
#         return_function_only (optional): bool, default is False, can be set to True, if only 
#                                          the mapping function should be returned.
#         cut_off_tails (optional): bool, default is False, can be set to True, if the tails
#                                   of the intensity distribution with only few counts should
#                                   not be taken into account for the interpolation function
# outputs: updated_picture: (if return_function_only is False) np.array of the input picture
#                           but with the histogram matching the template_picture's histogram
#          interpolation_function: the function that interpolates between intensity values from
#                                  the new and the old data (careful, the indices are 
#                                  the templ values, so interpolation_function[i] gives
#                                  back the interpolation_function value of templ_values[i])
#          templ_values: the template values for execution of the interpolation_function
def match_histograms(picture_to_be_transformed, template_picture, return_function_only=False, cut_off_tails=False):
    # save the old shape to later create a transformed picture from its flattened version
    old_shape = picture_to_be_transformed.shape

    # get the counts for each value for both pictures
    # for the picture to be transformed: also need the inverse to later recreate
    # the picture
    trafo_values, trafo_inverse, trafo_counts = np.unique(picture_to_be_transformed,
                                                          return_inverse=True,
                                                          return_counts=True)
    templ_values, templ_counts = np.unique(template_picture,
                                           return_counts=True)

    if cut_off_tails:
        # do not take into account count bins that are smaller than 0.01% of the total counts
        where_trafo_counts_bigger_10_minus_4_max = np.where(trafo_counts < np.max(trafo_counts) * 10 ** (-4))

        trafo_counts[where_trafo_counts_bigger_10_minus_4_max] = 0

        where_templ_counts_bigger_10_minus_4_max = np.where(templ_counts < np.max(templ_counts) * 10 ** (-4))
        templ_counts[where_templ_counts_bigger_10_minus_4_max] = 0
    # calculate the cumulative distribution functions (normalised over quantiles)
    # calculate CDFs
    trafo_cdf = np.cumsum(trafo_counts)
    templ_cdf = np.cumsum(templ_counts)
    # normalise them, by dividing by the total number of counts    
    trafo_cdf_norm = trafo_cdf / np.max(trafo_cdf)
    templ_cdf_norm = templ_cdf / np.max(templ_cdf)

    # get a linear interpolation function that maps the quantiles of the
    # "picture-to-be-transformed"'s CDF to the template picture's normalised CDF
    # from np.interp documentation numpy.interp(x, xp, fp, ...):
    # Returns the one-dimensional piecewise linear interpolant to a function with given 
    # discrete data points (xp, fp), evaluated at x.
    # use the old dtype as output
    interpolation_function = np.interp(trafo_cdf_norm, templ_cdf_norm, templ_values)

    # if only the return function should be returned, finished, do so
    if return_function_only:
        return interpolation_function, templ_values
    # otherwise: additionally calculate the updated_picture - the "picture_to_be_transformed", 
    # that's histogram matches the "template picture"
    else:
        # evaluate the interpolation function at the inverse transformation values
        # to map the old pixel values to the new ones, then reshape this flat 
        # array, such that the picture has the same shape as the input and return
        # both the updated_picture and the corresponding interpolation_function
        updated_picture = interpolation_function[trafo_inverse].reshape(old_shape)
        return updated_picture, interpolation_function


# input: new_microscopy_data: np.array of the new microscopy data
# output: new_thresholds: np.array with new thresholds in the order threshold_for_max_init,
#                         threshold_for_double_thresholding_init,
#                         threshold_for_watershed_splitting_init
def get_intensity_thresholds_from_histogram_matching_with_optimised_data(new_microscopy_data):
    # load the sample data from memory
    sample_thresh_array = np.load("thresholds_that_match_sample_data_for_histogram_matching.npy")
    sample_microscopy_data = np.load("sample_data_for_histogram_matching.npy")
    # interpolate between the new and the old histogram data
    histogram_interpolation_function, sample_vals = match_histograms(sample_microscopy_data, new_microscopy_data,
                                                                     return_function_only=True)
    new_thresholds = []
    # execute the interpolation function at the threshold values 
    # this has to be done via the sample vals, (see np.interp documentation and match_histograms function)
    for thresh in sample_thresh_array:
        equivalent_of_thresh = histogram_interpolation_function[np.where(sample_vals == thresh)]
        # if there is no equivalent, because the value is to large or too small, set it to the max/min respectively
        if len(equivalent_of_thresh) == 0:
            if np.max(new_microscopy_data) > thresh:
                new_thresholds.append(np.max(new_microscopy_data))
            else:
                new_thresholds.append(np.min(new_microscopy_data))
        # if there is an equivalent, set the new threshold to its equivalent
        else:
            new_thresholds.append(int(equivalent_of_thresh))
    return new_thresholds


# from the metadata, calculate the physical volume of a single pixel in nanometers using information on 
# the sampling in x,y and z-direction and the unit in which these values are given
# input: metadata: dict, as specified for the read in
# output: px_volume_in_nm: float, the volume of a single pixel in nm
def get_physical_volume_of_one_pixel_in_nm(metadata):
    # get scaling factor to nanometers from the unit
    if metadata['x_size_unit'] == 'nm':
        unit_scaling_factor = 1
    elif metadata['x_size_unit'] in ['um', 'μm']:
        unit_scaling_factor = 10 ** 3
    elif metadata['x_size_unit'] == 'mm':
        unit_scaling_factor = 10 ** 6
    elif metadata['x_size_unit'] == 'cm':
        unit_scaling_factor = 10 ** 7
    elif metadata['x_size_unit'] == 'm':
        unit_scaling_factor = 10 ** 9
    # if the physical unit is not specified correctly, raise an Error
    else:
        raise ValueError('The unit of the x-pixel size is not properly specified in the metadata '
                         + '(should be "nm" or ["μm", "um"], "mm", "cm" or "m")'
                         + '\nIf it is not automatically read in, please specify it in the configuration (.yml) file.')

    # calculate the volume of one pixel in 'unknown' units
    volume_of_px = metadata['phys_size_x'] * metadata['phys_size_y'] * metadata['phys_size_z']
    # convert the 'unknown' unit size to nm
    px_volume_in_nm = unit_scaling_factor ** 3 * volume_of_px

    # return the pixel volume in nanometer
    return px_volume_in_nm


# automatically calculate thresholds for the different settings used to find kinetochores from the normalised
# cross-correlation function
# inputs: metadata: dict, as specified for the read in
def calculate_thresholds_for_detection_norm_cc(metadata):
    # calculate the size threshold that is used to decide which objects a considered to be 'big'
    # after performing a watershed on initial seed points as performed by the function
    # 'get_additional_points_from_big_objects'
    threshold_for_big_object_size = 114 / (500 * 130 * 130) * get_physical_volume_of_one_pixel_in_nm(metadata)
    return threshold_for_big_object_size


# get thresholds for automated kinetochore segmentation based on features of the histogram and their size
# input: time_points, np.array/list of all frame numbers to be investigated
#        data_folder, microscopy_data_filename_pre_index, microscopy_data_filename_post_index,
#           microscopy_data_file_extension: str, the file naming conventions
#        roi: np.slice, the region of interest of the microscopy data
#             metadata: dict, as specified for the read in
#        display: bool, whether or not the histogram should be plotted
# output: thresholds_dict: a dictionary containing the thresholds calculated by the respective functions above,
#                          the keys are:
#                          ["max_points_init", "double_thresholding_init", "watershed_intensity_init",
#                           "watershed_obj_size", "slope_bleaching"]
def get_thresholds(time_points,
                   data_folder, microscopy_data_filename_pre_index, microscopy_data_filename_post_index,
                   microscopy_data_file_extension, roi, metadata, display=False):
    # read in the microscopy data of the first frame
    microscopy_data_of_first_frame = ri.load_image_from_memory(np.min(time_points), data_folder,
                                                               microscopy_data_filename_pre_index,
                                                               microscopy_data_filename_post_index,
                                                               microscopy_data_file_extension,
                                                               roi)
    # get the initial thresholds based on the intensity histogram
    thresholds_intensity_moments_init = \
        get_intensity_thresholds_from_histogram_matching_with_optimised_data(microscopy_data_of_first_frame)
    thresh_for_max_init, thresh_for_double_thresholding_init, thresh_watershed_splitting_init = \
        thresholds_intensity_moments_init

    # get the threshold above which an object is considered 'big' and is split
    threshold_for_big_object_size = calculate_thresholds_for_detection_norm_cc(metadata)

    # get the expected value of bleaching by comparing the first and last frames' histograms
    slope_bleaching_enum = get_bleaching_thresholds_from_histogram(time_points, data_folder,
                                                                   microscopy_data_filename_pre_index,
                                                                   microscopy_data_filename_post_index,
                                                                   microscopy_data_file_extension,
                                                                   roi, display=display)
    # get the amount of bleaching in between subsequent frame assuming it is a linear process
    if np.max(time_points) == np.min(time_points):
        slope_bleaching = 0
    else:
        slope_bleaching_denominator = np.max(time_points) - np.min(time_points)
        slope_bleaching = slope_bleaching_enum / slope_bleaching_denominator

    dict_keys = ["max_points_init", "double_thresholding_init", "watershed_intensity_init", "watershed_obj_size",
                 "slope_bleaching"]
    dict_vals = [thresh_for_max_init,
                 thresh_for_double_thresholding_init,
                 thresh_watershed_splitting_init,
                 threshold_for_big_object_size, slope_bleaching]

    thresholds_dict = dict(zip(dict_keys, dict_vals))

    return thresholds_dict


# get thresholds in frame taking into account bleaching
# inputs: frame_no: int, number of the current frame
#         time_points: list/np.array containing all frames which will be examined
#         slope_bleaching: expected intensity difference in consecutive frames in the relevant
#                          intensity range caused by bleaching
#         intensity_thresholds: tuple/array/list containing the intensity thresholds to be changed
#         return_dict_instead: boolean, if True: instead of returning a tuple with the new threshold value, return a
#                              dictionary with the following keyword arguments:
#                              ["max_points", "double_thresholding", "watershed_intensity"]
# outputs: new_intensity_threshs: list of the transformed intensity thresholds (same length as
#                                 the "intensity_thresholds" put in)
#          new_dict: a dictionary with the new threshold values
def get_thresholds_after_considering_bleaching(frame_no, time_points, slope_bleaching,
                                               intensity_thresholds, return_dict_instead=False):
    import sys
    sys.stdout = sys.__stdout__

    print(time_points)
    print(intensity_thresholds)
    print(slope_bleaching)

    # calculate expected difference in intensity value due to bleaching
    bleaching_impact = (frame_no - np.min(time_points)) * slope_bleaching

    # create list to store new intensity thresholds
    new_intensity_threshs = []
    # get the thresholds by subtracting this difference and converting to integer values
    for thresh in intensity_thresholds:
        new_intensity_threshs.append(int(thresh - bleaching_impact))

    # return the thresholds
    if not return_dict_instead:
        return new_intensity_threshs
    else:
        intensity_threshold_for_max, intensity_threshold_for_double_thresholding, threshold_for_watershed_splitting = \
            new_intensity_threshs
        dict_keys = ["max_points", "double_thresholding", "watershed_intensity"]
        dict_vals = [intensity_threshold_for_max, intensity_threshold_for_double_thresholding,
                     threshold_for_watershed_splitting]
        new_thresholds_dict = dict(zip(dict_keys, dict_vals))
        return new_thresholds_dict
