import numpy as np


# remove nesting from list iteratively
# inputs: l: nested list (can be nested on multiple levels)
# output: non_nested_list: list, but not-nested on any level
def remove_nesting(nested_list):
    def create_list_generator_iteratively(nested_list_2):
        for i in nested_list_2:
            if type(i) == list:
                for x in remove_nesting(i):
                    yield x
            else:
                yield i
    non_nested_list = list(create_list_generator_iteratively(nested_list))
    return non_nested_list


# check if a sequence (e.g. list, np.array, tuple) of lists has any items
# input: sequence (iterable object, len(sequence must be defined)) of lists
# output: bool, True if the sequence contains any lists, False otherwise
def has_items(seq):
    return np.any(len(x) != 0 for x in seq)
