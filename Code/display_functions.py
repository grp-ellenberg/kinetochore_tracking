import numpy as np
import warnings
import sys

from read_in import get_scale_factor

with warnings.catch_warnings():
    warnings.simplefilter(action='ignore', category=UserWarning)
    import napari

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib.colors import LinearSegmentedColormap, ListedColormap
from mpl_toolkits.axes_grid1 import make_axes_locatable
from IPython.display import display_html
import matplotlib.cm as cm
from matplotlib.ticker import MaxNLocator

import os


# create a plot of the number of points found over the frame number and a histogram
# of the number of points found in each frame
# inputs: point_objects: list of instances of the PointObject class that were created
#                         during the segmentation step
#         p: parameters object created from reading in the configurations file
#         display: bool, whether or not the plots should be displayed in this step
def save_plot_of_points_found_over_frame(point_objects, p, display=False):

    # use the matplotlib dark style
    plt.style.use('dark_background')

    # create a list of number of points found (active points) in each frame
    number_of_points_found = [len(po.active_points) for po in point_objects]

    # print the mean of the number of points found
    print("mean number of points found: " + str(np.mean(number_of_points_found)))

    # create a histogram of points found
    # create the bins, starting from the minimum number of points found, ranging to the maximum number
    # adding a little space on both sides for the bars
    bins_for_hist = np.arange(min(number_of_points_found) - 0.5, max(number_of_points_found) + 1.5)
    # create a matplotlib figure and get the current axes
    plt.figure()
    ax = plt.figure().gca()
    # make sure all labels will be shown in integer format only
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    # give the plot a title and x-and y-labels
    plt.title("Histogram of points found per frame")
    plt.xlabel("Number of points found")
    plt.ylabel("Counts")
    # plot the actual histogram of # points found
    plt.hist(number_of_points_found, bins=bins_for_hist, rwidth=0.8)
    # save the figure in the analysis folder
    plt.savefig(p.analysis_folder + 'manually_adjusted_segmentation_points/histogram_of_points_found_per_frame.png',
                dpi=300)
    # if the display option is chosen, display the plot
    if display:
        plt.show()
    else:
        plt.close()

    # create a matplotlib figure and get the current axes
    plt.figure()
    ax = plt.figure().gca()
    # make sure all labels will be shown in integer format only
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    # give the plot a title and x-and y-labels
    plt.title("Scatter plot of points found over frame number")
    plt.xlabel("Frame number")
    plt.ylabel("Number of points found")
    # plot the number of points found over the frames/time-points
    plt.plot(p.time_points, number_of_points_found, 'o')
    # save the figure in the analysis folder
    plt.savefig(p.analysis_folder + 'manually_adjusted_segmentation_points/points_found_over_frame.png',
                dpi=300)
    # if the display option is chosen, display the plot
    if display:
        plt.show()
    else:
        plt.close()

    # create a bar plot indicating which points are False positives and which are correct (using the manual points
    # if they exist)

    if point_objects[0].manual_points is not None:
        list_has_close_eq = []
        list_has_no_close_eq = []
        number_of_manual_points = []
        number_true_positives, number_false_negatives, number_false_positives = 0, 0, 0

        from scipy.spatial.distance import cdist
        # get the number of points with close equivalents and the number of those who don't
        for po in point_objects:
            number_of_manual_points.append(len(po.manual_points))
            min_dist_to_manual_points = np.min(cdist(po.active_points, po.manual_points), axis=1)
            has_close_equivalent_in_manual_point = [min_dist_to_manual_point < 3 for min_dist_to_manual_point in
                                                    min_dist_to_manual_points]
            num_close_equivalent_exists = np.sum(has_close_equivalent_in_manual_point)
            num_no_close_equivalent = len(has_close_equivalent_in_manual_point) - num_close_equivalent_exists
            list_has_close_eq.append(num_close_equivalent_exists)
            list_has_no_close_eq.append(num_no_close_equivalent)
            # if all manual points are given, get the true positive and false positive rates, as well as the number
            # of false negatives
            if len(po.manual_points) == p.number_expected:
                number_true_positives += num_close_equivalent_exists
                number_false_positives += num_no_close_equivalent
                number_false_negatives += p.number_expected - num_close_equivalent_exists

        # create a bar plot
        n = len(list_has_no_close_eq)
        x_ind = p.time_points
        width = 0.5

        plt.figure(figsize=(15, 5))
        p1 = plt.bar(x_ind, list_has_close_eq, width, color='green', label='TP')
        p2 = plt.bar(x_ind, list_has_no_close_eq, width,
                     bottom=list_has_close_eq, color='red', label='FP')
        x_ind_list = np.arange(min(p.time_points) + 0.5, max(p.time_points) + 1.5)
        steps = plt.step(x_ind_list, number_of_manual_points, color='blue', label='# manual points')
        plt.ylabel('Number of points')
        plt.xlabel('Frame number')
        plt.ylim(0, p.number_expected + 2)
        plt.xlim(min(p.time_points) - 1, max(p.time_points) + 1)
        plt.title('Number of points with or without manual equivalent in segmentation')
        plt.legend(loc='upper center',  bbox_to_anchor=(0.5, -0.2), ncol=3)
        plt.tight_layout()
        # save the figure in the analysis folder
        plt.savefig(p.analysis_folder + 'manually_adjusted_segmentation_points/close_equivalents_manually.png',
                    dpi=300)
        # if the display option is chosen, display the plot
        if display:
            plt.show()
        else:
            plt.close()

        # print the precision and recall values, if a manual solution is given
        print("precision: ")
        print(number_true_positives/(number_true_positives + number_false_positives))
        print("recall: ")
        print(number_true_positives/(number_true_positives + number_false_negatives))

    return


# show the model used for segmentation using the napari viewer
# input: p: the Parameters class that contains all parameters, including the model of the downsampled region of interest
#           which will be shown in this function
def show_model_using_napari(p):
    
    with napari.gui_qt():

        # create an instance of a napari Viewer object and set the dimensions in which it will be displayed to 3
        viewer = napari.Viewer()
        viewer.dims.ndisplay = 3
        # display the part of the model (ROI) which will be used for segmentation purposes using the napari viewer

        up_f = p.model_upsampling_factor

        viewer.add_image(p.model_roi_downsampled,
                         colormap='turbo',
                         name='downsampled model ROI',
                         opacity=1.0,
                         scale=(up_f, up_f, up_f))

        viewer.add_image(p.model_roi,
                         colormap='turbo',
                         name='model ROI',
                         opacity=1.0)
        viewer.add_image(p.model_array,
                         colormap='turbo',
                         name='model',
                         opacity=1.0)
    return


# add coordinate system to a napari viewer using a vectors layer
def add_coordinate_system(viewer, image=None):
    # create an empty array for the vectors
    vectors = np.zeros((3, 2, 3), dtype=np.float32)
    # if no image is given, make each vector 20 px long
    if image is None: 
        shape = [20, 20, 20]
    # otherwise, use half the image length in each dimension
    else:
        shape = 0.5*image.shape
    # set the projections in each dimension
    for i in range(len(shape)):
        vectors[i, 1, i] = shape[i]
    # add the vectors to the viewer
    viewer.add_vectors(vectors, edge_width=3)


def show_original_and_segmented_data_napari(metadata_var, orig_5_d_image=None, segmented_4_d_image=None,
                                            channel_var=None, time_points=None,
                                            blending='additive', gamma=0.85, create_movie=False, add_points=True):
    
    if orig_5_d_image is not None and channel_var is not None and time_points is not None:
        
        # get the 4D image from the original
        if metadata_var['DimOrder BF Array'] == 'TZYXC':
            channel_4_d_image = orig_5_d_image[:, :, :, :, channel_var]
            # reorder the array such that it is an xyz array
            channel_4_d_image = channel_4_d_image.transpose((0, 3, 2, 1))
        elif metadata_var['DimOrder BF Array'] == 'TZCYX':
            channel_4_d_image = orig_5_d_image[:, :, channel_var, :, :]
            # reorder the array such that it is an xyz array
            channel_4_d_image = channel_4_d_image.transpose((0, 3, 2, 1))
        else:
            warnings.warn(f"Adapt the function show_original_and_segmented_data_napari to \
                          include {metadata_var['DimOrder BF Array']}",
                          UserWarning)
            print("Adapt the function show_original_and_segmented_data_napari to \
            include {metadata_var['DimOrder BF Array']}")
            sys.exit()
        # only take time frames that were examined from original image:
        relevant_channel_4_d_image = channel_4_d_image.take(time_points, axis=0)
        # set some contrast limits
        contrast_lim_org = [relevant_channel_4_d_image.min(), np.round(relevant_channel_4_d_image.max() * 0.7)]
        
        # get the 'old' scaling before interpolation
        scale_org = (metadata_var['phys_size_x'], metadata_var['phys_size_y'], metadata_var['phys_size_z'])
    else:
        print("Either the original image, or the channel variable or the time points were not given to the function")
        relevant_channel_4_d_image, scale_org, contrast_lim_org = None, None, None
    
    if segmented_4_d_image is not None:
    
        # get the new scale
        scale_new = (metadata_var['phys_size_x'], metadata_var['phys_size_y'], metadata_var['new_phys_size_z'])

        # get the new contrast limits
        contrast_lim_new = [segmented_4_d_image.min(), segmented_4_d_image.max()]
    
    else:
        print("No segmented image was given to the function.")
        contrast_lim_new, scale_new = None, None

    # add these images to the Napari viewer with the correct scaling and only the relevant time frames
    with napari.gui_qt():
        viewer = napari.Viewer()
        if orig_5_d_image is not None and channel_var is not None and time_points is not None:
            viewer.add_image(relevant_channel_4_d_image,
                             name=f'original image of channel {channel_var}',
                             scale=scale_org,
                             contrast_limits=contrast_lim_org,
                             blending=blending,
                             gamma=gamma)
        if segmented_4_d_image is not None:
            viewer.add_image(segmented_4_d_image,
                             name=f'segmented image of channel {channel_var}',
                             scale=scale_new,
                             contrast_limits=contrast_lim_new,
                             blending=blending,
                             gamma=gamma,
                             colormap='inferno',
                             opacity=0.5)
            
        # already display this in 3D mode
        viewer.dims.ndisplay = 3
        
        if create_movie:
            print("Use the following commands to create a movie: \n \
                  f: add a key frame right after the current key-frame \n \
                  r: replace the current key-frame with this view \n \
                  a: go to the next key-frame \n \
                  b: go to the previous key-frame \n \
                  w: interpolate through all key-frames \n \
                  more info: https://github.com/guiwitz/naparimovie")
            from naparimovie import Movie
            # %gui qt5
            # create napari movie object
            movie = Movie(myviewer=viewer)
        
        if add_points:
            # annotate the background and all the objects, in that order
            pts_layer = viewer.add_points(size=5)
            pts_layer.mode = "add"

    if create_movie:
        print("saving and creating the movie")
        movie.make_movie(name='napari_movie.mp4', resolution=300, fps=25)
    if add_points:
        coordinates = pts_layer.data
        coordinates_int = np.round(coordinates).astype(int)
        print(coordinates_int)
        return coordinates_int


# save a video to .avi using the cv2 library
# inputs: image_path: a folder that contains images as .png files
#         video_name: the name of the output video
#         frames_per_second: the number of frames per second (default is 1)
def save_video(image_path, video_name, frames_per_second=1.):
    
    # this function depends on the opencv library
    import cv2
    
    # from https://stackoverflow.com/questions/44947505/how-to-make-a-movie-out-of-images-in-python
    # answer by BoboDarph, 14.02.20
    
    # append the video name by .avi
    video_name = image_path + video_name + '.avi'
    
    # use list comprehensions to create a list of images
    images = [img for img in os.listdir(image_path) if img.endswith(".png")]
    # read in the first image and get its width, height and layers
    frame = cv2.imread(os.path.join(image_path, images[0]))
    height, width, layers = frame.shape

    # create an empty video of given width and height
    # fourcc: compression information
    video = cv2.VideoWriter(video_name, fourcc=0, fps=frames_per_second, frameSize=(width, height))

    # for all images in the list, add them to the video
    for image in images:
        video.write(cv2.imread(os.path.join(image_path, image)))

    # close all windows and save the video
    cv2.destroyAllWindows()
    video.release()


# change from pixel to physical units in plot
# inputs:ax = axis of plot (matplotlib object)
#        metadata = dictionary of metadata
# outputs: ax = axis of plot (matplotlib object)
def change_plot_scale(ax, metadata):
    
    # idea from: https://stackoverflow.com/questions/10171618/changing-plot-scale-by-a-factor-in-matplotlib 
    # 13.02.20, answer by Oystein
    
    # set the scales
    scale_x, scale_y = metadata["phys_size_x"], metadata["phys_size_y"]

    # change the x and y ticks
    ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*scale_x))
    ax.xaxis.set_major_formatter(ticks_x)

    ticks_y = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*scale_y))
    ax.yaxis.set_major_formatter(ticks_y)
    
    # if it is a 3D plot: change the z-ticks
    if ax.name == "3d":
        scale_z = metadata["phys_size_z"]
        ticks_z = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*scale_z))
        ax.zaxis.set_major_formatter(ticks_z)
    
    # return the axes
    return ax


def show_napari(array, metadata,
                blending='additive',
                gamma=0.85,
                verbose=True):
    """
    Show the multidimensional array using the Napari viewer
    :param array: multidimensional NumPy.Array containing the pixel data
    :param metadata: dictionary with CZI or OME-TIFF metadata
    :param blending: NapariViewer option for blending
    :param gamma: NapariViewer value for Gamma
    :param verbose: show additional output
    """

    with napari.gui_qt():

        # create scale factor with all ones
        scale_factors = [1.0] * len(array.shape)

        # initialize the napari viewer
        print('Initializing Napari Viewer ...')
        viewer = napari.Viewer()

        if metadata['ImageType'] == 'ometiff':

            # find position of dimensions
            pos_z = metadata['DimOrder BF Array'].find('Z')
            pos_c = metadata['DimOrder BF Array'].find('C')
            pos_t = metadata['DimOrder BF Array'].find('T')

            # get the scale_factors from the metadata
            scale_f = get_scale_factor(metadata)
            # modify the tuple for the scales for napari
            scale_factors[pos_z] = scale_f['zx']

            if True:
                print('Dim PosT : ', pos_t)
                print('Dim PosC : ', pos_c)
                print('Dim PosZ : ', pos_z)
                print('Scale Factors : ', scale_factors)

            # add all channels as layers
            for ch in range(metadata['size_c']):

                try:
                    # get the channel name
                    ch_name = metadata['Channels'][ch]
                except KeyError:
                    # or use CH1 etc. as string for the name
                    ch_name = 'CH' + str(ch + 1)

                # cutout channel
                channel = array.take(ch, axis=pos_c)
                print('Shape Channel : ', ch, channel.shape)

                # actually show the image array
                print('Scaling Factors: ', scale_factors)

                # get min-max values for initial scaling
                clim = [channel.min(), np.round(channel.max() * 0.85)]
                if verbose:
                    print('Scaling: ', clim)
                viewer.add_image(channel,
                                 name=ch_name,
                                 scale=tuple(scale_factors),
                                 contrast_limits=clim,
                                 blending=blending,
                                 gamma=gamma)


# Panda styled table: Set the background color to black for zero-values
# set background of cells with zero entry to black in pandas (styled) table
def color_zeroes_black(val):
    is_zero = val == 0
    return [f'background-color: black' if v else '' for v in is_zero]


# plot a table, a plot of the selected quantity and the segmented image
def select_plot(plot_label_img, plot_data_frame, input_prop):
    # create a figure with two subplots
    fig, ax = plt.subplots(ncols=2, figsize=(15, 5))

    # set make the figures background translucent
    fig.patch.set_facecolor('#E0E0E0')
    fig.patch.set_alpha(0.0)

    # create a bar plot of one of the properties displayed in the data frame
    # 1. set the title
    plot_title = 'Plot of ' + input_prop
    # 2. use a colormap from Red, over Yellow to Green
    color_map_lin = cm.RdYlGn(np.linspace(0., 1., np.max(plot_label_img)))  # noqa
    # 3. use the pandas df.plot command to create a bar plot of column 'input_prop' at axis 0 of the figure,
    #    set the colormap and title
    df_plot = plot_data_frame[input_prop].plot(kind='bar', ax=ax[0], title=plot_title, color=color_map_lin)
    # 4. set the x and y labels of the plot
    df_plot.set_xlabel('label')
    df_plot.set_ylabel('value')

    # middle of stack:
    z_show = int(plot_label_img.shape[-1:][0] / 2)

    # want to use the same colormap for the displayed figure as for the bar plot, but set the background
    # (zero values) to black:
    # 1. create a mask where all the zero values are masked
    masked_label_image = np.ma.masked_where(plot_label_img == 0, plot_label_img)
    # 2. set the colormap to be a copy of the colormap before
    c_map_zero_black = cm.RdYlGn  # noqa
    # 3. set the masked part of the image to 'black'
    c_map_zero_black.set_bad(color='black')
    # 4. plot the masked label image using the colormap created for displaying it
    im = ax[1].imshow(masked_label_image[..., z_show], cmap=c_map_zero_black)
    # add a title
    ax[1].set_title(f'Corresponding labeled image, slice {z_show}')

    # add color bar
    divider = make_axes_locatable(ax[1])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(im, cax=cax, orientation='vertical')

    # 'style' the dataframe:
    # 1. the background_gradient creates a gradient over the same color map for each column
    #    (mins and maxs can be observed easily)
    # 2. apply(color_zeroes_black) sets all cell backgrounds to black if their value is 0
    # 3. the maximum of each column can be highlighted in dark blue
    df_output = plot_data_frame.head(10).style. \
        background_gradient(cmap='RdYlGn', axis=0). \
        apply(color_zeroes_black)
    #  .highlight_max(axis=0, color = 'darkblue')
    # to display the table at the same time as the plots, use the html package
    display_html(df_output._repr_html_(), raw=True) # noqa


# Colormap that diverges at zero:
# Zero is black, positive values are displayed using the colormap c_map_2, negative values are displayed using the
# colormap c_map_1, values between and one [0, 1) are displayed in the colormap c_map_2, the min and max values are
# passed in v_min and v_max
def new_cmap_diverging_at_zero(v_min, v_max, c_map_1=plt.cm.cool_r, c_map_2=plt.cm.RdYlGn, div_0='no'):  # noqa

    # number of distinct colors in the color scale
    no_colors = 512.

    # check if the minimum value is actually smaller than zero
    if v_min < 0:

        # get ratios how to divide the color scale v_max --> 1, 1 --> 0, 0 --> v_min
        v_max_v1 = float(v_max - 1) / float(v_max - v_min)
        v1_v0 = 1. / float(v_max - v_min)
        v0_v_min = np.abs(float(v_min) / float(v_max - v_min))

        # multiply these ratios with the number of colors --> want to sample pi values
        p1 = int(v_max_v1 * no_colors)
        p2 = int(v1_v0 * no_colors)
        p3 = int(v0_v_min * no_colors)

        # sample color values from the color maps and create some 'black' values in the middle
        colors1 = c_map_1(np.linspace(0.0, 1., p3 - 1))
        colors2 = np.ones(shape=(p2 + 1, 4))
        colors2[:] = [0., 0., 0., 1.]
        colors3 = c_map_2(np.linspace(0., 1, p1))

        # stack these colors and create a new colormap with them, name it 'diverging_colormap'
        colors = np.vstack((colors1, colors2, colors3))
        # create a new colormap with them, name it 'diverging_colormap'
        new_c_map = LinearSegmentedColormap.from_list('diverging_colormap', colors)

    elif div_0 == 'diverging_0':
        new_c_map = c_map_2

    # if the minimum value is not negative, just pass the 'positive'-part of the colormap
    else:
        # get ratios how to divide the color scale v_max --> 1, 1 --> 0
        v1_v0 = 1. / float(v_max)
        v_max_v1 = float(v_max - 1) / float(v_max)

        # multiply these ratios with the number of colors --> want to sample pi values
        p1 = int(v_max_v1 * no_colors)
        p2 = int(v1_v0 * no_colors)

        # sample color values from the color maps and create some 'black' values in the middle
        colors3 = c_map_2(np.linspace(0., 1, p1))
        colors2 = np.ones(shape=(p2 + 1, 4))
        colors2[:] = [0., 0., 0., 1.]

        # stack these colors
        colors = np.vstack((colors2, colors3))
        # create a new colormap with them, name it 'diverging_colormap'
        new_c_map = LinearSegmentedColormap.from_list('diverging_colormap', colors)

    return new_c_map


# Sum over the z-axis and plot the result
# display the sum over the z-axis in the x-y-plane for time point t
# create plot that displays the behaviour over time, summing over the z-axis
def plot_sum_z(image, metadata_var, t=-1):
    # sum over the z-axis
    xy_image = np.sum(image, axis=image.ndim - 1)

    # get minimum and maximum values
    v_min = np.min(xy_image)
    v_max = np.max(xy_image)

    # get a colormap that diverges at zero, to distinguish between new and old object
    div_c_map = new_cmap_diverging_at_zero(v_min, v_max)

    # Plot slice for the given plane and slice
    fig = plt.figure(figsize=(8, 6))

    # set make the figures background translucent
    fig.patch.set_facecolor('#E0E0E0')
    fig.patch.set_alpha(0.0)

    # set the extent in x- and y-direction
    extent_x = float(metadata_var["size_x"]) * metadata_var['phys_size_x']
    extent_y = float(metadata_var["size_y"]) * metadata_var['phys_size_y']

    if t == -1:
        plt.imshow(xy_image, cmap=div_c_map, vmin=v_min, vmax=v_max,
                   extent=[0, extent_x, 0, extent_y]
                   )
    else:
        plt.imshow(xy_image[t, ...], cmap=div_c_map, vmin=v_min, vmax=v_max,
                   extent=[0, extent_x, 0, extent_y]
                   )
    # Jana: add color bar (scale) to the plot, label the axes and add a title
    plt.colorbar()
    plt.xlabel(f"x in {metadata_var['x_size_unit']}")
    plt.ylabel(f"y in {metadata_var['y_size_unit']}")
    plt.title(f'Top-view when summing over z-axis')


# Plot centroid traces
# Takes sorted centroid array and creates 3D matplotlib scatter plot where each trace gets darker over time
def plot_centroid_traces(sorted_centroids_array, metadata_var, name_tags, phi=130, theta=30, cmap_string='RdYlGn',
                         gradient='darker'):
    # nested function that creates the legends for each of the colormaps created below
    def plot_color_gradients(c_map_list, figure, name_tags_dict):

        # create the image of a 'gradient' which then can be plotted using the colormaps
        gradient_plt = np.linspace(0, 1, 256)
        gradient_plt = np.vstack((gradient_plt, gradient_plt))

        # get the number of subplots that have to be done (= # of objects)
        no_of_subplots = len(c_map_list)

        # add a subplot with an arrow that shows the direction of increasing time and the label for the 'legend'
        ax = figure.add_subplot(gs[0, 2])
        ax.set_title("Object's color over \n increasing time", fontsize=15)
        ax.arrow(0.1, 0.5, 0.8, 0, fc='w', ec='w', head_width=0.1, head_length=0.075, length_includes_head=True)
        ax.set_axis_off()

        # for each object plot its colormap and size it correctly. Label the color bars so the
        # label of the corresponding object can be observed
        for j in range(0, no_of_subplots):
            ax = figure.add_subplot(gs[j + 1, 2])
            ax.imshow(gradient_plt, aspect=10, cmap=c_map_list[j], )
            ax.set_axis_off()
            name_tag = name_tags_dict[j]
            ax.set_title(f'object {j + 1}, name tag {name_tag}')

    # get the number of objects
    number_of_objects = sorted_centroids_array.shape[2]

    # create a figure, a grid and add one subplot for the 3D projection of the traces
    fig = plt.figure(figsize=(10, 7))
    gs = fig.add_gridspec(number_of_objects + 1, 3)
    ax_3_d = fig.add_subplot(gs[:, 0:2], projection='3d')

    # set make the figure's background translucent
    fig.patch.set_facecolor('#E0E0E0')
    fig.patch.set_alpha(0.0)

    # set make the axis' background translucent
    ax_3_d.patch.set_facecolor('#E0E0E0')
    ax_3_d.patch.set_alpha(0.0)

    # create evenly spaced colors for each object from a colormap (here RdYlGn)

    if cmap_string == 'diverging_0':
        # tweak the colormap, such that the color of the objects correspond to the colors in the other plots
        # (here: by tag, so take into account that zero for background does not exist, and negative labels
        # are only -2, -4, -6, ...)
        min_tag = name_tags[0]
        max_tag = name_tags[number_of_objects - 1]
        c_map_var = new_cmap_diverging_at_zero(min_tag / 2., max_tag + 1, div_0='diverging_0')

    else:
        c_map_var = plt.cm.get_cmap(cmap_string)
    colors_3d_plot = c_map_var(np.linspace(0, 1, number_of_objects))
    fig.subplots_adjust(top=0.95, bottom=0.01, left=0.2, right=0.99)

    # create an empty list for storing all color maps (for each object)
    my_c_maps = []

    # for every object, plot the centroids as a x-y-z-scatter plot with their own colormap
    for i in range(number_of_objects):

        # get the x y and z sequence from the centroids array
        x_vals_sequence = sorted_centroids_array[:, 0, i]*metadata_var['phys_size_x']
        y_vals_sequence = sorted_centroids_array[:, 1, i]*metadata_var['phys_size_y']
        z_vals_sequence = sorted_centroids_array[:, 2, i]*metadata_var['new_phys_size_z']

        # create a costume color map for each trace starting at the initial value derived from the colormap
        # above and going darker
        num = 256
        vals = np.ones((num, 4))

        r_val = colors_3d_plot[i][0]
        g_val = colors_3d_plot[i][1]
        b_val = colors_3d_plot[i][2]

        offset = 0.6

        if gradient == 'darker':
            # extract RGB values and make them 'offset'*100 percent darker over time

            vals[:, 0] = np.linspace(r_val, max(r_val - offset, 0), num)
            vals[:, 1] = np.linspace(g_val, max(g_val - offset, 0), num)
            vals[:, 2] = np.linspace(b_val, max(b_val - offset, 0), num)

        if gradient == 'lighter':
            # extract RGB values and make them 'offset'*100 percent lighter over time

            vals[:, 0] = np.linspace(r_val, min(r_val + offset, 1), num)
            vals[:, 1] = np.linspace(g_val, min(g_val + offset, 1), num)
            vals[:, 2] = np.linspace(b_val, min(b_val + offset, 1), num)

        if gradient == 'no_gradient':
            # just make the colormap stay the same over time
            vals[:, 0] = np.linspace(r_val, r_val, num)
            vals[:, 1] = np.linspace(g_val, g_val, num)
            vals[:, 2] = np.linspace(b_val, b_val, num)

        new_cm = ListedColormap(vals)

        # store the colormap
        my_c_maps.append(new_cm)

        # take n samples from the new colormap to pass it to the scatter3D function
        new_colors = new_cm(np.linspace(0, 1, x_vals_sequence.shape[0]))

        # create a 3D scatter plot from the centroids
        ax_3_d.scatter3D(x_vals_sequence, y_vals_sequence, z_vals_sequence, c=new_colors, marker='o')

    # set the xyz-limits to be the limits of the image
    ax_3_d.set_xlim(0, float(metadata_var["size_x"])*metadata_var['phys_size_x'])
    ax_3_d.set_ylim(0, float(metadata_var["size_y"])*metadata_var['phys_size_y'])
    ax_3_d.set_zlim(0, float(metadata_var["new_size_z"])*metadata_var['new_phys_size_z'])

    # label the axes
    ax_3_d.set_xlabel(f"x in {metadata_var['x_size_unit']}")
    ax_3_d.set_ylabel(f"y in {metadata_var['y_size_unit']}")
    ax_3_d.set_zlabel(f"z in {metadata_var['z_size_unit']}")

    # change to physical units
    # ax_3_d = change_plot_scale(ax_3_d, metadata_var)

    # label the plot
    ax_3_d.set_title("3D Plot of Centroid Traces \n over time \n", fontsize=15)
    ax_3_d.view_init(theta, phi)

    plot_color_gradients(my_c_maps, fig, name_tags)

    # make the layout tight, so objects do not overlap
    plt.tight_layout()
