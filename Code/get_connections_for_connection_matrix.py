from scipy.spatial.distance import cdist


# from a list of points (coordinates_of_objs), get the n-nearest neighbours of
# each object and return a list of sets with the initial object and its n nearest neighbours
# and return them as a list of unique (i->j and j->i will only be returned once)
# tuples
# inputs: coordinates_of_objs: list of np.arrays with coordinates 
#         n: number of closest neighbours to consider
# output: list_of_unique_tuples: pairs of close neighbours as a list of lists
#         (e.g.[[0, 2], [1, 2]] for three objects 0,1 and 2)
def get_n_nearest_neighbours_of_each_obj_in_same_image(coordinates_of_objs, n):
    # get the distance between each object and all other objects (including itself)
    distance = cdist(coordinates_of_objs, coordinates_of_objs)
    # get the n minimum distance objects (exclude the object itself)
    n_obj_with_min_distance = distance.argsort(axis=0)[1:n + 1, :]
    # create a list to store all tuples
    list_of_tuples = []
    # loop over all objects and create tuples with the n objects that
    # are closest to it, to make it easier to remove duplicates, only take the [min(i, j), max(i, j)] pairs
    for i in range(len(coordinates_of_objs)):
        list_of_tuples += [[min(i, j), max(i, j)] for j in n_obj_with_min_distance[:, i]]

    # remove duplicates from the list
    b_set = set(tuple(x) for x in list_of_tuples)  # convert the inner lists to tuples so they are hashable
    list_of_unique_tuples = [list(x) for x in b_set]

    # return the unique tuples of n closest neighbours
    return list_of_unique_tuples


# from two lists of points (coordinates_of_objs) in two images, get the n-nearest 
# neighbours in the second image of each object in the first image 
# and return a of lists, where each sub-list at position k contains all n objects closest
# to object k
# and j is one of the n objects closest i's position in the next image
# inputs: coordinates_im1: list of np.arrays with coordinates of objects in the first image
#         coordinates_im2: list of np.arrays with coordinates of objects in the second image
#         n: number of closest neighbors to consider
# output: list_of_tuples: list of lists where the sub-list at position k contains all n
#                         objects in image 2 closest to the position of object k from image 1
#                          e.g. for two objects l_k in im1, three objects r_j in im2 and n=2:
#                         [[2, 0], [0, 2]] means:
#                         l_0 is closest to r_2, its second nearest neighbour is r_0
#                         l_1 is closest to r_0, its second nearest neighbour is r_1
def get_n_nearest_neighbours_of_each_obj_in_next_image(coordinates_im1, coordinates_im2, n):
    # get the distance between the objects in image 1 and image 2
    distance = cdist(coordinates_im2, coordinates_im1)
    # get the n minimum distance objects
    n_obj_with_min_distance = distance.argsort(axis=0)[0:n, :]
    # create a list to store all tuples
    list_of_tuples = []
    # loop over all objects and create tuples with the n objects that
    # are closest to it
    for i in range(len(coordinates_im1)):
        list_of_tuples.append([j for j in n_obj_with_min_distance[:, i]])
    # return tuples
    return list_of_tuples


# create tuples ijk for all possible merging configurations: the nearest neighbour pairs to take
# into account from image 1 (nearest_neighbours_same_im) and the nearest neighbours to each 
# element from image 1 to image 2, such that only close elements can merge and only transform
# into an element that is close by.
# The result is given back as ijk tuples, where the transition is L_i + L_j --> R_k
# inputs: nearest_neighbours_to_next_im: list of lists where the sub-list at position k contains all n
#                                        objects in image 2 closest to the position of object k from image 1
#         nearest_neighbours_in_same_im: list_of_unique_tuples: pairs of close neighbours in image 1
#                                        as a list of lists
# outputs:ijk_tuples: list of lists (tuples [i,j,k]) where the transition is L_i + L_j --> R_k
#                         
def get_possible_split_ijk_for_merging(nearest_neighbours_in_same_im, nearest_neighbours_to_next_im):
    # rename the variables to be clarify notation
    nns_same_im = nearest_neighbours_in_same_im
    nns_next_im = nearest_neighbours_to_next_im
    # store ijk tuples in this list
    ijk_tuples = []
    # loop over all nearest neighbour pairs in the first image and find objects that are close
    # to this pair in the next image
    for nn_same_im in nns_same_im:
        # merge all of their nearest neighbours in the next image 
        all_nns_next_im = nns_next_im[nn_same_im[0]] + nns_next_im[nn_same_im[1]]
        # get rid of duplicates
        all_nns_next_im_unique = list(dict.fromkeys(all_nns_next_im))
        # create the ijk tuples
        for k in all_nns_next_im_unique:
            ijk_tuples.append([nn_same_im[0], nn_same_im[1], k])
    return ijk_tuples


# create tuples ijk for all possible splitting configurations: the nearest neighbour pairs to take
# into account from image 2 (nearest_neighbours_same_im) and the nearest neighbours to each 
# element from image 1 to image 2, such that one element can only transform into two elements that
# are close to its initial position and close to each other. 
# The result is given back as ijk tuples, where the transition is L_i --> R_j + R_k
# inputs: nearest_neighbours_to_next_im: list of lists where the sub-list at position k contains all n
#                                        objects in image 2 closest to the position of object k from image 1
#         nearest_neighbours_in_same_im: list_of_unique_tuples: pairs of close neighbours in image 2 
#                                        as a list of lists
# outputs: ijk_tuples: list of lists (tuples [i,j,k]) where the transition is L_i --> R_j + R_k
def get_possible_split_ijk_for_splitting(nearest_neighbours_in_same_im, nearest_neighbours_in_last_im):
    # rename the variables to be clarify notation
    nns_same_im = nearest_neighbours_in_same_im
    nns_last_im = nearest_neighbours_in_last_im
    # store ijk tuples in this list
    ijk_tuples = []
    # loop over all nearest neighbour pairs in the second image and find objects that are close
    # to this pair in the image before    
    for nn_same_im in nns_same_im:
        # merge all of their nearest neighbours from the last image
        all_nns_last_im = nns_last_im[nn_same_im[0]] + nns_last_im[nn_same_im[1]]
        # get rid of duplicates
        all_nns_last_im_unique = list(dict.fromkeys(all_nns_last_im))
        # create the ijk tuples
        for i in all_nns_last_im_unique:
            ijk_tuples.append([i, nn_same_im[0], nn_same_im[1]])
    return ijk_tuples


def get_possible_connections(points_im_1, points_im_2, nn_move, nn_split_merge):
    # get nn_split_merge nearest neighbours within the same image for which splitting and
    # merging will be considered
    same_im1_neighbours = get_n_nearest_neighbours_of_each_obj_in_same_image(points_im_1, nn_split_merge)
    same_im2_neighbours = get_n_nearest_neighbours_of_each_obj_in_same_image(points_im_2, nn_split_merge)

    # get nn_move nearest neighbours in next image (these are the possible LR transitions)
    next_im_neighbours = get_n_nearest_neighbours_of_each_obj_in_next_image(points_im_1, points_im_2, nn_move)
    last_im_neighbours = get_n_nearest_neighbours_of_each_obj_in_next_image(points_im_2, points_im_1, nn_move)

    # get splitting and merging triples
    ijk_splitting = get_possible_split_ijk_for_splitting(same_im2_neighbours, last_im_neighbours)
    ijk_merging = get_possible_split_ijk_for_merging(same_im1_neighbours, next_im_neighbours)

    return next_im_neighbours, ijk_splitting, ijk_merging
