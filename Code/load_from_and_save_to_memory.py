import pickle


# use pickle to save a custom class object to memory
# inputs: class_object: instance of a custom class
#         file_name: the file_name (including path to directory) where the file should be saved at
def save_class_object_to_memory(class_object, file_name):
    if not file_name.endswith('.pkl'):
        file_name += '.pkl'
    with open(file_name, 'wb') as output:
        pickle.dump(class_object, output, pickle.HIGHEST_PROTOCOL)
    return


# use pickle to load a custom class object from memory
# inputs: file_name: the file_name (including path to directory) where the file should be saved at
# outputs: class_object: instance of a custom class that was stored at 'file_name'
def load_class_object_from_memory(file_name):
    if not file_name.endswith('.pkl'):
        file_name += '.pkl'
    with open(file_name, 'rb') as pickle_input:
        class_object = pickle.load(pickle_input)
    return class_object


# read in the segmentation point objects from memory
# inputs: p: instance of the parameters class, containing all inputs from the .yml configuration file. Here the
#            'analysis_folder' and 'time_points' are used
#         folder: str, path to the point objects
# outputs: point_objects: a list containing instances of the custom DetectedObjectsList (point_object.py), containing
#                         the detected objects and their relevant parameters for a single time step
def load_point_objects_from_memory(p, folder=None):

    if folder is None:
        folder = p.analysis_folder + "manually_adjusted_segmentation_points/"

    # create an empty list to store the point_object for each time point
    points_objects = []
    for time_point in p.time_points:
        with open(folder + 'point_object_' + str(time_point) + '.pkl', 'rb') as input:
            point_object = pickle.load(input)
            points_objects.append(point_object)

    return points_objects
