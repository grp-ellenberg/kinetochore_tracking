from load_from_and_save_to_memory import load_class_object_from_memory, load_point_objects_from_memory
from os_functions import try_creating_folder
from find_better_tracks_using_alternatives_obj_oriented import find_better_tracks_using_alternatives
import pandas as pd
from find_outliers_from_pairs import create_excel_with_results, loop_over_tracks_to_find_outliers_using_pair_info
from load_from_and_save_to_memory import save_class_object_to_memory

import read_in_config_file

# define the configuration file
# config_file = ""
config_file = "C:/Users/janaw/Documents/GitHub/kinetochore-tracking/Code/config_judith_old_data_new_computer.yml"
try:
    if config_file == "":
        config_file = read_in_config_file.gui_file_name()
except NameError:
    config_file = read_in_config_file.gui_file_name()

p = read_in_config_file.read_in_config_file(config_file)

# load the tracking outcome
tracking_folder = p.analysis_folder + "Tracking_outcome/linear_programming_problem_interpretation_all_direct_i_lpp.pkl"
i_lpp = load_class_object_from_memory(tracking_folder)
if not hasattr(i_lpp, "points_removed"):
    i_lpp.points_removed = []

# try loading alternative outcomes
loaded_alt_i_lpps = []
for i in range(10):
    try:
        class_to_load = p.analysis_folder + "Tracking_outcome/linear_programming_problem_interpretation_alternative_" \
                        + str(i) + ".pkl"  # direct_i_lpp
        loaded_alt_i_lpps.append(load_class_object_from_memory(class_to_load))
    except FileNotFoundError:
        break

# load the segmentation
segmentation_folder = p.analysis_folder + "manually_adjusted_segmentation_points/final_segmentation/"
point_objects = load_point_objects_from_memory(p, folder=segmentation_folder)

# make sure all folders exist (in case the "create plots" cell wasn't run)
p.tracking_outcome_folder = p.analysis_folder + "Tracking_Outcome_Plots/"
try_creating_folder(p.tracking_outcome_folder)
p.tracking_distance_plot_folder = p.analysis_folder + "Tracking_distance_plots/"
try_creating_folder(p.tracking_distance_plot_folder)
p.tracking_pair_plot_folder = p.analysis_folder + "Tracking_pair_plots/"
try_creating_folder(p.tracking_pair_plot_folder)


# create a pandas dataframe with the sorted interpretation of the linear programming problem
pandas_df, i_lpp = create_excel_with_results(i_lpp, p, excel_name="tracking_outcome_excel.xlsx",
                                             weight_factor_pair_finding=2.0, batch_size=5)
# analyse the pair information, find outliers and the costs for the transitions
pandas_df, i_lpp = loop_over_tracks_to_find_outliers_using_pair_info(pandas_df, i_lpp, p)

for j, alt_i_lpp in enumerate(loaded_alt_i_lpps):
    # get the results dataframe for the alternative tracking approach
    alt_pandas_df, alt_i_lpp = create_excel_with_results(alt_i_lpp, p,
                                                         excel_name="tracking_outcome_excel_alt" + str(j) + ".xlsx",
                                                         weight_factor_pair_finding=2.0, batch_size=5)
    # analyse the pair information, find outliers and the costs for the transitions
    alt_pandas_df, loaded_alt_i_lpps[j] = loop_over_tracks_to_find_outliers_using_pair_info(alt_pandas_df, alt_i_lpp, p)
    # rename the columns in the dataframe to mark them as "alternative" number j
    alt_pandas_df.rename(columns=lambda x: x + "_alt" + str(j), inplace=True)
    # add the alternative columns to the pandas dataframe that contains the "standard" interpretation of the lpp
    pandas_df = pd.merge(pandas_df, alt_pandas_df,  how='outer', left_on=['time_index', 'object_index_is'],
                         right_on=['time_index_alt' + str(j), 'object_index_is_alt' + str(j)])
# save the pandas dataframe to file
pandas_df.to_pickle(p.analysis_folder + "Tracking_outcome/" + "combined_tracking_outcomes.pkl")

# also save the updated ilpps to file
try_creating_folder(p.analysis_folder + "Tracking_outcome")
save_class_object_to_memory(i_lpp, p.analysis_folder +
                            "Tracking_outcome/linear_programming_problem_before_correction.pkl")
for i, alt_i_lpp in enumerate(loaded_alt_i_lpps):
    save_class_object_to_memory(i_lpp, p.analysis_folder +
                                "Tracking_outcome/linear_programming_problem_before_correction_alt" + str(i) + ".pkl")

find_better_tracks_using_alternatives(pandas_df, i_lpp, loaded_alt_i_lpps, p)