import numpy as np
import skimage.morphology
import skimage.measure
import napari
import point_functions as pf
import label_image_functions as lif


# calculate the score image for the normalised cross-correlation
# inputs: image: np.array, the data containing objects that are similar to the 'atlas' that should be detected
#         atlas: np.array, of same dim as image, the 'model' of the object to be detected
#         potential points: np.array (size: number of points x dimension),
#                           points that possibly contain objects similar to the atlas
#         xyz_region: tuple/array, len=dim, size of the region around the potential points that should be examined
# output: score image, np.array, same size as image, containing the score from the normalised correlation of the
#                      image with the atlas,
#         calculated in the xzy-region around potential points, the scores are in the range [-1, 1]
def calculate_norm_cc_score_image(image, atlas, potential_points, xyz_region):
    # create a mask from the atlas
    atlas_mask = atlas != 0

    # normalise the atlas by dividing through the mean, then subtract 1
    # (1 = the mean of the normalised atlas)
    atlas_mean = np.mean(atlas)
    if atlas_mean != 0:
        norm_atlas = (atlas / atlas_mean) - 1
    else:
        norm_atlas = 0.001 * np.ones(shape=atlas.shape)
        print("changed the norm_atlas")

    # get the sum of the element-wise multiplication of the normalised atlas with itself
    norm_atlas_sq_sum = np.sum(norm_atlas * norm_atlas)

    # create a score_image set to the lowest possible score value: minus 1
    score_image = - np.ones(shape=image.shape, dtype=float)

    # create an area of size i = 2x+1, j = 2y+1, k = 2z+1, where (x,y,z) is given by xyz_region
    # around each potential point
    for i in range(- xyz_region[0], xyz_region[0] + 1):
        for j in range(- xyz_region[1], xyz_region[1] + 1):
            for k in range(- xyz_region[2], xyz_region[2] + 1):

                # get the score for area around each potential point
                for pp_index, potential_point in enumerate(potential_points):

                    potential_point_local = potential_point + [i, j, k]

                    # get an image part of the same size as the atlas at the potential centre point
                    image_part = lif.get_masked_part_at_point_from_big_image(image, atlas_mask,
                                                                             potential_point_local)

                    # normalise the image part by dividing through the mean, then subtract 1
                    # (1 = the mean of the normalised atlas)
                    image_part_mean = np.mean(image_part)
                    if image_part_mean != 0:
                        norm_image_part = (image_part / image_part_mean) - 1
                    else:
                        norm_image_part = 0.001 * np.ones(shape=image_part.shape)

                    # calculate the score as:
                    # sum(norm_img*mask)/sqrt(sum(norm_img*norm_img) * sum(mask*mask))

                    # get the sum of the element-wise multiplication of the normalised image part with itself
                    norm_image_part_sq_sum = np.sum(norm_image_part * norm_image_part)

                    # calculate the numerator
                    score_num = np.sum(norm_atlas * norm_image_part)

                    # calculate the denominator
                    score_denominator = np.sqrt(norm_atlas_sq_sum * norm_image_part_sq_sum)

                    # get the score
                    score = score_num / score_denominator

                    # set the score in the score_image to the correct value
                    score_image[
                        potential_point_local[0], potential_point_local[1], potential_point_local[2]] = score
    return score_image, image_part


# find maximum points in a score image, that are in a region of a size >= threshold_num_connected of points that
# all have a score > threshold_score
# inputs: score_image, np.array of scores that are a measure for the likelihood of finding an object there
#         threshold_score, float, the minimum value in the score image that qualifies the points to be considered
#                          a potential candidate to belong to a potential candidate region
#         threshold_num_connected, int, the minimum number of pixels that have to be greater than the threshold score to 
#                                  consider maximum value of the region as a 'hit'
#         display: bool, whether or not the score image and the potential points shall be displayed
# outputs: object_centres: np.array (size = (number_of_points_found, dim)), points in the threshold 
def get_potential_points_from_score_image(score_image, threshold_score, threshold_num_connected, display=False):
    # if one is looking for a connected component: create a label image and determine the centre of mass for all
    # labelled components greater than a certain size, get its centre of mass

    # check which of the image points fulfill both criteria (> threshold_score & in cluster with
    # at least threshold_num_connected objects)
    score_image_greater_thresh = np.copy(score_image)

    # get the score image of the parts that are greater than the threshold given
    # (input: threshold_score)
    mask_smaller_threshold = score_image < threshold_score

    score_image_greater_thresh[mask_smaller_threshold] = 0

    # get a label image from the score image
    label_score_image = skimage.measure.label(np.logical_not(mask_smaller_threshold))

    # remove objects smaller than threshold_num_connected from the label image
    # (in_place: modify label_score_image_all_objects and do not make a copy)
    label_score_image_small_items_removed = skimage.morphology.remove_small_objects(
        label_score_image,
        min_size=max(2, threshold_num_connected),
        in_place=True)

    # get the centroids of the regions
    region_props = skimage.measure.regionprops(label_score_image_small_items_removed, intensity_image=score_image)

    # create an empty list of centre points of objects
    object_centres = []
    areas = []

    # if the threshold of the number of connected pixels is 1, also include single pixels that were filtered out before
    if threshold_num_connected <= 1:
        label_score_image_small_items_only = label_score_image - label_score_image_small_items_removed
        object_centres.append(np.column_stack(np.where(label_score_image_small_items_only == 1)))

    # get the maximum value of each score image as the centre of the score image
    for region in region_props:
        # wanted: coordinates in data of the maximum intensity within the region:
        # 1) get the maximum intensity of the score image within the region
        # 2) get coordinates of this maximum intensity in the region (in the bbox)
        # 3) get the coordinates of the bbox
        # 4) add the bbox 'start' coordinates to the coordinates of the max within
        #    the bbox

        # step 1: get max intensity
        max_value = region.max_intensity

        # step 2: get the intensity image and the coordinates of the max
        intensity_image = region.intensity_image
        max_intensity_coord_bbox = np.argwhere(intensity_image == max_value)

        # step 3: get the bounding box coordinates
        x_min, y_min, z_min, x_max, y_max, z_max = region.bbox

        # step 4: add the coordinates of bbox and max within the bbox
        max_intensity_coord_img = max_intensity_coord_bbox + [x_min, y_min, z_min]

        # append these coordinates to the object_centres array
        object_centres.append(max_intensity_coord_img[0])

        # also save the area
        areas.append(region.area)

    # print the minimum and maximum area, this only works if the area is greater
    # than 1 pixel and there is at least 1 point.
    if areas:
        try:
            print(f"The minimum area is {np.min(areas)}")
            print(f"The maximum area is {np.max(areas)}")
        except ValueError:
            print("Found only 1 pixel areas.")
        print(f"The number of objects is {len(object_centres)}")

    else:
        print(f"There are no objects")
        return np.array([])

    object_centres = np.asarray(object_centres, dtype=int)

    # display the calculations, if the 'display' variable is true
    if display:
        # create a napari viewer window
        with napari.gui_qt():
            viewer = napari.Viewer()
            # add the score image ([-1, +1] floats do not work --> rescale)
            viewer.add_image((score_image + 1.) * 127.,
                             name=f'score image (rescaled)',
                             colormap='turbo')
            viewer.dims.ndisplay = 3
            # add the object centres of the points found
            viewer.add_points(object_centres,
                              size=1,
                              name='object centres using normalised cc',
                              face_color='gray')
    try:
        # output some information on the results
        print(f"The minimum score is {np.min(score_image)}")
        print(f"The maximum score is {np.max(score_image)}")
    except ValueError:
        print("No points found")

    # return the points and the score image 
    return object_centres


# get the centre of regions within in 'image' that resemble the 'atlas' from 'potential_points' using
# two thresholds: one for the minimum score in a the score image (threshold_score, in the range [-1, 1])
# and the minimum number of connected components (threshold_num_connected) that have a score greater than 
# the first threshold
def centres_from_norm_corr(image, atlas, potential_points, threshold_score, threshold_num_connected,
                           xyz_region=(1, 1, 1), display=False):
    # calculate the score image
    score_image, mask = calculate_norm_cc_score_image(image, atlas, potential_points, xyz_region)

    # calculate points fulfilling both criteria mentioned in the description
    object_centres = get_potential_points_from_score_image(score_image, threshold_score,
                                                           threshold_num_connected, display=display)

    # return the points and the score image 
    return object_centres, score_image, mask


# add additional points to a list of points that were already found after thresholding them
# inputs: additional_points: list of additional points (np.arrays with 3 entries),
def separate_and_threshold_additional_points(additional_points, intensity_image, thresh_intensity,
                                             list_of_all_additional_points, list_of_additional_points_above_thresh,
                                             all_points_already_found, thresholded_points_already_found):
    empty_points = np.array([[None, None, None], ])

    if len(additional_points) != 0:
        thresholded_additional_points = pf.get_points_with_intensity_value_above_threshold(additional_points,
                                                                                           intensity_image,
                                                                                           thresh_intensity)
        all_additional_points_only = pf.multidim_set_diff(np.array(all_points_already_found),
                                                          np.array(additional_points))
        additional_points_only_thresholded = pf.multidim_set_diff(np.array(thresholded_points_already_found),
                                                                  np.array(thresholded_additional_points))

        print("found " + str(len(additional_points_only_thresholded)) + " more points")
    else:
        all_additional_points_only = empty_points
        additional_points_only_thresholded = empty_points
        print("found no additional points")

    list_of_all_additional_points.append(all_additional_points_only)
    list_of_additional_points_above_thresh.append(additional_points_only_thresholded)

    updated_list_of_thresholded_points = np.concatenate((thresholded_points_already_found,
                                                         additional_points_only_thresholded))
    updated_list_of_all_points = np.concatenate((all_points_already_found, all_additional_points_only))

    return (list_of_all_additional_points, list_of_additional_points_above_thresh, updated_list_of_all_points,
            updated_list_of_thresholded_points)


def get_additional_points_from_score_image(score_image, intensity_image,
                                           thresh_score, thresh_intensity, thresh_num_connected,
                                           list_of_all_additional_points, list_of_additional_points_above_thresh,
                                           all_points_already_found, thresholded_points_already_found, display=None):
    additional_points = get_potential_points_from_score_image(score_image,
                                                              threshold_score=thresh_score,
                                                              threshold_num_connected=thresh_num_connected,
                                                              display=display)

    output = separate_and_threshold_additional_points(additional_points, intensity_image, thresh_intensity,
                                                      list_of_all_additional_points,
                                                      list_of_additional_points_above_thresh,
                                                      all_points_already_found, thresholded_points_already_found)

    return output


def separate_additional_points(new_points, points_found_prior):
    if len(new_points) != 0 and len(points_found_prior) != 0:
        all_additional_points_only = pf.multidim_set_diff(np.array(points_found_prior, order='C', dtype=np.int32),
                                                          np.array(new_points, order='C', dtype=np.int32))
    elif len(new_points) == 0:
        all_additional_points_only = []
    else:
        all_additional_points_only = new_points

    return all_additional_points_only


def get_additional_points_from_score_image_detected_obj_list(score_image, detected_obj, thresh_score,
                                                             thresh_num_connected):
    potential_additional_points = get_potential_points_from_score_image(score_image, threshold_score=thresh_score,
                                                                        threshold_num_connected=thresh_num_connected)

    additional_points = separate_additional_points(potential_additional_points, detected_obj.coordinates)

    print("found " + str(len(additional_points)) + " more potential points")

    return additional_points
