from load_from_and_save_to_memory import save_class_object_to_memory
import point_functions as pf
import list_functions as lf
from os_functions import try_creating_folder
from local_max_3D import find_local_max_3d
import correlation_functions as cf

import warnings
import yaml
import sys
import numpy as np

# import napari for 3D data visualisation and qtpy to custom add sliders etc.
# to napari windows
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=UserWarning)
    import napari
from qtpy.QtCore import Qt
from qtpy.QtWidgets import QSlider, QLabel, QVBoxLayout, QGroupBox, QPushButton
from qtpy.QtGui import QColor, QFont
import pyqtgraph as pg

from scipy.spatial.distance import cdist
import filters

# create a global variable for the point_objects_to_return and the new_thresholds_dict
point_objects_to_return, new_thresholds_dict = None, None


# reshape a list of points such that it can be interpreted as a n-dimensional points-layer
# by the napari viewer
# input: points_list: list of np.array that contains points
# output: points_nd: np.array that can be interpreted as n-dimensional points layer by napari
def get_nd_points_layer_napari(pts_list):
    points_nd = np.concatenate(
        [np.concatenate((np.full((len(pts), 1), i), pts), axis=1) for i, pts in enumerate(pts_list)],
        axis=0,
    )
    return points_nd


# initiate a pyqt slider --> set min and max values, the step size, create a label for it
# and set it to its initial value.
# these sliders only work with integer values, so scaling the input/output must be used to
# represent/mimic float values
# idea from: https://forum.image.sc/t/custom-sliders-to-highlight-pixel-subsets/32486 16.05.20
# inputs: min_value, max_value: int, the minimum and maximum possible values
#         step_size: int, the sensitivity of the slider/ the interval which values it can take
#         name: string, the label that will be used for the slider, additionally a
#               'current value' will be displayed
#         init_value: int, the initial value of this slider
# outputs: label_obj: QLabel, the label corresponding to this slider
#          slider_obj: QSlider, the slider with all the inputs set
def initiate_pyqt_sliders_to_napari_viewer(min_value, max_value, step_size, name, init_value):
    # create a pyqt QLabel object, containing the name of the slider and stating,
    # that this is the initial value
    label_obj = QLabel(name + " Current value: (initial) " + str(int(init_value)))
    label_font = QFont("SansSerif", 13)
    label_obj.setFont(label_font)

    # create a horizontal pyqt QSlider object
    slider_obj = QSlider(Qt.Horizontal)
    # set the min, max and step size
    slider_obj.setMinimum(min_value)
    slider_obj.setMaximum(max_value)
    slider_obj.setSingleStep(step_size)
    # initialise it
    slider_obj.setValue(init_value)
    # only change value, if mouse is released
    slider_obj.setTracking(False)
    # return the label and the slider object
    return label_obj, slider_obj


# function that adds several pyqt sliders to a napari viewer, pyqt slider objects always are
# integer sliders, so for float objects the output has to be scaled in the function that is called
# and for the input of this function.
# For each change of a slider the 'call_back_fct' is called, all slider values are passed to this
# function.
# Also add a plot widget, if the 'add_plot_widget function is True', it can be modified in the
# call_back_fct (last argument, if this bool is True)
# inputs: viewer: napari.Viewer object to which the pyqt sliders should be added
#         min_values: int, list of minimum values for the sliders (minimum value that can be set
#                     by sliding)
#         max_values: int, list of maximum values for the sliders (maximum value that can be set
#                     by sliding)
#         step_size_values: int, interval in which the slider value can be increased/decreased
#         call_back_fct: list function that should be called when the corresponding slider is moved,
#                        in addition the call_back_fcts_args will be passed to this function
#                        its arguments are: viewer, sliders, names_list, labels,
#                        call_back_fcts_args_list, init_value_list, plot_widget
#         call-back_fct_args_list: the arguments that should be passed to the call-back function
#         names_list: list of names (Str) of the sliders that will be displayed on top of the sliders.
#                     (Additionally 'current value + "value"') will be displayed
#         init_value_list: list of initial values (int) with which the sliders will be initialised
#         add_plot_widget: bool, whether or not a box for potential plots by the call_back_fct should
#                          be added
#         add_save_button: bool, whether or not a save button for the new points should be added
#         analysis_folder: str, path where the new points will be saved
# outputs: sliders: list of pyqt slider objects, the respective slider's value can be accessed with
#                   "sliders[i].value()"
def add_intensity_pyqt_sliders_to_napari_viewer(viewer_1, min_values_1, max_values_1, step_size_values,
                                                call_back_fct, call_back_fct_args_list,
                                                names_list_1, init_value_list_1, add_plot_widget=True,
                                                add_save_button=True, analysis_folder=""):
    # create a pyqt-box for placing all objects
    box = QVBoxLayout()
    # create a list of sliders and their respective labels
    labels, sliders_1 = [], []
    # loop over all inputs to create the sliders
    for i in range(len(min_values_1)):
        # create the labels and the slider objects and initialise them, they are not
        # coupled to their call-back functions, yet
        label_obj, slider_obj = initiate_pyqt_sliders_to_napari_viewer(min_values_1[i], max_values_1[i],
                                                                       step_size_values[i],
                                                                       names_list_1[i], init_value_list_1[i])
        # add the labels and the sliders to their respective lists, to be able to pass them
        # to the call-back functions
        labels.append(label_obj)
        sliders_1.append(slider_obj)
        # place the labels and sliders in the pyqt box
        box.addWidget(label_obj)
        box.addWidget(slider_obj)

    if add_save_button:
        # add a save button to save all points that are currently displayed for all time frames
        save_button = QPushButton("Save Points (all frames)")
        save_button.setStyleSheet("background-color: darkgreen")
        save_button_font = QFont("SansSerif", 13)
        save_button.setFont(save_button_font)
        save_button.setCheckable(True)
        # noinspection PyUnresolvedReferences
        save_button.clicked.connect(lambda: save_button_clicked(call_back_fct_args_list, analysis_folder, sliders_1))
        box.addWidget(save_button)
        save_button_single_frame = QPushButton("Save Points (this frame)")
        save_button_single_frame.setStyleSheet("background-color: green")
        save_button_single_frame.setFont(save_button_font)
        save_button_single_frame.setCheckable(True)
        # noinspection PyUnresolvedReferences
        save_button_single_frame.clicked.connect(lambda: save_button_clicked_single_frame(call_back_fct_args_list,
                                                                                          analysis_folder,
                                                                                          sliders_1, viewer_1))
        box.addWidget(save_button_single_frame)

    # additionally, add a plot-widget in which things can be displayed, if desired
    if add_plot_widget:
        plot_widget = pg.PlotWidget(title='Points found over frame')
        viewer_1.window.add_dock_widget(plot_widget, area='right')
    else:
        plot_widget = None

    # connect the callback function to the sliders
    for i in range(len(sliders_1)):
        # the arguments are defined and then given to the slider function:
        # - the napari viewer, so layers can be changed interactively
        # - ALL sliders, so the functions may depend on several of their values
        # - the name of the current slider and the corresponding pyqt label object, such that the
        #   current value can be displayed.
        # - the respective (additional) call-back function arguments
        # - if desired (add_plot_widget=True) the plot widget, such that changes can be displayed
        if add_plot_widget:
            args = (viewer_1, sliders_1, names_list_1, labels, call_back_fct_args_list, init_value_list_1, plot_widget)
        else:
            args = viewer_1, sliders_1, names_list_1, labels, call_back_fct_args_list, init_value_list_1
            # connect the slider to its callback function and pass the arguments
        # noinspection PyUnresolvedReferences
        sliders_1[i].valueChanged.connect(lambda: call_back_fct(*args))

    # add some space to all sides of the box
    box.addStretch(1)
    # use a group box to add the objects collected in the QVBoxLayout object (no title)
    group_box = QGroupBox('')
    # set the layout
    group_box.setLayout(box)
    # add the widget to the napari viewer, on the right side
    viewer_1.window.add_dock_widget(group_box, area='right')

    return sliders_1, plot_widget


# if the button is clicked: save the new points for the currently displayed time step/frame to file
def save_button_clicked_single_frame(callback_fct_args, analysis_folder, sliders, viewer):
    global point_objects_to_return, new_thresholds_dict
    _, time_points, point_objects = callback_fct_args

    # get the current frame displayed in the viewer
    current_frame = viewer.layers[0].dims.indices[0]

    # get the corresponding time point
    time_point = min(time_points) + current_frame

    # if the folder containing the manually adjusted points does not exist yet, try creating it
    manually_adjusted_points_save_folder = analysis_folder + "manually_adjusted_segmentation_points/"
    try_creating_folder(manually_adjusted_points_save_folder)

    # save the points in the current frame to folder
    save_class_object_to_memory(point_objects[current_frame], manually_adjusted_points_save_folder + 'point_object_' +
                                str(time_point) + '.pkl')
    np.save(manually_adjusted_points_save_folder + "new_automatic_points_in_frame_" + str(time_point),
            point_objects[current_frame].active_points)
    np.save(manually_adjusted_points_save_folder + "indigo_points_in_frame_" + str(time_point),
            point_objects[current_frame].all_local_max_points)
    print("Saved points in the analysis folder under in the manually_adjusted_segmentation_points folder with the name"
          ": new_automatic_points_in_frame_ + " + str(time_point))
    point_objects_to_return[time_point] = point_objects[time_point]

    # create a thresholds dictionary with the new thresholds values and save it to the analysis folder
    new_thresholds_dict = {"double_thresholding_init": sliders[0].value(),
                           "max_points_init": sliders[1].value(),
                           "slope_bleaching": float(sliders[2].value() / 1000),
                           "watershed_intensity_init": sliders[3].value(),
                           "watershed_obj_size": sliders[4].value(),
                           "min_dist_between_double_thresh_and_local_max_point": sliders[5].value(),
                           "thresh_multiplied_image_local_max": sliders[6].value(),
                           "time_correlation_factor": sliders[7].value(),
                           "intensity_VS_size_weight": sliders[8].value()
                           }

    # save the new thresholds dictionary to the analysis folder
    with open(manually_adjusted_points_save_folder + 'manually_adjusted_segmentation_thresholds_for_frame_' +
              str(time_point) + '.yml', 'w') as outfile:
        yaml.dump(new_thresholds_dict, outfile, default_flow_style=False)

    return


# if the button is clicked: save the new points to file
def save_button_clicked(callback_fct_args, analysis_folder, sliders):
    global point_objects_to_return, new_thresholds_dict
    _, time_points, point_objects = callback_fct_args

    manually_adjusted_points_save_folder = analysis_folder + "manually_adjusted_segmentation_points/"
    try_creating_folder(manually_adjusted_points_save_folder)

    for i, time_point in enumerate(time_points):
        save_class_object_to_memory(point_objects[i], manually_adjusted_points_save_folder + 'point_object_' +
                                    str(time_point) + '.pkl')
        np.save(manually_adjusted_points_save_folder + "new_automatic_points_in_frame_" + str(time_point),
                point_objects[i].active_points)
        np.save(manually_adjusted_points_save_folder + "indigo_points_in_frame_" + str(time_point),
                point_objects[i].all_local_max_points)
    print("Saved points in the analysis folder under in the manually_adjusted_segmentation_points folder with the name"
          ": new_automatic_points_in_frame_ + number")
    point_objects_to_return = point_objects

    # create a thresholds dictionary with the new thresholds values and save it to the analysis folder
    new_thresholds_dict = {"double_thresholding_init": sliders[0].value(),
                           "max_points_init": sliders[1].value(),
                           "slope_bleaching": float(sliders[2].value() / 1000),
                           "watershed_intensity_init": sliders[3].value(),
                           "watershed_obj_size": sliders[4].value(),
                           "min_dist_between_double_thresh_and_local_max_point": sliders[5].value(),
                           "thresh_multiplied_image_local_max": sliders[6].value(),
                           "time_correlation_factor": sliders[7].value(),
                           "intensity_VS_size_weight": sliders[8].value()
                           }

    # save the new thresholds dictionary to the analysis folder
    with open(manually_adjusted_points_save_folder + 'manually_adjusted_segmentation_thresholds.yml', 'w') as outfile:
        yaml.dump(new_thresholds_dict, outfile, default_flow_style=False)

    return


# callback function that thresholds points based on their initial value and the slope of bleaching
# the new threshold is calculated as:
# thresh_value - slope_bleaching*(time_steps[i]-min(time_steps))
# inputs: thresh_value: the initial threshold value
#         all_points: list (time) of list (number 1,2,3,...) of points (2d or 3d pixel coordinates,
#                     usually in np.array form) before thresholding is applied
#         images: the score or intensity image in which the value should be greater than the
#                 threshold at a given point
#         time_points: the frame number/ time stamp of the respective points
#         slope_bleaching: the value by which the threshold decreased in two subsequent time steps
# outputs: new_thresholded_points
def update_thresholding_points(thresh_value, all_points, images, time_points, slope_bleaching):
    # create a list to store the thresholded points for each time step
    new_thresholded_points = []
    # every time step/frame: first calculate the threshold corresponding to that time step/frame,
    # then get all points above these threshold
    for i in range(len(images)):
        intensity_threshold = int(thresh_value - (time_points[i] - np.min(time_points)) * slope_bleaching)
        points_thresh_one_layer = pf.get_points_with_intensity_value_above_threshold(all_points[i],
                                                                                     images[i],
                                                                                     intensity_threshold)
        # save the thresholded points to the list
        new_thresholded_points.append(points_thresh_one_layer)
    # return the thresholded points for all time-steps/frames
    return new_thresholded_points


# remove layers from a viewer given a tuple of their names
# inputs: viewer: napari.Viewer instance in which the layers should be removed
#         layer_tuple: tuple of the names (str) of layers that should be removed, or the start
#                      of the names
def pop_layers(viewer_1, layer_tuple):
    # store the indices of the layers to be removed in a list
    pop_indices = []
    # for all layers, check if the name starts with one of the names given in the
    # layer tuples, if yes, save the index to the list
    for index, layer in enumerate(viewer_1.layers):
        if layer.name.startswith(layer_tuple):  # double parentheses needed!
            pop_indices.append(index)
    # reverse the order of indices to remove the layers (the indices are updated, so the
    # reversing is needed to not randomly pop other layers)
    for index in reversed(pop_indices):
        viewer_1.layers.pop(index)
    return viewer_1


# update all points that are thresholded twice before intensity thresholding (score value, # points above score value).
# These points usually are called 'low_value_points', 'mid_value_points', 'high_value_points' according to their
# score in the score image.
# Updating means removing the old layers containing these points and adding new layers with the current thresholds
# applied
# inputs: viewer: napari.Viewer instance containing the layers,
#         value: int, initial intensity threshold for intensity thresholding
#         slope_bleaching: float, the bleaching expected to occur between two subsequent frames
#         name: str, the name that should be displayed above the slider
#         label_obj: pyqt label obj, the slider's label object in which the name is displayed, can be set to None,
#                    if there is no need to change it
#         arguments: a variable containing all low, mid and high value points, all local max points, all intensity
#                    images, and all time_points of the data
# output: the new low, mid and high value points (list (frames), of list (# points) of nD np.arrays)
def update_threshold_double_thresholding_points(viewer_1, low_val_thresh, mid_and_high_val_thresh,
                                                slope_bleaching, name, label_obj, arguments):
    # unpack the arguments variable
    images, time_points, point_objects = arguments

    # if a non-None label_obj is passed to the function, its text is updated
    if label_obj is not None:
        label_obj.setText(name + " Current value: " + str(low_val_thresh))

    for i, time_point in enumerate(time_points):
        current_low_val_thresh = low_val_thresh - (time_point - np.min(time_points)) * slope_bleaching
        current_mid_hig_val_thresh = mid_and_high_val_thresh - (time_point - np.min(time_points)) * slope_bleaching
        point_objects[i].update_low_value_points(current_low_val_thresh)
        point_objects[i].update_mid_and_high_value_points(current_mid_hig_val_thresh)

    if viewer_1 is not None:
        # calculate the new low value points
        new_low_value_points = [po.active_low_value_points for po in point_objects]

        # update the points layer in the napari viewer, if it contains any points
        update_points_layer_with_new_points(viewer_1, "thresh_low_value_points", new_low_value_points)

        # calculate the new mid value points and add them to the viewer
        new_mid_value_points = [po.active_mid_value_points for po in point_objects]

        # update the points layer in the napari viewer, if it contains any points
        update_points_layer_with_new_points(viewer_1, "thresh_middle_value_points", new_mid_value_points)

        # calculate the new high value points
        new_hig_value_points = [po.active_hig_value_points for po in point_objects]

        # update the points layer in the napari viewer, if it contains any points
        update_points_layer_with_new_points(viewer_1, "thresh_high_value_points", new_hig_value_points)

    # return the new thresholded points
    return point_objects


# update the thresholded points of local maxima.Updating means removing the old layers containing these points and
# adding a new layer with the current thresholds applied.
# inputs: viewer: napari.Viewer instance containing the layers,
#         value: int, initial intensity threshold for intensity thresholding
#         slope_bleaching: the bleaching expected to occur between two subsequent frames
#         name: str, the name that should be displayed above the slider
#         label_obj: the slider's label object in which the name is displayed, can be set to None, if there is no need
#                    to change it
#         arguments: a variable containing all low, mid and high value points, all local max points,
#                    all intensity images, and all time_points of the data
# output: the new local thresholded points of local maxima (list (frames), of list (# points) of nD np.arrays)
def update_threshold_local_max_points(viewer_1, value, slope_bleaching, name, label_obj, arguments):
    # unpack the arguments variable
    images, time_points, point_objects = arguments

    for i, time_point in enumerate(time_points):
        point_objects[i].update_local_max_thresh(value - (time_point - np.min(time_points)) * slope_bleaching)

    # if a non-None label-obj is passed, change it, so the current value is displayed
    if label_obj is not None:
        label_obj.setText(name + " Current value: " + str(value))

    # calculate the new points, and if there are any, add a layer displaying these points
    new_local_max_points = [po.active_local_max_points for po in point_objects]

    if viewer_1 is not None:
        # update the points layer in the napari viewer, if it contains any points
        update_points_layer_with_new_points(viewer_1, "thresh_local_max_points", new_local_max_points)

    # return the points of local maxima
    return point_objects


# update the thresholded points of local maxima.Updating means removing the old layers containing these points and
# adding a new layer with the current thresholds applied.
# inputs: viewer: napari.Viewer instance containing the layers,
#         value: int, initial intensity threshold for intensity thresholding
#         slope_bleaching: the bleaching expected to occur between two subsequent frames
#         name: str, the name that should be displayed above the slider
#         label_obj: the slider's label object in which the name is displayed, can be set to None, if there is no need
#                    to change it
#         arguments: a variable containing all low, mid and high value points, all local max points,
#                    all intensity images, and all time_points of the data
# output: the new local thresholded points of local maxima (list (frames), of list (# points) of nD np.arrays)
def update_threshold_local_max_intensity(viewer_1, value, slope_bleaching, name, label_obj, arguments):
    # unpack the arguments variable
    images, time_points, point_objects = arguments

    for i, time_point in enumerate(time_points):
        point_objects[i].update_local_max_intensity_thresh(value - (time_point - np.min(time_points)) * slope_bleaching)

    # if a non-None label-obj is passed, change it, so the current value is displayed
    if label_obj is not None:
        label_obj.setText(name + " Current value: " + str(value))

    # calculate the new points, and if there are any, add a layer displaying these points
    new_local_max_points = [po.active_local_max_points_intensity for po in point_objects]

    if viewer_1 is not None:
        # update the points layer in the napari viewer, if it contains any points
        update_points_layer_with_new_points(viewer_1, "thresh_local_max_intensity", new_local_max_points)

    # return the points of local maxima
    return point_objects


def update_threshold_local_max_prod_score(viewer_1, thresh_value_prod_max, slope_bleaching,
                                          sigma_gauss_time_correlation,
                                          time_correlation, intensity_more,
                                          arguments, use_time_correlation=False):
    viewer_1.status = "updating threshold local max in multiplied img "

    # unpack the arguments variable
    images, time_points, point_objects = arguments

    if use_time_correlation:

        # the time correlation should be given in percent, but the slider can only take integer values, so divide by 100
        # to get %
        time_correlation /= 100
        # divide the sigma for the gaussian by 10
        sigma_gauss_time_correlation /= 10

        # get the product score image, in which intensity is weighted more by the power "intensity_more"/100
        # normalise the prod_score image, by division by the maximum in each image to get the 'prod_score_images'
        prod_score_images = [po.microscopy_data ** (intensity_more / 100) * po.score_image for po in point_objects]
        prod_score_images = [score_image / np.max(score_image) for score_image in prod_score_images]

        # get the time correlated images, which are a sum of the image at time t and the Gaussian-blurred versions of the
        # images at t-1 and t+1 (with sigma=1 for the blur).
        if len(point_objects) > 1:
            t_corr_score_ims = []
            for i in range(len(prod_score_images)):
                if i == 0:
                    t_corr_score_ims.append((1 - time_correlation) * prod_score_images[i] +
                                            time_correlation * filters.gaussian_blur_xyz(prod_score_images[i + 1],
                                                                                         sigma_gauss_time_correlation))
                elif i == len(prod_score_images) - 1:
                    t_corr_score_ims.append((1 - time_correlation) * prod_score_images[i] +
                                            time_correlation * filters.gaussian_blur_xyz(prod_score_images[i - 1],
                                                                                         sigma_gauss_time_correlation))
                else:
                    t_corr_score_ims.append((1 - 2 * time_correlation) * prod_score_images[i] +
                                            time_correlation * filters.gaussian_blur_xyz(prod_score_images[i + 1],
                                                                                         sigma_gauss_time_correlation) +
                                            time_correlation * filters.gaussian_blur_xyz(prod_score_images[i - 1],
                                                                                         sigma_gauss_time_correlation))
        else:
            t_corr_score_images = prod_score_images

        for i, t_corr_score_image in enumerate(t_corr_score_ims):
            # remove the old local_max_prod_score_points from the point_objects
            po = point_objects[i]

            where_is_not_local_max_prod_score = np.logical_not(po.is_local_max_point_prod_score)
            number_of_local_max_prod_score = np.sum(po.is_local_max_point_prod_score)

            # remove the length of these points and their values
            po.length -= number_of_local_max_prod_score
            po.active_local_max_points_prod_score = np.zeros(shape=(0, 3), dtype=float)
            po.all_local_max_points_prod_score = np.zeros(shape=(0, 3), dtype=float)
            # delete the old coordinates and their properties
            po.coordinates = po.coordinates[where_is_not_local_max_prod_score]
            po.is_chosen = po.is_chosen[where_is_not_local_max_prod_score]
            po.is_split = po.is_split[where_is_not_local_max_prod_score]
            po.centre_intensities = po.centre_intensities[where_is_not_local_max_prod_score]
            po.mean_intensities = po.mean_intensities[where_is_not_local_max_prod_score]
            po.resolution_corrected_coordinates = po.resolution_corrected_coordinates[where_is_not_local_max_prod_score]
            po.is_low_value_point = po.is_low_value_point[where_is_not_local_max_prod_score]
            po.is_mid_value_point = po.is_mid_value_point[where_is_not_local_max_prod_score]
            po.is_hig_value_point = po.is_hig_value_point[where_is_not_local_max_prod_score]
            po.is_local_max_point = po.is_local_max_point[where_is_not_local_max_prod_score]
            po.is_local_max_point_prod_score = po.is_local_max_point_prod_score[where_is_not_local_max_prod_score]
            po.is_local_max_point_intensity = po.is_local_max_point_intensity[where_is_not_local_max_prod_score]

            # find new local max points in the time-correlated score image
            local_max_points_prod_score = find_local_max_3d(t_corr_score_image, po.seg_mask,
                                                            number_of_max=120)

            local_max_prod_score_dist_to_other_points_greater_two = np.where(np.min(cdist(local_max_points_prod_score,
                                                                                          po.coordinates),
                                                                                    axis=1) > 2)
            local_max_points_prod_score = local_max_points_prod_score[
                local_max_prod_score_dist_to_other_points_greater_two]
            local_max_points_prod_score = cf.separate_additional_points(local_max_points_prod_score,
                                                                        po.coordinates)

            local_max_point_prod_score_dists = np.min(cdist(local_max_points_prod_score,
                                                            po.coordinates), axis=1)

            po.add_points(local_max_points_prod_score, "local_max_point_prod_score",
                          po.microscopy_data)

            po.local_max_point_prod_score_dists = local_max_point_prod_score_dists

            if po.length > 0:
                po.get_split_objects_for_all_objects(po.microscopy_data)
                po.get_watershed_sizes_of_objects_at_point_from_image(po.microscopy_data)
                po.get_all_points_for_first_time()

            po.update_local_max_prod_score_thresh(thresh_value_prod_max -
                                                  (time_points[i] - np.min(time_points)) * slope_bleaching)

        if viewer_1 is not None:
            # calculate the new points, and if there are any, add a layer displaying these points
            thresh_new_local_max_points = [po.active_local_max_points_prod_score for po in point_objects]
            # update the points layer in the napari viewer, if it contains any points
            update_points_layer_with_new_points(viewer_1, "thresh_local_max_prod_score", thresh_new_local_max_points)

            # remove the old 'all_local_max_prod_score' layer and add a new one
            pop_layers(viewer_1, ("all_local_max_prod_score",))

            local_max_prod_score_no = [str(i) for po in point_objects
                                       for i in range(len(po.all_local_max_points_prod_score))]

            local_max_points_prod_score_text = {
                'text': '{number}',
                'size': 4,
                'color': 'indigo',
                'translation': np.array([-2, 0, 0]),
                'visible': False
            }

            local_max_points_prod_score_properties = {'number': local_max_prod_score_no}

            viewer_1.add_points(
                get_nd_points_layer_napari([po.all_local_max_points_prod_score for po in point_objects]),
                size=1,
                properties=local_max_points_prod_score_properties,
                name='all_local_max_prod_score',
                face_color='indigo',
                visible=False,
                text=local_max_points_prod_score_text
            )
    else:
        for i, po in enumerate(point_objects):
            po.update_local_max_prod_score_thresh(thresh_value_prod_max -
                                                  (time_points[i] - np.min(time_points)) * slope_bleaching)
        # calculate the new points, and if there are any, add a layer displaying these points
        thresh_new_local_max_points = [po.active_local_max_points_prod_score for po in point_objects]
        # update the points layer in the napari viewer, if it contains any points
        update_points_layer_with_new_points(viewer_1, "thresh_local_max_prod_score", thresh_new_local_max_points)

    # return the points of local maxima
    return point_objects


# update a points layer in the napari Viewer
# inputs: viewer_1: napari.Viewer instance containing the layers,
#         name_of_points_layer: str with the name of the layer in the Napari viewer that should be updated
#         new_merged_points: list containing the new points (list (frames), of list (# points) of nD np.arrays)
#                            it will be made compatible with napari using the "get_nd_points_layer_napari" command
#                            and added to the viewer_1 as its data
def update_points_layer_with_new_points(viewer_1, name_of_points_layer, new_points):
    # if there are any points in the list of new merged points, add a layer with these
    for index, layer in enumerate(viewer_1.layers):
        if layer.name.startswith(name_of_points_layer):
            if lf.has_items(new_points):
                new_points_nd = get_nd_points_layer_napari(new_points)
                layer.data = new_points_nd
            elif lf.has_items(new_points):
                layer.data = get_nd_points_layer_napari(new_points)
            else:
                layer.data = None
    return


# calculate the new split points from the watershed thresholds, the intensity images and the points to be split
# inputs: images: the score or intensity image in which the value should be greater than the
#                 threshold at a given point
#         time_points: the frame number/ time stamp of the respective points
#         initial_threshold_value_ws: initial (non-bleached) intensity value up to which a watershed, seeded at each
#                                     detected kinetochore centre should be performed for size detection for splitting
#         watershed_obj_size: size above which detected kinetochore objects filled by the watershed are considered
#                             to big and to consist of 2 kinetochores, so they are split
#         points_to_be_split: seed points for the watershed --> detected points
#         slope_bleaching: the value by which the threshold decreased in two subsequent time steps
# output: list of all points, including the split points (list (frames), of list (# points) of nD np.arrays)
def calculate_new_split_points(viewer_1, images, time_points, initial_threshold_value_ws, watershed_obj_size,
                               slope_bleaching, point_objects):
    if viewer_1 is not None:
        viewer_1.status = 'calculating new split points ...'

    # create a list to store the split points for each frame
    new_split_points_list = []

    # create a variable indicating whether any of the watershed intensities was updated
    any_updated = False

    # loop over all frames, get the bleaching-corrected threshold for that point and calculate the split objects
    for i, frame_no in enumerate(time_points):
        # perform a watershed on the intensity image to get 'big' objects
        threshold_for_watershed_splitting = \
            int(initial_threshold_value_ws - (time_points[i] - np.min(time_points)) * slope_bleaching)
        if threshold_for_watershed_splitting != point_objects[i].threshold_dict["watershed_intensity"]:
            point_objects[i].update_ws_intensity(images[i], threshold_for_watershed_splitting)
            any_updated = True
            point_objects[i].threshold_dict["watershed_intensity"] = threshold_for_watershed_splitting

        if watershed_obj_size != point_objects[i].threshold_dict["watershed_obj_size"]:
            point_objects[i].update_ws_size(watershed_obj_size)
        point_objects[i].update_active_points()
        new_split_points_list.append(point_objects[i].active_points)

    if viewer_1 is not None:
        # add the new split points to the viewer
        update_points_layer_with_new_points(viewer_1, "unified_points_split", new_split_points_list)

        # if any of the watershed intensities was updated, update the layer with the watershed-intensity-label
        if any_updated and any(layer.name.startswith('watershed_intensity') for layer in viewer_1.layers):

            # create a np.array with boolean values indicating where the image intensity is higher than the watershed
            # intensity
            watershed_intensity_booleans = np.zeros(shape=images.shape, dtype=bool)

            viewer_1.status = "updating watershed label layer"
            # loop over all frames, get the bleaching-corrected threshold for that point and calculate the split objects
            for i, frame_no in enumerate(time_points):
                # perform a watershed on the intensity image to get 'big' objects
                threshold_for_watershed_splitting = \
                    int(initial_threshold_value_ws - (time_points[i] - np.min(time_points)) * slope_bleaching)
                watershed_intensity_booleans[i] = images[i] > threshold_for_watershed_splitting

            # if there are any points in the list of new merged points, add a layer with these
            for index, layer in enumerate(viewer_1.layers):
                if layer.name.startswith('watershed_intensity'):
                    layer.data = watershed_intensity_booleans

    return point_objects


# merge points for several time-steps
# inputs: points_list: list of different points lists, a points list is supposed to itself contain a list of
#                      points (nD np.arrays) for each time frame [object in points list is (list (frames),
#                      of list (# points) of nD np.arrays)]
# outputs: merged_points_list: (list (frames), of list (# points) of nD np.arrays) of merged points,
#                              if a point is contained in two input lists in the SAME frame, it will only
#                              be in this list once (this is a union)
def add_merged_points_to_viewer(viewer_1, point_objects):
    merged_points_list = []

    for i, po in enumerate(point_objects):
        merged_points_list.append(np.concatenate((po.active_mid_value_points, po.active_low_value_points,
                                                  po.active_hig_value_points, po.active_local_max_points,
                                                  po.active_local_max_points_prod_score,
                                                  po.active_local_max_points_intensity)))

    update_points_layer_with_new_points(viewer_1, "unified_points_before_splitting", merged_points_list)

    return


# update a plot widget with a pyqt plot with labels for the number of frames over time
# inputs: plot_widget: pyqt plot widget on which the plot will be added
#         time_points: the frame number/ time stamp of the respective points
#         point_objects: list of instances of point_object, an object that for each frame contains all points
#                        and their characteristics
#         y_labels_tuple: tuple of str, labels that will be added to the legend for each of the tuples,
#                         if a legend is added, default = None
def update_pyqt_points_over_frame_plot(plot_widget, time_points, point_objects, is_first_time=False):
    # get the number of merged/split points at each frame (for plotting)
    mid_value_points_lengths = [len(po.active_mid_value_points) for po in point_objects]
    low_value_points_lengths = [len(po.active_low_value_points) for po in point_objects]
    hig_value_points_lengths = [len(po.active_hig_value_points) for po in point_objects]
    local_max_points_lengths = [len(po.active_local_max_points) for po in point_objects]
    local_max_points_intensity_lengths = [len(po.active_local_max_points_intensity) for po in point_objects]
    local_max_points_prod_score_lengths = [len(po.active_local_max_points_prod_score) for po in point_objects]
    new_split_points_lengths = [len(po.active_points) - len(po.active_mid_value_points)
                                - len(po.active_low_value_points) - len(po.active_hig_value_points)
                                - len(po.active_local_max_points) - len(po.active_local_max_points_intensity)
                                - len(po.active_local_max_points_prod_score) for po in point_objects]

    y_tuple = (new_split_points_lengths, low_value_points_lengths, mid_value_points_lengths, hig_value_points_lengths,
               local_max_points_lengths, local_max_points_intensity_lengths, local_max_points_prod_score_lengths)
    # plot the number of points found over the frame number as a bar plot
    if is_first_time:
        add_pyqt_plot(plot_widget, time_points, y_tuple,
                      plot_title="# points per frame over time points",
                      colors=["white", "blue", "green", "yellow", "red", "pink", "purple"],
                      is_first_time=True,
                      y_labels_tuple=("extra points from splitting",
                                      "low value points",
                                      "middle value points",
                                      "high value points",
                                      "local score shape max points",
                                      "local intensity max points",
                                      "local product image max points"))
    else:
        add_pyqt_plot(plot_widget, time_points, y_tuple,
                      plot_title="# points per frame over time points",
                      colors=["white", "blue", "green", "yellow", "red", "pink", "violet"])

    # add labels on the top, bottom and right axis
    plot_item = plot_widget.plotItem
    plot_item.showAxis('top')
    plot_item.setLabels(left='number of objects found (category=color)',
                        top='total number of objects found',
                        bottom='frame number')

    # set the number of points in each frame as x-axis label at the top
    string_list = []

    for j in range(len(y_tuple[0])):
        sum_for_tuple = 0
        for i in range(len(y_tuple)):
            sum_for_tuple += y_tuple[i][j]
        string_list.append(str(sum_for_tuple))

    x_axis_top = plot_widget.getAxis('top')
    ticks = [list(zip(time_points, tuple(string_list)))]
    x_axis_top.setTicks(ticks)

    return


# add a pyqt plot to a pyqt plot widget
# inputs: plot_widget: pyqt plot widget on which the plot will be added
#         x: x values, list, np.array axis over which should be plotted
#         y_tuple: tuple of lists/ np.arrays of y_values which should
#                  (each) be plotted over x
#        plot_title: (opt, str) title that will be added to the plot, default = None
#        colors: (opt, object that is recognised as color by pyqt, e.g.
#                'r', 'w', tuple/list of list of three numbers in range
#                 0, 255), default = None
#        is_first_time: whether the function is called for the first time (default = False),
#                       if it is True, a legend item is added
#         y_labels_tuple: tuple of str, labels that will be added to the legend for each of the tuples,
#                         if a legend is added, default = None
def add_pyqt_plot(plot_widget, x, y_tuple, plot_title=None, colors=None, is_first_time=False, y_labels_tuple=None):
    # clear the current plot widget
    plot_widget.clear()

    # add a legend, if the function is called for the first time
    if is_first_time:
        legend = pg.LegendItem((40, 30), offset=(70, 20))
        legend.setParentItem(plot_widget.graphicsItem())
    else:
        legend = None

    # set the initial offset to zero
    y0 = [0] * len(y_tuple[0])

    # set the random seed to 0, such that always the same colors are used, if random
    # colors are used.
    np.random.seed(0)

    # loop over all different y-value series and add them to the plot
    for i, y in enumerate(y_tuple):
        # set the pen (width, style and color of the plot)
        if colors is None:
            # if no colors list/tuple is put in, create random colors
            bar_color = np.random.randint(0, high=255, size=3)
        else:
            # otherwise use the colors as defined by the input
            bar_color = QColor(colors[i])
        # plot the y sequence over the x sequence and label it
        if y_labels_tuple is not None:
            bg = pg.BarGraphItem(x=x, height=y, y0=y0, width=0.3, name=y_labels_tuple[i], brush=bar_color,
                                 pen=bar_color)
            if is_first_time:
                legend.addItem(bg, name=y_labels_tuple[i])
        else:
            bg = pg.BarGraphItem(x=x, height=y, y0=y0, width=0.3, brush=bar_color, pen=bar_color)
        plot_widget.addItem(bg)

        y0 = [y0[i] + y[i] for i in range(len(y0))]

    # if a plot_title is passed to the function, add the title
    if plot_title is not None:
        plot_widget.setTitle(plot_title)
    return


# callback function that is executed if one of the sliders is changed
# inputs: viewer: napari.Viewer() instance that contains all layers that will be updated
#         sliders: list of pyqt QSlider object that's values correspond to the thresholds
#         label_objs: labels corresponding to the sliders (will be updated to display)
#                     the current values
#         arguments: additional arguments needed to update the layers:
#                    - all_low_value_points: all points segmented from the score image
#                                            with a relatively low score value
#                    - all_hig_value_points: all points segmented from the score image
#                                            with a high score value
#                    - all_mid_value_points: all points segmented from the score image
#                                            with a relatively medium high score value
#                    - all_local_max_points: all point segmented from the score image
#                                            that are local maxima
#                    - images: np.array containing the intensity images for each frame
#                    - time_points: list of the time_points/frame_numbers corresponding
#                                   to the images
#         init_values: initial values of the sliders (only used for displaying them)
#         plot_widget: plot_widget in which the # of points over time will be plotted
def callback_value_changed(viewer, sliders, names, label_objs, arguments, init_values, plot_widget):
    old_stdout = sys.stdout

    sys.stdout = open("segmentation_output_during_interactive_segmentation.txt", 'w')

    if viewer is not None:
        # change the viewer status: show that the function was called
        viewer.status = 'Updating all layers - Please wait ...'

    # unpack the slider values
    low_value_thresh = sliders[0].value()
    mid_and_high_value_thresh = sliders[1].value()
    thresh_local_max = sliders[2].value()
    thresh_local_max_intensity = sliders[3].value()
    thresh_local_max_prod_score = sliders[4].value()
    slope_bleaching = sliders[5].value() / 1000.
    watershed_intensity_init = sliders[6].value()
    watershed_object_size_init = sliders[7].value()
    min_dist_to_local_max_point = sliders[8].value()
    sigma_gauss_time_correlation = sliders[9].value()
    time_correlation = sliders[10].value()
    intensity_more_value = sliders[11].value()

    # unpack the arguments
    images, time_points, point_objects = arguments

    # update the thresholds dict for the minimum distance to a local maximum for every point_object
    for point_object in point_objects:
        point_object.threshold_dict["min_dist_local_max"] = min_dist_to_local_max_point

    if label_objs is not None:
        # update the labels to show the current value of each slider, also add the initial value in ()
        for i, label_obj in enumerate(label_objs):
            label_obj.setText(names[i] + " Current value: " + str(sliders[i].value())
                              + " (initial: " + str(init_values[i]) + ")")

    if viewer is not None:
        viewer.status = "Updating local max points"

    # update the local maxima points and add them to the viewer
    point_objects = update_threshold_local_max_points(viewer, thresh_local_max, slope_bleaching,
                                                      None, None, (images, time_points, point_objects))
    point_objects = update_threshold_local_max_prod_score(viewer, thresh_local_max_prod_score,
                                                          slope_bleaching, sigma_gauss_time_correlation,
                                                          time_correlation, intensity_more_value,
                                                          (images, time_points, point_objects))
    point_objects = update_threshold_local_max_intensity(viewer, thresh_local_max_intensity, slope_bleaching, None,
                                                         None, (images, time_points, point_objects))

    if viewer is not None:
        viewer.status = "Updating double thresh points"
    # update all double thresholding points from the score image (low, mid, hig value points) and add
    # them to the viewer
    point_objects = update_threshold_double_thresholding_points(viewer, low_value_thresh, mid_and_high_value_thresh,
                                                                slope_bleaching,
                                                                None, None, (images, time_points, point_objects))
    if viewer is not None:
        viewer.status = "Adding merged points"

        # merge the points found above and add them to the viewer
        add_merged_points_to_viewer(viewer, point_objects)

        viewer.status = "Try to add split points"

    # split the points
    point_objects = calculate_new_split_points(viewer, images, time_points, watershed_intensity_init,
                                               watershed_object_size_init, slope_bleaching, point_objects)

    if viewer is not None:
        viewer.status = "Updating plot"

        update_pyqt_points_over_frame_plot(plot_widget, time_points, point_objects)

        # change the viewer status: show that the function finished
        viewer.status = 'Updated all layers'

    sys.stdout = old_stdout

    return point_objects


# display the current selected points in a napari viewer interactively, the function is already given a list of
# potential points and just adjusting the thresholds
# inputs: thresholds_dict: dictionary with thresholds, such as defined by 'get_threshold_values_for_segmentation.py'
#         point_objects: a list of PointObjects for each frame. They contain information on the way the points
#                        have been detected, intensity information etc.
#         p: a Parameters object containing all variables specified in the configurations file by the user
# output: sliders: the thresholds as selected with the sliders when the napari window is closed
def display_interactive_segmentation_thresholds(thresholds_dict, point_objects, p):
    # make the point_objects_to_return global, so it can be changed in the save function and initialise it with the
    # point objects given to this function, such that if the save button is not pressed, the points are returned
    # correctly
    global point_objects_to_return, new_thresholds_dict
    point_objects_to_return, new_thresholds_dict = point_objects, thresholds_dict

    old_stdout = sys.stdout

    sys.stdout = open(p.analysis_folder + "segmentation_output_during_interactive_segmentation.txt", 'w')

    with napari.gui_qt():

        # create a napari viewer instance, change the display to 3D
        viewer = napari.Viewer()
        viewer.dims.ndisplay = 3

        print("Change the sliders in the napari window to adapt the thresholds interactively.")

        # add all images and points layers
        images = np.array([po.microscopy_data for po in point_objects])
        # noinspection PyTypeChecker
        viewer.add_image(images,
                         name='microscopy data',
                         colormap='turbo',
                         contrast_limits=[np.min(images), np.max(images)],
                         opacity=1.0)

        # add all images and points layers
        score_images = np.array([po.score_image for po in point_objects])
        # noinspection PyTypeChecker
        viewer.add_image(score_images,
                         name='score images',
                         colormap='twilight',
                         contrast_limits=[np.min(score_images), np.max(score_images)],
                         visible=False)

        multiplied_images = score_images * images

        norm_multiplied_images = multiplied_images / np.max(multiplied_images)

        viewer.add_image(norm_multiplied_images,
                         name='normalised score images multiplied with images',
                         colormap='turbo',
                         contrast_limits=[np.min(norm_multiplied_images), np.max(norm_multiplied_images)],
                         visible=False)

        try:
            # noinspection PyTypeChecker
            viewer.add_labels(np.array(images > thresholds_dict["watershed_intensity_init"], dtype=bool),
                              name='watershed_intensity',
                              visible=False)
        except MemoryError:
            print("can't add watershed intensity to the display, as that would cause a memory error for "
                  "the display."
                  "Either try to adapt the values without seeing it, or try splitting your series into several "
                  "time points.")

        local_max_prod_score_no = [str(i) for po in point_objects
                                   for i in range(len(po.all_local_max_points_prod_score))]

        local_max_points_prod_score_text = {
            'text': '{number}',
            'size': 4,
            'color': 'indigo',
            'translation': np.array([-2, 0, 0]),
            'visible': False
        }

        local_max_points_prod_score_properties = {'number': local_max_prod_score_no}

        viewer.add_points(get_nd_points_layer_napari([po.all_local_max_points_prod_score for po in point_objects]),
                          size=1,
                          properties=local_max_points_prod_score_properties,
                          name='all_local_max_prod_score',
                          face_color='indigo',
                          visible=False,
                          text=local_max_points_prod_score_text
                          )

        viewer.add_points(get_nd_points_layer_napari([po.active_local_max_points_prod_score for po in point_objects]),
                          size=1,
                          name='thresh_local_max_prod_score',
                          face_color='blueviolet',
                          visible=False,
                          )

        # add the intensity local maxima

        local_max_intensity_no = [str(i) for po in point_objects for i in range(len(po.all_local_max_points_intensity))]

        local_max_points_intensity_text = {
            'text': '{number}',
            'size': 4,
            'color': 'hotpink',
            'translation': np.array([-2, 0, 0]),
            'visible': False
        }

        local_max_points_intensity_properties = {'number': local_max_intensity_no}

        viewer.add_points(get_nd_points_layer_napari([po.all_local_max_points_intensity for po in point_objects]),
                          size=1,
                          text=local_max_points_intensity_text,
                          properties=local_max_points_intensity_properties,
                          name='all_local_max_intensity',
                          face_color='hotpink',
                          visible=False)

        viewer.add_points(get_nd_points_layer_napari([po.active_local_max_points_intensity for po in point_objects]),
                          size=1,
                          name='thresh_local_max_intensity',
                          face_color='pink',
                          visible=False)

        # add all mid_value, hig_value, low_value points and their thresholded equivalent

        viewer.add_points(get_nd_points_layer_napari([po.all_mid_value_points for po in point_objects]),
                          size=1,
                          name='all_middle_value_points',
                          face_color='darkgreen')

        viewer.add_points(get_nd_points_layer_napari([po.active_mid_value_points for po in point_objects]),
                          size=1,
                          name='thresh_middle_value_points',
                          face_color='lime')

        viewer.add_points(get_nd_points_layer_napari([po.all_low_value_points for po in point_objects]),
                          size=1,
                          name='all_low_value_points',
                          face_color='dodgerblue')

        viewer.add_points(get_nd_points_layer_napari([po.active_low_value_points for po in point_objects]),
                          size=1,
                          name='thresh_low_value_points',
                          face_color='cyan')

        viewer.add_points(get_nd_points_layer_napari([po.all_hig_value_points for po in point_objects]),
                          size=1,
                          name='all_high_value_points',
                          face_color='goldenrod')

        viewer.add_points(get_nd_points_layer_napari([po.active_hig_value_points for po in point_objects]),
                          size=1,
                          name='thresh_high_value_points',
                          face_color='yellow')

        # add the local maxima in the score image and their thresholded equivalent

        viewer.add_points(get_nd_points_layer_napari([po.all_local_max_points for po in point_objects]),
                          size=1,
                          name='all_local_max_points',
                          face_color='darkred',
                          visible=False)

        viewer.add_points(get_nd_points_layer_napari([po.active_local_max_points for po in point_objects]),
                          size=1,
                          name='thresh_local_max_points',
                          face_color='red',
                          visible=False)

        # add a layer for the unified points before and after splitting

        viewer.add_points(get_nd_points_layer_napari([po.active_points for po in point_objects]),
                          size=1,
                          name='unified_points_split',
                          opacity=0.8,
                          visible=False)

        viewer.add_points(size=1,
                          name='unified_points_before_splitting',
                          face_color='gray',
                          visible=False)

        # if an excel_file with manual points was given, add them to the display
        if point_objects[0].manual_points is not None:
            viewer.add_points(get_nd_points_layer_napari([np.array(po.manual_points, dtype=int)
                                                          for po in point_objects]),
                              size=1,
                              name='manual_points',
                              face_color='orange',
                              opacity=0.5,
                              visible=False)

        # add value sliders to change all thresholds interactively
        # as the pyqt sliders can only take integer values, manually multiply the float values by 1000
        # set the minimum values, use the minimum intensity for intensity thresholds, 0, 0 for the bleaching slope
        # (it is a difference)
        # and for the watershed size
        int_min = np.min(np.array(images))
        int_max = np.max(np.array(images))
        min_values = [int_min,
                      int_min,
                      int_min,
                      int_min,
                      int_min,
                      1,
                      int_min,
                      0,
                      0,
                      0,
                      0,
                      50]
        # set the max values, use the max intensity for the intensity thresholds
        # For the bleaching slope: use the maximum intensity and divide it by the number of steps multiplied by 10,
        #                          as all bleaching slope related values are multiplied by 1000, divide by 1000
        # For the size threshold an arbitrary max of 300 is chosen

        if np.max(p.time_points) == np.min(p.time_points):
            max_slope_bleaching = 1
        else:
            max_slope_bleaching = round(int_max / (np.max(p.time_points) - np.min(p.time_points)) * 1000)

        max_values = [int_max,
                      int_max,
                      int_max,
                      int_max,
                      int_max,
                      max_slope_bleaching,
                      int_max,
                      1500,
                      round(np.max(point_objects[0].local_max_point_dists)),
                      1000,
                      100,
                      200]

        # set the steps to 1 for each slider
        step_values = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

        # create a list with a name for all sliders
        names_list = ['Set value: "low value points": ',
                      'Set value: "mid and high value points": ',
                      'Set value: "local max points": ',
                      'Set value: "local max intensity points": ',
                      'Set value: "local max norm score points": ',
                      'Set value: "slope bleaching": ',
                      'Set value: "ws intensity": ',
                      'Set value: "ws size": ',
                      'Set value: "min distance to local max point"',
                      'Set value: "blur in time correlation (sigma*100)"',
                      'Set value: "time correlation"',
                      'Set value: "weigh intensity more"']

        # create an entry for the minimum distance between a double_thresh_point and a local_max_point in the
        # threshold_dict
        # of every point_object
        for point_object in point_objects:
            point_object.threshold_dict["min_dist_local_max"] = 0

        # create a list of all call-back function arguments
        call_back_fcts_args_list = (images, p.time_points, point_objects)

        # set the initial values for each of the thresholds
        init_value_list = [thresholds_dict["double_thresholding_init"],
                           thresholds_dict["double_thresholding_init"],
                           thresholds_dict["max_points_init"],
                           thresholds_dict["max_points_init"],
                           thresholds_dict["max_points_init"],
                           round(thresholds_dict["slope_bleaching"] * 1000),
                           thresholds_dict["watershed_intensity_init"],
                           thresholds_dict["watershed_obj_size"],
                           0,
                           0,
                           0,
                           100]

        # call the function that initialises all sliders and connects them to the update functions of the layers
        # via the call_back_value_changed function
        sliders, plot_widget = add_intensity_pyqt_sliders_to_napari_viewer(viewer, min_values, max_values,
                                                                           step_values,
                                                                           callback_value_changed,
                                                                           call_back_fcts_args_list,
                                                                           names_list, init_value_list,
                                                                           analysis_folder=p.analysis_folder)

        if plot_widget is not None:
            update_pyqt_points_over_frame_plot(plot_widget, p.time_points, point_objects, is_first_time=True)

    sys.stdout = old_stdout

    return new_thresholds_dict, point_objects_to_return
