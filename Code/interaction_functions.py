import SimpleITK as sitk
import ipython_display_classes as idc
import ipywidgets as ipyw
import segmentation_methods as smeth
from ipywidgets import fixed, interact


# part I of old method to interact with different thresholding functions
def different_thresholding_methods(method):
    if method == 'Otsu':
        threshold = sitk.OtsuThresholdImageFilter()

    elif method == 'Huang':
        threshold = sitk.HuangThresholdImageFilter()

    elif method == 'IsoData':
        threshold = sitk.IsoDataThresholdImageFilter()

    elif method == 'Li':
        threshold = sitk.LiThresholdImageFilter()

    elif method == 'MaxEntropy':
        threshold = sitk.MaximumEntropyThresholdImageFilter()

    elif method == 'KittlerIllingworth':
        threshold = sitk.KittlerIllingworthThresholdImageFilter()

    elif method == 'Moments':
        threshold = sitk.MomentsThresholdImageFilter()

    elif method == 'Yen':
        threshold = sitk.YenThresholdImageFilter()

    elif method == 'RenyiEntropy':
        threshold = sitk.RenyiEntropyThresholdImageFilter()

    elif method == 'Shanbhag':
        threshold = sitk.ShanbhagThresholdImageFilter()

    else:
        raise ValueError("Method '" + method + "' does not exist")

    t = threshold.GetThreshold()

    return t


# part II of old method to interact with different thresholding functions
def apply_thresholding(image, method_var, metadata_var):
    threshold = different_thresholding_methods(method_var)
    thresholded_image = image > threshold
    idc.ImageSliceViewer3D(thresholded_image, metadata_var=metadata_var, cmap='jet')
    return


# apply threshold to image
def apply_threshold_to_image_from_list(image_1, mask_var, method_var):
    if method_var == 'Otsu':
        thresholded_image = smeth.segment_image_otsu_threshold(image_1, mask_var)

    elif method_var == 'Huang':
        thresholded_image = smeth.segment_image_huang_threshold(image_1, mask_var)

    elif method_var == 'IsoData':
        thresholded_image = smeth.segment_image_iso_data_threshold(image_1, mask_var)

    elif method_var == 'Li':
        thresholded_image = smeth.segment_image_li_threshold(image_1, mask_var)

    elif method_var == 'MaxEntropy':
        thresholded_image = smeth.segment_image_max_entropy(image_1, mask_var)

    elif method_var == 'KittlerIllingworth':
        thresholded_image = smeth.segment_image_kittler_illingworth_threshold(image_1, mask_var)

    elif method_var == 'Moments':
        thresholded_image = smeth.segment_image_moments_threshold(image_1, mask_var)

    elif method_var == 'Yen':
        thresholded_image = smeth.segment_image_yen_threshold(image_1, mask_var)

    elif method_var == 'RenyiEntropy':
        thresholded_image = smeth.segment_image_renyi_entropy_threshold(image_1, mask_var)

    elif method_var == 'Shanbhag':
        thresholded_image = smeth.segment_image_shanbag_threshold(image_1, mask_var)

    else:
        raise ValueError("Method '" + str(method_var) + "' does not exist")
    return thresholded_image


# interactive application of different thresholding methods
def interact_apply_thresholding(image, metadata_var, mask=None):
    def thresholding_selection(image_1, mask_var, metadata_var_1, method_var):
        thresh_image = apply_threshold_to_image_from_list(image_1, mask_var, method_var)
        if mask_var is None:
            idc.ImageSliceViewer3D(thresh_image, metadata_var=metadata_var_1, cmap='jet')
        else:
            idc.ImageSliceViewer3D(thresh_image + 2 * mask, metadata_var=metadata_var_1, cmap='jet')

    ipyw.interact(thresholding_selection, image=fixed(image), metadata_var=fixed(metadata_var), mask_var=fixed(mask),
                  method_var=ipyw.Dropdown(
                      options=['Otsu', 'Huang', 'IsoData', 'Li', 'MaxEntropy', 'KittlerIllingworth', 'Moments', 'Yen',
                               'RenyiEntropy', 'Shanbhag'],
                      value='RenyiEntropy', description='Thresholding Method', disabled=False,
                      style={'description_width': 'initial'}))
    return


# open interactive thresholding interface in napari
def open_interactive_thresholding_interface_in_napari(image):
    import napari
    import numpy as np
    from qtpy.QtWidgets import QLabel, QVBoxLayout, QGroupBox, QComboBox

    def combo_box_callback(viewer_1, image_1, method_var):
        method_dict = {0: 'Otsu', 1: 'Huang', 2: 'IsoData', 3: 'Li', 4: 'MaxEntropy', 5: 'KittlerIllingworth',
                       6: 'Moments', 7: 'Yen', 8: 'RenyiEntropy', 9: 'Shanbhag'}
        new_thresholded_image = apply_threshold_to_image_from_list(image_1, None, method_dict[method_var])

        for layer in viewer_1.layers:
            if layer.name.startswith("thresholded_image"):
                layer.data = new_thresholded_image
        return

    with napari.gui_qt():
        viewer = napari.Viewer(title="Interactive thresholding")
        viewer.dims.ndisplay = 3  # open the viewer in the 3D view
        viewer.add_image(image,
                         name='image',
                         colormap='turbo')
        viewer.add_image(np.zeros(shape=image.shape),
                         name='thresholded_image',
                         colormap='red',
                         opacity=0.5)

        # create a Combo-Box to change the thresholding method
        threshold_combobox = QComboBox()
        threshold_combobox.addItems(['Otsu', 'Huang', 'IsoData', 'Li', 'MaxEntropy', 'KittlerIllingworth', 'Moments',
                                     'Yen', 'RenyiEntropy', 'Shanbhag'])
        # add a label to the QSlider
        thresholding_style_label = QLabel('Thresholding Method: ')

        # Connect the dropdown selection to its callback function to change the vector style dependent on the selection
        # noinspection PyUnresolvedReferences
        threshold_combobox.currentIndexChanged.connect(lambda value=threshold_combobox: combo_box_callback(viewer,
                                                                                                           image,
                                                                                                           value))

        # create a pyqt-box for placing all PyQt-objects
        box = QVBoxLayout()
        box.addWidget(thresholding_style_label)
        box.addWidget(threshold_combobox)

        # use a group box to add the objects collected in the QVBoxLayout object (no title)
        group_box = QGroupBox('')
        # set the layout
        group_box.setLayout(box)
        # add the widget to the napari viewer, on the right side
        viewer.window.add_dock_widget(group_box, name='Thresholding Method: ', area='left')

    return
