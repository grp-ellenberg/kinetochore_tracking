from PyQt5.QtWidgets import QFileDialog


# get the path to a certain file and its name using a graphical user interface
# input: str, directory where to start (optional)
# output: path + filename  
def gui_file_name(directory=None):
    # if the directory is not specified otherwise, start in the home directory
    if directory is None:
        directory = './'
    # create a dialog to select the filename where all files are visible
    file_name = QFileDialog.getOpenFileName(caption="Select data file...", directory=directory,
                                            filter="All files (*);; SM Files (*.sm)")
    # return the file's full name (including its path)
    return file_name[0]
