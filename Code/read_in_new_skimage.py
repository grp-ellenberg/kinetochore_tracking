# Source:
# https://github.com/zeiss-microscopy/OAD/blob/master/jupyter_notebooks/Read_CZI_and_OMETIFF_and_display_widgets_and_
# napari/modules/imgfileutils.py 06.02.20, author: sebi06

import numpy as np
import skimage
import skimage.io
import tifffile
import xmltodict

import yaml
import os

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

import warnings


#  read in a yaml configuration file
def read_in_config(path_of_config_yaml):
    """
    create python dictionary from .yaml configuration file
    :return: metadata - dictionary with keys for the relevant metadata
    """
    with open(path_of_config_yaml) as file:
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format
        config = yaml.load(file, Loader=yaml.Loader)
    return config


# import as a flat tuple
def import_as_flat_tuple(x):
    if type(x) is not tuple:
        return x,
    elif len(x) == 0:
        return ()
    else:
        return import_as_flat_tuple(x[0]) + import_as_flat_tuple(x[1:])


def create_metadata_dict():
    """
    A Python dictionary will be created to hold the relevant metadata.
    :return: metadata - dictionary with keys for the relevant metadata
    """

    metadata = {'Directory': None,
                'Filename': None,
                'Extension': None,
                'ImageType': None,
                'Name': None,
                'AcqDate': None,
                'TotalSeries': None,
                'size_x': None,
                'size_y': None,
                'size_z': None,
                'size_c': None,
                'size_t': None,
                'Sizes BF': None,
                'DimOrder BF': None,
                'DimOrder BF Array': None,
                'DimOrder CZI': None,
                'Axes': None,
                'Shape': None,
                'isRGB': None,
                'ObjNA': None,
                'ObjMag': None,
                'ObjID': None,
                'ObjName': None,
                'ObjImmersion': None,
                'phys_size_x': None,
                'phys_size_y': None,
                'phys_size_z': None,
                'x_size_unit': None,
                'y_size_unit': None,
                'z_size_unit': None,
                'DetectorModel': [],
                'DetectorName': [],
                'DetectorID': None,
                'InstrumentID': None,
                'Channels': [],
                'ImageIDs': [],
                'NumPy.dtype': None
                }

    return metadata


def get_metadata_ometiff(image_file):

    with tifffile.TiffFile(image_file) as tif:
        # get OME-XML metadata as string
        ome_xml = tif.ome_metadata
        print(type(ome_xml))
        ome_xml = xmltodict.parse(ome_xml)['OME']

    # create dictionary for metadata and get OME-XML data
    metadata = create_metadata_dict()

    # get directory and filename etc.
    try:
        metadata['AcqDate'] = ome_xml["Image"]["AcquisitionDate"]
    except KeyError:
        metadata['AcqDate'] = None
    metadata['Name'] = ome_xml["Image"]["Pixels"]["TiffData"][0]["UUID"]["@FileName"]

    # get image dimensions
    metadata['size_t'] = int(ome_xml["Image"]["Pixels"]["@SizeT"])
    metadata['size_z'] = int(ome_xml["Image"]["Pixels"]["@SizeZ"])
    metadata['size_c'] = int(ome_xml["Image"]["Pixels"]["@SizeC"])
    metadata['size_x'] = int(ome_xml["Image"]["Pixels"]["@SizeX"])
    metadata['size_y'] = int(ome_xml["Image"]["Pixels"]["@SizeY"])

    # get number of series
    metadata['TotalSeries'] = len(ome_xml["Image"]["Pixels"]["TiffData"])
    metadata['Sizes BF'] = [metadata['TotalSeries'],
                            metadata['size_t'],
                            metadata['size_z'],
                            metadata['size_c'],
                            metadata['size_y'],
                            metadata['size_x']]

    # get dimension order
    metadata['DimOrder BF'] = ome_xml["Image"]["Pixels"]["@DimensionOrder"]

    # reverse the order to reflect later the array shape
    metadata['DimOrder BF Array'] = metadata['DimOrder BF'][::-1]

    # get the scaling
    metadata['phys_size_x'] = float(ome_xml["Image"]["Pixels"]["@PhysicalSizeX"])
    metadata['x_size_unit'] = ome_xml["Image"]["Pixels"]["@PhysicalSizeXUnit"]
    metadata['phys_size_y'] = float(ome_xml["Image"]["Pixels"]["@PhysicalSizeY"])
    metadata['y_size_unit'] = ome_xml["Image"]["Pixels"]["@PhysicalSizeYUnit"]
    metadata['phys_size_z'] = float(ome_xml["Image"]["Pixels"]["@PhysicalSizeZ"])
    metadata['z_size_unit'] = ome_xml["Image"]["Pixels"]["@PhysicalSizeZUnit"]

    # get all image IDs
    for i in range(metadata['TotalSeries']):
        metadata['ImageIDs'].append(i)

    # get information about the instrument and objective
    try:
        metadata['InstrumentID'] = ome_xml["Instrument"]["@ID"]
    except KeyError:
        metadata['InstrumentID'] = None

    try:
        metadata['DetectorModel'] = ome_xml["Instrument"]["Detector"]["@Model"]
        metadata['DetectorID'] = ome_xml["Instrument"]["Detector"]["@ID"]
        metadata['DetectorModel'] = ome_xml["Instrument"]["Detector"]["@Type"]
    except KeyError:
        metadata['DetectorModel'] = None
        metadata['DetectorID'] = None
        metadata['DetectorModel'] = None

    try:
        metadata['ObjNA'] = ome_xml["Instrument"]["Objective"]["@LensNA"]
        metadata['ObjID'] = ome_xml["Instrument"]["Objective"]["@ID"]
        metadata['ObjMag'] = ome_xml["Instrument"]["Objective"]["@NominalMagnification"]
    except KeyError:
        metadata['ObjNA'] = None
        metadata['ObjID'] = None
        metadata['ObjMag'] = None

    # get channel names
    for c in range(metadata['size_c']):
        metadata['Channels'].append(ome_xml["Image"]["Pixels"]["Channel"]["@ID"][c])
    return metadata


def create_ipyviewer(array, metadata):
    import ipywidgets as ipyw
    """
    Creates a simple interactive viewer inside a Jupyter Notebook.
    Works with OME-TIFF files and the respective metadata
    :param array: multidimensional array containing the pixel data
    :param metadata: dictionary with the meta information
    :return: out - interactive widgets
    :return: ui - ui for interactive widgets
    """

    # time slider
    t = ipyw.IntSlider(description='Time:',
                       min=1,
                       max=metadata['size_t'],
                       step=1,
                       value=1,
                       continuous_update=False)

    # z-plane slider
    z = ipyw.IntSlider(description='Z-Plane:',
                       min=1,
                       max=metadata['size_z'],
                       step=1,
                       value=1,
                       continuous_update=False)

    # channel slider
    c = ipyw.IntSlider(description='Channel:',
                       min=1,
                       max=metadata['size_c'],
                       step=1,
                       value=1)

    # slider for contrast
    r = ipyw.IntRangeSlider(description='Display Range:',
                            min=array.min(),
                            max=array.max(),
                            step=1,
                            value=[array.min(), array.max()],
                            continuous_update=False)

    # disable slider that are not needed
    if metadata['size_t'] == 1:
        t.disabled = True
    if metadata['size_z'] == 1:
        z.disabled = True
    if metadata['size_c'] == 1:
        c.disabled = True

    sliders = metadata['DimOrder BF Array'][:-2] + 'R'

    if sliders == 'CTZR':
        ui = ipyw.VBox([c, t, z, r])

        def get_tzc_czi(c_ind, t_ind, z_ind, r_ind):
            display_image(array, sliders, c=c_ind, t=t_ind, z=z_ind, v_min=r_ind[0], v_max=r_ind[1])

        out = ipyw.interactive_output(get_tzc_czi, {'c_ind': c, 't_ind': t, 'z_ind': z, 'r': r})

    elif sliders == 'TZCR':
        ui = ipyw.VBox([t, z, c, r])

        def get_tzc_czi(t_ind, z_ind, c_ind, r_ind):
            display_image(array, sliders, t=t_ind, z=z_ind, c=c_ind, v_min=r_ind[0], v_max=r_ind[1])

        out = ipyw.interactive_output(get_tzc_czi, {'t_ind': t, 'z_ind': z, 'c_ind': c, 'r': r})

    elif sliders == 'TCZR':
        ui = ipyw.VBox([t, c, z, r])

        def get_tzc_czi(t_ind, c_ind, z_ind, r_ind):
            display_image(array, sliders, t=t_ind, c=c_ind, z=z_ind, v_min=r_ind[0], v_max=r_ind[1])

        out = ipyw.interactive_output(get_tzc_czi, {'t_ind': t, 'c_ind': c, 'z_ind': z, 'r': r})

    elif sliders == 'CZTR':
        ui = ipyw.VBox([c, z, t, r])

        def get_tzc_czi(c_ind, z_ind, t_ind, r_ind):
            display_image(array, sliders, c=c_ind, z=z_ind, t=t_ind, v_min=r_ind[0], v_max=r_ind[1])

        out = ipyw.interactive_output(get_tzc_czi, {'c_ind': c, 'z_ind': z, 't_ind': t, 'r': r})

    elif sliders == 'ZTCR':
        ui = ipyw.VBox([z, t, c, r])

        def get_tzc_czi(z_ind, t_ind, c_ind, r_ind):
            display_image(array, sliders, z=z_ind, t=t_ind, c=c_ind, v_min=r_ind[0], v_max=r_ind[1])

        out = ipyw.interactive_output(get_tzc_czi, {'z_ind': z, 't_ind': t, 'c_ind': c, 'r': r})

    elif sliders == 'ZCTR':
        ui = ipyw.VBox([z, c, t, r])

        def get_tzc_czi(z_ind, c_ind, t_ind, r_ind):
            display_image(array, sliders, z=z_ind, c=c_ind, t=t_ind, v_min=r_ind[0], v_max=r_ind[1])

        out = ipyw.interactive_output(get_tzc_czi, {'z_ind': z, 'c_ind': c, 't_ind': t, 'r': r})

    else:
        ui = None
        out = None

    """
    ui = ipyw.VBox([t, z, c, r])
    def get_TZC_ometiff(t, z, c, r):
        display_image(array, metadata, 'TZCR', t=t, z=z, c=c, v_min=r[0], v_max=r[1])
    out = ipyw.interactive_output(get_TZC_ometiff, {'t': t, 'z': z, 'c': c, 'r': r})
    """

    return out, ui  # , t, z, c, r


def display_image(array, sliders, t=0, c=0, z=0, v_min=0, v_max=1000):
    """
    Displays OME-TIFF image using a simple interactive viewer
    inside a Jupyter Notebook with dimension sliders.
    :param array: multidimensional array containing the pixel data
    :param sliders: string specifying the required sliders
    :param t: time index
    :param c: channel index
    :param z: slice index
    :param v_min: minimum value
    :param v_max: maximum value
    """

    if sliders == 'TZCR':
        image = array[t - 1, z - 1, c - 1, :, :]

    elif sliders == 'CTZR':
        image = array[c - 1, t - 1, z - 1, :, :]

    elif sliders == 'TCZR':
        image = array[t - 1, c - 1, z - 1, :, :]

    elif sliders == 'CZTR':
        image = array[c - 1, z - 1, t - 1, :, :]

    elif sliders == 'ZTCR':
        image = array[z - 1, t - 1, c - 1, :, :]

    else:  # sliders == 'ZCTR'
        image = array[z - 1, c - 1, z - 1, :, :]

    # display the labeled image
    fig, ax = plt.subplots(figsize=(8, 8))
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    im = ax.imshow(image, vmin=v_min, vmax=v_max, interpolation='nearest', cmap=plt.get_cmap('gray'))
    fig.colorbar(im, cax=cax, orientation='vertical')
    print('Min-Max (Current Plane):', image.min(), '-', image.max())


def get_scale_factor(metadata):
    """
    Add scaling factors to the metadata dictionary
    :param metadata: dictionary with CZI or OME-TIFF metadata
    :return: metadata - dictionary with additional keys for scaling factors
    """

    # set default scale factors to 1
    scale_factors = {'xy': 1.0, 'zx': 1.0}

    try:
        # get the factor between XY scaling
        scale_factors['xy'] = np.round(metadata['phys_size_x'] / metadata['phys_size_y'], 3)
        # get the scale factor between XZ scaling
        scale_factors['zx'] = np.round(metadata['phys_size_z'] / metadata['phys_size_x'], 3)
    except KeyError as e:
        print('Key not found: ', e)

    return scale_factors


def get_image(path):
    # open a 5D ImageJ hyperstack file
    with tifffile.TiffFile(path) as tif:
        # get the first image series as numpy array
        image_stack = tif.asarray()

        # directly memory-map contiguous image data in the file as numpy array.
        # if the image data is not stored contiguously, a temporary memory-mapped
        # file is created
        # image_mmap = tif.asarray(out='memmap')
        # print(image_mmap.shape)

        # stack only specific pages in the file
        # image_subset = tif.asarray([1, 2, 4, 5])  # get slices 1,2,4
        # print(image_subset.shape)

        # get image data from a specific page as numpy array
        # image = tif.pages[10].asarray()
        return image_stack


# load one frame from the memory and return the ROI of the image
# inputs: frame_number: int, frame number of the frame from which the data should be loaded
#         data_folder: folder containing the microscopy data
#         microscopy_data_filename_pre_index, microscopy_data_filename_post_index,
#         microscopy data_file_extension: all strings, the naming conventions for the data
#                                        and their file format (.tif/.tiff)
#         roi: the region of interest that should be loaded (if you want to load the full image
#              use "None"
#         image_path: The full image path, in that case all of the above can be "None"
# output: the region of interest of the specified frame, np.array
def load_image_from_memory(frame_number, data_folder, microscopy_data_filename_pre_index,
                           microscopy_data_filename_post_index, microscopy_data_file_extension,
                           roi, image_path=None):

    if image_path is None:

        # create a string from the frame number given
        index = str(frame_number)

        # append the frame number, to match the data naming conventions
        while len(index) < 3:
            index = "0" + index

        # set the path to the image
        # change working directory to code directory
        image_file_name = microscopy_data_filename_pre_index + index + microscopy_data_filename_post_index
        image_path = data_folder + image_file_name + microscopy_data_file_extension

    # import one image
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=UserWarning)
        full_image = skimage.io.imread(image_path)
    orig_image_xyz = full_image.T

    # get the region of interest of that image
    if roi is not None:
        orig_image_xyz_roi = orig_image_xyz[roi]
    else:
        orig_image_xyz_roi = orig_image_xyz

    return orig_image_xyz_roi


# get the points in a single frame and their respective tracking indices from
# Excel data as provided by Judith
# input: frame_number: int, the number of the frame from which the points should
#                      be loaded
#        frames: np.array, from the Excel column containing the frame number
#                to which a point corresponds
#        x_com, y_com, z_com: np.array, centre of mass from Judith's tracking
#                             in x,y and z direction in pixels
# output: points_in_frame: np.array, points of tracked Kinetochores in the
#                          current frame
#         track_index_in_frame: np.array, Judith's tracking indices of this frame
def get_points_in_frame(frame_number, frames, track_indices, x_com, y_com, z_com):
    # get the array indices of the frames corresponding to the image
    # careful: labeling of images starts with 0 for the file names,
    # but 1 for the excel/ Judith's data analysis
    list_indices_of_frame = np.where(frames == frame_number + 1)
    # get the manual tracking label
    track_index_in_frame = track_indices[list_indices_of_frame]

    # get the coordinates of the points
    x_com_frame, y_com_frame, z_com_frame = x_com[list_indices_of_frame], y_com[list_indices_of_frame], \
        z_com[list_indices_of_frame]

    # reformat the coordinates, to have an array in which one entry is [x,y,z] position of a single point
    points_in_frame = np.array([x_com_frame, y_com_frame, z_com_frame]).T

    return points_in_frame, track_index_in_frame
