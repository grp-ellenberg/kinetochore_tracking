import SimpleITK as sitk
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.widgets import Slider


# class: tracks the index, slices the stack, gets slider value when an event happens
# initialise the class: set axis, title, get the #slices, rows, columns
class FigSlideBar(object):
    def __init__(self, ax_1, stack_array_1):
        self.ax = ax_1
        ax_1.set_title('Use the slider to select a slice:')

        self.stack_array = stack_array_1
        self.slices, cols, rows = stack_array_1.shape
        self.ind = self.slices // 2  # floor operator: start in the middle of the stack, if not specified otherwise

        self.im = ax_1.imshow(self.stack_array[self.ind, :, :])
        self.update()

    def if_slide(self):
        self.ind = int(z_slider.val)
        self.update()

    def update(self):
        self.im.set_data(self.stack_array[self.ind, :, :])
        ax.set_ylabel('slice %s' % self.ind)
        self.im.axes.figure.canvas.draw()


if __name__ == "__main__":
    # set matplotlib to display the image with equal aspect ratio
    mpl.rc('image', aspect='equal')

    # import the 3D image
    path = "C:/Users/Jana/Documents/mitotic_cell_atlas_v1.0.1_exampledata/Data_tifs/161122_gfpBUBR1cM04-A03_MitoSys1/" \
           "cell0002_R0005/rawtif/TR1_2_W0001_P0004_T0001.tif"
    image = sitk.ReadImage(path)
    depth = image.GetDepth() - 1

    # display the whole stack in Fiji
    sitk.Show(sitk.ReadImage(path), "Test_opening_tif", debugOn=True)

    # create a 2D slice from the 3D stack and display it using matplotlib [z, y, x]
    two_dim_slice = sitk.GetArrayViewFromImage(image)[depth-int(depth/3), :, :]
    plt.imshow(two_dim_slice)
    plt.show()

    # crop the slice and display it
    cropped_two_dim_slice = sitk.GetArrayViewFromImage(image)[depth-int(depth/3), 3:40, :]
    plt.imshow(cropped_two_dim_slice)
    plt.show()

    # create a matplotlib figure
    fig, ax = plt.subplots()
    # make space for slider bar
    fig.subplots_adjust(left=0.25, bottom=0.25)

    # create numpy array from sitk image
    stack_array = sitk.GetArrayFromImage(image)

    # create slider bar position and color
    ax_color = "lightgoldenrodyellow"
    ax_freq = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=ax_color)

    # add a the slide bar
    z_slider = Slider(ax_freq, "depth slider", 0, depth, valinit=0, valstep=1)

    # create an instance of the FigSlideBar class
    fig_with_slider = FigSlideBar(ax, stack_array)

    # connect the mouse motion to the update of the picture
    fig.canvas.mpl_connect('motion_notify_event', fig_with_slider.if_slide)

    # display the figure
    plt.show()
