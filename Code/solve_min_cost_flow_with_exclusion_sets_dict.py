import pulp
import numpy as np
import sys
import os.path
from datetime import datetime
import list_functions as lf
from tqdm import tqdm
from interpret_lpp import interpret_lpp
from load_from_and_save_to_memory import save_class_object_to_memory
from os_functions import try_creating_folder


# get indices for every object in a slice for a list of slices
# input: list of slices
# output: list of indices
def get_indices_for_slice_in_list_of_slices(list_of_slices):
    # create a nested list of the always existing indices
    indices_corresponding_to_slices = []
    for s in list_of_slices:
        indices_corresponding_to_slices.append(list(range(s.start, s.stop)))
    # remove the nesting from this function
    indices_corresponding_to_slices = lf.remove_nesting(indices_corresponding_to_slices)
    return indices_corresponding_to_slices


# get the pulp variables corresponding to the variables in the cost vector and connection matrix columns.
# If a connection is forced to exists, it will be set to 1 here. Also, the difference between binary and continuous
# connections is set here.
# input: connection_matrix: ConnectionMatrix object
# output: x: a list of pulp variables and binary numbers
def get_x_from_connection_matrix(connection_matrix, pulp_dict, pulp_var_dict_index, cost_vector):
    # get the cost values from the CostVector
    cost_vals = cost_vector.val
    # extract some parameters from the connection matrix (the number of columns and the column names)
    no_cols = connection_matrix.matrix.shape[1]
    col_names = connection_matrix.column_names

    # initialise the list
    x = []
    in_1, in_2, out_1, out_2 = [], [], [], []

    # get the slices that should be treated as if they always existed
    always_existing_slices_list = [connection_matrix.d_to_t_minus.slice,
                                   connection_matrix.t_plus_to_a.slice]
    # get the indices of the slices that always exist
    always_existing_column_indices = get_indices_for_slice_in_list_of_slices(always_existing_slices_list)

    # create a variable to store the a_to_d_slice index
    a_to_d_slice_index = connection_matrix.a_to_d.slice.start

    # add the variables for the first frame to the pulp variables
    for i in range(int(no_cols)):
        # if the variable in the slice always exists, force it to be one
        if i in always_existing_column_indices:
            x.append(1)
        # if the index corresponds to the appear to disappear slice, the Pulp variable is to be treated as continuous.
        # this is because the number might vary, based on the number of chosen appear/disappear/split/merge events
        elif i == a_to_d_slice_index:
            x.append(pulp.LpVariable(col_names[i], lowBound=0, upBound=2, cat="Continuous"))
            pulp_dict["index"][col_names[i]] = pulp_var_dict_index
            pulp_dict["cost"][col_names[i]] = cost_vals[i]
            for key in cost_vector.costs_dict:
                pulp_dict["detailed_cost"][key][col_names[i]] = cost_vector.costs_dict[key][i]
            pulp_var_dict_index += 1
            # append the in and out lists
            in_1.append(connection_matrix.in_1[i])
            if connection_matrix.in_2 is not None:
                in_2.append(connection_matrix.in_2[i])
            out_1.append(connection_matrix.out_1[i])
            if connection_matrix.out_2 is not None:
                in_2.append(connection_matrix.out_2[i])

        # otherwise (if this is some other kind of transition [normal transition, appear, disappear, split, merge]),
        # set the corresponding pulp variable to be binary
        else:
            x.append(pulp.LpVariable(col_names[i], cat="Binary"))
            pulp_dict["index"][col_names[i]] = pulp_var_dict_index
            pulp_dict["cost"][col_names[i]] = cost_vals[i]
            for key in cost_vector.costs_dict:
                pulp_dict["detailed_cost"][key][col_names[i]] = cost_vector.costs_dict[key][i]
            pulp_var_dict_index += 1
            # append the in and out lists
            in_1.append(connection_matrix.in_1[i])
            if connection_matrix.in_2 is not None:
                in_2.append(connection_matrix.in_2[i])
            out_1.append(connection_matrix.out_1[i])
            if connection_matrix.out_2 is not None:
                in_2.append(connection_matrix.out_2[i])

    return x, (in_1, in_2, out_1, out_2), pulp_dict, pulp_var_dict_index


# Add the constraints defined by the exclusion sets (if any) for the objects to the linear programming problem
# Exclusion sets prevent conflicting hypothesis from being chosen at the same time.
# inputs: connection_matrix: ConnectionMatrix object
#         lpp: linear programming problem (instance of pulp.LpProblem)
#         x: the solution vector x corresponding to the connection matrix
# output: lpp: the linear programming problem to which the constraints as defined by the exclusion sets have been added
def add_exclusion_sets_to_lpp(connection_matrix, lpp, x, sense="out"):
    if connection_matrix.exclusion_sets_in is not None:
        # check if the constraints should be added for the second set of objects in a connection matrix
        if sense == "out":
            ex_sets = connection_matrix.exclusion_sets_out
            val_1, val_2 = connection_matrix.out_1, connection_matrix.out_2

        # check if the constraints should be added for the first set of objects in a connection matrix
        elif sense == "in":
            ex_sets = connection_matrix.exclusion_sets_in
            val_1, val_2 = connection_matrix.in_1, connection_matrix.in_2

        else:
            raise (ValueError, "'sense' must be either 'in' or 'out")

        # add the exclusion sets as constraints to the linear programming problem
        if val_2 is not None:
            for ex_set in ex_sets:
                lpp += pulp.LpConstraint(pulp.lpSum((val_1 == j) * x + (val_2 == j) * x for j in ex_set),
                                         rhs=1, name="ex_set" + str(ex_set))  # sense: ==
        else:
            for ex_set in ex_sets:
                lpp += pulp.LpConstraint(pulp.lpSum((val_1 == j) * x for j in ex_set),
                                         rhs=1, name="ex_set" + str(ex_set))  # sense: ==
    return lpp


# Solve the minimum cost flow problem using the pulp package.

# PULP: The pulp package is a Python interface for more basic linear programming solvers, such as Coin, CPLEX, Gurobi or
# XPRESS that have to be installed separately.

# Idea: Use the connection matrices of an individual transition to find a global optimum using linear programming:
# The minimum cost flow finds the minimum_x (c^T*x), where:
# - c is the concatenation of all cost vectors ([cost_vector.val for cost_vector in cost_vectors])
# - ^T means transpose
# - x is the concatenation of individual solution vectors, indicating which edges of the connection_matrices are chosen
# This minimisation is subject to the constraint A_(t,tp1) * x_(t,tp1) + A_(tp1,tp2) * x_(tp1,tp2) == b,where only
# the parts present in both time-steps (meaning in frame tp1) are considered.
# - A is the connection matrix (connection_matrix.matrix)
# - b is the sum of flow vector (connection_matrix.b) defining how many objects appear at node b[i].
# - t / tp1 stand for the time points t and t+1, as all ingoing and outgoing vertices for one object have to be taken
#           into account, so for objects that are not in the first or last frame, two connection matrices have to be
#           used simultaneously. Objects in the first and last frame have to be treated separately, for these A * x == b
#           is sufficient as a constraint.
# Additionally, exclusion sets are used that define if two probable objects can not be chosen at the same time
# (if the hypothesis they exist is conflicting) take these into account.

# inputs: connection_matrices: a connection matrix object, such as defined by the ConnectionMatrix class (in it all
#                              possible edges how an object in frame i can transition into an object in frame i+1 are
#                              defined, the main feature is connection_matrix.matrix, a matrix in which the rows
#                              represent the objects, the columns the possible edges and the entries correspond to
#                              possible transition, the values indicate how many objects are "part of that flow").
#         cost_vectors: a cost vector object corresponding to the connection matrix.The cost vector has the form as
#                       defined by the class CostVector. Its main feature cost_vector.val gives a vector with the
#                       costs for each column in the connection_matrix.
#         display: binary variable indicating whether or not to print which connections have been chosen and the
#                  total cost of a flow
# output: solution_vector: np.array containing the solution x to the above problem

def solve_min_flow_eq_linear_programming(connection_matrices, cost_vectors, p, display=False,
                                         alternative_cost_vectors=None, points_1_list=None, points_2_list=None,
                                         indicator_split_list_point_1=None, indicator_split_list_point_2=None,
                                         save_to_folder_name="ilpp"):
    # ToDo: To allow for split, merge appear and disappear event given a constant number of objects, add connections
    #       between the disappear node of a a transition t,tp1 and the appear node of the subsequent transition tp1, tp2

    # save the parameters to a log file
    exclusion_sets_log_file = p.log_file_exclusion_sets
    sys.stdout = open(p.analysis_folder + exclusion_sets_log_file, 'w')

    # create the linear programming problem (minimisation) and give it a name
    lpp = pulp.LpProblem("MinimumCostFlow", sense=pulp.LpMinimize)

    # create a list to store all values that are part of the main objective "cost function" that should be minimized
    x_list_minimisation = []
    in_list_1_minimisation, in_list_2_minimisation = [], []
    out_list_1_minimisation, out_list_2_minimisation = [], []
    c_list_minimisation = []
    x_at_tp1 = None

    # create a nested dictionary to relate the pulp variable names to their indices and store their costs
    pulp_var_dict_index = 0
    pulp_dict = {"index": {}, "cost": {}, "detailed_cost": {}}

    # Note: in the following tp1 stands for t+1
    for i, connection_matrix_at_t, cost_vector_at_t, connection_matrix_at_tp1, cost_vector_at_tp1 \
            in tqdm(zip(range(len(connection_matrices[:-1])), connection_matrices[:-1],
                        cost_vectors[:-1], connection_matrices[1:], cost_vectors[1:]),
                    total=len(connection_matrices[:-1])):

        # define the variables of both matrices in a concise way
        b_at_t, b_at_tp1 = connection_matrix_at_t.b, connection_matrix_at_tp1.b
        cm_values_at_t, cm_values_at_tp1 = connection_matrix_at_t.matrix, connection_matrix_at_tp1.matrix
        row_names_at_t, row_names_at_tp1 = connection_matrix_at_t.row_names, connection_matrix_at_tp1.row_names

        # if this is the first iteration, all the new variables for the time step t have to be added
        if connection_matrix_at_t.is_first_connection_matrix:

            print("Found first connection matrix")
            # add the names of the detailed_costs to the pulp_dict "detailed_cost" nested dictionary
            for key in cost_vector_at_t.costs_dict:
                pulp_dict["detailed_cost"][key] = {}

            # get the solution vector at time t+1
            x_at_t, ins_and_outs, pulp_dict, pulp_var_dict_index = \
                get_x_from_connection_matrix(connection_matrix_at_t, pulp_dict, pulp_var_dict_index, cost_vector_at_t)
            x_list_minimisation.append(x_at_t)
            c_list_minimisation.append(cost_vector_at_t.val)
            in_list_1_minimisation.append(ins_and_outs[0])
            in_list_2_minimisation.append(ins_and_outs[1])
            out_list_1_minimisation.append(ins_and_outs[2])
            out_list_2_minimisation.append(ins_and_outs[3])

            # add exclusion sets that prevent conflicting hypothesis from being chosen at the same time to the linear
            # programming problem. As this is only done for the second frame of the connection_matrix_at_tp1 in the
            # remainder of the loop, it is done for all objects in the connection_matrix_at_t here.
            lpp = add_exclusion_sets_to_lpp(connection_matrix_at_t, lpp, x_at_t, sense="in")
            lpp = add_exclusion_sets_to_lpp(connection_matrix_at_t, lpp, x_at_t, sense="out")

            # add special connections to the first connection matrix
            # add the connections to the T+ in the first frame and the connections to all objects in the first frame
            row_id_tp = [row_names_at_t.index(rn) for rn in row_names_at_t if str(rn).startswith('TP')]
            # for the forced construction of objects, the additional vertices to appear/disappear etc.
            # should be ignored, so j should be in the range (4, length of the vectors), to ignore the
            # first four (0,1,2,3) columns of the connection matrix
            lpp += pulp.LpConstraint(pulp.lpSum(cm_values_at_t[row_id_tp, j] * x_at_t[j]
                                                for j in range(1, len(x_at_t))),
                                     rhs=-connection_matrix_at_t.no_expected, name="f1, TP")
            print("Forced the construction of " + str(connection_matrix_at_t.no_expected)
                  + " objects in frame " + str(connection_matrix_at_t.frame_numbers[0]))

            # add the connections for the objects from the first frame of the first connection matrix, as their input
            # is only dependent on this transition

            object_in_first_frame_indices = [i for i in range(len(row_names_at_t)) if str(
                row_names_at_t[i]).startswith(str(connection_matrix_at_t.frame_numbers[0]))]

            for i_at_t in object_in_first_frame_indices:
                lpp += pulp.LpConstraint(pulp.lpSum(cm_values_at_t[i_at_t, j] * x_at_t[j]
                                                    for j in range(len(x_at_t))),
                                         rhs=b_at_t[i_at_t], name='f1, obj' + str(row_names_at_t[i_at_t]))

        else:
            # the former solution vector at time t+1, now is the solution vector at time t
            x_at_t = x_at_tp1

        # get the solution vector at time t+1
        x_at_tp1, ins_and_outs, pulp_dict, pulp_var_dict_index = get_x_from_connection_matrix(connection_matrix_at_tp1,
                                                                                              pulp_dict,
                                                                                              pulp_var_dict_index,
                                                                                              cost_vector_at_tp1)
        x_list_minimisation.append(x_at_tp1)
        c_list_minimisation.append(cost_vector_at_tp1.val)
        in_list_1_minimisation.append(ins_and_outs[0])
        in_list_2_minimisation.append(ins_and_outs[1])
        out_list_1_minimisation.append(ins_and_outs[2])
        out_list_2_minimisation.append(ins_and_outs[3])

        # add exclusion sets that prevent conflicting hypothesis from being chosen at the same time to the lpp
        lpp = add_exclusion_sets_to_lpp(connection_matrix_at_tp1, lpp, x_at_tp1, sense="out")

        print("Adding possible connections for objects in frame " + str(connection_matrix_at_t.frame_numbers[1]))

        shared_row_names_t_and_tp1 = set(row_names_at_t).intersection(row_names_at_tp1)

        xs_concatenated = x_at_t + x_at_tp1

        # add the constraints for the nodes to the lpp, where the input is stored in one ConnectionMatrix at t
        # and the output in the ConnectionMatrix at t+1
        for row_id in shared_row_names_t_and_tp1:
            if not str(row_id).startswith(("A", "D", "T")):
                i_at_t, i_at_tp1 = row_names_at_t.index(row_id), row_names_at_tp1.index(row_id)
                cm_values_concatenated = np.concatenate((cm_values_at_t[i_at_t, :], cm_values_at_tp1[i_at_tp1, :]),
                                                        axis=0)

                lpp += pulp.LpConstraint(pulp.lpSum(cm_values_concatenated[j] * xs_concatenated[j]
                                                    for j in range(len(xs_concatenated))),
                                         rhs=b_at_t[i_at_t], name='obj' + str(row_id))

        # add special connections to the last connection matrix
        if connection_matrix_at_tp1.is_last_connection_matrix:
            # add the connections to the T- in the last frame and the connections to all objects in the last frame
            row_id_tm = [row_names_at_tp1.index(rn) for rn in row_names_at_tp1 if str(rn).startswith('TM')]
            # for the forced destruction of objects, the additional vertices to appear/disappear etc.
            # should be ignored, so j should be in the range (4, length of the vectors), to ignore the
            # first four (0,1,2,3) columns of the connection matrix
            lpp += pulp.LpConstraint(pulp.lpSum(cm_values_at_tp1[row_id_tm, j] * x_at_tp1[j]
                                                for j in range(2, len(x_at_tp1))),
                                     rhs=connection_matrix_at_tp1.no_expected, name="lf, TM")

            print("Forced the destruction of " + str(connection_matrix_at_tp1.no_expected)
                  + " objects in frame " + str(connection_matrix_at_tp1.frame_numbers[1]))

            # add the connections for the objects from the last frame of the first connection matrix, as their input
            # is only dependent on this transition

            object_in_last_frame_indices = [i for i in range(len(row_names_at_tp1)) if str(
                row_names_at_tp1[i]).startswith(str(connection_matrix_at_tp1.frame_numbers[1]))]

            for i_at_tp1 in object_in_last_frame_indices:
                lpp += pulp.LpConstraint(pulp.lpSum(cm_values_at_tp1[i_at_tp1, j] * x_at_tp1[j]
                                                    for j in range(len(x_at_tp1))),
                                         rhs=b_at_tp1[i_at_tp1], name='lf, obj' + str(row_names_at_tp1[i_at_tp1]))

    # adding the main objective to the minimisation problem: minimise the costs for all transitions
    lpp += pulp.lpSum([c_list_minimisation[j] * x_list_minimisation[j] for j in range(len(x_list_minimisation))]), \
        "objective: minimise costs"

    print("Solving linear programming problem")
    pulp.LpSolverDefault.msg = 1
    # call the .solve() method to solve the linear programming problem, optionally, a solver could be specified here
    lpp.solve()  # solver=pulp.GLPK_CMD()

    lpp.writeLP(p.analysis_folder + "pulp_linear_programming_problem_kinetochores.lp")

    # the status is printed, the different outcomes are:
    # OPTIMAL: Optimal solution exists and is found.
    # NOT SOLVED: The default setting before a problem has been solved.
    # INFEASIBLE: The problem has no feasible solution.
    # UNBOUNDED: The cost function is unbounded.
    # UNDEFINED: Feasible solution hasn't been found (but may exist).)
    print("Status (main):", pulp.LpStatus[lpp.status])

    interpretation_ins_and_outs = (in_list_1_minimisation, in_list_2_minimisation,
                                   out_list_1_minimisation, out_list_2_minimisation)

    try:
        i_lpp = interpret_lpp(lpp, interpretation_ins_and_outs, points_1_list, points_2_list,
                              indicator_split_list_point_1, indicator_split_list_point_2,
                              pulp_dict, p, weight_factor_pair_finding=2.0)
        try_creating_folder(p.analysis_folder + "Tracking_outcome")
        file_name_ilpp = p.analysis_folder + "Tracking_outcome/" + save_to_folder_name + ".pkl"
        # if the file already exists, add a time-stamp
        if os.path.isfile(file_name_ilpp):
            file_name_ilpp = p.analysis_folder + "Tracking_outcome/" + save_to_folder_name + \
                              str(datetime.now()) + ".pkl"
        save_class_object_to_memory(i_lpp, file_name_ilpp)
    except TypeError:
        print("Interpretation went wrong, returning lpp instead of i_lpp")
        i_lpp = lpp

    # if alternative cost vectors are given to this function: solve the linear programming problem for these as well
    alt_i_lpps = None
    if alternative_cost_vectors is not None:
        alt_i_lpps = []
        for i, alternative_cost_vector_obj in enumerate(alternative_cost_vectors):
            alt_lpp = lpp
            alt_costs_list = []
            for cost_vector_at_certain_time in alternative_cost_vector_obj:
                alt_costs_list.append(cost_vector_at_certain_time.val)
            alt_lpp.objective += pulp.lpSum([alt_costs_list[j] * x_list_minimisation[j]
                                             for j in range(len(x_list_minimisation))])
            print("Solving linear programming problem for alternative # " + str(i))
            pulp.LpSolverDefault.msg = 1
            # call the .solve() method to solve the linear programming problem, optionally, a solver could be
            # specified here
            alt_lpp.solve()  # solver=pulp.GLPK_CMD()

            print("Status (alternative) " + str(i) + ":" + pulp.LpStatus[alt_lpp.status])

            # add a file with the alternative lpps
            alt_lpp.writeLP(p.analysis_folder + "pulp_linear_programming_problem_kinetochores_alternative_costs" +
                            str(i) + ".lp")
            try:
                alt_i_lpp = interpret_lpp(alt_lpp, interpretation_ins_and_outs,
                                          points_1_list, points_2_list,
                                          indicator_split_list_point_1, indicator_split_list_point_2,
                                          pulp_dict, p, weight_factor_pair_finding=2.0)
                alt_i_lpps.append(alt_i_lpp)
                file_name_alt_ilpp = p.analysis_folder + "Tracking_outcome/" + save_to_folder_name + "_alt_" + \
                    str(i) + ".pkl"
                # if the file already exists, add a time-stamp
                if os.path.isfile(file_name_alt_ilpp):
                    file_name_alt_ilpp = p.analysis_folder + "Tracking_outcome/" + save_to_folder_name + "_alt_" + \
                                         str(i) + str(datetime.now()) + ".pkl"
                save_class_object_to_memory(alt_i_lpp, file_name_alt_ilpp)
                print("Hash of alt ilpp")
                print(hash(alt_i_lpp))
            except TypeError:
                print("Interpretation went wrong, returning alt_lpp instead of alt_i_lpp for iteration # " + str(i))
                alt_i_lpps.append(alt_lpp)

    if display:

        # All connections that are part of the solution are printed
        print("The following connections have been chosen:")
        number_chosen = 0
        for index, variable in enumerate(lpp.variables()):
            if variable.varValue is not None and variable.varValue > 0:
                print(str(variable.name) + " = " + str(variable.varValue))
                number_chosen += 1
            else:
                pass

        # The total cost for minimisation is printed
        print("The flow cost is: " + str(pulp.value(lpp.objective)))
        print("In total " + str(number_chosen) + " connections chosen")

    sys.stdout = p.standard_output_jupyter
    print("Status: " + str(pulp.LpStatus[lpp.status]))

    return i_lpp, interpretation_ins_and_outs, pulp_dict, alt_i_lpps
