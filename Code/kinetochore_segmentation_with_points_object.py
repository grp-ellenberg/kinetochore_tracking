import numpy as np
import sys
import yaml
import time
import matplotlib.pyplot as plt
import local_max_3D
import get_threshold_values_for_segmentation as gtv
import segmentation_methods as smeth
import label_image_functions as lif
import correlation_functions as cf
import filters
from scipy.spatial.distance import cdist
from os_functions import try_creating_folder

import read_in as ri

import point_object as po

from tqdm import tqdm
from termcolor import colored


# inputs: parameters object created by 'read_in_config_file.py' that contains the parameters given to
#         the algorithm by the user
def segment_kinetochores(p, sigma_gauss=(0.5, 0.5, 0.5)):
    # create empty lists to store some values

    # number of points found for each frame
    number_of_points_found = []

    # create empty list to store potential points
    point_objects = []
    all_points_split = []

    # get thresholds from the microscopy data
    thresholds_dict = gtv.get_thresholds(p.time_points, p.data_folder,
                                         p.microscopy_data_filename_pre_index,
                                         p.microscopy_data_filename_post_index,
                                         p.microscopy_data_file_extension,
                                         p.roi, p.metadata)

    old_stdout = sys.stdout
    sys.stdout = open(p.analysis_folder + p.log_file_registration, 'w')

    print("Starting segmentation at" + str(time.ctime(time.time())))

    # create dictionaries to store the manual and automatically segmented points
    manual_points_save_folder = p.analysis_folder + "manual_points/"
    try_creating_folder(manual_points_save_folder)

    automatic_points_save_folder = p.analysis_folder + "automatic_segmentation_points/"
    try_creating_folder(automatic_points_save_folder)

    # save the thresholds of the automatically segmented points to the folder for the automatically segmented points
    with open(automatic_points_save_folder + 'automatic_segmentation_thresholds.yml', 'w') as outfile:
        yaml.dump(thresholds_dict, outfile, default_flow_style=False)

    for i, frame_no in enumerate(tqdm(p.time_points)):

        # get thresholds in frame taking into account bleaching depending on time point/frame number
        new_t_dict = gtv.get_thresholds_after_considering_bleaching(frame_no, p.time_points,
                                                                    thresholds_dict["slope_bleaching"],
                                                                    (thresholds_dict["max_points_init"],
                                                                     thresholds_dict["double_thresholding_init"],
                                                                     thresholds_dict["watershed_intensity_init"]),
                                                                    return_dict_instead=True
                                                                    )

        new_threshold_dict = {**thresholds_dict, **new_t_dict}

        print("____________________________frame number " + str(frame_no) + "____________________________")

        print("reading in microscopy data")
        # read in the ROI of the microscopy image of the frame
        microscopy_data_of_frame = ri.load_image_from_memory(frame_no, p.data_folder,
                                                             p.microscopy_data_filename_pre_index,
                                                             p.microscopy_data_filename_post_index,
                                                             p.microscopy_data_file_extension,
                                                             p.roi)

        microscopy_data_of_frame = filters.gaussian_blur_xyz(microscopy_data_of_frame, sigma_gauss)

        detected_objects_list = po.DetectedObjectsList([], microscopy_data_of_frame, p.metadata, new_threshold_dict,
                                                       frame_no, p.number_expected)

        print("creating a mask by thresholding")
        # create a mask to mark the ROI and get all points in the mask
        # use a moments threshold to create a mask
        seg_mask, seg_thresh = smeth.segment_image_moments_threshold(
            microscopy_data_of_frame - np.min(microscopy_data_of_frame),
            return_threshold=True)

        # remove small regions from the mask (e.g. single pixels)
        print("removing small components")
        seg_mask = lif.remove_small_components_from_binary_img(seg_mask, 10)
        detected_objects_list.seg_mask = seg_mask
        # get all points within the mask
        print("creating array of potential points")
        potential_points = np.column_stack(np.where(seg_mask == 1))

        print("There are " + str(len(potential_points)) + " potential points")

        print("_________________ \ncalculating points")

        print("\n medium score points")
        hig_value_points, score_image, mask_cc = cf.centres_from_norm_corr(microscopy_data_of_frame,
                                                                           p.model_roi_downsampled, potential_points,
                                                                           threshold_score=0.7,
                                                                           threshold_num_connected=4,
                                                                           xyz_region=(0, 0, 0))

        print("The maximum score is: " + str(np.max(score_image)))

        detected_objects_list.add_points(hig_value_points, "hig_value_point", microscopy_data_of_frame)

        print("_________________ \nmid score points")

        mid_value_points = cf.get_additional_points_from_score_image_detected_obj_list(score_image,
                                                                                       detected_objects_list, 0.6, 4)

        detected_objects_list.add_points(mid_value_points, "mid_value_point", microscopy_data_of_frame)

        print("_________________ \nlow score points")

        low_value_points = cf.get_additional_points_from_score_image_detected_obj_list(score_image,
                                                                                       detected_objects_list, 0.5, 4)

        detected_objects_list.add_points(low_value_points, "low_value_point", microscopy_data_of_frame)

        print("_________________ \nlocal max score points")

        local_max_points = local_max_3D.find_local_max_3d(score_image, seg_mask)
        print("Found " + str(len(local_max_points)) + " more potential points")
        local_max_points = cf.separate_additional_points(local_max_points,
                                                         detected_objects_list.coordinates)

        # for each local maximum point (if any): get the distance to the nearest point that was found before
        if len(local_max_points) != 0 and len(detected_objects_list.coordinates) != 0:
            detected_objects_list.local_max_point_dists = np.min(cdist(local_max_points,
                                                                       detected_objects_list.coordinates),
                                                                 axis=1)
        else:
            detected_objects_list.local_max_point_dists = None

        # add the local max points to the list of all coordinates
        detected_objects_list.add_points(local_max_points, "local_max_point", microscopy_data_of_frame)

        # find local maxima also in the prod_score_image

        print("_________________ \nlocal max prod_score points")

        detected_objects_list.add_score_image(score_image)
        prod_score_image = score_image * microscopy_data_of_frame
        detected_objects_list.norm_score_prod_image = score_image / np.sqrt(np.sum(prod_score_image ** 2))

        local_max_points_prod_score = local_max_3D.find_local_max_3d(prod_score_image,
                                                                     seg_mask, number_of_max=100)
        local_max_intensity_dist_to_other_points_greater_two = np.where(np.min(cdist(local_max_points_prod_score,
                                                                                     detected_objects_list.coordinates),
                                                                               axis=1) > 2)
        local_max_points_prod_score = local_max_points_prod_score[local_max_intensity_dist_to_other_points_greater_two]
        local_max_points_prod_score = cf.separate_additional_points(local_max_points_prod_score,
                                                                    detected_objects_list.coordinates)

        print("Found " + str(len(local_max_points_prod_score)) + " more potential points")

        local_max_point_prod_score_dists = np.min(cdist(local_max_points_prod_score,
                                                        detected_objects_list.coordinates), axis=1)

        detected_objects_list.add_points(local_max_points_prod_score, "local_max_point_prod_score",
                                         microscopy_data_of_frame)

        detected_objects_list.local_max_point_prod_score_dists = local_max_point_prod_score_dists

        # find local maxima also in the intensity image

        print("_________________ \nlocal max intensity points")

        local_max_points_intensity = local_max_3D.find_local_max_3d(microscopy_data_of_frame, seg_mask)
        # remove points very close to other points found before
        local_max_intensity_dist_to_other_points_greater_two = np.where(np.min(cdist(local_max_points_intensity,
                                                                                     detected_objects_list.coordinates),
                                                                               axis=1) > 2)
        local_max_points_intensity = local_max_points_intensity[local_max_intensity_dist_to_other_points_greater_two]
        local_max_points_intensity = cf.separate_additional_points(local_max_points_intensity,
                                                                   detected_objects_list.coordinates)
        # remove points very close to other local_max_intensity points
        dist_to_other_local_max_intensity_points = cdist(local_max_points_intensity, local_max_points_intensity)
        # add values to the diagonal, because it is zero (distance from point to itself)
        dist_to_other_local_max_intensity_points += np.identity(dist_to_other_local_max_intensity_points.shape[0])*3

        local_max_intensity_dist_to_own_points_greater_two = np.where(np.min(dist_to_other_local_max_intensity_points,
                                                                             axis=1) > 2)
        local_max_points_intensity = local_max_points_intensity[local_max_intensity_dist_to_own_points_greater_two]

        print("Found " + str(len(local_max_points_intensity)) + " more potential points")

        local_max_point_intensity_dists = np.min(cdist(local_max_points_intensity,
                                                       detected_objects_list.coordinates), axis=1)

        detected_objects_list.add_points(local_max_points_intensity, "local_max_point_intensity",
                                         microscopy_data_of_frame)

        detected_objects_list.local_max_point_intensity_dists = local_max_point_intensity_dists

        print("_________________ \nsplitting big objects")

        if detected_objects_list.length > 0:

            detected_objects_list.get_split_objects_for_all_objects(microscopy_data_of_frame)
            detected_objects_list.get_watershed_sizes_of_objects_at_point_from_image(microscopy_data_of_frame)
            detected_objects_list.get_all_points_for_first_time()
            split_points = detected_objects_list.active_points

        else:

            split_points = []

        all_points_split.append(split_points)
        print(colored("found " + str(len(split_points)) + " points in total", "green"))
        number_of_points_found.append(len(split_points))

        # save the data:
        print("saving data")
        np.save(automatic_points_save_folder + "automatic_points_in_frame_" + str(frame_no), split_points)

        if p.excel_filename is not None:
            # load the points from manual tracking (for comparison)
            points_from_manual_tracking_of_frame, tracking_indices_from_manual_tracking_in_frame = \
                ri.get_points_in_frame(frame_no, p.frames, p.track_indices, p.x_COM, p.y_COM, p.z_COM)
            detected_objects_list.add_manual_points_and_tracking_indices(points_from_manual_tracking_of_frame,
                                                                         tracking_indices_from_manual_tracking_in_frame)

            np.save(manual_points_save_folder + "manual_points_in_frame_" + str(frame_no),
                    points_from_manual_tracking_of_frame)
            np.save(manual_points_save_folder + "manual_points_in_frame_" + str(frame_no) + "_tracking_indices",
                    tracking_indices_from_manual_tracking_in_frame)

        point_objects.append(detected_objects_list)

    sys.stdout = old_stdout

    # use the matplotlib dark style
    plt.style.use('dark_background')
    # plot the number of points found over the frame number
    plt.plot(p.time_points, number_of_points_found)
    plt.title("Number of points found over frame")
    plt.xlabel("frame number")
    plt.ylabel("number of points found")
    plt.savefig(automatic_points_save_folder + "plot_of_number_of_points_found_by_cc_over_frame.png")
    plt.show()

    return thresholds_dict, point_objects
