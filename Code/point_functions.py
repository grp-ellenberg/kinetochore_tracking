import numpy as np


# multidimensional intersection between the 1st dimension of two arrays (get all common values)
# input: a1, a2: the two arrays
# output: the intersection between the two arrays
def multidim_intersect(a1, a2):
    # create rows using the same dtype for both object, otherwise the trick
    # using the 'view' converting does not work.
    if a1.dtype != a2.dtype:
        a1 = a1.astype(a2.dtype)
    # create rows
    a1_rows = a1.view([('', a1.dtype)]*a1.shape[1])
    a2_rows = a2.view([('', a2.dtype)]*a2.shape[1])
    # use the 1D intersect function to find the intersection
    intersected = np.intersect1d(a1_rows, a2_rows)
    # reformat the array to its original format
    intersection_formatted = intersected.view(a1.dtype).reshape(-1, a1.shape[1])
    return intersection_formatted


# multidimensional set difference between the 1st dimension of two arrays
# (get all values that are in a2 but not a1)
# input: a1, a2: the two arrays
# output: the intersection between the two arrays
def multidim_set_diff(a1, a2):
    # create rows using the same dtype for both object, otherwise the trick
    # using the 'view' converting does not work.
    if a1.dtype != a2.dtype:
        a1 = a1.astype(a2.dtype)
    a1_rows = a1.view([('', a1.dtype)]*a1.shape[1])
    a2_rows = a2.view([('', a2.dtype)]*a2.shape[1])
    # use the 1D intersect function to find the intersection
    set_diff = np.setdiff1d(a2_rows, a1_rows)
    # reformat the array to its original format
    set_diff_formatted = set_diff.view(a1.dtype).reshape(-1, a1.shape[1])
    return set_diff_formatted


# multidimensional union difference between the 1st dimension of two arrays
# (get all unique values that are in a1 and a2)
# input: a1, a2: the two arrays
# output: the union of the two arrays
def multidim_union(a1, a2):
    merged = np.concatenate((a1, a2), axis=0)
    union = np.unique(merged, axis=0)
    return union


# get points with an intensity above a certain threshold in an intensity image
# inputs: points: list or np.array containing the points
#         intensity_image: np.array of the image in which thresholding at the points shall
#                          be performed
#         threshold: float or integer above which points are considered 'valid' and returned
# output: selected_points: all points in the array that have an intensity greater than the
#                          threshold in the intensity image
def get_points_with_intensity_value_above_threshold(points, intensity_image, threshold):
    # create array to store intensity values (works for input point lists and arrays)
    intensity_values_of_points = np.zeros(shape=(len(points)))
    # make sure points are given as ints 
    points_int = points.astype(int)
    # get the intensity of every point
    for i, point in enumerate(points_int):
        intensity_values_of_points[i] = intensity_image[point[0], point[1], point[2]]
    selected_points = points[np.where(intensity_values_of_points > threshold)]
    return selected_points
